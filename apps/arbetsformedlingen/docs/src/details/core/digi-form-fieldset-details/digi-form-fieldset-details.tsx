import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-form-fieldset-details',
  styleUrl: 'digi-form-fieldset-details.scss'
})
export class DigiFormFieldsetDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  get fieldsetCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-fieldset 
	af-form="Formulärnamnet" 
	af-legend="Det här är en legend" 
	af-name="Fältgruppnamn"
>
	<digi-form-checkbox af-label="Alternativ 1"></digi-form-checkbox>
	<digi-form-checkbox af-label="Alternativ 2"></digi-form-checkbox>
	<digi-form-checkbox af-label="Alternativ 3"></digi-form-checkbox>
	<digi-button>
		Skicka
	</digi-button>
</digi-form-fieldset>
`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-fieldset 
	[attr.af-form]="'Formulärnamnet'" 
	[attr.af-legend]="'Det här är en legend'" 
	[attr.af-name]="'Fältgruppnamn'"
>
	<digi-form-checkbox af-label="Alternativ 1"></digi-form-checkbox>
	<digi-form-checkbox af-label="Alternativ 2"></digi-form-checkbox>
	<digi-form-checkbox af-label="Alternativ 3"></digi-form-checkbox>
	<digi-button>
		Skicka
	</digi-button>
</digi-form-fieldset>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormFieldset 
	afForm="Formulärnamnet" 
	afLegend="Det här är en legend" 
	afName="Fältgruppnamn"
>
	<DigiFormCheckbox afLabel="Alternativ 1"></DigiFormCheckbox>
	<DigiFormCheckbox afLabel="Alternativ 2"></DigiFormCheckbox>
	<DigiFormCheckbox afLabel="Alternativ 3"></DigiFormCheckbox>
	<DigiButton>
		Skicka
	</DigiButton>
</DigiFormFieldset>`
    };
  }
  render() {
    return (
      <div class="digi-form-fieldset-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Fältgrupperingskomponenten använder en container för att gruppera
              formulärelement.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}

            <digi-code-example
              af-code={JSON.stringify(this.fieldsetCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-form-fieldset
                afForm="Formulärnamnet"
                afLegend="Det här är en legend"
                afName="Fältgruppnamn">
                <digi-form-checkbox afLabel="Alternativ 1"></digi-form-checkbox>
                <digi-form-checkbox afLabel="Alternativ 2"></digi-form-checkbox>
                <digi-form-checkbox afLabel="Alternativ 3"></digi-form-checkbox>
                <br />
                <digi-button>Skicka</digi-button>
              </digi-form-fieldset>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <div>
              <h2> Beskrivning </h2>
              <p>
                Det första elementet i en fältkomponenten måste vara en{' '}
                <digi-code af-code="af-legend" /> som beskriver fältgruppen.
                Attributet <digi-code af-code="af-form" /> används för att ange
                vilket formulär fältgrupperingskomponenten tillhör och{' '}
                <digi-code af-code="af-name" /> anger ett namn för fältgruppen.
              </p>
            </div>
          )}
        </digi-typography>
      </div>
    );
  }
}
