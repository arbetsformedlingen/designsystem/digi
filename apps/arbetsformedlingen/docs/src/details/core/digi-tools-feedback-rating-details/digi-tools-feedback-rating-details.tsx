import { Component, h, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FeedbackRatingVariation,
  FeedbackRatingType
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-tools-feedback-rating-details',
  styleUrl: 'digi-tools-feedback-rating-details.scss'
})
export class DigiToolsFeedbackRating {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() showExample = true;
  @State() feedbackRatingVariation: FeedbackRatingVariation =
    FeedbackRatingVariation.PRIMARY;
  @State() feedbackRatingType: FeedbackRatingType = FeedbackRatingType.GRID;
  @State() afVariation: string;
  @State() ratingType = 'Betygsbanner';

  get ratingCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
    <digi-tools-feedback-rating      
      af-question="Hur nöjd är du med tjänsten?"
      af-variation="${this.feedbackRatingVariation}"
      af-type="${this.feedbackRatingType}"
      af-ratings="Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd"
      af-thank-you-heading="Tack för din feedback!"  
      af-product-area="Produktområde"
      af-name="Börjat studera"
      af-question-type="Innehåll"
      af-product="Designsystem" 
      af-application="Applikationsnamn"
      af-link-text="Skriv gärna mer feedback här"
      af-link="https://webropol.se/"
      af-use-custom-link="false">
    </digi-tools-feedback-rating>`,
      [CodeExampleLanguage.ANGULAR]: `\
    <digi-tools-feedback-rating
      [attr.af-question]="'Hur nöjd är du med tjänsten?'"
      [attr.af-variation]="FeedbackRatingVariation.${Object.keys(
        FeedbackRatingVariation
      ).find(
        (key) => FeedbackRatingVariation[key] === this.feedbackRatingVariation
      )}"
      [attr.af-type]="FeedbackRatingType.${Object.keys(FeedbackRatingType).find(
        (key) => FeedbackRatingType[key] === this.feedbackRatingType
      )}"
      [attr.af-product]="'Designsystem'"
      [attr.af-application]="'Applikationsnamn'"
      [attr.af-product-area]="'Produktområde'"
      [attr.af-name]="'Börjat studera'"
      [attr.af-question-type]="'Innehåll'"
      [attr.af-ratings]="'Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd'"
      [attr.af-thank-you-heading]="'Tack för din feedback!'" 
      [attr.af-link-text]="'Skriv gärna mer feedback här'"
      [attr.af-link]="'https://webropol.se/'"
      [attr.af-use-custom-link]="false">
    </digi-tools-feedback-rating>`,
      [CodeExampleLanguage.REACT]: `\
    <DigiToolsFeedbackRating
      afQuestion="Hur nöjd är du med tjänsten?"
      afVariation={FeedbackRatingVariation.${Object.keys(
        FeedbackRatingVariation
      ).find(
        (key) => FeedbackRatingVariation[key] === this.feedbackRatingVariation
      )}}
      afType={FeedbackRatingType.${Object.keys(FeedbackRatingType).find(
        (key) => FeedbackRatingType[key] === this.feedbackRatingType
      )}}
      afProduct="Designsystem"
      afApplication="Applikationsnamn"
      afProductArea="Produktområde"
      afName="Börjat studera"
      afQuestionType="Innehåll"
      afThankYouHeading="Tack för din feedback!"  
      afRatings="Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd"  
      afLinkText="Skriv gärna mer feedback här"
      afLink="https://webropol.se/"
      afUseCustomLink={false}>
    </DigiToolsFeedbackRating>`
    };
  }
  get baseRatingCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
    <digi-tools-feedback-rating
      af-question="Hur nöjd är du med tjänsten?"
      af-type="minimal"
      af-product="Designsystem"  
      af-application="Applikationsnamn"
      af-product-area="Produktområde"
      af-name="Börjat studera"
      af-question-type="Innehåll"
      af-ratings="Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd"  
      af-thank-you-heading="Tack för din feedback!"  
      af-link-text="Skriv gärna mer feedback här"
      af-link="https://webropol.se/"
      af-use-custom-link="false">
    </digi-tools-feedback-rating>`,
      [CodeExampleLanguage.ANGULAR]: `\
    <digi-tools-feedback-rating
      [attr.af-question]="'Hur nöjd är du med tjänsten?'"      
      [attr.af-type]="FeedbackRatingType.MINIMAL"
      [attr.af-product]="'Designsystem'"
      [attr.af-application]="'Applikationsnamn'"
      [attr.af-product-area]="'Produktområde'"
      [attr.af-name]="'Börjat studera'"
      [attr.af-question-type]="'Innehåll'"
      [attr.af-ratings]="'Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd'"
      [attr.af-thank-you-heading]="'Tack för din feedback!'" 
      [attr.af-link-text]="'Skriv gärna mer feedback här'"
      [attr.af-link]="'https://webropol.se/'"
      [attr.af-use-custom-link]="false">
    </digi-tools-feedback-rating>`,
      [CodeExampleLanguage.REACT]: `\
    <DigiToolsFeedbackRating
      afQuestion="Hur nöjd är du med tjänsten?"
      afType={FeedbackRatingType.MINIMAL}
      afProduct="Designsystem"
      afApplication="Applikationsnamn"
      afProductArea="Produktområde"
      afName="Börjat studera"
      afQuestionType="Innehåll"
      afRatings="Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd"
      afThankYouHeading="Tack för din feedback!"
      afLinkText="Skriv gärna mer feedback här"
      afLink="https://webropol.se/"
      afUseCustomLink={false}>
    </DigiToolsFeedbackRating>`
    };
  }

  render() {
    return (
      <div class="digi-tools-feedback-rating-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Betygskomponenten används för att snabbt samla in feedback i
              tjänster och andra flöden i form av betyg 1 till 5.
            </digi-typography-preamble>
          )}
        </digi-typography>
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={
              this.ratingType === 'Betygsbanner'
                ? JSON.stringify(this.ratingCode)
                : JSON.stringify(this.baseRatingCode)
            }
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}>
            <div class="slot__controls" slot="controls">
              <digi-form-fieldset
                afName="Komponentvariant"
                afLegend="Komponentvariant"
                onChange={(e) =>
                  (this.ratingType = (e.target as HTMLFormElement).value)
                }>
                <digi-form-radiobutton
                  afName="Komponentvariant"
                  afLabel="Betygsbanner"
                  afValue="Betygsbanner"
                  afChecked={this.ratingType === 'Betygsbanner'}
                />
                <digi-form-radiobutton
                  afName="Komponentvariant"
                  afLabel="Baskomponent"
                  afValue="Baskomponent"
                  afChecked={this.ratingType === 'Baskomponent'}
                />
              </digi-form-fieldset>
              {this.ratingType === 'Betygsbanner' && (
                <digi-form-fieldset
                  afName="Variant"
                  afLegend="Variant"
                  onChange={(e) =>
                    (this.feedbackRatingVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variant"
                    afLabel="Primär"
                    afValue={FeedbackRatingVariation.PRIMARY}
                    afChecked={
                      this.feedbackRatingVariation ===
                      FeedbackRatingVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variant"
                    afLabel="Sekundär"
                    afValue={FeedbackRatingVariation.SECONDARY}
                    afChecked={
                      this.feedbackRatingVariation ===
                      FeedbackRatingVariation.SECONDARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variant"
                    afLabel="Tertiär"
                    afValue={FeedbackRatingVariation.TERTIARY}
                    afChecked={
                      this.feedbackRatingVariation ===
                      FeedbackRatingVariation.TERTIARY
                    }
                  />
                </digi-form-fieldset>
              )}
              {this.ratingType === 'Betygsbanner' && (
                <digi-form-fieldset
                  afName="Typ"
                  afLegend="Typ"
                  onChange={(e) =>
                    (this.feedbackRatingType = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Typ"
                    afLabel="Grid"
                    afValue={FeedbackRatingType.GRID}
                    afChecked={
                      this.feedbackRatingType === FeedbackRatingType.GRID
                    }
                  />
                  <digi-form-radiobutton
                    afName="Typ"
                    afLabel="Fullbredd"
                    afValue={FeedbackRatingType.FULLWIDTH}
                    afChecked={
                      this.feedbackRatingType === FeedbackRatingType.FULLWIDTH
                    }
                  />
                  
                </digi-form-fieldset>
              )}
            </div>
            {this.ratingType === 'Baskomponent' && (
              <digi-tools-feedback-rating
                af-question="Hur nöjd är du med tjänsten?"
                af-ratings="Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd"
                af-type={FeedbackRatingType.MINIMAL}
                af-product-area="Produktområde"
                af-name="Börjat studera"
                af-question-type="Innehåll"
                af-product="Designsystem"
                af-application="Applikationsnamn"
                af-link-text="Skriv gärna mer feedback här"
                af-link="https://webropol.se/"></digi-tools-feedback-rating>
            )}
            {this.ratingType === 'Betygsbanner' && (
              <digi-tools-feedback-rating
                af-question="Hur nöjd är du med tjänsten?"
                af-variation={this.feedbackRatingVariation}
                af-ratings="Inte alls nöjd;Inte särskilt nöjd;Ganska nöjd;Nöjd;Mycket nöjd"
                af-type={this.feedbackRatingType}
                af-product-area="Produktområde"
                af-name="Börjat studera"
                af-question-type="Innehåll"
                af-product="Designsystem"
                af-application="Applikationsnamn"
                af-link-text="Skriv gärna mer feedback här"
                af-link="https://webropol.se/"></digi-tools-feedback-rating>
            )}
          </digi-code-example>
        </digi-layout-container>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <p>
                Komponenten har utvecklats främst för (men inte begränsad till)
                spårning med Piwik Pro.
              </p>
              <p>
                Om man vill använda komponenten i kombination med andra
                spårningsverktyg än Piwik Pro för att fånga upp data, kan man
                lyssna efter två inbyggda events som triggas vid respektive
                händelser:
              </p>
              <digi-list>
                <li>Klick på stjärna</li>
                <li>Klick på den eventuella länken efter inskick</li>
              </digi-list>
              <digi-layout-block
                af-variation="symbol"
                af-vertical-padding
                af-margin-bottom>
                <h3>Obligatoriskt attribut</h3>
                <ul>
                  <li>
                    Det behöver alltid skickas med en frågerubrik via attributet{' '}
                    <digi-code af-code="af-question" />, även om man väljer den
                    avskalade varianten (baskomponenten). Rubriken är i det
                    fallet visuellt dold men används för att skickas in till
                    Piwik och finns lämpligt placerad i HTML-strukturen bl.a.
                    utifrån ett tillgänglighetsperspektiv.
                  </li>
                </ul>
              </digi-layout-block>
              <h3>Varianter</h3>
              <p>
                Komponenten finns i tre varianter med olika bakgrundsfärger, som
                man växlar mellan med
                <digi-code af-code="af-variation" />.
              </p>
              <p>
                Det finns en fullbredd- och grid-variant. Det finns även en
                designmässigt avskalad variant av komponenten kallad för
                baskomponenten ("minimal"). Varianterna väljs med
                <digi-code af-code="af-type" />.
              </p>
              <h3>Anpassningar</h3>
              <p>
                Betygsbeskrivningen kan modifieras med{' '}
                <digi-code af-code="af-ratings" /> och måste följa samma
                formatering som i kodexemplet ovan, dvs. att alternativen är
                separerade med ett semikolon. Betygsbeskrivningen för betyg 1
                samt 5 skrivs ut i komponenten under rubriken.
              </p>
              <p>
                Om en extern länk till ett formulär ska visas efter att
                användaren har klickat på stjärna, anges URL:en med{' '}
                <digi-code af-code="af-link" /> samt den obligatoriska
                länkbeskrivningen med <digi-code af-code="af-link-text" />.
              </p>
              <h2>Attribut specifika för AF</h2>{' '}
              <p>Nedan info gäller enbart internt på Arbetsförmedlingen.</p>
              <p>
                Det finns följande attribut som kan skickas med i komponenten
                för att data ska skickas vidare till Piwik Pro:
              </p>
              <ul>
                <li>
                  Produkt, enligt produktkatalogen -{' '}
                  <digi-code af-code="af-product" />{' '}
                </li>
                <li>
                  Tjänst, enligt produktkatalogen -{' '}
                  <digi-code af-code="af-application" />{' '}
                </li>
                <li>
                  Produktområde, enligt produktkatalogen -{' '}
                  <digi-code af-code="af-product-area" />{' '}
                </li>
                <li>
                  Namn - <digi-code af-code="af-name" />{' '}
                </li>
                <li>
                  Frågetyp - <digi-code af-code="af-question-type" />{' '}
                </li>
              </ul>
              <p>
                Om anpassad Webropol-länk ska användas, använd{' '}
                <digi-code af-code="af-use-custom-link" /> .
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
