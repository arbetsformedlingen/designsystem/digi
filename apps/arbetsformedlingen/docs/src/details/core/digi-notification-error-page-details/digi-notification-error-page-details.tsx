/* eslint-disable no-useless-escape */
import { Component, h, Host, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  ErrorPageStatusCodes
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-notification-error-page-details',
  styleUrl: 'digi-notification-error-page-details.scss'
})
export class DigiNotificationErrorPage {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() afHttpStatusCode: ErrorPageStatusCodes =
    ErrorPageStatusCodes.UNAUTHORIZED;
  @State() afCustomHeadingChecked = false;
  @State() afCustomBodyTextChecked = false;
  @State() withSearchFieldChecked = true;
  @State() withLinksChecked = true;

  linkClickHandler(e) {
    e.detail.preventDefault();
    history.back();
  }

  get layoutBlockCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-notification-error-page 
	af-http-status-code="${this.afHttpStatusCode}"
	${this.afCustomHeadingChecked ? 'af-custom-heading="Egen rubrik"' : ''}\n\>
	${this.afCustomBodyTextChecked ? '<p slot="bodytext">\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris euismod consequat dui, nec aliquam lorem molestie sit amet. Quisque tempus gravida ex a cursus.\n	</p>' : ''}
	${this.withSearchFieldChecked ? '<digi-form-input-search \n		slot="search"\n		afLabel="Sök på webbplatsen"\n		af-variation="medium"\n		af-type="search"\n		af-button-text="Sök">\n	</digi-form-input-search>' : ''}	
	${
    this.withLinksChecked
      ? '<ul slot="links">\n		<li>\n			<digi-link-internal afHref="/" af-variation="small">\n\
				Till föregående sida\n\
			</digi-link-internal>\n\
	  	</li>\n\
	  	<li>\n\
		  	<digi-link-internal afHref="/" af-variation="small">\n\
			  	Till startsidan\n\
		  	</digi-link-internal>\n\
	  	</li>\n\
  	</ul>'
      : ''
  }
</digi-notification-error-page>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-notification-error-page 
	[attr.af-http-status-code]="ErrorPageStatusCodes.${Object.keys(ErrorPageStatusCodes).find((key) => ErrorPageStatusCodes[key] === this.afHttpStatusCode)}"
	${this.afCustomHeadingChecked ? '[attr.af-custom-heading]="Egen rubrik"' : ''}\n\>
	${this.afCustomBodyTextChecked ? '<p slot="bodytext">\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris euismod consequat dui, nec aliquam lorem molestie sit amet. Quisque tempus gravida ex a cursus.\n	</p>' : ''}
	${this.withSearchFieldChecked ? '<digi-form-input-search\n		slot="search"\n		af-label="Sök på webbplatsen"\n		af-variation="medium"\n		af-type="search"\n		af-button-text="Sök">\n    </digi-form-input-search>' : ''}	
	${
    this.withLinksChecked
      ? '<ul slot="links">\n		<li>\n			<digi-link-internal af-href="/" af-variation="small">\n\
				Till föregående sida\n\
			</digi-link-internal>\n\
		</li>\n\
		<li>\n\
			<digi-link-internal afHref="/" af-variation="small">\n\
				Till startsidan\n\
			</digi-link-internal>\n\
		</li>\n\
	</ul>'
      : ''
  }
</digi-notification-error-page>`,
      [CodeExampleLanguage.REACT]: `\
<DigiNotificationErrorPage 
	afHttpStatusCode={ErrorPageStatusCodes.${Object.keys(ErrorPageStatusCodes).find((key) => ErrorPageStatusCodes[key] === this.afHttpStatusCode)}}
	${this.afCustomHeadingChecked ? 'afCustomHeading="Egen rubrik"' : ''}\n\>
	${this.afCustomBodyTextChecked ? '<p slot="bodytext">\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris euismod consequat dui, nec aliquam lorem molestie sit amet. Quisque tempus gravida ex a cursus.\n	</p>' : ''}
	${this.withSearchFieldChecked ? '<DigiFormInputSearch\n		slot="search"\n		afLabel="Sök på webbplatsen"\n		afVariation="medium"\n		afType="search"\n		afButtonText="Sök">\n	</DigiFormInputSearch>' : ''}	
	${
    this.withLinksChecked
      ? '<ul slot="links">\n		<li>\n			<DigiLinkInternal afHref="/" afVariation="small">\n\
				Till föregående sida\n\
			</DigiLinkInternal>\n\
		</li>\n\
		<li>\n\
			<DigiLinkInternal afHref="/" afVariation="small">\n\
				Till startsidan\n\
			</DigiLinkInternal>\n\
		</li>\n\
	</ul>'
      : ''
  }
</DigiNotificationErrorPage>`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-notification-error-page-details">
          <digi-typography>
            {!this.afShowOnlyExample && (
              <digi-typography-preamble>
                Felmeddelandesidor hjälper våra användare med riktad information
                om vad som har inträffat och visar på specifika vägar vidare,
                anpassat för enskilda felet. Sidorna lämpar sig när vi av olika
                anledningar inte kan visa rätt sida.
              </digi-typography-preamble>
            )}
            <digi-layout-container af-no-gutter af-margin-bottom>
              <article>
                {!this.afShowOnlyExample && <h2>Exempel</h2>}
                <digi-code-example
                  af-code={JSON.stringify(this.layoutBlockCode)}
                  af-hide-controls={this.afHideControls ? 'true' : 'false'}
                  af-hide-code={this.afHideCode ? 'true' : 'false'}>
                  <div class="slot__controls" slot="controls">
                    <digi-form-select
                      afLabel="Variant"
                      onAfOnChange={(e) => {
                        this.afHttpStatusCode = (e.target as any).value;
                        this.afCustomHeadingChecked = false;
                        this.afCustomBodyTextChecked = false;

                        if (
                          this.afHttpStatusCode ==
                            ErrorPageStatusCodes.UNAUTHORIZED ||
                          this.afHttpStatusCode ==
                            ErrorPageStatusCodes.NOT_FOUND ||
                          this.afHttpStatusCode == ErrorPageStatusCodes.GONE
                        ) {
                          this.withSearchFieldChecked = true;
                        } else {
                          this.withSearchFieldChecked = false;
                        }
                      }}
                      af-variation="small"
                      af-start-selected="0">
                      <option value="401">401 Unauthorized</option>
                      <option value="403">403 Forbidden</option>
                      <option value="404">404 Not Found</option>
                      <option value="410">410 Gone Permanently</option>
                      <option value="500">500 Internal Server Error</option>
                      <option value="503">503 Service Unavailable</option>
                      <option value="504">504 Gateway Timeout</option>
                    </digi-form-select>

                    <digi-form-fieldset style={{ marginTop: '10px' }}>
                      <digi-form-checkbox
                        afLabel="Egen rubrik"
                        afChecked={this.afCustomHeadingChecked}
                        onAfOnChange={() =>
                          (this.afCustomHeadingChecked =
                            !this.afCustomHeadingChecked)
                        }></digi-form-checkbox>
                      <digi-form-checkbox
                        afLabel="Egen brödtext"
                        afChecked={this.afCustomBodyTextChecked}
                        onAfOnChange={() =>
                          (this.afCustomBodyTextChecked =
                            !this.afCustomBodyTextChecked)
                        }></digi-form-checkbox>
                    </digi-form-fieldset>
                    <digi-form-fieldset style={{ marginTop: '10px' }}>
                      <digi-form-checkbox
                        afLabel="Med sökfält"
                        afChecked={this.withSearchFieldChecked}
                        onAfOnChange={() =>
                          (this.withSearchFieldChecked =
                            !this.withSearchFieldChecked)
                        }></digi-form-checkbox>
                    </digi-form-fieldset>
                    <digi-form-fieldset style={{ marginTop: '10px' }}>
                      <digi-form-checkbox
                        afLabel="Med länkar"
                        afChecked={this.withLinksChecked}
                        onAfOnChange={() =>
                          (this.withLinksChecked = !this.withLinksChecked)
                        }></digi-form-checkbox>
                    </digi-form-fieldset>
                  </div>
                  <div class="digi-notification-error-page-details__container">
                    {this.afCustomBodyTextChecked ? (
                      <div>
                        <digi-notification-error-page
                          key="withcustombody"
                          afHttpStatusCode={this.afHttpStatusCode}
                          afCustomHeading={
                            this.afCustomHeadingChecked
                              ? 'Egen rubrik'
                              : undefined
                          }>
                          <p slot="bodytext">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Mauris euismod consequat dui, nec aliquam
                            lorem molestie sit amet. Quisque tempus gravida ex a
                            cursus.{' '}
                          </p>
                          {this.withSearchFieldChecked && (
                            <digi-form-input-search
                              slot="search"
                              afLabel="Sök på webbplatsen"
                              af-variation="medium"
                              af-type="search"
                              af-button-text="Sök"></digi-form-input-search>
                          )}
                          {this.withLinksChecked && (
                            <ul slot="links">
                              <li>
                                <digi-link-internal
                                  afHref="#"
                                  af-variation="small"
                                  onAfOnClick={(e) => this.linkClickHandler(e)}>
                                  Till föregående sida
                                </digi-link-internal>
                              </li>
                              <li>
                                <digi-link-internal
                                  afHref="/"
                                  af-variation="small">
                                  Till startsidan
                                </digi-link-internal>
                              </li>
                            </ul>
                          )}
                        </digi-notification-error-page>
                      </div>
                    ) : (
                      <div>
                        <digi-notification-error-page
                          key="withoutcustombody"
                          afHttpStatusCode={this.afHttpStatusCode}
                          afCustomHeading={
                            this.afCustomHeadingChecked
                              ? 'Egen rubrik'
                              : undefined
                          }>
                          {this.withSearchFieldChecked && (
                            <digi-form-input-search
                              slot="search"
                              afLabel="Sök på webbplatsen"
                              af-variation="medium"
                              af-type="search"
                              af-button-text="Sök"></digi-form-input-search>
                          )}
                          {this.withLinksChecked && (
                            <ul slot="links">
                              <li>
                                <digi-link-internal
                                  afHref="#"
                                  af-variation="small"
                                  onAfOnClick={(e) => this.linkClickHandler(e)}>
                                  Till föregående sida
                                </digi-link-internal>
                              </li>
                              <li>
                                <digi-link-internal
                                  afHref="/"
                                  af-variation="small">
                                  Till startsidan
                                </digi-link-internal>
                              </li>
                            </ul>
                          )}
                        </digi-notification-error-page>
                      </div>
                    )}
                  </div>
                </digi-code-example>
              </article>
            </digi-layout-container>
            {!this.afShowOnlyExample && (
              <digi-layout-container af-no-gutter af-margin-bottom>
                <h2>Beskrivning</h2>
                <h3>Varianter</h3>
                <p>
                  Komponenten består av ett antal felmeddelandesidor, som
                  baseras på följande <b>HTTP-statuskoder:</b>
                </p>
                <digi-list>
                  <li>401 Unauthorized</li>
                  <li>403 Forbidden</li>
                  <li>404 Not Found</li>
                  <li>410 Gone Permanently</li>
                  <li>500 Internal Server Error</li>
                  <li>503 Service Unavailable</li>
                  <li>504 Gateway Timeout</li>
                </digi-list>
                <h3>Färdig text som är möjlig att override:a</h3>
                <p>
                  Sidorna innehåller färdig text som har arbetats fram för att
                  vara lättförståelig och tillgänglig för alla användare.
                  <br />
                  Det är dock möjligt att skicka in valfri rubrik med hjälp av
                  <digi-code af-code='af-custom-heading="Egen rubrik"'></digi-code>
                  <br />
                  samt valfritt textinnehåll med en text omsluten av
                  paragrafelement inuti komponenten:{' '}
                  <digi-code af-code='<p slot="bodytext">"Egen text"</p>'></digi-code>
                </p>
                <h3>Sökfält och länkar</h3>
                <p>
                  Det är möjligt att använda sig av ett sökfält samt länkar på
                  felmeddelandesidorna.
                </p>
              </digi-layout-container>
            )}
          </digi-typography>
        </div>
      </Host>
    );
  }
}
