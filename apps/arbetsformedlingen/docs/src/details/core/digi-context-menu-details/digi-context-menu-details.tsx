import { Component, Prop, State, h } from '@stencil/core';
import {
  NavigationContextMenuHorizontalPosition,
  CodeExampleLanguage,
  InfoCardHeadingLevel
} from '@digi/arbetsformedlingen';

const menuItems = [
  { id: 0, title: 'Menyval 1' },
  { id: 1, title: 'Menyval 2' },
  { id: 2, title: 'Menyval 3' }
];

@Component({
  tag: 'digi-context-menu-details',
  styleUrl: 'digi-context-menu-details.scss'
})
export class DigiNavigationContextMenuDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() horizontalPosition: NavigationContextMenuHorizontalPosition =
    NavigationContextMenuHorizontalPosition.START;

  @State() menuPosition = 'bottom-left';

  get navigationContextMenuItemTypeCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-context-menu
	af-title="Rullgardinsmeny"
	af-menu-position="${this.menuPosition}"
>
</digi-context-menu>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-context-menu
	[attr.af-title]="'Rullgardinsmeny'"
	[attr.af-menu-position]="'${this.menuPosition}'"
	[attr.af-menu-items]="menuItems"
>
</digi-context-menu>`,
      [CodeExampleLanguage.REACT]: `\
<DigiContextMenu
	afTitle="Rullgardinsmeny"
	afMenuPosition="${this.menuPosition}"
	afMenuItems={menuItems}
>
</DigiContextMenu>`
    };
  }
  render() {
    return (
      <div class="digi-navigation-context-menu-details">
        <digi-typography>
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.navigationContextMenuItemTypeCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              {!this.afHideControls && (
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    af-legend="Position"
                    af-name="Position"
                    onChange={(e) =>
                      (this.menuPosition = (e.target as HTMLInputElement).value)
                    }>
                    <digi-form-radiobutton
                      afName="Position"
                      afLabel="Vänster"
                      afValue="left-bottom"
                      afChecked={true}
                    />
                    <digi-form-radiobutton
                      afName="Position"
                      afLabel="Höger"
                      afValue="right-bottom"
                    />
                  </digi-form-fieldset>
                </div>
              )}
              <div style={{ height: '120px' }}>
                <digi-context-menu
                  af-title="Rullgardinsmeny"
                  af-menu-position={this.menuPosition}
                  afMenuItems={menuItems}
                />
              </div>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-margin-bottom afNoGutter>
              <h2>Beskrivning</h2>
              <p>
                Visningsnamnet på rullgardinsmenyn anges via{' '}
                <digi-code af-code="'Rullgardinsmeny'" />. Ange vilket val som
                ska vara förvalt genom <digi-code af-code="af-active-item" />.
                Värdet anges som ett nummer, om inget värde anges är det första
                elementet (0) i listan förvalt.
              </p>

              <h3>Varianter</h3>
              <p>
                Rullgardinsmenyn finns enbart i en variant. Vid användning av
                typen "knapp" emittas ett event med valt värde vid klick och
                rullgardinsmenyn stänger sig. Vid användning av knappar behövs
                ej attributet <digi-code af-code="af-href" />.
              </p>
              <h3>För att skicka in data</h3>
              <p>För att skicka in data så ser objektstrukturen ut så här:</p>
              <digi-code-block
                af-language="typescript"
                af-code={`_menuItems: IMenuItems[] = ${JSON.stringify(menuItems, null, 2)}`}></digi-code-block>
              <br />
              <p>
                Om du använder HTML så kan du använda följande javascript för
                att skicka in alternativen i väljaren. Se till att byta ut id.
              </p>
              <digi-code-block
                af-language="javascript"
                af-variation="dark"
                af-code='const menuItems = JSON.stringify(_menuItems);
document.getElementById("id").setAttribute("af-menu-items", menuItems);'></digi-code-block>
              <h3>För att testa</h3>
              <p>För att testa i HTML så kan du använda följande script</p>
              <digi-code-block
                af-language="javascript"
                af-code="const $menu = document.querySelector('digi-context-menu')
$menu.addEventListener('afChangeItem', (e) => console.log('Valt alternativ', e.details.item, e.details.idx))
$menu.addEventListener('afFocusItem', (e) => console.log('Fokus flyttat till alternativ ', e.details.item, e.details.idx))
$menu.addEventListener('afToggleMenu', (e) => console.log(e.details === true ? 'Meny öppnad' : 'Meny stängd'))
"></digi-code-block>
              <br />
              <p>För att testa i React så kan du använda följande event:</p>
              <digi-code-block
                af-language="typescript"
                af-code="onAfChangeItem={(e) => console.log('Valt alternativ', e.details.item, e.details.idx)}
onAfFocusItem={(e) => console.log('Fokus flyttat till alternativ ', e.details.item, e.details.idx)}
onAfToggleMenu={(e) => console.log(e.details === true ? 'Meny öppnad' : 'Meny stängd')}"></digi-code-block>
              <br />
              <digi-info-card
                afHeadingLevel={InfoCardHeadingLevel.H3}
                afHeading="Riktlinjer">
                <digi-list>
                  <li>
                    Rullgardinsmeny ska alltid ha en etikett intill sig där det
                    framgår vad användaren ska göra sitt val utifrån.
                  </li>
                  <li>
                    Rullgardinsmenyn ska i utgångsläget innehålla en text, där
                    det tydligt framgår vad man väljer (till exempel "Välj
                    yrke", inte endast "Välj").
                  </li>
                  <li>
                    Hela fältet för rullgardinsmeny är klickyta som fäller
                    ut/fäller ihop menyn.
                  </li>
                  <li>
                    De valbara alternativen i en rullgardinsmeny i formulär får
                    inte vara länkar.
                  </li>
                </digi-list>
              </digi-info-card>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
