import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-breakpoint-observer-details',
  styleUrl: 'digi-util-breakpoint-observer-details.scss'
})
export class DigiUtilBreakpointObserverDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-breakpoint-observer-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              <digi-code af-code={`<digi-util-breakpoint-observer>`} />
              observerar förändringar i satta fönsterstorlekar och skapar events
              när brytpunkterna ändras. Det är användbart när du behöver ändra
              egenskaper eller värden i javascript baserat på olika fasta
              storlekar på fönstret.
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
