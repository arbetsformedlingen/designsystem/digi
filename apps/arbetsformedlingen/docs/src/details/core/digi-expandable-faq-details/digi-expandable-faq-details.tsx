import { Component, Prop, h, State } from '@stencil/core';
import {
  ExpandableFaqVariation,
  CodeExampleLanguage
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-expandable-faq-details',
  styleUrl: 'digi-expandable-faq-details.scss'
})
export class DigiExpandableFAQDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() expandableFaqVariation: ExpandableFaqVariation =
    ExpandableFaqVariation.PRIMARY;

  get faqCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-expandable-faq
	af-heading="Vanliga frågor" 
	af-variation="${this.expandableFaqVariation}"
>
	<digi-expandable-faq-item af-heading="Fråga 1">
		<p>
			Svar 1 - Reprehenderit ea nulla dolore non fugiat aute. 
			Reprehenderit velit velit non eiusmod. Voluptate commodo consectetur ullamco velit minim quis. 
			Reprehenderit consectetur nisi officia est fugiat id anim incididunt qui eiusmod.
		</p>
	</digi-expandable-faq-item>
	<digi-expandable-faq-item af-heading="Fråga 2">
		<p>
			Svar 2 - In veniam nostrud esse velit velit incididunt ullamco non adipisicing. 
			Dolore sint quis aute nostrud. Pariatur commodo ullamco dolor nulla ut. 
			Consequat amet velit veniam ipsum reprehenderit tempor. 
			Eu labore enim officia anim laboris magna eu eu ad. Pariatur voluptate sint nisi ut nulla.
		</p>
	</digi-expandable-faq-item>
	<digi-expandable-faq-item af-heading="Fråga 3">
		<p>
			Svar 3 - Officia laboris commodo quis ex nisi cupidatat officia dolore est. 
			Ad dolore exercitation sunt deserunt Lorem do esse reprehenderit ex non. 
			Proident occaecat elit enim ullamco.
		</p>
	</digi-expandable-faq-item>
</digi-expandable-faq>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-expandable-faq
	[attr.af-heading]="Vanliga frågor" 
	[attr.af-variation]="ExpandableFaqVariation.${Object.keys(ExpandableFaqVariation).find((k) => ExpandableFaqVariation[k] == this.expandableFaqVariation)}"
>
	<digi-expandable-faq-item [attr.af-heading]="Fråga 1">
		<p>
			Svar 1 - Reprehenderit ea nulla dolore non fugiat aute. 
			Reprehenderit velit velit non eiusmod. Voluptate commodo consectetur ullamco velit minim quis. 
			Reprehenderit consectetur nisi officia est fugiat id anim incididunt qui eiusmod.
		</p>
	</digi-expandable-faq-item>
	<digi-expandable-faq-item [attr.af-heading]="Fråga 2">
		<p>
			Svar 2 - In veniam nostrud esse velit velit incididunt ullamco non adipisicing. 
			Dolore sint quis aute nostrud. Pariatur commodo ullamco dolor nulla ut. 
			Consequat amet velit veniam ipsum reprehenderit tempor. 
			Eu labore enim officia anim laboris magna eu eu ad. Pariatur voluptate sint nisi ut nulla.
		</p>
	</digi-expandable-faq-item>
	<digi-expandable-faq-item [attr.af-heading]="Fråga 3">
		<p>
			Svar 3 - Officia laboris commodo quis ex nisi cupidatat officia dolore est. 
			Ad dolore exercitation sunt deserunt Lorem do esse reprehenderit ex non. 
			Proident occaecat elit enim ullamco.
		</p>
	</digi-expandable-faq-item>
</digi-expandable-faq>`,
      [CodeExampleLanguage.REACT]: `\
<DigiExpandableFaq
	afHeading="Vanliga frågor" 
	afVariation={ExpandableFaqVariation.${Object.keys(ExpandableFaqVariation).find((k) => ExpandableFaqVariation[k] == this.expandableFaqVariation)}}
>
	<DigiExpandableFaqItem afHeading="Fråga 1">
		<p>
			Svar 1 - Reprehenderit ea nulla dolore non fugiat aute. 
			Reprehenderit velit velit non eiusmod. Voluptate commodo consectetur ullamco velit minim quis. 
			Reprehenderit consectetur nisi officia est fugiat id anim incididunt qui eiusmod.
		</p>
	</DigiExpandableFaqItem>
	<DigiExpandableFaqItem afHeading="Fråga 2">
		<p>
			Svar 2 - In veniam nostrud esse velit velit incididunt ullamco non adipisicing. 
			Dolore sint quis aute nostrud. Pariatur commodo ullamco dolor nulla ut. 
			Consequat amet velit veniam ipsum reprehenderit tempor. 
			Eu labore enim officia anim laboris magna eu eu ad. Pariatur voluptate sint nisi ut nulla.
		</p>
	</DigiExpandableFaqItem>
	<DigiExpandableFaqItem afHeading="Fråga 3">
		<p>
			Svar 3 - Officia laboris commodo quis ex nisi cupidatat officia dolore est. 
			Ad dolore exercitation sunt deserunt Lorem do esse reprehenderit ex non. 
			Proident occaecat elit enim ullamco.
		</p>
	</DigiExpandableFaqItem>
</DigiExpandableFaq>`
    };
  }
  render() {
    return (
      <div class="digi-expandable-faq-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <digi-code
                af-variation="light"
                af-code="<digi-expandable-faq-item>"
              />{' '}
              för att skapa en lista med vanliga frågor och utfällbara svar.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.faqCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variant"
                  onChange={(e) =>
                    (this.expandableFaqVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={ExpandableFaqVariation.PRIMARY}
                    afChecked={
                      this.expandableFaqVariation ===
                      ExpandableFaqVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={ExpandableFaqVariation.SECONDARY}
                    afChecked={
                      this.expandableFaqVariation ===
                      ExpandableFaqVariation.SECONDARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Tertiär"
                    afValue={ExpandableFaqVariation.TERTIARY}
                    afChecked={
                      this.expandableFaqVariation ===
                      ExpandableFaqVariation.TERTIARY
                    }
                  />
                </digi-form-fieldset>
              </div>
              <digi-expandable-faq
                afHeading="Vanliga frågor"
                afVariation={this.expandableFaqVariation}>
                <digi-expandable-faq-item afHeading="Fråga 1">
                  <p>
                    Svar 1 - Cillum fugiat sunt adipisicing non consectetur.
                    Aute do eiusmod id enim ea ad tempor cupidatat. Enim nulla
                    cupidatat eu anim cupidatat dolore pariatur labore
                    consequat. Do ea sit ut anim eu culpa amet. Nostrud eiusmod
                    excepteur Lorem sunt. Irure ipsum commodo Lorem consequat
                    ipsum dolore veniam aliquip fugiat dolor culpa ut aliqua
                    excepteur. Commodo eiusmod dolore elit ullamco mollit eu ut
                    aute aute laboris magna ullamco.
                  </p>
                </digi-expandable-faq-item>
                <digi-expandable-faq-item afHeading="Fråga 2">
                  <p>
                    Svar 2 - Occaecat excepteur fugiat nisi aliqua elit. Aliqua
                    irure irure commodo exercitation exercitation ipsum minim
                    tempor ex ea ad magna exercitation. Ea nostrud aute velit
                    Lorem sunt aliqua dolore.
                  </p>
                </digi-expandable-faq-item>
                <digi-expandable-faq-item afHeading="Fråga 3">
                  <p>
                    Svar 3 - Quis proident minim reprehenderit dolor ipsum eu
                    consectetur excepteur culpa et. Ut excepteur nisi officia
                    quis incididunt commodo ut cillum non magna cillum. Minim
                    voluptate velit reprehenderit laborum fugiat velit sint ex
                    sint. Et ipsum mollit reprehenderit voluptate laborum
                    eiusmod commodo cupidatat cillum occaecat. Elit minim ea ex
                    deserunt labore mollit nisi sint magna.
                  </p>
                </digi-expandable-faq-item>
              </digi-expandable-faq>
            </digi-code-example>
          </digi-layout-container>

          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Kodexempel</h3>
              <p>
                Rubriken läggs in med{' '}
                <digi-code af-variation="light" af-code="af-heading" /> och
                rubriknivån är <digi-code af-variation="light" af-code="h2" />{' '}
                som standard. Vill du ändra rubriknivå kan du använda{' '}
                <digi-code af-variation="light" af-code="af-heading-level" /> .
                Typescript-användare bör importera och använda
                <digi-code
                  af-variation="light"
                  af-code="ExpandableFaqItemHeadingLevel"
                />
                . Stylingen på kommer se likadan ut även om du ändrar till en
                lägre rubriknivå.
              </p>
              <h3>Varianter</h3>
              <p>
                FAQ item-komponenten finns i tre varianter, en primär (primary)
                i ljusgrönt som är standard, en sekundär (secondary) i grått,
                samt en tertiär (tertiary) i vit. Variant sätts med hjälp av{' '}
                <digi-code af-variation="light" af-code="af-variation" />.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
