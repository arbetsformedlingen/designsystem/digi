import { Component, h, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  InfoCardVariation
} from '@digi/arbetsformedlingen';
@Component({
  tag: 'digi-header-notification-details',
  styleUrl: 'digi-header-notification-details.scss'
})
export class DigiHeaderNotification {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() value = '8';

  get headerNotificationCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-header-notification af-notification-amount="${this.value}">
	<a href="/">
		<digi-icon-bell-filled></digi-icon-bell-filled>
			Notiser
	</a>
</digi-header-notification>
				`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-header-notification [attr.af-notification-amount]="${this.value}">
	<a href="/" [routerLink]="['/']">
		<digi-icon-bell-filled></digi-icon-bell-filled>
			Notiser
	</a>
</digi-header-notification>
				`,
      [CodeExampleLanguage.REACT]: `\
<digi-header-notification afNotificationAmount="${this.value}">
	<a href="/">
		<digi-icon-bell-filled></digi-icon-bell-filled>
			Notiser
	</a>
</digi-header-notification>
				`
    };
  }

  render() {
    return (
      <div class="digi-header-notification-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Används för att visa antal olästa
              notifieringar/aviseringar/uppdateringar tillsammans med en ikon.
            </digi-typography-preamble>
          )}
        </digi-typography>
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={JSON.stringify(this.headerNotificationCode)}
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}>
            <div class="slot__controls" slot="controls">
              <digi-form-fieldset
                afLegend="Variant"
                onChange={(e) =>
                  (this.value = (e.target as HTMLFormElement).value)
                }>
                <digi-form-radiobutton
                  afName="Variant"
                  afLabel="Nummer under 100"
                  afValue="9"
                  afChecked={true}
                />
                <digi-form-radiobutton
                  afName="Variant"
                  afLabel="Nummer över 99"
                  afValue="100"
                />
              </digi-form-fieldset>
            </div>
            <digi-header-notification af-notification-amount={this.value}>
              <a href="/">
                <digi-icon-bell-filled></digi-icon-bell-filled>
                Notiser
              </a>
            </digi-header-notification>
          </digi-code-example>
        </digi-layout-container>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Komponenten tar emot nummerinnehåll med{' '}
                <digi-code af-code="af-notification-amount" />. Om numret är
                större än 99, visas "99+" som innehåll.
              </p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-block af-container="none" af-margin-bottom>
              <digi-info-card
                afHeading="Bra att veta"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.PRIMARY}>
                <p>
                  Komponenten kan enbart användas inuti komponenten{' '}
                  <a href="/komponenter/digi-header/oversikt">
                    <digi-code af-code="<digi-header>"></digi-code>
                  </a>
                </p>
              </digi-info-card>
              <br />
            </digi-layout-block>
          )}
        </digi-typography>
      </div>
    );
  }
}
