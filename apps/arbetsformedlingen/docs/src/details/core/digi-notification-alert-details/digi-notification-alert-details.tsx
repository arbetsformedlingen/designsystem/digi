import { Component, Prop, h, State } from '@stencil/core';
import {
  NotificationAlertSize,
  NotificationAlertHeadingLevel,
  NotificationAlertVariation,
  CodeExampleLanguage,
  FormCheckboxVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-notification-alert-details',
  styleUrl: 'digi-notification-alert-details.scss'
})
export class DiginNotificationAlertTagDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() closeable = false;
  @State() notificationAlertSize: NotificationAlertSize =
    NotificationAlertSize.LARGE;
  @State() notificationAlertHeadingLevel: NotificationAlertHeadingLevel =
    NotificationAlertHeadingLevel.H2;
  @State() notificationAlertVariation: NotificationAlertVariation =
    NotificationAlertVariation.INFO;

  get notificationAlertCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-notification-alert
	af-size="${this.notificationAlertSize}"
	af-variation="${this.notificationAlertVariation}"\
	${this.notificationAlertSize == NotificationAlertSize.LARGE ? `\n\taf-heading="Informationsmeddelande"\t` : ''}\
	${this.closeable ? `\n\taf-closeable="${this.closeable}"\t` : ''}
>
	Meddelandebeskrivning
</digi-notification-alert>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-notification-alert
	[attr.af-size]="NotificationAlertSize.${Object.keys(NotificationAlertSize).find((key) => NotificationAlertSize[key] === this.notificationAlertSize)}"
	[attr.af-variation]="NotificationAlertVariation.${Object.keys(NotificationAlertVariation).find((key) => NotificationAlertVariation[key] === this.notificationAlertVariation)}"\
	${this.notificationAlertSize == NotificationAlertSize.LARGE ? `\n\t[attr.af-heading]="Information"\t` : ''}\
	${this.closeable ? `\n\t[attr.af-closeable]="${this.closeable}"\t` : ''}
>
	Meddelandebeskrivning
</digi-notification-alert>`,
      [CodeExampleLanguage.REACT]: `\
<DigiNotificationAlert
	afSize={NotificationAlertSize.${Object.keys(NotificationAlertSize).find((key) => NotificationAlertSize[key] === this.notificationAlertSize)}}
	afVariation={NotificationAlertVariation.${Object.keys(NotificationAlertVariation).find((key) => NotificationAlertVariation[key] === this.notificationAlertVariation)}}\
	${this.notificationAlertSize == NotificationAlertSize.LARGE ? `\n\tafHeading="Information"\t ` : ''}\
	${this.closeable ? `\n\tafCloseable={${this.closeable}}\t` : ''}
>
	Meddelandebeskrivning
</DigiNotificationAlert>`
    };
  }

  render() {
    return (
      <div class="digi-notification-alert-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Informationsmeddelande (Alert) används för att tillfälligt lyfta
              fram viktig information till användaren då hen behöver den.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.notificationAlertCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variant"
                  onChange={(e) =>
                    (this.notificationAlertVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Info"
                    afValue={NotificationAlertVariation.INFO}
                    afChecked={
                      this.notificationAlertVariation ===
                      NotificationAlertVariation.INFO
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Fara"
                    afValue={NotificationAlertVariation.DANGER}
                    afChecked={
                      this.notificationAlertVariation ===
                      NotificationAlertVariation.DANGER
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Varning"
                    afValue={NotificationAlertVariation.WARNING}
                    afChecked={
                      this.notificationAlertVariation ===
                      NotificationAlertVariation.WARNING
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Godkänd"
                    afValue={NotificationAlertVariation.SUCCESS}
                    afChecked={
                      this.notificationAlertVariation ===
                      NotificationAlertVariation.SUCCESS
                    }
                  />
                </digi-form-fieldset>
                <digi-form-fieldset
                  afName="Storlek"
                  afLegend="Storlek"
                  onChange={(e) =>
                    (this.notificationAlertSize = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Liten"
                    afValue={NotificationAlertSize.SMALL}
                    afChecked={
                      this.notificationAlertSize === NotificationAlertSize.SMALL
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Mellan"
                    afValue={NotificationAlertSize.MEDIUM}
                    afChecked={
                      this.notificationAlertSize ===
                      NotificationAlertSize.MEDIUM
                    }
                  />
                  <digi-form-radiobutton
                    afName="Storlek"
                    afLabel="Stor"
                    afValue={NotificationAlertSize.LARGE}
                    afChecked={
                      this.notificationAlertSize === NotificationAlertSize.LARGE
                    }
                  />
                </digi-form-fieldset>
                <digi-form-fieldset af-legend="Stängbar">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afVariation={FormCheckboxVariation.PRIMARY}
                    afChecked={this.closeable}
                    onAfOnChange={() =>
                      this.closeable
                        ? (this.closeable = false)
                        : (this.closeable = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
              </div>
              <digi-notification-alert
                af-heading="Information"
                af-size={this.notificationAlertSize}
                af-variation={this.notificationAlertVariation}
                af-heading-level={this.notificationAlertHeadingLevel}
                af-closeable={this.closeable}>
                Meddelandebeskrivning
              </digi-notification-alert>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Storlek</h3>
              <p>
                Informationsmeddelande finns i storlekarna stor (l), mellan (m)
                och liten (s), varav stor är standard. Använd{' '}
                <digi-code af-code="af-size"></digi-code> för att välja vilken
                du vill ha, t.ex.{' '}
                <digi-code af-code="af-size='small'"></digi-code>.
              </p>
              <h3>Varianter</h3>
              <p>
                Det går att ställa in olika varianter av komponenten med hjälp
                av <digi-code af-code="af-variation"></digi-code>. Det går att
                välja mellan info, danger, success och warning.
              </p>
              <h3>Stängbart informationsmeddelande</h3>
              <p>
                Informationsmeddelande går att göra stängbart med hjälp av{' '}
                <digi-code af-code="afCloseable"></digi-code> tillsammans med en
                boolean.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
