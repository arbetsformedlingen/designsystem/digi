import { Component, Prop, h, State, getAssetPath } from '@stencil/core';
import {
  CodeExampleLanguage,
  LayoutMediaObjectAlignment
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-layout-media-object-details',
  styleUrl: 'digi-layout-media-object-details.scss'
})
export class DigiLayoutMediaObjectDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() layoutMediaObjectAlignment: LayoutMediaObjectAlignment =
    LayoutMediaObjectAlignment.START;

  get layoutMediaObjectCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-layout-media-object af-alignment="${this.layoutMediaObjectAlignment}">
  <digi-media-image slot="media">
  </digi-media-image>
    ...  
</digi-layout-media-object>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-layout-media-object
  [attr.af-alignment]="LayoutMediaObjectAlignment.${Object.keys(
    LayoutMediaObjectAlignment
  ).find(
    (key) => LayoutMediaObjectAlignment[key] === this.layoutMediaObjectAlignment
  )}">
  <digi-media-image slot="media">
  </digi-media-image>
    ...  
</digi-layout-media-object>`,
      [CodeExampleLanguage.REACT]: `\
<DigiLayoutMediaObject
  afAlignment={LayoutMediaObjectAlignment.${Object.keys(
    LayoutMediaObjectAlignment
  ).find(
    (key) => LayoutMediaObjectAlignment[key] === this.layoutMediaObjectAlignment
  )}}>
  <DigiMediaImage slot="media">
  </DigiMediaImage>
    ...  
</DigiLayoutMediaObject>`
    };
  }
  render() {
    return (
      <div class="digi-layout-media-object-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Det här är medieobjekt-komponenten. Den används oftast för att
              lägga bilder med text på sidan.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.layoutMediaObjectCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    af-legend="Justering"
                    af-name="Justering"
                    onChange={(e) =>
                      (this.layoutMediaObjectAlignment = (
                        e.target as HTMLFormElement
                      ).value)
                    }>
                    <digi-form-radiobutton
                      afName="Justering"
                      afLabel="Topp"
                      afValue={LayoutMediaObjectAlignment.START}
                      afChecked={true}
                    />
                    <digi-form-radiobutton
                      afName="Justering"
                      afLabel="Center"
                      afValue={LayoutMediaObjectAlignment.CENTER}
                    />
                    <digi-form-radiobutton
                      afName="Justering"
                      afLabel="Botten"
                      afValue={LayoutMediaObjectAlignment.END}
                    />
                    <digi-form-radiobutton
                      afName="Justering"
                      afLabel="Stretch"
                      afValue={LayoutMediaObjectAlignment.STRETCH}
                    />
                  </digi-form-fieldset>
                </div>
                <digi-layout-media-object
                  afAlignment={this.layoutMediaObjectAlignment}>
                  <digi-media-image
                    slot="media"
                    afUnlazy
                    af-src={getAssetPath('/assets/images/logotype-green.svg')}
                    afAlt="Arbetsförmedlingens logotyp i grön färg"></digi-media-image>
                  <h3>Medieobjektet</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  </p>
                </digi-layout-media-object>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <p>
                Med hjälp av <digi-code af-code="af-alignment" />
                kan medieobjektet justeras till start, end, center eller
                stretch. Start är standard. Typskriptanvändare bör importera och
                använda <digi-code af-code="LayoutMediaObjectAlignment" />
                för att ställa in justering.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
