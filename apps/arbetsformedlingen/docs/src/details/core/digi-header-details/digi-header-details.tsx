import { Component, h, Prop, State, getAssetPath } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';
@Component({
  tag: 'digi-header-details',
  styleUrl: 'digi-header-details.scss'
})
export class DigiHeader {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() isActive = true;

  get headerCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
	<digi-header
		af-system-name="Designsystem"
		af-hide-system-name="false"
		af-menu-button-text="Meny">
		<a slot="header-logo" aria-label="Designsystemets startsida" href="/"></a>
			<div slot="header-content">
				<digi-header-notification af-notification-amount="8">
					<a href="/">
						<digi-icon-bell-filled></digi-icon-bell-filled>
						Notiser
					</a>
				</digi-header-notification>
				<digi-header-avatar
					af-src="/assets/images/avatar.svg"
					af-alt="Profilbild på Linda Karlsson"
					af-name="Linda Karlsson"
					af-signature="KALIA"
					af-is-logged-in="true"
					af-hide-signature="true"
				></digi-header-avatar>
			</div>

			<div slot="header-navigation">
				<digi-header-navigation
					af-close-button-text="Stäng"
					af-close-button-aria-label="Stäng meny"
					af-nav-aria-label="Huvudmeny"
				>
				<digi-header-navigation-item af-current-page="true">
					 <a href="/">Mina bokningar</a>
				</digi-header-navigation-item>
				<digi-header-navigation-item>
					<a href="/">Grupper</a>
				</digi-header-navigation-item>
				<digi-header-navigation-item>
					<a href="/">Kontakt</a>
				</digi-header-navigation-item>
			</digi-header-navigation>
			</div>
		</digi-header>
		`,

      [CodeExampleLanguage.ANGULAR]: `\
	<digi-header
		[attr.af-system-name]="'Designsystem'"
		[attr.af-hide-system-name]="false"
		[attr.af-menu-button-text]="'Meny'"
		>
		<a slot="header-logo" [attr.aria-label]="'Designsystemets startsida'" href="/" [routerLink]="['/']"></a>

		<div slot="header-content">
			<digi-header-notification [attr.af-notification-amount]="8">
				<a href="/" [routerLink]="['/']">
					<digi-icon-bell-filled></digi-icon-bell-filled>
					Notiser
				</a>
			</digi-header-notification>
			<digi-header-avatar
				[attr.af-src]="/assets/images/avatar.svg"
				[attr.af-alt]="'Profilbild på Linda Karlsson'"
				[attr.af-name]="'Linda Karlsson'"
				[attr.af-signature]="'KALIA'"
				[attr.af-is-logged-in]="true"
				[attr.af-hide-signature]="true"
			></digi-header-avatar>
		</div>
		<div slot="header-navigation">
			<digi-header-navigation
				[attr.af-close-button-text]="'Stäng'"
				[attr.af-close-button-aria-label]="'Stäng meny'"
				[attr.af-nav-aria-label]="'Huvudmeny'"
		>
				<digi-header-navigation-item [attr.af-current-page]="true">
					<a href="/" [routerLink]="['/']">Mina bokningar</a>
				</digi-header-navigation-item>
				<digi-header-navigation-item>
					<a href="/" [routerLink]="['/']">Grupper</a>
				</digi-header-navigation-item>
				<digi-header-navigation-item>
					<a href="/" [routerLink]="['/']">Kontakt</a>
				</digi-header-navigation-item>
			</digi-header-navigation>
		</div>
</digi-header>
`,
      [CodeExampleLanguage.REACT]: `\
<DigiHeader
    afSystemName="Designsystem"
    afHideSystemName={false}
    afMenuButtonText="Meny"
>
    <a slot="header-logo" aria-label="Designsystemets startsida" href="/"></a>
    <div slot="header-content">
        <DigiHeaderNotification afNotificationAmount={8}>
            <a href="/">
                <DigiIconBellFilled></DigiIconBellFilled>
                Notiser
            </a>
        </DigiHeaderNotification>
        <DigiHeaderAvatar
						afSrc="/assets/images/avatar.svg"
						afAlt="Profilbild på Linda Karlsson"
            afName="Linda Karlsson"
            afSignature="KALIA"
            afIsLoggedIn={true}
            afHideSignature={true}
        ></DigiHeaderAvatar>
    </div>
    <div slot="header-navigation">
        <DigiHeaderNavigation
            afCloseButtonText="Stäng"
            afCloseButtonAriaLabel="Stäng meny"
            afNavAriaLabel="Huvudmeny"
        >
            <DigiHeaderNavigationItem afCurrentPage={true}>
                <a href="/">Mina bokningar</a>
            </DigiHeaderNavigationItem>
            <DigiHeaderNavigationItem>
                <a href="/">Grupper</a>
            </DigiHeaderNavigationItem>
            <DigiHeaderNavigationItem>
                <a href="/">Kontakt</a>
            </DigiHeaderNavigationItem>
        </DigiHeaderNavigation>
    </div>
</DigiHeader>
		 `
    };
  }

  render() {
    return (
      <div class="digi-tag-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används som bas tillsammans med
              menyvalskomponenten, avatar, och notis för att skapa sidhuvud.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.headerCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls"></div>
              <digi-header
                af-system-name="Designsystem"
                af-hide-system-name="false"
                af-menu-button-text="Meny">
                <a
                  slot="header-logo"
                  aria-label="Designsystemets startsida"
                  href="/"></a>

                <div slot="header-content">
                  <digi-header-notification af-notification-amount="8">
                    <a href="/">
                      <digi-icon-bell-filled></digi-icon-bell-filled>
                      Notiser
                    </a>
                  </digi-header-notification>
                  <digi-header-avatar
                    af-src={getAssetPath('/assets/images/avatar.svg')}
                    afAlt="Profilbild på Linda Karlsson"
                    af-name="Linda Karlsson"
                    af-signature="KALIA"
                    af-is-logged-in="true"
                    af-hide-signature="true"></digi-header-avatar>
                </div>

                <div slot="header-navigation">
                  <digi-header-navigation
                    af-close-button-text="Stäng"
                    af-close-button-aria-label="Stäng meny"
                    af-nav-aria-label="Huvudmeny">
                    <digi-header-navigation-item af-current-page="true">
                      <a href="/">Mina bokningar</a>
                    </digi-header-navigation-item>
                    <digi-header-navigation-item>
                      <a href="/">Grupper</a>
                    </digi-header-navigation-item>
                    <digi-header-navigation-item>
                      <a href="/">Kontakt</a>
                    </digi-header-navigation-item>
                  </digi-header-navigation>
                </div>
              </digi-header>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <p>
                Komponenten används av{' '}
                <digi-code af-code="digi-header-navigation"></digi-code>,
                <digi-code af-code="digi-header-navigation-item"></digi-code>,{' '}
                <digi-code af-code="digi-header-avatar"></digi-code>
                <digi-code af-code="digi-header-notification"></digi-code>.
                Attributet <digi-code af-code="af-system-name"></digi-code>{' '}
                används för att ange namn för systemet.{' '}
                <digi-code af-code="af-hide-system-name"></digi-code> används
                för att dölja namn på systemet.
              </p>
              <p>
                {' '}
                <a href="/komponenter/digi-header-navigation/oversikt">
                  <digi-code af-code="<digi-header-navigation>"></digi-code>
                </a>
                ,
                <a href="/komponenter/digi-header-navigation-item/oversikt">
                  <digi-code af-code="<digi-header-navigation-item>"></digi-code>
                </a>
                ,
                <a href="/komponenter/digi-header-notification/oversikt">
                  <digi-code af-code="<digi-header-notification>"></digi-code>
                </a>
                ,
                <a href="/komponenter/digi-header-avatar/oversikt">
                  <digi-code af-code="<digi-header-avatar>"></digi-code>
                </a>
                får enbart användas inuti komponenten{' '}
                <digi-code af-code="<digi-header>"></digi-code>
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
