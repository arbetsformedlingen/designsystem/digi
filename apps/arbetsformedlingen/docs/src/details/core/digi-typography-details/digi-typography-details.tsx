import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  TypographyVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-typography-details',
  styleUrl: 'digi-typography-details.scss'
})
export class DigiTypographyDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() typographyVariation: TypographyVariation = TypographyVariation.SMALL;

  get typographyCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-typography
	af-variation="${this.typographyVariation}"
> 
	<h1>Heading 1</h1>
	<h2>Heading 2</h2>
	<h3>Heading 3</h3>
	<h4>Heading 4</h4>
	<h5>Heading 5</h5>
	<h6>Heading 6</h6>
	<h1>Paragraph</h1>
	<p>
		Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
	</p>
	<blockquote>
		Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
	</blockquote>
</digi-typography>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-typography
	[attr.af-variation]="TypographyVariation.${Object.keys(TypographyVariation).find((key) => TypographyVariation[key] === this.typographyVariation)}"
>
	<h1>Heading 1</h1>
	<h2>Heading 2</h2>
	<h3>Heading 3</h3>
	<h4>Heading 4</h4>
	<h5>Heading 5</h5>
	<h6>Heading 6</h6>
	<h1>Paragraph</h1>
	<p>
		Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
	</p>
	<blockquote>
		Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
	</blockquote>
</digi-typography>`,
      [CodeExampleLanguage.REACT]: `\
<DigiTypography
	afVariation={TypographyVariation.${Object.keys(TypographyVariation).find((key) => TypographyVariation[key] === this.typographyVariation)}}
>
	<h1>Heading 1</h1>
	<h2>Heading 2</h2>
	<h3>Heading 3</h3>
	<h4>Heading 4</h4>
	<h5>Heading 5</h5>
	<h6>Heading 6</h6>
	<h1>Paragraph</h1>
	<p>
		Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
	</p>
	<blockquote>
		Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
	</blockquote>
</DigiTypography>`
    };
  }

  render() {
    return (
      <div class="digi-typography-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              <digi-code af-code="digi-typography"></digi-code>-taggen ska
              omsluta alla HTML-taggar som innehåller text för att formatera dem
              enligt Arbetsförmedlingens grafiska profil.
            </digi-typography-preamble>
          )}

          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.typographyCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    afName="Storlek"
                    afLegend="Storlek"
                    onChange={(e) =>
                      (this.typographyVariation = (
                        e.target as HTMLFormElement
                      ).value)
                    }>
                    <digi-form-radiobutton
                      afName="Storlek"
                      afLabel="Liten"
                      afValue={TypographyVariation.SMALL}
                      afChecked={
                        this.typographyVariation === TypographyVariation.SMALL
                      }
                    />
                    <digi-form-radiobutton
                      afName="Storlek"
                      afLabel="Stor"
                      afValue={TypographyVariation.LARGE}
                      afChecked={
                        this.typographyVariation === TypographyVariation.LARGE
                      }
                    />
                  </digi-form-fieldset>
                </div>
                <digi-typography afVariation={this.typographyVariation}>
                  <h1>Heading 1</h1>
                  <h2>Heading 2</h2>
                  <h3>Heading 3</h3>
                  <h4>Heading 4</h4>
                  <h5>Heading 5</h5>
                  <h6>Heading 6</h6>
                  <h1>Paragraph</h1>
                  <p>
                    Lorem ipsum dolor sit amet, test link adipiscing elit.
                    Nullam dignissim convallis est. Quisque aliquam. Donec
                    faucibus. Nunc iaculis suscipit dui. Nam sit amet sem.
                    Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
                    vehicula, nisl. Praesent mattis, massa quis luctus
                    fermentum, turpis mi volutpat justo, eu volutpat enim diam
                    eget metus. Maecenas ornare tortor. Donec sed tellus eget
                    sapien fringilla nonummy. Mauris a ante. Suspendisse quam
                    sem, consequat at, commodo vitae, feugiat in, nunc. Morbi
                    imperdiet augue quis tellus.
                  </p>
                  <blockquote>
                    Lorem ipsum dolor sit amet, test link adipiscing elit.
                    Nullam dignissim convallis est. Quisque aliquam. Donec
                    faucibus. Nunc iaculis suscipit dui. Nam sit amet sem.
                    Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
                    vehicula, nisl. Praesent mattis, massa quis luctus
                    fermentum, turpis mi volutpat justo, eu volutpat enim diam
                    eget metus. Maecenas ornare tortor. Donec sed tellus eget
                    sapien fringilla nonummy. Mauris a ante. Suspendisse quam
                    sem, consequat at, commodo vitae, feugiat in, nunc. Morbi
                    imperdiet augue quis tellus.
                  </blockquote>
                </digi-typography>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Storlekar</h3>
              <p>
                Komponenten finns i liten (s) och stor storlek (l) som ställs in
                med <digi-code af-code="af-variation" />. Standard är liten.
                <br />
                Typskript-användare bör importera och använda{' '}
                <digi-code af-code="TypographyVariation" />
                för att ställa in storlek.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
