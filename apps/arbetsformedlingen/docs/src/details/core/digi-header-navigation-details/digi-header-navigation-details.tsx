import { Component, h, Prop } from '@stencil/core';
import {
  CodeExampleLanguage,
  InfoCardHeadingLevel,
  InfoCardVariation
} from '@digi/arbetsformedlingen';
@Component({
  tag: 'digi-header-navigation-details',
  styleUrl: 'digi-header-navigation-details.scss'
})
export class DigiHeaderNavigation {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  get headerNavigationCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
			<digi-header-navigation
				af-close-button-text="Stäng"
				af-close-button-aria-label="Stäng meny"
				af-nav-aria-label="Huvudmeny"
			>
				<digi-header-navigation-item af-current-page="true">
					<a href="/">Mina bokningar</a>
				</digi-header-navigation-item>
				<digi-header-navigation-item>
					<a href="/">Grupper</a>
				</digi-header-navigation-item>
				<digi-header-navigation-item>
					<a href="/">Kontakt</a>
				</digi-header-navigation-item>
			</digi-header-navigation>
				`,
      [CodeExampleLanguage.ANGULAR]: `\
				<digi-header-navigation
					[attr.af-close-button-text]="'Stäng'"
					[attr.af-close-button-aria-label]="'Stäng meny'"
					[attr.af-nav-aria-label]="'Huvudmeny'"
				>
					<digi-header-navigation-item [attr.af-current-page]="true">
						<a href="/" [routerLink]="['/']">Mina bokningar</a>
					</digi-header-navigation-item>
					<digi-header-navigation-item>
						<a href="/" [routerLink]="['/']">Grupper</a>
					</digi-header-navigation-item>
					<digi-header-navigation-item>
						<a href="/" [routerLink]="['/']">Kontakt</a>
					</digi-header-navigation-item>
				</digi-header-navigation>
			</div>
				`,
      [CodeExampleLanguage.REACT]: `\
				<DigiHeaderNavigation
					afCloseButtonText="Stäng"
					afCloseButtonAriaLabel="Stäng meny"
					afNavAriaLabel="Huvudmeny"
				>
					<DigiHeaderNavigationItem afCurrentPage={true}>
						<a href="/">Mina bokningar</a>
					</DigiHeaderNavigationItem>
					<DigiHeaderNavigationItem>
						<a href="/">Grupper</a>
					</DigiHeaderNavigationItem>
					<DigiHeaderNavigationItem>
						<a href="/">Kontakt</a>
					</DigiHeaderNavigationItem>
				</DigiHeaderNavigation>
			</div>
				`
    };
  }

  render() {
    return (
      <div>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används tillsammans med{' '}
              <a href="/komponenter/digi-header-navigation-item/oversikt">
                <digi-code af-code="<digi-header-navigation-item>"></digi-code>{' '}
              </a>
              för att skapa menyer i header komponenten.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.headerNavigationCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-header-navigation
                af-close-button-text="Stäng"
                af-close-button-aria-label="Stäng meny"
                af-nav-aria-label="Huvudmeny">
                <digi-header-navigation-item
                  af-current-page="true"
                  af-backdrop="true">
                  <a href="">Mina bokningar</a>
                </digi-header-navigation-item>
                <digi-header-navigation-item>
                  <a href="">Grupper</a>
                </digi-header-navigation-item>
                <digi-header-navigation-item>
                  <a href="">Kontakt</a>
                </digi-header-navigation-item>
              </digi-header-navigation>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <p>
                Komponenten måste användas tillsammans med{' '}
                <a href="/komponenter/digi-header-navigation-item/oversikt">
                  <digi-code af-code="<digi-header-navigation-item>"></digi-code>
                </a>
                .
              </p>
              <h4>Nuvarande sida</h4>
              <p>
                Attributet <digi-code af-code="af-current-page" /> används för
                att markera ett menyval som nuvarande sida.
              </p>
              <h4>Antal av sidor</h4>
              <p>
                Max antalet för{' '}
                <digi-code af-code="digi-header-navigation-item" /> är 6 st som
                kan ha i
                <a href="/komponenter/digi-header-navigation-item/oversikt">
                  <digi-code af-code="<digi-header-navigation-item>"></digi-code>
                </a>
              </p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-block af-container="none" af-margin-bottom>
              <digi-info-card
                afHeading="Bra att veta"
                afHeadingLevel={`h3` as InfoCardHeadingLevel}
                afVariation={InfoCardVariation.PRIMARY}>
                <p>
                  Komponenten kan enbart användas inuti komponenten{' '}
                  <a href="/komponenter/digi-header-navigation/oversikt">
                    <digi-code af-code="<digi-header-navigation>"></digi-code>
                  </a>
                  .
                </p>
              </digi-info-card>
              <br />
            </digi-layout-block>
          )}
        </digi-typography>
      </div>
    );
  }
}
