import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-expandable-accordion-details',
  styleUrl: 'digi-expandable-accordion-details.scss'
})
export class DigiExpandableAccordionDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  get faqAccordionCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-expandable-accordion
	af-heading="Utfällbar rubrik"
>
	<p>Ea nulla enim enim voluptate mollit proident.</p>
</digi-expandable-accordion>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-expandable-accordion
	[attr.af-heading]="Utfällbar rubrik"
>
	<p>Ea nulla enim enim voluptate mollit proident.</p>
</digi-expandable-accordion>`,
      [CodeExampleLanguage.REACT]: `\
<DigiExpandableAccordion
	afHeading="Utfällbar rubrik"
>
	<p>Ea nulla enim enim voluptate mollit proident.</p>
</DigiExpandableAccordion>`
    };
  }

  render() {
    return (
      <div class="digi-expandable-accordion-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten kan användas för att minska vertikalt utrymme
              på en sida med stora mängder information. Den är komprimerad som
              standard och expanderas genom att klicka på rubrikytan.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.faqAccordionCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-expandable-accordion
                afHeading="Utfällbar rubrik"
                afExpanded={false}>
                <p>Ea nulla enim enim voluptate mollit proident.</p>
              </digi-expandable-accordion>
            </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container afNoGutter afMarginBottom>
              <h2>Beskrivning</h2>
              <h3>Kodexempel</h3>
              <p>
                Rubriken läggs in med{' '}
                <digi-code af-variation="light" af-code="af-heading" /> och
                rubriknivån är <digi-code af-variation="light" af-code="h2" />{' '}
                som standard. Vill du ändra rubriknivå kan du använda{' '}
                <digi-code af-variation="light" af-code="af-heading-level" /> .
                Typescript-användare bör importera och använda
                <digi-code
                  af-variation="light"
                  af-code="ExpandableFaqItemHeadingLevel"
                />
                . Stylingen på kommer se likadan ut även om du ändrar till en
                lägre rubriknivå.
              </p>
              <h3>Använding</h3>
              <p>Utfällbara rubriker ska inte användas nästlade i varandra.</p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
