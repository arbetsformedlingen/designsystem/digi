import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  TableSize,
  TableVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-table-details',
  styleUrl: 'digi-table-details.scss'
})
export class DigiTableDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() tableVariation: TableVariation = TableVariation.PRIMARY;
  @State() tableSize: TableSize = TableSize.MEDIUM;
  get tableCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-table 
	af-size="${this.tableSize}" 
	af-variation="${this.tableVariation}"
>
	<table>
	<caption>Rubrik</caption>
		<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">
					Mass (10<sup>24</sup>kg)
				</th>
				<th scope="col">Diameter (km)</th>
				<th scope="col">
					Density (kg/m<sup>3</sup>)
				</th>
				<th scope="col">
					Gravity (m/s<sup>2</sup>)
				</th>
				<th scope="col">Length of day (hours)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">Mercury</th>
				<td>0.330</td>
				<td>4,879</td>
				<td>5427</td>
				<td>3.7</td>
				<td>4222.6</td>
			</tr>
			<tr>
				...
			</tr>
			<tr>
				...
			</tr>
			<tr>
				...
			</tr>
		</tbody>
	</table>
</digi-table>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-table 
	[attr.af-size]="TableSize.${Object.keys(TableSize).find(
    (key) => TableSize[key] === this.tableSize
  )}" 
	[attr.af-variation]="TableVariation.${Object.keys(TableVariation).find(
    (key) => TableVariation[key] === this.tableVariation
  )}" 
>
	<table>
	<caption>Rubrik</caption>
		<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">
					Mass (10<sup>24</sup>kg)
				</th>
				<th scope="col">Diameter (km)</th>
				<th scope="col">
					Density (kg/m<sup>3</sup>)
				</th>
				<th scope="col">
					Gravity (m/s<sup>2</sup>)
				</th>
				<th scope="col">Length of day (hours)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">Mercury</th>
				<td>0.330</td>
				<td>4,879</td>
				<td>5427</td>
				<td>3.7</td>
				<td>4222.6</td>
			</tr>
			<tr>
				...
			</tr>
			<tr>
				...
			</tr>
			<tr>
				...
			</tr>
		</tbody>
	</table>
</digi-table>`,
      [CodeExampleLanguage.REACT]: `\
<DigiTable 
	afSize={TableSize.${Object.keys(TableSize).find(
    (key) => TableSize[key] === this.tableSize
  )}} 
	afVariation={TableVariation.${Object.keys(TableVariation).find(
    (key) => TableVariation[key] === this.tableVariation
  )}} 
>
	<table>
	<caption>Rubrik</caption>
		<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">
					Mass (10<sup>24</sup>kg)
				</th>
				<th scope="col">Diameter (km)</th>
				<th scope="col">
					Density (kg/m<sup>3</sup>)
				</th>
				<th scope="col">
					Gravity (m/s<sup>2</sup>)
				</th>
				<th scope="col">Length of day (hours)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">Mercury</th>
				<td>0.330</td>
				<td>4,879</td>
				<td>5427</td>
				<td>3.7</td>
				<td>4222.6</td>
			</tr>
			<tr>
				...
			</tr>
			<tr>
				...
			</tr>
			<tr>
				...
			</tr>
		</tbody>
	</table>
</DigiTable>`
    };
  }

  render() {
    return (
      <div class="digi-table-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              <digi-code af-code="<digi-table>"></digi-code> används för att
              omsluta en HTML-tabell så att den formateras enligt
              Arbetsförmedlingens grafiska profil.
            </digi-typography-preamble>
          )}

          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.tableCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    afName="Variation"
                    afLegend="Variation"
                    onChange={(e) =>
                      (this.tableVariation = (
                        e.target as HTMLFormElement
                      ).value)
                    }>
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Primär"
                      afValue={TableVariation.PRIMARY}
                      afChecked={this.tableVariation === TableVariation.PRIMARY}
                    />
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Sekundär"
                      afValue={TableVariation.SECONDARY}
                      afChecked={
                        this.tableVariation === TableVariation.SECONDARY
                      }
                    />
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Tertiär"
                      afValue={TableVariation.TERTIARY}
                      afChecked={
                        this.tableVariation === TableVariation.TERTIARY
                      }
                    />
                  </digi-form-fieldset>
                  <digi-form-fieldset
                    afName="Storlek"
                    afLegend="Storlek"
                    onChange={(e) =>
                      (this.tableSize = (e.target as HTMLFormElement).value)
                    }>
                    <digi-form-radiobutton
                      afName="Storlek"
                      afLabel="Liten"
                      afValue={TableSize.SMALL}
                      afChecked={this.tableSize === TableSize.SMALL}
                    />{' '}
                    <digi-form-radiobutton
                      afName="Storlek"
                      afLabel="Mellan"
                      afValue={TableSize.MEDIUM}
                      afChecked={this.tableSize === TableSize.MEDIUM}
                    />
                  </digi-form-fieldset>
                </div>
                <digi-table
                  afSize={this.tableSize}
                  afVariation={this.tableVariation}>
                  <table>
                    <caption>Rubrik</caption>
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">
                          Mass (10<sup>24</sup>kg)
                        </th>
                        <th scope="col">Diameter (km)</th>
                        <th scope="col">
                          Density (kg/m<sup>3</sup>)
                        </th>
                        <th scope="col">
                          Gravity (m/s<sup>2</sup>)
                        </th>
                        <th scope="col">Length of day (hours)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">Mercury</th>
                        <td>0.330</td>
                        <td>4,879</td>
                        <td>5427</td>
                        <td>3.7</td>
                        <td>4222.6</td>
                      </tr>
                      <tr>
                        <th scope="row">Venus</th>
                        <td>4.87</td>
                        <td>12,104</td>
                        <td>5243</td>
                        <td>8.9</td>
                        <td>2802.0</td>
                      </tr>
                      <tr>
                        <th scope="row">Earth</th>
                        <td data-af-table-cell="active">5.97</td>
                        <td>12,756</td>
                        <td>5514</td>
                        <td>9.8</td>
                        <td>24.0</td>
                      </tr>
                      <tr>
                        <th scope="row">Mars</th>
                        <td>0.642</td>
                        <td>6,792</td>
                        <td>3933</td>
                        <td>3.7</td>
                        <td>24.7</td>
                      </tr>
                    </tbody>
                  </table>
                </digi-table>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Storlekar</h3>
              <p>
                Tabellkomponenten finns i två storlekar: liten och mellan och
                ställs in med <digi-code af-code="af-size"></digi-code>. Mellan
                är standardstorleken.
              </p>
              <h3>Varianter</h3>
              <p>
                Tabellkomponenten finns i tre olika varianter: primär, sekundär
                och tertiär och ställs in med{' '}
                <digi-code af-code="af-variation"></digi-code>. Primärvarianten
                är standard.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
