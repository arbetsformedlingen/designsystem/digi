import { Component, h, State, Prop } from '@stencil/core';
import {
  CodeExampleLanguage,
  ProgressListItemVariation,
  ProgressListItemHeadingLevel,
  ProgressListItemType,
  ProgressListItemStatus
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-progress-list-item-details',
  styleUrl: 'digi-progress-list-item-details.scss'
})
export class DigiProgressListItem {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() progressListItemVariation: ProgressListItemVariation =
    ProgressListItemVariation.PRIMARY;
  @State() progressListItemHeadingLevel: ProgressListItemHeadingLevel =
    ProgressListItemHeadingLevel.H2;
  @State() progressListItemType: ProgressListItemType =
    ProgressListItemType.CIRCLE;
  @State() progressListItemStatus: ProgressListItemStatus =
    ProgressListItemStatus.CURRENT;
  @State() expandable = false;
  @State() expanded = true;

  get progressListItemCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-progress-list-item
	af-heading="Rubrik"
	af-step-status="${this.progressListItemStatus}"
	af-variation="${this.progressListItemVariation}"
	af-heading-level="${this.progressListItemHeadingLevel}"
	af-type="${this.progressListItemType}"
	af-expandable="${this.expandable}"
	af-expanded="${this.expanded}"
>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-list-item>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-progress-list-item 
	[attr.af-heading]="'Rubrik'"
	[attr.af-step-status]="ProgressListItemStatus.${Object.keys(ProgressListItemStatus).find((key) => ProgressListItemStatus[key] === this.progressListItemStatus)}"
	[attr.af-variation]="ProgressListItemVariation.${Object.keys(ProgressListItemVariation).find((key) => ProgressListItemVariation[key] === this.progressListItemVariation)}"
	[attr.af-heading-level]="ProgressListItemHeadingLevel.${Object.keys(ProgressListItemHeadingLevel).find((key) => ProgressListItemHeadingLevel[key] === this.progressListItemHeadingLevel)}"
	[attr.af-type]="ProgressListItemType.${Object.keys(ProgressListItemType).find((key) => ProgressListItemType[key] === this.progressListItemType)}"
	[attr.af-expandable]="${this.expandable}"
	[attr.af-expanded]="${this.expanded}"
>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</digi-progress-list-item>`,
      [CodeExampleLanguage.REACT]: `\
<DigiProgressListItem 
	afHeading="Rubrik"
	afStepStatus={ProgressListItemStatus.${Object.keys(ProgressListItemStatus).find((key) => ProgressListItemStatus[key] === this.progressListItemStatus)}}
	afVariation={ProgressListItemVariation.${Object.keys(ProgressListItemVariation).find((key) => ProgressListItemVariation[key] === this.progressListItemVariation)}}
	afHeadingLevel={ProgressListItemHeadingLevel.${Object.keys(ProgressListItemHeadingLevel).find((key) => ProgressListItemHeadingLevel[key] === this.progressListItemHeadingLevel)}}
	afType={ProgressListItemType.${Object.keys(ProgressListItemType).find((key) => ProgressListItemType[key] === this.progressListItemType)}}
	afExpandable={${this.expandable}}
	afExpanded={${this.expanded}}
>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit.
	Etiam magna neque, interdum vel massa eget, condimentum
	rutrum velit. Sed vitae ullamcorper sem.
</DigiProgressListItem>`
    };
  }

  render() {
    return (
      <div class="digi-progress-list-item-details">
        {!this.afShowOnlyExample && (
          <digi-typography-preamble>
            Den här komponenten kan användas enskilt eller tillsammans med{' '}
            <digi-code af-code="<digi-progress-list>" /> för att visualisera ett
            användarflöde i vertikal riktning.
          </digi-typography-preamble>
        )}

        <digi-layout-container af-no-gutter af-margin-bottom>
          <article>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
              af-code={JSON.stringify(this.progressListItemCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <digi-progress-list-item
                afHeading="Rubrik"
                af-heading-level={this.progressListItemHeadingLevel}
                af-step-status={this.progressListItemStatus}
                af-variation={this.progressListItemVariation}
                af-type={this.progressListItemType}
                af-expandable={this.expandable}
                af-expanded={this.expanded}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                magna neque, interdum vel massa eget, condimentum rutrum velit.
                Sed vitae ullamcorper sem.
              </digi-progress-list-item>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.progressListItemVariation = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Primär"
                    afValue={ProgressListItemVariation.PRIMARY}
                    afChecked={
                      this.progressListItemVariation ===
                      ProgressListItemVariation.PRIMARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Sekundär"
                    afValue={ProgressListItemVariation.SECONDARY}
                    afChecked={
                      this.progressListItemVariation ===
                      ProgressListItemVariation.SECONDARY
                    }
                  />
                  <digi-form-radiobutton
                    afName="Variation"
                    afLabel="Tertiär"
                    afValue={ProgressListItemVariation.TERTIARY}
                    afChecked={
                      this.progressListItemVariation ===
                      ProgressListItemVariation.TERTIARY
                    }
                  />
                </digi-form-fieldset>

                <digi-form-fieldset
                  afName="Typ"
                  afLegend="Typ"
                  onChange={(e) =>
                    (this.progressListItemType = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Typ"
                    afLabel="Cirkel"
                    afValue={ProgressListItemType.CIRCLE}
                    afChecked={
                      this.progressListItemType === ProgressListItemType.CIRCLE
                    }
                  />
                  <digi-form-radiobutton
                    afName="Typ"
                    afLabel="Ikon"
                    afValue={ProgressListItemType.ICON}
                    afChecked={
                      this.progressListItemType === ProgressListItemType.ICON
                    }
                  />
                </digi-form-fieldset>

                <digi-form-fieldset
                  afName="Status"
                  afLegend="Status"
                  onChange={(e) =>
                    (this.progressListItemStatus = (
                      e.target as HTMLFormElement
                    ).value)
                  }>
                  <digi-form-radiobutton
                    afName="Status"
                    afLabel="Klar"
                    afValue={ProgressListItemStatus.DONE}
                    afChecked={
                      this.progressListItemStatus ===
                      ProgressListItemStatus.DONE
                    }
                  />
                  <digi-form-radiobutton
                    afName="Status"
                    afLabel="Nuvarande"
                    afValue={ProgressListItemStatus.CURRENT}
                    afChecked={
                      this.progressListItemStatus ===
                      ProgressListItemStatus.CURRENT
                    }
                  />
                  <digi-form-radiobutton
                    afName="Status"
                    afLabel="Kommande"
                    afValue={ProgressListItemStatus.UPCOMING}
                    afChecked={
                      this.progressListItemStatus ===
                      ProgressListItemStatus.UPCOMING
                    }
                  />
                </digi-form-fieldset>
                <digi-form-fieldset afName="Expanderbar" afLegend="Expanderbar">
                  <digi-form-checkbox
                    afLabel="Ja"
                    afChecked={this.expandable}
                    onAfOnChange={() =>
                      this.expandable
                        ? (this.expandable = false)
                        : (this.expandable = true)
                    }></digi-form-checkbox>
                </digi-form-fieldset>
                <digi-form-select
                  afLabel="Rubriknivå"
                  onAfOnChange={(e) =>
                    (this.progressListItemHeadingLevel = (
                      e.target as HTMLDigiFormSelectElement
                    ).value as ProgressListItemHeadingLevel)
                  }
                  af-variation="small"
                  af-start-selected={1}>
                  <option value={ProgressListItemHeadingLevel.H1}>
                    {ProgressListItemHeadingLevel.H1}
                  </option>
                  <option value={ProgressListItemHeadingLevel.H2}>
                    {ProgressListItemHeadingLevel.H2}
                  </option>
                  <option value={ProgressListItemHeadingLevel.H3}>
                    {ProgressListItemHeadingLevel.H3}
                  </option>
                  <option value={ProgressListItemHeadingLevel.H4}>
                    {ProgressListItemHeadingLevel.H4}
                  </option>
                  <option value={ProgressListItemHeadingLevel.H5}>
                    {ProgressListItemHeadingLevel.H5}
                  </option>
                  <option value={ProgressListItemHeadingLevel.H6}>
                    {ProgressListItemHeadingLevel.H6}
                  </option>
                </digi-form-select>
              </div>
            </digi-code-example>
          </article>
        </digi-layout-container>
        {!this.afShowOnlyExample && (
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              <h2>Beskrivning</h2>
              <p>
                Rubriknivån för alla förloppsteg kan ändras med &nbsp;
                <digi-code af-code="af-heading-level" />, standard är H2.
                Typescript-användare bör importera och använda &nbsp;
                <digi-code af-code="ProgressListHeadingLevel" />. Stylingen på
                kommer se likadan ut även om du ändrar till en annan rubriknivå.
              </p>

              <h3>Variationer</h3>
              <p>
                Komponenten finns i tre varianter, en primär (primary) i grönt
                som är standard; en sekundär (secondary) i blått och en tertiär
                (tertiary) som är i grått. Variation sätts med hjälp av &nbsp;
                <digi-code af-code="af-variation" />.
              </p>
              <h3>Status</h3>
              <p>
                Komponenten kan visa olika statusar (Klar, nuvarande och
                kommande) beroende på var i flödet användaren är. Statusen sätts
                med hjälp av &nbsp;
                <digi-code af-code="af-step-status" />.
              </p>
              <h3>Typ</h3>
              <p>
                Indikatorn kan vara en cirkel (circle) eller ikon (icon).
                Standard är cirkel och sätts med hjälp av &nbsp;
                <digi-code af-code="af-type" />.
              </p>
              <h3>Expanderbar</h3>
              <p>
                Komponenten kan väljas att vara expanderbar och sätts i så fall
                med attributet &nbsp;
                <digi-code af-code="af-expandable" />. För att sätta den som
                utfälld direkt så används attributet &nbsp;
                <digi-code af-code="af-expanded" />. Det går även att välja egna
                ikoner till knappen som expanderar och kollapsar varje item.
                Detta görs genom &nbsp;
                <digi-code af-code="af-expand-icon" />
                &nbsp; och &nbsp;
                <digi-code af-code="af-collapse-icon" />. Exempel för att sätta
                ett plustecken: &nbsp;
                <digi-code af-code='af-expand-icon="plus"' />.
              </p>
              <h3>Tillgänglighet</h3>
              <p>
                Om komponenten används enskilt så bör den omslutas av ett
                list-element (digi-list, ol, ul), eller ett element med
                attributet &nbsp;
                <digi-code af-code='role="list"' />.
              </p>
              <h4>Hur status förmedlas till hjälpmedel </h4>
              <p>
                Status på Förloppslistans steg förmedlas efter rubriken "Rubrik,
                status", exempelvis "Beslut fattas, status kommande".
              </p>
              <p>Beroende på stegets status så sätts default till;</p>
              <digi-list>
                <li>status klar</li>
                <li>status aktuell</li>
                <li>status kommande</li>
              </digi-list>
              <p>
                Vill du själv beskriva stegets status kan du skriva över
                default-texten genom propertyn{' '}
                <digi-code af-code="afStepStatusAriaLabel" />
              </p>
            </article>
          </digi-layout-container>
        )}
      </div>
    );
  }
}
