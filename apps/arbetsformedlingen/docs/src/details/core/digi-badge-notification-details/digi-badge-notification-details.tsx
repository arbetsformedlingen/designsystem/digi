import { Component, h, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  BadgeNotificationShape
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-badge-notification-details',
  styleUrl: 'digi-badge-notification-details.scss'
})
export class DigiBadgeNotification {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() showExample = true;
  @State() BadgeNotificationShape: BadgeNotificationShape =
    BadgeNotificationShape.CIRCLE;

  @State() value = '9';

  get badgeNotificationCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-badge-notification 
	af-value="${this.value}"
>
</digi-badge-notification>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-badge-notification
	[attr.af-value]="${this.value}"
>
</digi-badge-notification>`,
      [CodeExampleLanguage.REACT]: `\
<DigiBadgeNotification
	afValue="${this.value}"
>
</DigiBadgeNotification>`
    };
  }

  render() {
    return (
      <div class="digi-badge-notification-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Används för att visa information om nya uppdateringar och
              aviseringar. Den används tillsammans med andra komponenter och kan
              inte användas ensam. Notifikationsindikator kan finnas med ord
              eller med en siffra. Den används tillsammans med andra
              komponenter, såsom länkknappar, för att indikera nya aviseringar.
            </digi-typography-preamble>
          )}
        </digi-typography>
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={JSON.stringify(this.badgeNotificationCode)}
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}>
            <div
              style={{
                width: '300px'
              }}>
              <digi-badge-notification
                af-value={this.value}></digi-badge-notification>
            </div>
            <div class="slot__controls" slot="controls">
              <digi-form-fieldset
                afName="Variant"
                afLegend="Variant"
                onChange={(e) =>
                  (this.value = (e.target as HTMLFormElement).value)
                }>
                <digi-form-radiobutton
                  afName="Variant"
                  afLabel="Ensiffrigt antal "
                  afValue="9"
                  afChecked={true}
                />
                <digi-form-radiobutton
                  afName="Variant"
                  afLabel="Tvåsiffrigt antal"
                  afValue="10"
                />

                <digi-form-radiobutton
                  afName="Variant"
                  afLabel="Tresiffrigt antal"
                  afValue="199"
                />
                <digi-form-radiobutton
                  afName="Variant"
                  afLabel="Ord"
                  afValue="Ny sida"
                />
              </digi-form-fieldset>
            </div>
          </digi-code-example>
        </digi-layout-container>
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>
                Indikatorn kan vara cirkulär eller pillerformad. För fler
                siffror eller ord blir notifikationsindikatorn pillerformad.
                Notifikationsindikatorn visas i cirkulär form endast för siffror
                som är mindre än 99.
              </p>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
