import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-detect-click-outside-details',
  styleUrl: 'digi-util-detect-click-outside-details.scss'
})
export class DigiUtilDetectClickOutSideDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-detect-click-outside-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              <digi-code af-code={`<digi-util-detect-click-outside>`} />
              övervakar klickhändelser och skapar events när klicket är inuti
              eller utanför. Kan till exempel användas för att stänga en meny
              när man klickar utanför den.
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
