import { Component, h, Prop, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  LoaderSpinnerSize
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-loader-spinner-details',
  styleUrl: 'digi-loader-spinner-details.scss'
})
export class DigiLoaderSpinner {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() spinnerExampleData: { [key: string]: unknown } = {
    size: LoaderSpinnerSize.MEDIUM
  };

  get spinnerExampleCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-loader-spinner 
	af-size="${this.spinnerExampleData.size}"
	${this.spinnerExampleData.size !== 'small' ? 'af-text="Laddar"' : ''}>
</digi-loader-spinner>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-loader-spinner 
	[attr.af-size]="LoaderSpinnerSize.${Object.keys(LoaderSpinnerSize).find((key) => LoaderSpinnerSize[key] === this.spinnerExampleData.size)}"
	${this.spinnerExampleData.size !== 'small' ? '[attr.af-text]="Laddar"' : ''}>
</digi-loader-spinner>`,
      [CodeExampleLanguage.REACT]: `\
<DigiLoaderSpinner 
	afSize={LoaderSpinnerSize.${Object.keys(LoaderSpinnerSize).find((key) => LoaderSpinnerSize[key] === this.spinnerExampleData.size)}}
	${this.spinnerExampleData.size !== 'small' ? 'afText="Laddar"' : ''}>
</DigiLoaderSpinner>`
    };
  }

  @State() showExample = true;

  changeLoaderSpinnerExampleData(newData) {
    this.showExample = false;
    setTimeout(() => (this.showExample = true), 0);
    this.spinnerExampleData = newData;
  }

  changeSizeHandler(e) {
    this.changeLoaderSpinnerExampleData({
      ...this.spinnerExampleData,
      size: e.target.value
    });
  }

  render() {
    return (
      <div class="digi-loader-spinner-details">
        {!this.afShowOnlyExample && (
          <digi-typography-preamble>
            Används för att upplysa användaren om att vi väntar på till exempel
            ett svar från systemet.
          </digi-typography-preamble>
        )}
        <digi-layout-container af-no-gutter af-margin-bottom>
          {!this.afShowOnlyExample && <h2>Exempel</h2>}
          <digi-code-example
            af-code={JSON.stringify(this.spinnerExampleCode)}
            af-controls-position="end"
            af-hide-controls={this.afHideControls ? 'true' : 'false'}
            af-hide-code={this.afHideCode ? 'true' : 'false'}>
            <div class="slot__controls" slot="controls">
              <digi-form-fieldset
                af-legend="Storlekar"
                af-name="Storlekar"
                onChange={(e) => this.changeSizeHandler(e)}>
                <digi-form-radiobutton
                  af-name="Storlekar"
                  afLabel="Small"
                  afValue={LoaderSpinnerSize.SMALL}
                />
                <digi-form-radiobutton
                  af-name="Storlekar"
                  afLabel="Medium"
                  afChecked={true}
                  afValue={LoaderSpinnerSize.MEDIUM}
                />
                <digi-form-radiobutton
                  af-name="Storlekar"
                  afLabel="Large"
                  afValue={LoaderSpinnerSize.LARGE}
                />
              </digi-form-fieldset>
            </div>
            {this.showExample && (
              <digi-loader-spinner
                af-size={this.spinnerExampleData.size}
                af-text={
                  this.spinnerExampleData.size !== 'small' ? 'Laddar' : ''
                }></digi-loader-spinner>
            )}
          </digi-code-example>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>

              <h3>Storlekar</h3>
              <p>
                Spinnern finns i tre storlekar, small, medium och large, som du
                kan välja med{' '}
                <digi-code af-variation="light" af-code="af-size" />.
              </p>
              <p>Den minsta storleken är till för ikoner</p>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h3>Användning</h3>
              <digi-list>
                <li>
                  Ge komponenten ett id genom att använda{' '}
                  <digi-code af-variation="light" af-code=" af-id" />, annars
                  kommer ett autogeneras.
                </li>
                <li>
                  Ange texten med attributet{' '}
                  <digi-code af-variation="light" af-code=" af-text" />{' '}
                </li>
              </digi-list>
            </digi-layout-container>
          )}
        </digi-layout-container>
      </div>
    );
  }
}
