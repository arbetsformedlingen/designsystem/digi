import { Component, h, Host, Prop, State } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-calendar-datepicker-details',
  styleUrl: 'digi-calendar-datepicker-details.scss'
})
export class DigiCalendarDatepicker {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @Prop() afMinDate: Date;
  @Prop() afMaxDate: Date;
  @Prop() selectedDates: Array<Date> = [];
  @Prop() value: Array<Date> = [];

  @State() showWeek = false;
  @State() multipleDates = false;
  @State() minAndMax = false;
  @State() hasDescription = false;

  @State() afLabelDescription  = "Exempel: 1992-06-26";

  setMinAndMax() {
	this.minAndMax = !this.minAndMax;

	if (this.minAndMax) {
		this.afMinDate = new Date();
	  this.afMaxDate = new Date(new Date().setDate(new Date().getDate() + 14));
	  this.selectedDates = [];
	  this.value = [];
	}
	else {
	  this.afMinDate = null;
	  this.afMaxDate = null;
	}
  } 


	get calendarDatepickerCode() {
	  return {
	    [CodeExampleLanguage.HTML]: `\
<digi-calendar-datepicker\
${this.multipleDates ? '\n\taf-multiple-dates="true"' : ''}\
${this.showWeek ? '\n\taf-show-week-number="true"' : ''}\
${this.hasDescription ? `\n\taf-label-description="${this.afLabelDescription}"` : ''}\
${this.multipleDates || this.showWeek || this.hasDescription ? '\n>' : '>\n'}\
</digi-calendar-datepicker>
${this.minAndMax ? `
<script>
const datepicker = document.querySelector('digi-calendar-datepicker');
datepicker.afMinDate = new Date();
datepicker.afMaxDate = new Date(new Date().setDate(new Date().getDate() + 14));
</script>` : ''}`,
	    [CodeExampleLanguage.ANGULAR]: `\
${this.minAndMax ? `
<!--component.ts-->
minDate: Date = new Date();

maxDate: Date = (() => {
    const date = new Date();
    date.setDate(date.getDate() + 14);
    return date;
})();

<!--template.html-->` : ''}\
\n<digi-calendar-datepicker\
${this.multipleDates ? '\n\t[attr.af-multiple-dates]="true"' : ''}\
${this.showWeek ? '\n\t[attr.af-show-week-number]="true"' : ''}\
${this.hasDescription ? `\n\t[attr.af-label-description]="${this.afLabelDescription}"` : ''}\
${this.minAndMax ? `
	[afMinDate]="minDate"
	[afMaxDate]="maxDate"` : ''}\
${this.multipleDates || this.showWeek || this.hasDescription ? '\n>' : '>\n'}\
</digi-calendar-datepicker>
`,
	    [CodeExampleLanguage.REACT]: `\
<DigiCalendarDatepicker\
${this.multipleDates ? '\n\tafMultipleDates={true}' : ''}\
${this.showWeek ? '\n\tafShowWeekNumber={true}' : ''}\
${this.minAndMax ? `
	afMinDate={new Date()}'
	afMaxDate={(() => {
    const date = new Date();
    date.setDate(date.getDate() + 14);
    return date;
})()}'` : ''}\
${this.hasDescription ? `\n\tafLabelDescription="${this.afLabelDescription}"` : ''}\
${this.multipleDates || this.showWeek || this.hasDescription ? '\n' : '\ '}\
/>`,
		};
	}

	render() {
		return (
      <Host>
        <div class="digi-calendar-datepicker-details">
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Datumväljaren används för att visuellt representera ett datum och
              ge konsekventa medel för att navigera dess delar genom vyer i
              dagar, månader och år.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.calendarDatepickerCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}
              >
                <digi-calendar-datepicker
                  afMultipleDates={this.multipleDates}
                  afShowWeekNumber={this.showWeek}
				  afMaxDate={this.afMaxDate}
				  afMinDate={this.afMinDate}
				  value={this.value}
				  afSelectedDates={this.selectedDates}
                  afLabelDescription={
                    this.hasDescription ? `${this.afLabelDescription}` : ""
                  }
                />
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset afLegend="Visa veckonummer">
                    <digi-form-checkbox
                      afLabel="Ja"
                      afChecked={this.showWeek}
                      onAfOnChange={() =>
                        this.showWeek
                          ? (this.showWeek = false)
                          : (this.showWeek = true)
                      }
                    ></digi-form-checkbox>
                  </digi-form-fieldset>
                  <digi-form-fieldset afLegend="Välj multipla dagar">
                    <digi-form-checkbox
                      afLabel="Ja"
                      afChecked={this.multipleDates}
                      onAfOnChange={() =>
                        this.multipleDates
                          ? (this.multipleDates = false)
                          : (this.multipleDates = true)
                      }
                    ></digi-form-checkbox>
                  </digi-form-fieldset>
				  <digi-form-fieldset afLegend="Begränsa valbara dagar">
	                  <digi-form-checkbox
						afLabel="Ja (exempel med 14 dagar framöver)"
	                    afChecked={this.minAndMax}
	                    onAfOnChange={() =>
	                      this.setMinAndMax()
	                    }
	                  ></digi-form-checkbox>
	                </digi-form-fieldset>
                  <digi-form-fieldset afLegend="Valfri beskrivande text">
                  <digi-form-checkbox
                      afLabel="Ja"
                      afChecked={this.hasDescription}
                      onAfOnChange={() =>
                        this.hasDescription
                          ? (this.hasDescription = false)
                          : (this.hasDescription = true)
                      }
                    ></digi-form-checkbox>
					<br/>
				{this.hasDescription ? <digi-form-input
                      afLabel="Text"
                      afValue={this.afLabelDescription}
                      onAfOnKeyup={(event) =>
                        (this.afLabelDescription = event.target.value as string)
                      }
                      onAfOnChange={(event) =>
                        (this.afLabelDescription = event.target.value as string)
                      }
                    ></digi-form-input> : ""
                  }

                  </digi-form-fieldset>
                </div>
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Generellt</h3>
              <p>
                Datumväljaren är tillgänglighetsanpassad och går att navigera 
                runt i med knapparna{' '} 
                <digi-code af-code="upp" />, <digi-code af-code="ned" />,{' '}
                <digi-code af-code="höger" />, <digi-code af-code="vänster" />,{' '} 
                <digi-code af-code="tab" />, <digi-code af-code="shift-tab" />,{' '} 
                <digi-code af-code="space" /> och <digi-code af-code="enter" />.
              </p>
              <h3>Beskrivande text</h3>
              <p>
                Använd beskrivande text för att förtydliga eller förmedla
                exempel på det efterfrågade formatet. Ett exempel på en
                beskrivande text skulle kunna vara: "Exempel: 1992-06-26". Tänk
                på att beskrivande text i en etikett är valfritt att använda.
                Mer information om hur du ska tänka när du skriver etiketter kan
                du hitta i vårt designmönster för formulär:
              </p>
              <digi-link-internal afHref="/designmonster/formular" af-variation="small">
                Designmönster för formulär 
              </digi-link-internal>
            </digi-layout-container>
          )}
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
            <h3>Användning</h3>
                  <digi-list>
                <li>
                  Det går att sätta förvalda datum om du skickar in en lista av
                  datum in i{' '}
                  <digi-code af-code="af-selected-dates" />. Om bara ett datum
                  ska vara förvalt skicka in en listamed bara ett värde.
                </li>
                <li>
                  Du får ut en lista av datum när datumet uppdateras i
                  komponenten genom eventet{' '}
                  <digi-code af-code="af-on-date-change" />. Även om man bara
                  kan välja ett värde får du ut en lista med datumet som är
                  markerat.
                </li>
                <li>
                  Det går att markera och få ut mer än ett datum i kalender om{' '}
                  du anger <digi-code af-code="af-multiple-dates='true'" />.
                </li>
                <li>
                  Det går av välja om du vill visa veckonummer i kalendern med{' '}
                  <digi-code af-code="af-show-week-number='true'" />. Veckorna
                  går ej att välja och är bara till för att hjälpa visuellt.
                </li>
                <li>
                  Det går av sätta tidigaste och senaste valbara datum i
                  komponenten genom att skicka in datum i{' '}
                  <digi-code af-code="af-min-date" /> och{' '}
                  <digi-code af-code="af-max-date" />. Se till att det tidigaste
                  valbara datumet är innan det senaste, annars kan kan inte
                  välja några datum!
                </li>
                <li>
                  Det går att ändra texterna i komponenten genom dessa attribut:
                  <ul>
                    <li>
                      <digi-code af-code="af-label" /> sätter den första texten
                      ovanför inmatningsfältet.
                    </li>
                    <li>
                      <digi-code af-code="af-label-description" /> sätter den
                      andra texten ovanför inmatningsfältet.
                    </li>
                    <li>
                      <digi-code af-code="af-validation-wrong-format" /> sätter
                      texten på felmeddelandet ifall man skriver in ett format i
                      inmatningsfältet som inte går att tyda till ett datum.
                    </li>
                    <li>
                      <digi-code af-code="af-validation-disabled-date" /> sätter
                      texten på felmeddelandet ifall man skriver in ett datum i
                      inmatningsfältet som inte är ett valbart datum.
                    </li>
                  </ul>
                </li>
                <li>
                  Dessa är de olika formaten på datum som inmatningsfältet kan
                  förstå: <digi-code af-code="2023-10-03" />,
                  <digi-code af-code="2023/10/03" />,{' '}
                  <digi-code af-code="20231003" />,{' '}
                  <digi-code af-code="231003" />.
                </li>
                <li>
                  För att skriva in fler datum än ett kan användaren separera
                  datumen med <digi-code af-code="," />,
                  <digi-code af-code="&" /> eller <digi-code af-code="och" />.
                </li>
              </digi-list>
            </digi-layout-container>
          )}
        </div>
      </Host>
    );
	}
}
