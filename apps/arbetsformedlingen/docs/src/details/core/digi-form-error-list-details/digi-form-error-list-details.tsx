import { Component, Prop, h, State } from '@stencil/core';
import {
  CodeExampleLanguage,
  FormCheckboxVariation,
  FormErrorListHeadingLevel
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-form-error-list-details',
  styleUrl: 'digi-form-error-list-details.scss'
})
export class DigiFormErrorListDetails {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;
  @State() formErrorListHeadingLevel: FormErrorListHeadingLevel =
    FormErrorListHeadingLevel.H2;
  @State() description = false;

  get errorListCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-form-error-list
	af-heading="Kontrollera uppgifterna"\
	${this.description ? `\n  af-description="Beskrivande text"` : ''}>
	<a href="#input_1_id">Felmeddelandelänk 1</a>
	<a href="#input_2_id">Felmeddelandelänk 2</a>
</digi-form-error-list>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-form-error-list
	[attr.af-heading]="'Kontrollera uppgifterna'"\
	${this.description ? `\n  [attr.af-description]="'Beskrivande text'"` : ''}>
	<a href="#input_1_id">Felmeddelandelänk 1</a>
	<a href="#input_2_id">Felmeddelandelänk 2</a>
</digi-form-error-list>`,
      [CodeExampleLanguage.REACT]: `\
<DigiFormErrorList
	afHeading="Kontrollera uppgifterna"\
	${this.description ? `\n  afDescription="Beskrivande text"` : ''}>
	<a href="#input_1_id">Felmeddelandelänk 1</a>
	<a href="#input_2_id">Felmeddelandelänk 2</a>
</DigiFormErrorList>`
    };
  }

  render() {
    return (
      <div class="digi-form-error-list-details">
        <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Felmeddelandelistan används då inskickade formulär felvaliderar
              och visas högst upp i formuläret. Här samlar vi
              valideringsmeddelanden från alla de fält som är felaktiga, med
              länkar till respektive fält.
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
              {!this.afShowOnlyExample && <h2>Exempel</h2>}
              <digi-code-example
                af-code={JSON.stringify(this.errorListCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset afLegend="Beskrivande text">
                    <digi-form-checkbox
                      afLabel="Ja"
                      afVariation={FormCheckboxVariation.PRIMARY}
                      afChecked={this.description}
                      onAfOnChange={() =>
                        this.description
                          ? (this.description = false)
                          : (this.description = true)
                      }></digi-form-checkbox>
                  </digi-form-fieldset>
                </div>
                {!this.description && (
                  <digi-form-error-list
                    afEnableHeadingFocus={false}
                    afHeading="Kontrollera uppgifterna"
                    afHeadingLevel={this.formErrorListHeadingLevel}>
                    <a href="#input_1_id">Felmeddelandelänk 1</a>
                    <a href="#input_2_id">Felmeddelandelänk 2</a>
                  </digi-form-error-list>
                )}
                {this.description && (
                  <digi-form-error-list
                    afHeading="Kontrollera uppgifterna"
                    afHeadingLevel={this.formErrorListHeadingLevel}
                    af-description="Beskrivande text">
                    <a href="#input_1_id">Felmeddelandelänk 1</a>
                    <a href="#input_2_id">Felmeddelandelänk 2</a>
                  </digi-form-error-list>
                )}
              </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Sätt fokus på rubriken</h2>
              <p>
                Det är viktigt att flytta fokus till meddelandets rubrik. Detta
                görs enklast med metoden{' '}
                <digi-code af-code="afMSetHeadingFocus"></digi-code>
              </p>
              <digi-code-block
                af-code={`<!-- Exempel i standard Javascript -->
<digi-form-error-list
	class="my-list"
	af-heading="Sätt fokus på rubriken"
>
	<a href="#input_1_id">Obligatoriskt fält</a>
	<a href="#input_2_id">Felaktigt format</a>
</digi-form-error-list>
<script>
	const myList = document.querySelector('.my-list');
	await myList.afMSetHeadingFocus(); // Kom ihåg att alla metoder är asynkrona
</script>`}></digi-code-block>
              <h2>Stoppa ankarlänkarna</h2>
              <p>
                I vissa fall kan man behöva sätta fokus programmatiskt, till
                exempel om ankarlänkarna krockar med klientbaserad routing (i
                till exempel Angular eller liknande ramverk). Med attributet
                <digi-code af-code="af-override-link"></digi-code> kan man
                stoppa länken för att sedan sätta fokus manuellt.
              </p>
              <digi-code-block
                af-code={`<!-- Exempel i standard Javascript -->
<digi-form-error-list
	class="my-list"
	af-heading="Stoppa ankarlänkarna"
	af-override-link
>
	<a href="#input_1_id">Obligatoriskt fält</a>
	<a href="#input_2_id">Felaktigt format</a>
</digi-form-error-list>
<script>
	const myList = document.querySelector('.my-list');
	myList.addEventListener('afOnClickLink', methodForSettingYourOwnFocus);
</script>

<!-- Exempel i Angular -->
<digi-form-error-list
	af-heading="Stoppa ankarlänkarna"
	[attr.af-override-link]="true"
	(afOnClickLink)="methodForSettingYourOwnFocus($event)"
>
	<a href="#input_1_id">Obligatoriskt fält</a>
	<a href="#input_2_id">Felaktigt format</a>
</digi-form-error-list>

<!-- Exempel i React -->
<DigiFormErrorList
	af-heading="Stoppa ankarlänkarna"
	af-override-link={true}
	afOnClickLink={methodForSettingYourOwnFocus($event)}
>
	<a href="#input_1_id">Obligatoriskt fält</a>
	<a href="#input_2_id">Felaktigt format</a>
</DigiFormErrorList>`}></digi-code-block>
            </digi-layout-container>
          )}
        </digi-typography>
      </div>
    );
  }
}
