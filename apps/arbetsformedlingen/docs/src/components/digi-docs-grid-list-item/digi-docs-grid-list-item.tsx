import { Component, h, Host, Prop } from '@stencil/core';

@Component({
  tag: 'digi-docs-grid-list-item',
  styleUrl: 'digi-docs-grid-list-item.scss',
  scoped: true
})
export class GridListItem {
  @Prop() cardVariant: 'variant1' | 'variant2';

  render() {
    return (
      <Host
        class={{ 'digi-docs-grid-list-item': true, [this.cardVariant]: true }}>
        <div class="digi-docs-grid-list-item__image">
          <slot name="image"></slot>
        </div>
        <div class="digi-docs-grid-list-item__content">
          <slot name="content"></slot>
        </div>
        <div class="digi-docs-grid-list-item__footer">
          <slot name="footer"></slot>
        </div>
      </Host>
    );
  }
}
