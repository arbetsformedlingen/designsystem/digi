import { Component, h, Listen, Prop, State, Watch } from '@stencil/core';
import 'arbetsformedlingen-dist';

import state from '../../store/store';

import { AppData } from '../../data/appData';
import { Route, match, href } from 'stencil-router-v2';
import { InternalRouterState } from 'stencil-router-v2/dist/types';
import { router } from '../../global/router';

import { setDocumentTitle } from '../../global/documentTitle';
import { setAnalytics } from '../../services/analytics.service';
import {
  LayoutColumnsVariation,
  LinkVariation,
  UtilBreakpointObserverBreakpoints
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs',
  styleUrl: 'digi-docs.scss'
})
export class DigiDocs {
  Router = router;

  private _newComponent;
  private _appVar = 0;

  @State() connected: boolean;
  @State() navOpen: boolean;
  @State() isLoading = true;
  @State() updateAvailable = false;
  @State() swRegistration: any;
  @State() responsiveTwoColumns: LayoutColumnsVariation =
    LayoutColumnsVariation.TWO;
  @State() responsiveThreeColumns: LayoutColumnsVariation =
    LayoutColumnsVariation.THREE;
  @State() responsive321Columns: LayoutColumnsVariation =
    LayoutColumnsVariation.THREE;
  @State() responsiveFourColumns: LayoutColumnsVariation =
    LayoutColumnsVariation.FOUR;

  @Prop() afRoot = '/';

  @Listen('afOnToggle')
  toggleHandler(e: any) {
    if (e.target.matches('.digi-docs__sidebar-toggle')) {
      state.activeNav = !state.activeNav;
      this.navOpen = state.activeNav;
    }
  }

  componentWillLoad() {
    state.routerRoot = this.afRoot;
    state.router.activePath = this.Router.activePath;
    state.hideHeader = state.hideFooter = state.hideNavigation = false;

    this.setNewRouterPaths();

    this.Router.onChange(
      'activePath',
      (_activePath: InternalRouterState['activePath']) => {
        this.setOldRouterPaths();

        state.hideHeader = state.hideFooter = state.hideNavigation = false;
        state.router.activePath = _activePath;
        this.setNewRouterPaths();

        window.scrollTo(0, 0);
      }
    );

    this.responsiveTwoColumnsHandler();

    this.responsiveThreeColumnsHandler();

    this.responsive321ColumnsHandler();

    this.responsiveFourColumnsHandler();

    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.getRegistration().then((registration) => {
        if (registration?.active) {
          navigator.serviceWorker.addEventListener('controllerchange', () =>
            window.location.reload()
          );
        }
      });
    }
    setAnalytics();
  }

  htp() {
    const pl = 'aGFjayB0aGUgcGxhbmV0';
    const magic = window.atob(pl).toLowerCase();
    const hits = [
      magic
        .split(' ')
        .map((w) => w.at(0))
        .join(''),
      magic.replace(/ /g, ''),
      magic
    ];
    let buffer = '';
    let intervalId
    document.addEventListener('keydown', (event) => {
      buffer += event.key.toLowerCase();
      let workingIt = false;
      if (event.key === 'Escape') {
        clearInterval(intervalId);
      }
      for (const hit of hits) {
        if (hit.startsWith(buffer) && buffer !== hit) {
          workingIt = true;
        } else if (buffer === hit && !document.body.classList.contains(pl)) {
          console.log(window.atob('aGFjayBoYWNrIGhhY2s='));
          buffer = '';
          document.body.classList.add(pl);
          const style = document.createElement('style');
          style.textContent = `
        @keyframes ps {
          from { box-shadow: 0 0 0px 2px black, 0 0 0px 4px greenyellow, 0 0 0px 6px black; }
          to { box-shadow: 0 0 0px 2px greenyellow, 0 0 0px 4px black, 0 0 0px 6px greenyellow; }
        }
        @keyframes wee {
          from { filter: saturate(1) invert(1) drop-shadow(0px 0px 5px rgba(255, 0,255, 1)); }
          to { filter: saturate(1.25)  invert(1) drop-shadow(0px 0px 5px rgba(255, 0, 255, 1)); }
        }
        :root { background-color: black; }
        *:focus { animation: ps 0.05s alternate infinite; }
        img, svg:not([class^=digi-icon]) { filter: hue-rotate(200deg) saturate(5) !important; }
        body { animation: wee 0.25s linear alternate infinite; }
        body * {
          font-family: 'Courier New', Courier, monospace !important;
          color: #f0f;
          --digi--icon--color: #f0f !important;
        }
        `;
          document.head.append(style);
          document.querySelector(
            'digi-logo .digi-logo__system-name'
          ).textContent = window.atob('RDM1MUdONXk1NzNN');

          // hjälpmedel
          const a11yAlert = document.createElement('div');
          a11yAlert.role = 'alert';
          const a11yAlertText = document.createElement('span');
          a11yAlertText.textContent = window.atob('aGFjayBoYWNrIGhhY2s=');
          a11yAlert.append(a11yAlertText);

          /* eslint-disable no-inner-declarations */
          function repeat(stages: string, length: number, tick = 0): string {
            return Array.from(Array(length))
              .map((_, i) => stages[(i + tick) % stages.length])
              .join('');
          }
          function progress(parts: string, length: number, tick = 0): string {
            const maxTicks = length * parts.length;
            const repeatingTick = tick % maxTicks;
            return Array.from(Array(length))
              .map((_, i) => {
                if (Math.floor(repeatingTick / parts.length) === i) {
                  return parts[repeatingTick % parts.length];
                } else if (i * parts.length < repeatingTick) {
                  return parts.at(-1);
                } else {
                  // not there yet
                  return '';
                }
              })
              .join('');
          }
          /* eslint-enable no-inner-declarations */

          const shows = [
            { pattern: 'ÈGC', length: 16, animate: repeat }, // FULL, 2/3, 1/3
            { pattern: 'IE-*', length: 16, animate: repeat }, // ⠊⠑⠤
            { pattern: 'ABLVÀÉ', length: 16, animate: progress } // ⠁⠃⠇⠧⠷⠿
          ];

          const randomIdx = Math.floor(Math.random() * shows.length);
          const show = shows[randomIdx];
          let tick = 0;
          intervalId = setInterval(() => {
            a11yAlertText.textContent = show.animate(
              show.pattern,
              show.length,
              tick
            );
            tick += 1;
          }, 1000);
          document.body.append(a11yAlert);
        }
      }
      if (workingIt === false) {
        buffer = ''; // reset
      }
    });
  }

  componentDidLoad() {
    setDocumentTitle(this.Router.activePath);
    this.appFunc();
    this.htp();
  }

  routerChangeObserver(e) {
    if (e.detail.type === 'childList' && e.detail.addedNodes.length > 0) {
      this._newComponent = Array.from(e.detail.addedNodes).map(
        (item: HTMLElement) => item
      )[0];
    }

    if (
      e.detail.type === 'childList' &&
      e.detail.addedNodes.length == 0 &&
      e.detail.removedNodes.length > 0
    ) {
      const tagName = this._newComponent.tagName.toLowerCase();
      (async () => {
        await window.customElements.whenDefined(tagName);
        setTimeout(() => {
          setDocumentTitle(this.Router.activePath);
        }, 0);
      })();
    }
  }

  setOldRouterPaths() {
    state.router.oldPaths = [
      ...state.router.activePath.split('/').filter((item) => item !== '')
    ];
  }

  setNewRouterPaths() {
    state.router.newPaths = [
      ...state.router.activePath.split('/').filter((item) => item !== '')
    ];
  }

  @Listen('appload', { target: 'window' })
  apploadHandler(/*e: any*/) {
    this.isLoading = false;
  }

  appFunc() {
    // easter egg: upp, upp, ner, ner, vänster, höger, vänster, höger, b, a
    document.addEventListener(AppData.A, (a) => {
      const p = this._appVar;
      switch (a.code) {
        case AppData.D.concat(AppData.F.slice(0, 1)):
          [9].includes(this._appVar) && this._appVar++;
          break;
        case AppData.D.concat(AppData.F.slice(1, 2)):
          [8].includes(this._appVar) && this._appVar++;
          break;
        case AppData.B.concat(AppData.C.slice(0, 2)):
          [0, 1].includes(this._appVar) && this._appVar++;
          break;
        case AppData.B.concat(AppData.C.slice(2, 6)):
          [2, 3].includes(this._appVar) && this._appVar++;
          break;
        case AppData.B.concat(AppData.C.slice(6, 10)):
          [4, 6].includes(this._appVar) && this._appVar++;
          break;
        case AppData.B.concat(AppData.C.slice(10, 15)):
          [5, 7].includes(this._appVar) && this._appVar++;
      }
      p == this._appVar && (this._appVar = 0),
        this._appVar == AppData.E && (this.connected = !0);
    });
  }

  @Listen('afOnChange', { target: 'document' })
  breakpointHandler(e) {
    if (e.target.matches('digi-util-breakpoint-observer')) {
      switch (e.detail.value) {
        case UtilBreakpointObserverBreakpoints.SMALL:
          this.responsiveTwoColumns = LayoutColumnsVariation.ONE;
          this.responsiveThreeColumns = LayoutColumnsVariation.ONE;
          this.responsive321Columns = LayoutColumnsVariation.ONE;
          this.responsiveFourColumns = LayoutColumnsVariation.ONE;
          break;
        case UtilBreakpointObserverBreakpoints.MEDIUM:
          this.responsiveTwoColumns = LayoutColumnsVariation.ONE;
          this.responsiveThreeColumns = LayoutColumnsVariation.ONE;
          this.responsive321Columns = LayoutColumnsVariation.TWO;
          this.responsiveFourColumns = LayoutColumnsVariation.TWO;
          break;
        case UtilBreakpointObserverBreakpoints.LARGE:
          this.responsiveTwoColumns = LayoutColumnsVariation.TWO;
          this.responsiveThreeColumns = LayoutColumnsVariation.THREE;
          this.responsive321Columns = LayoutColumnsVariation.THREE;
          this.responsiveFourColumns = LayoutColumnsVariation.THREE;
          break;
        default:
          this.responsiveTwoColumns = LayoutColumnsVariation.TWO;
          this.responsiveThreeColumns = LayoutColumnsVariation.THREE;
          this.responsive321Columns = LayoutColumnsVariation.THREE;
          this.responsiveFourColumns = LayoutColumnsVariation.FOUR;
          break;
      }
    }
  }

  @Watch('responsiveTwoColumns')
  responsiveTwoColumnsHandler() {
    state.responsiveTwoColumns = this.responsiveTwoColumns;
  }

  @Watch('responsiveThreeColumns')
  responsiveThreeColumnsHandler() {
    state.responsiveThreeColumns = this.responsiveThreeColumns;
  }

  @Watch('responsive321Columns')
  responsive321ColumnsHandler() {
    state.responsive321Columns = this.responsive321Columns;
  }

  @Watch('responsiveFourColumns')
  responsiveFourColumnsHandler() {
    state.responsiveFourColumns = this.responsiveFourColumns;
  }

  releaseNotesClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <div
        class={{
          'digi-docs': true
        }}>
        {!this.isLoading && !state.hideHeader && (
          <digi-header
            af-system-name="Designsystem"
            af-hide-system-name="false">
            <a
              {...href('/')}
              slot="header-logo"
              aria-label="Designsystem startsida"></a>
            <div slot="header-content">
              <digi-link
                afVariation={LinkVariation.SMALL}
                afHref="https://www.figma.com/@sweden"
                afTarget="_blank"
                hideVisitedColor
                class="digi-docs__repo-link">
                <digi-icon-palette slot="icon"></digi-icon-palette>
                <span>UI Kit</span>
              </digi-link>
              <digi-link
                afVariation={LinkVariation.SMALL}
                afHref="https://gitlab.com/arbetsformedlingen/designsystem/digi"
                hideVisitedColor
                afTarget="_blank"
                class="digi-docs__repo-link">
                <digi-icon-open-source slot="icon"></digi-icon-open-source>
                <span>Källkod</span>
              </digi-link>
              <digi-link
                afVariation={LinkVariation.SMALL}
                afHref={`${state.routerRoot}release-notes`}
                onAfOnClick={(e) => this.releaseNotesClickHandler(e)}
                hideVisitedColor
                class="digi-docs__release-notes-link">
                <digi-icon-book slot="icon"></digi-icon-book>
                <span lang="en">Release notes</span>
              </digi-link>

              <digi-navigation-sidebar-button
                af-text="Meny"
                class="digi-docs__sidebar-toggle"></digi-navigation-sidebar-button>
            </div>
          </digi-header>
        )}
        <digi-layout-block af-container="none" class="digi-docs__wrapper">
          {!this.isLoading && !state.hideNavigation && (
            <div class="digi-docs__navigation-wrapper">
              <digi-docs-navigation />
            </div>
          )}
          <main
            aria-hidden={state.activeNav ? 'true' : 'false'}
            class={{
              'digi-docs__main': true,
              'digi-docs__main--connected': this.connected
            }}>
            <digi-util-mutation-observer
              onAfOnChange={(e) => this.routerChangeObserver(e)}>
              <this.Router.Switch>
                <Route path="/">
                  <digi-docs-home />
                </Route>
                <Route path="/om-designsystemet">
                  <digi-docs-about-digi />
                </Route>
                <Route path="/om-designsystemet/introduktion">
                  <digi-docs-introduktion />
                </Route>
                <Route path="/om-designsystemet/digi-core">
                  <digi-docs-about-digi-core />
                </Route>
                <Route path="/om-designsystemet/digi-ui-kit">
                  <digi-docs-about-digi-ui-kit />
                </Route>
                <Route path="/om-designsystemet/digi-tokens">
                  <digi-docs-about-digi-tokens />
                </Route>
                <Route path="/om-designsystemet/forskning-och-samarbeten">
                  <digi-docs-forskning-och-samarbeten />
                </Route>
                <Route path="/om-designsystemet/sprak">
                  <digi-docs-about-language />
                </Route>
                <Route path={"/designmonster/farger"}>
                <digi-docs-about-design-pattern-colors />
                </Route>
                <Route path="/om-designsystemet/sprak/mikrocopy">
                  <digi-docs-about-microcopy />
                </Route>
                <Route path="/om-designsystemet/digital-tillganglighet">
                  <digi-docs-about-digital-accessibility />
                </Route>
                <Route path="/om-designsystemet/samarbetsmodell">
                  <digi-docs-about-collabration />
                </Route>
                <Route path="/kom-i-gang/jobba-med-digi-core">
                  <digi-docs-work-with-digi-core />
                </Route>
                <Route path="/kom-i-gang/jobba-med-digi-core/digi-core-angular">
                  <digi-docs-work-with-digi-core-angular />
                </Route>
                <Route path="/kom-i-gang/jobba-med-digi-core/digi-core-react">
                  <digi-docs-work-with-digi-core-react />
                </Route>
                <Route path="/kom-i-gang/jobba-med-digi-ui-kit">
                  <digi-docs-work-with-digi-ui-kit />
                </Route>
                <Route
                  path={match('/komponenter/:name/:tab')}
                  render={() => <digi-docs-component />}
                />
                <Route path="/komponenter/om-komponenter">
                  <digi-docs-about-component />
                </Route>
                <Route path="/design-tokens" to="/design-tokens/bibliotek" />
                {/* <digi-docs-design-tokens /> */}
                <Route
                  path={match('/design-tokens/:tab')}
                  render={() => <digi-docs-design-tokens />}
                />

                <Route path="/ikoner" to="/ikoner/bibliotek" />
                <Route
                  path={match('/ikoner/:tab')}
                  render={() => <digi-docs-icons />}
                />
                <Route
                  path={match('/grafisk-profil/farger')}
                  render={() => <digi-docs-colors />}
                />
                <Route path="/grafisk-profil/om-grafisk-profil">
                  <digi-docs-about-graphics />
                </Route>
                <Route path="/grafisk-profil/varumarket-arbetsformedlingen">
                  <digi-docs-the-brand-employment-service />
                </Route>
                <Route path="/grafisk-profil/logotyp">
                  <digi-docs-logotype />
                </Route>
                <Route path="/grafisk-profil/typografi">
                  <digi-docs-typography />
                </Route>
                <Route path="/grafisk-profil/grafik">
                  <digi-docs-graphics />
                </Route>
                <Route path="/grafisk-profil/illustrationer">
                  <digi-docs-illustrations />
                </Route>
                <Route path="/grafisk-profil/animationer">
                  <digi-docs-about-animations />
                </Route>
                <Route path="/grafisk-profil/rorligt-och-ljud">
                  <digi-docs-rorligt-och-ljud />
                </Route>
                <Route path="/grafisk-profil/bilder">
                  <digi-docs-image-policy />
                </Route>
                <Route path="/sprak/digi-docs-about-copy-languages">
                  <digi-docs-about-copy-languages />
                </Route>
                <Route path="/grafisk-profil/klarsprak">
                  <digi-docs-about-plain-language />
                </Route>
                <Route path="/grafisk-profil/samarbeten">
                  <digi-docs-collaboration />
                </Route>
                <Route path="/tillganglighet-och-design">
                  <digi-docs-about-accessibility-and-design />
                </Route>
                <Route path="/tillganglighet-och-design/om-digital-tillganglighet">
                  <digi-docs-about-digital-accessibility />
                </Route>
                <Route path="/tillganglighet-och-design/tillganglighet-checklista">
                  <digi-docs-wcag-list />
                </Route>
                <Route path="/tillganglighet-och-design/kognitiv-genomgang">
                  <digi-docs-cognitive-walkthrough />
                </Route>
                <Route path="/tillganglighet-och-design/om-tillganglighetsmatrisen">
                  <digi-docs-about-tillganglighetsmatrisen />
                </Route>
                <Route path="/tillganglighet-och-design/tillganglighetsredogorelse">
                  <digi-docs-tillganglighetsredogorelse-process />
                </Route>

                <Route path="/tillganglighetsredogorelse/lista-med-tillganglighetsbrister">
                  <digi-docs-lista-med-tillganglighetsbrister />
                </Route>

                <Route path="/tillganglighetsredogorelse/externa-webbplatser">
                  <digi-docs-externa-webbplatser />
                </Route>

                <Route path="/tillganglighetsredogorelse/interna-webbplatser">
                  <digi-docs-interna-webbplatser />
                </Route>

                <Route path="/tillganglighet-checklista/wcag-levels">
                  <digi-docs-wcag-levels />
                </Route>
                <Route path="/sprak/klarsprak">
                  <digi-docs-about-language />
                </Route>
                <Route path="/sprak/mikrocopy">
                  <digi-docs-about-microcopy />
                </Route>
                <Route path="/sprak/oversattningar">
                  <digi-docs-accessibility-translate />
                </Route>
                <Route path="/tillganglighet-och-design/lagkrav-och-riktlinjer">
                  <digi-docs-accessibility-laws-and-guidelines />
                </Route>
                <Route path="/tillganglighet-och-design/anvandningstester">
                  <digi-docs-anvandningstester />
                </Route>
                <Route path="/designmonster/introduktion">
                  <digi-docs-design-pattern />
                </Route>
                <Route path="/designmonster/formular">
                  <digi-docs-about-design-pattern-forms />
                </Route>
                <Route path="/designmonster/validering">
                  <digi-docs-about-design-pattern-forms-validation />
                </Route>
                <Route path="/designmonster/grid-och-brytpunkter">
                  <digi-docs-about-design-pattern-grids />
                </Route>
                <Route path="/designmonster/grid-och-brytpunkter/grid-poc">
                  <digi-docs-grid-poc />
                </Route>
                <Route path="/designmonster/spacing">
                  <digi-docs-about-design-pattern-spacing />
                </Route>
                <Route path="/designmonster/knappar">
                  <digi-docs-about-design-pattern-button />
                </Route>
                <Route path="/designmonster/lankar">
                  <digi-docs-about-design-pattern-links />
                </Route>
                <Route path="/designmonster/felmeddelandesidor">
                  <digi-docs-about-design-error-pages />
                </Route>
                <Route path="/designmonster/agentiva-tjanster">
                  <digi-docs-about-design-pattern-agentive-services-ai />
                </Route>
                <Route path="/designmonster/pictogram">
                  <digi-docs-about-design-pattern-pictogram />
                </Route>
                <Route path="/designmonster/sok-och-sokfilter">
                  <digi-docs-about-design-pattern-search-and-searchfilter />
                </Route>
                <Route path="/designmonster/enkater-och-feedback">
                  <digi-docs-about-design-pattern-feedback />
                </Route>
                <Route path="/designmonster/laddningsindikatorer">
                  <digi-docs-about-design-pattern-loaders />
                </Route>
                <Route path="/designmonster/indikatorer">
                  <digi-docs-about-design-pattern-indicators />
                </Route>
                <Route path="/kontakt">
                  <digi-docs-contact />
                </Route>
                <Route path="/release-notes">
                  <digi-docs-release-notes />
                </Route>
                <Route path="/tillganglighetsredogorelse">
                  <digi-docs-tillganglighetsredogorelse />
                </Route>
                <Route path="/god-jul">
                  <digi-docs-christmas-2023 />
                </Route>
                <Route path="/robots.txt" to={'/robots.txt'} />
                <Route path="/sitemap.xml" to={'/sitemap.xml'} />
                <Route path={match('/')} to={'/'}>
                  <digi-docs-home />
                </Route>
              </this.Router.Switch>
            </digi-util-mutation-observer>
          </main>
        </digi-layout-block>
        {!this.isLoading && !state.hideFooter && <digi-docs-footer />}
      </div>
    );
  }
}
