import {
  Component,
  h,
  State,
  Fragment,
  Prop,
  Watch,
  Element
} from '@stencil/core';
import { ButtonVariation, CodeVariation } from '@digi/arbetsformedlingen';
import { DocsColorSwatchVariation } from '../color-swatch/digi-docs-color-swatch-variation.enum';
import { TokenFormat } from '../../token-format.enum';
import type { HTMLStencilElement } from '@stencil/core/internal';

interface ColorItem {
  id: string
  heading: string
  displayColor: string
  subHeading?: string
  description?: string
  tokenName?: string
  pairs?: Array<{
    key: string
    value: string
  }>
}

@Component({
  tag: 'digi-docs-color-token-cards',
  styleUrl: 'digi-docs-color-token-cards.scss',
  scoped: true
})
export class ColorTokenCards {
  @Element() hostElement: HTMLStencilElement;

  @State() pageName = 'Color Token Cards';
  @State() _colorTokens = [];
  @State() _tokenFormat: TokenFormat;

  @Prop() colorTokens; // gammal

  @Prop() colors: ColorItem[] = []; 
  @Watch('colors')
  colorTokensChangeHandler() {
    this._colorTokens = [...this.colors];
  }

  @Prop() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;
  @Watch('tokenFormat')
  tokenFormatChangeHandler() {
    this._tokenFormat = this.tokenFormat;
  }

  scssValue(code: string) {
    return `$${code}`;
  }

  customPropertyValue(code: string) {
    return `--${code}`;
  }

  componentWillLoad() {
    this.colorTokensChangeHandler();
    this.tokenFormatChangeHandler();
  }

  componentDidLoad() {
    this.initToggleButtons();
  }

  initToggleButtons() {
    const toggleButtons = this.hostElement.querySelectorAll(
      'digi-button[data-toggle-button] button'
    );
    toggleButtons.forEach((button: HTMLElement) => {
      const controlledItems = this.getControlledItems();
      button.setAttribute('aria-expanded', 'false');
      button.setAttribute('aria-controls', controlledItems);
    });
  }

  toggleHandler(e) {
    const $this = e.target as HTMLElement;
    const $button = $this.querySelector('button');

    const groupElements = this.hostElement.querySelectorAll(
      `*[data-list-group="tokenListCollapse-${this.colors[0].id}"]`
    );

    const isExpanded = $button.getAttribute('aria-expanded') == 'true';

    $button.setAttribute('aria-expanded', String(!isExpanded));

    groupElements.forEach((item) => {
      item.setAttribute('hidden', String(isExpanded));
      item.setAttribute('aria-hidden', String(isExpanded));
      item.classList.toggle('hidden');
      item
        .closest('digi-card')
        .classList.toggle('digi-docs-color-token-cards__item--expanded');
    });
  }

  getControlledItems() {
    let controlsList = '';
    this.colors.forEach((token, index) => {
      if (index > 0) {
        controlsList += `${index > 1 ? ', ' : ''}#tokenListCollapse-${token.id}-${index - 1}`;
      }
    });
    return controlsList;
  }

  render() {
    return (
      <host>
        <div class="digi-docs-color-token-cards">
          <div class="digi-docs-color-token-cards__list">
            {this.colors.length > 0 && (
              <Fragment>
                {this.colors.map((token, i) => {
                  const isFirstColor = i === 0
                  const isSubColor = i > 0

                  const subColorHTMLProps = {
                    id: `tokenListCollapse-${this.colors[0].id}-${i}`,
                    'data-list-group': `tokenListCollapse-${this.colors[0].id}`,
                    'aria-hidden': "true"
                  }

                  return (
                    <digi-card
                      class={{
                        'digi-docs-color-token-cards__item': true,
                        'digi-docs-color-token-cards__item--variation': isSubColor
                      }}>
                      <div
                        class={{
                          'digi-docs-color-token-cards__content': true,
                          'digi-docs-color-token-cards__content--variation': isSubColor,
                          hidden: isSubColor
                        }}
                        {...(isSubColor ? subColorHTMLProps: {})}
                        hidden={isSubColor}
                        >
                        <div class="digi-docs-color-token-cards__content-start">
                          <div class="digi-docs-color-token-cards__header">
                            <digi-docs-color-swatch
                              token={JSON.stringify({
                                value: token.displayColor,
                                name: token.tokenName
                              })}
                              token-variation={DocsColorSwatchVariation.CIRCLE}
                              token-border={true}
                              token-color-format="HEX"></digi-docs-color-swatch>
                            <div>
                              <h3 class="digi-docs-color-token-cards__heading">
                                {token.heading}
                              </h3>
                              {token.subHeading && <p>{token.subHeading}</p>}
                            </div>
                          </div>
                          {token.description && (
                            <p class="digi-docs-color-token-cards__comment">
                              {token.description}
                            </p>
                          )}
                          <div class="digi-docs-color-token-cards__token">
                            {token?.pairs?.length > 0 && (
                              <dl class="digi-docs-color-token-cards__print-data">
                                {token.pairs.map((pair) => {
                                  return (
                                    <Fragment>
                                      <dt>{pair.key}</dt>
                                      <dd>{pair.value}</dd>
                                    </Fragment>
                                  );
                                })}
                              </dl>
                            )}
                            {token.tokenName != null && (
                              <digi-code
                                afVariation={CodeVariation.LIGHT}
                                afCode={token.tokenName}></digi-code>
                            )}
                          </div>
                        </div>
                        <div class="digi-docs-color-token-cards__content-end">
                          <div class="digi-docs-color-token-cards__swatch-list">
                            {this.colors.map((swatchToken) => {
                              return (
                                <digi-docs-color-swatch
                                  token={JSON.stringify({
                                    value: swatchToken.displayColor,
                                    name: swatchToken.tokenName
                                  })}
                                  token-border={true}
                                  token-color-format="HEX"
                                  token-variation={
                                    DocsColorSwatchVariation.CIRCLE
                                  }
                                  tokenSelected={
                                    swatchToken.id === token.id
                                  }></digi-docs-color-swatch>
                              );
                            })}
                          </div>
                          {isFirstColor && this.colors.length > 1 && (
                            <digi-button
                              afVariation={ButtonVariation.FUNCTION}
                              class="digi-docs-color-token-cards__toggle-btn"
                              data-toggle-button="true"
                              onAfOnClick={(e) => this.toggleHandler(e)}>
                              <digi-icon-chevron-down slot="icon"></digi-icon-chevron-down>
                              <span class="digi-docs-color-token-cards__toggle-btn-text">
                                <span>Visa varianter</span>
                                <span>Dölj varianter</span>
                              </span>
                            </digi-button>
                          )}
                        </div>
                      </div>
                      <div
                        slot="footer"
                        class="digi-docs-color-token-cards__footer"
                        style={{
                          '--TOKEN--FOOTER--COLOR': token.displayColor
                        }}></div>
                    </digi-card>
                  );
                })}
              </Fragment>
            )}
          </div>
        </div>
      </host>
    );
  }
}
