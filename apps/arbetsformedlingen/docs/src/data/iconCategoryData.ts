import { IconCategory } from '../taxonomies';

export const componentCategoryMap = {
  'digi-icon-users-solid': IconCategory.USER,
  'digi-icon-user-alt': IconCategory.USER
};
