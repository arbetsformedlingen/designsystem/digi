export const iconsComponent = {
  filePath: '',
  encapsulation: 'scoped',
  tag: 'digi-icons',
  readme: '# digi-icons\r\n\r',
  docs: '',
  docsTags: [],
  usage: {},
  props: [
    {
      name: 'afDesc',
      type: 'string',
      mutable: false,
      attr: 'af-desc',
      reflectToAttr: false,
      docs: 'Lägger till ett `desc`-element i svg:n',
      docsTags: [
        {
          text: 'Adds a `desc` element inside the svg',
          name: 'en'
        }
      ],
      values: [
        {
          type: 'string'
        }
      ],
      optional: false,
      required: false
    },
    {
      name: 'afTitle',
      type: 'string',
      mutable: false,
      attr: 'af-title',
      reflectToAttr: false,
      docs: 'Lägger till ett `title`-element i svg:n. SVGn använder denna titel till aria-labelledby',
      docsTags: [
        {
          text: 'Adds a `title` element inside the svg. The SVG uses aria-labelledby to link to this title.',
          name: 'en'
        }
      ],
      values: [
        {
          type: 'string'
        }
      ],
      optional: false,
      required: false
    },
    {
      name: 'afSvgAriaHidden',
      type: 'boolean',
      mutable: false,
      attr: 'af-svg-aria-hidden',
      reflectToAttr: false,
      docs: 'Sätter `aria-hidden` attribut på svg:n. Default är true',
      docsTags: [
        {
          text: 'Sets `aria-hidden` attribute on the svg element. Default is true',
          name: 'en'
        }
      ],
      values: [
        {
          type: 'boolean'
        }
      ],
      optional: false,
      required: false
    },
    {
      name: 'afSvgAriaLabelledby',
      type: 'string',
      mutable: false,
      attr: 'af-svg-aria-labelledby',
      reflectToAttr: false,
      docs: 'Sätter `aria-labelledby` attribut på svg:n. Använder title element som default om afTitle är satt',
      docsTags: [
        {
          text: 'Sets `aria-labelledby` attribute on the svg element. Uses title element as default if afTitle is set',
          name: 'en'
        }
      ],
      values: [
        {
          type: 'string'
        }
      ],
      optional: false,
      required: false
    }
  ],
  methods: [],
  events: [],
  listeners: [],
  styles: [
    {
      name: '--digi--icon--color',
      annotation: 'prop',
      docs: 'var(--digi--color--icons--primary);'
    },
    {
      name: '--digi--icon--height',
      annotation: 'prop',
      docs: 'auto;'
    },
    {
      name: '--digi--icon--width',
      annotation: 'prop',
      docs: 'initial;'
    }
  ],
  slots: [],
  parts: [],
  dependents: [],
  dependencies: [],
  dependencyGraph: {}
};
