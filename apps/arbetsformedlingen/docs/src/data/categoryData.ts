import { Category } from '../taxonomies';

export const componentCategoryMap = {
  'digi-button': Category.BUTTON,
  'digi-badge-notification': Category.BADGE,
  'digi-dialog': Category.DIALOG,
  'digi-loader-spinner': Category.LOADER,
  'digi-list': Category.LIST,
  'digi-table': Category.TABLE,
  'digi-util-keyup-handler': Category.DEV_UTIL,
  'digi-logo': Category.LOGO,
  'digi-quote-single': Category.QUOTE,
  'digi-quote-multi-container': Category.QUOTE,
  'digi-badge-status': Category.BADGE,
  'digi-loader-skeleton': Category.LOADER,
  'digi-footer': Category.FOOTER,

  'digi-code': Category.CODE,
  'digi-code-block': Category.CODE,

  'digi-form-label': Category.FORM,
  'digi-form-error-list': Category.FORM,
  'digi-form-file-upload': Category.FORM,
  'digi-form-fieldset': Category.FORM,
  'digi-form-input': Category.FORM,
  'digi-form-category-filter': Category.FORM,
  'digi-form-checkbox': Category.FORM,
  'digi-form-receipt': Category.FORM,
  'digi-form-filter': Category.FORM,
  'digi-form-radiobutton': Category.FORM,
  'digi-form-radiogroup': Category.FORM,
  'digi-form-input-search': Category.FORM,
  'digi-form-textarea': Category.FORM,
  'digi-form-validation-message': Category.FORM,
  'digi-form-select': Category.FORM,
  'digi-form-select-filter': Category.FORM,

  'digi-info-card': Category.CARD,
  'digi-info-card-multi-container': Category.CARD,
  'digi-info-card-multi': Category.CARD,

  'digi-layout-block': Category.LAYOUT,
  'digi-layout-columns': Category.LAYOUT,
  'digi-layout-container': Category.LAYOUT,
  'digi-layout-media-object': Category.LAYOUT,
  'digi-layout-error-page': Category.LAYOUT,

  'digi-link': Category.LINK,
  'digi-link-button': Category.LINK,
  'digi-link-external': Category.LINK,
  'digi-link-internal': Category.LINK,

  'digi-media-image': Category.MEDIA,
  'digi-media-figure': Category.MEDIA,

  'digi-navigation-breadcrumbs': Category.NAVIGATION,
  'digi-context-menu': Category.NAVIGATION,
  'digi-tablist': Category.NAVIGATION,

  'digi-navigation-tab': Category.NAVIGATION,
  'digi-navigation-tabs': Category.NAVIGATION,
  'digi-navigation-pagination': Category.NAVIGATION,
  'digi-navigation-sidebar': Category.NAVIGATION,
  'digi-navigation-sidebar-button': Category.NAVIGATION,
  'digi-navigation-vertical-menu': Category.NAVIGATION,
  'digi-navigation-vertical-menu-item': Category.NAVIGATION,

  'digi-notification-alert': Category.NOTIFICATION,
  'digi-notification-error-page': Category.NOTIFICATION,

  'digi-typography': Category.TYPOGRAPHY,
  'digi-typography-preamble': Category.TYPOGRAPHY,
  'digi-typography-heading-jumbo': Category.TYPOGRAPHY,
  'digi-typography-time': Category.TYPOGRAPHY,
  'digi-typography-meta': Category.TYPOGRAPHY,

  'digi-util-detect-click-outside': Category.DEV_UTIL,
  'digi-util-detect-focus-outside': Category.DEV_UTIL,
  'digi-util-intersection-observer': Category.DEV_UTIL,
  'digi-util-mutation-observer': Category.DEV_UTIL,
  'digi-util-breakpoint-observer': Category.DEV_UTIL,
  'digi-util-keydown-handler': Category.DEV_UTIL,

  'digi-tag': Category.TAG,
  'digi-tag-media': Category.TAG,

  'digi-calendar': Category.CALENDAR,
  'digi-calendar-datepicker': Category.CALENDAR,
  'digi-calendar-week-view': Category.CALENDAR,

  'digi-progress-indicator': Category.PROGRESS,

  'digi-expandable-accordion': Category.EXPANDABLE,
  'digi-expandable-faq': Category.EXPANDABLE,
  'digi-expandable-faq-item': Category.EXPANDABLE,

  'digi-progress-list': Category.PROGRESS,
  'digi-progress-list-item': Category.PROGRESS,

  'digi-chart-line': Category.CHART,
  'digi-bar-chart': Category.CHART,

  'digi-tools-languagepicker': Category.UTIL,
  'digi-tools-feedback': Category.UTIL,
  'digi-tools-feedback-banner': Category.UTIL,
  'digi-tools-feedback-rating': Category.UTIL,

  'digi-header': Category.HEADER,
  'digi-header-avatar': Category.HEADER,
  'digi-header-navigation': Category.HEADER,
  'digi-header-navigation-item': Category.HEADER,
  'digi-header-notification': Category.HEADER
};
