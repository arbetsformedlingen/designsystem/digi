import { Component, h, State } from '@stencil/core';
import { InfoCardHeadingLevel } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-about-animations',
  styleUrl: 'digi-docs-about-animations.scss',
  scoped: true
})
export class AboutAnimations {
  @State() pageName = 'Animationer';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-animations/digi-docs-about-animations.tsx">
          <span slot="preamble">
            Animationer ska vara måttfulla och användas sparsamt, på så sätt
            ökar sannolikheten att de blir användbara och tillgängliga. I
            arbetet med animationer, försök efterlikna fysiken i den riktiga
            världen, så att rörelser blir behagliga och realistiska. Animationer
            i designsystemet dokumenteras på komponentnivå.
          </span>
          <digi-layout-container af-margin-top af-margin-bottom>
            <div class="digi-docs__animations--info-card-container">
              <digi-info-card
                afHeading="Generella riktlinjer"
                afHeadingLevel={InfoCardHeadingLevel.H2}>
                <digi-list>
                  <li>
                    Animationer ska vara användbara, det vill säga tillföra
                    något för användaren.
                  </li>
                  <li>
                    Animationer ska vara tillgängliga, inte distrahera eller
                    hindra användaren från det de försöker åstadkomma.
                  </li>
                  <li>
                    Animationer bör försöka efterlikna fysiken i den riktiga
                    världen, så att de uppfattas som behagliga och realistiska.
                  </li>
                </digi-list>
              </digi-info-card>
            </div>
          </digi-layout-container>
        </digi-docs-page-layout>
      </host>
    );
  }
}
