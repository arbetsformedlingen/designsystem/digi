import { Component, h, State } from '@stencil/core';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-grid-poc',
  styleUrl: 'digi-docs-grid-poc.scss',
  scoped: true
})
export class GridPoc {
  @State() pageName = 'Grid Poc';

  componentWillLoad() {
    state.hideHeader = state.hideFooter = state.hideNavigation = true;
  }

  render() {
    return (
      <host>
        <digi-link-button
          afHref="/designmonster/grid-och-brytpunkter"
          af-target="_blank"
          af-size="medium"
          af-variation="secondary">
          Grid och brytpunkter
        </digi-link-button>
        <main>
          <div class="grid">
            <header class="grid__header"></header>
            <div class="grid__side-a"></div>
            <div class="grid__side-b"></div>

            <article class="grid__article"></article>

            <div class="grid__main">
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
              <div class="grid__item"></div>
            </div>
          </div>
        </main>
      </host>
    );
  }
}
