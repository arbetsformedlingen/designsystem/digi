import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';
// import colorBlindImage from '../../assets/images/pattern/design-color/colorblind.svg';
import splashOne from '../../assets/images/pattern/design-color/splashOne.svg';
import splashTwo from '../../assets/images/pattern/design-color/splashTwo.svg';
import splashThree from '../../assets/images/pattern/design-color/splashThree.svg';
import splashFour from '../../assets/images/pattern/design-color/splashFour.svg';
import splashFive from '../../assets/images/pattern/design-color/splashFive.svg';
import { LinkButtonSize } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-about-design-pattern-colors',
  styleUrl: 'digi-docs-about-design-pattern-colors.scss',
  scoped: true
})
export class AboutDesignPatternColors {
  @State() pageName = 'Webbfärger';
  Router = router;

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  isSubset(superObj, subObj) {
    return Object.keys(subObj).every((ele) => {
      if (typeof subObj[ele] == 'object') {
        return this.isSubset(superObj[ele], subObj[ele]);
      }
      return subObj[ele] === superObj[ele];
    });
  }

  groupTokens(arr = []) {
    const res = [];
    const map = {};
    let i, j, curr;
    for (i = 0, j = arr.length; i < j; i++) {
      curr = arr[i];
      if (!(curr.attributes.subitem in map)) {
        map[curr.attributes.subitem] = {
          name: curr.attributes.subitem,
          variations: []
        };
        res.push(map[curr.attributes.subitem]);
      }
      map[curr.attributes.subitem].variations.push(curr);
    }
    return res;
  }

  render() {
    return (
      <host>
        <digi-typography>
          <digi-docs-page-layout
            af-page-heading={this.pageName}
            af-edit-href="pages/about-design-pattern-colors/digi-docs-about-design-pattern-colors.tsx">
            <span slot="preamble">
              Den färgpalett som används i designsystemet bygger på
              Arbetsförmedlingens grafiska profil och vanliga färger för
              webbkommunikation. Paletten är viktig för att skapa igenkänning
              och stärka Arbetsförmedlingens varumärke.
            </span>
            <digi-layout-container af-margin-bottom af-margin-top>
              <p>
                Arbetsförmedlingens färgpalett är uppdelad för olika ändamål,
                men är baserad på samma profilfärger. Viss skillnad kan
                förekomma mellan webb, dokument-/presentationsmallar,
                statistikverktyg, tryck och videoproduktion. Paletten för webb
                är den mest omfattande och det är den som är grunden i våra
                designtokens och återfinns i UI-kittets komponenter.
              </p>
            </digi-layout-container>
            <br />
            <digi-layout-block
              af-variation="secondary"
              afVerticalPadding
              afMarginTop>
              <h2>Färgpalettens två huvudområden</h2>
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <p>
                    <b>Profilfärgerna</b> är nära knutna till
                    Arbetsförmedlingens grafiska profil. De samspelar väl och
                    används i olika proportioner för att skapa en tydlig och
                    igenkännbar identitet för Arbetsförmedlingen. Använd dessa
                    för illustrationer och bilder.
                  </p>
                  <p>
                    <b>Funktionsfärger</b> är färger som har specifika
                    funktioner. De kan till exempel användas som länkfärger för
                    navigering, varningsfärger eller för att illustrera
                    information i diagram. Dessa färger bygger på vanliga
                    funktionsfärger som ofta används på webben.
                    Funktionsfärgerna återfinns i specifika funktioner i
                    designsystemskomponenterna. Riktlinjen är att de färdiga
                    komponenterna i UI-kittet ska användas i första hand och att
                    färgerna finns som stöd vid behov.
                  </p>
                </div>
                <div class="logo-and-text">
                  <digi-logo af-variation="large" a-Color="primary"></digi-logo>
                  <p class="legend-paragraph">
                    Arbetsförmedlingens logotyp använder de två profilfärger som
                    starkast representerar varumärket.
                  </p>
                </div>
              </digi-layout-columns>
            </digi-layout-block>
            <br />
            <digi-layout-container af-margin-bottom af-margin-top>
              <h2>Profilfärger</h2>
              <digi-layout-columns af-variation={state.responsiveTwoColumns}>
                <div>
                  <p>
                    Arbetsförmedlingens profilfärger är blå och grön.
                    Tillsammans med den vita färgen som bas står de för trygghet
                    (profilblå) och växtkraft (profilgrön). Dessa färger finns i
                    all vår kommunikation.
                  </p>
                  <p>
                    Toningar av den gröna och “vita” förekommer i små volymer i
                    till exempel navigeringselement, illustrationer och
                    bakgrunder.
                  </p>
                  <p>
                    <b>Att tänka på:</b> Den primärgröna{' '}
                    <span lang="en">(leaf-500)</span> saknar tillräcklig
                    kontrast att användas både som textfärg och för bärande
                    grafik.
                  </p>
                  <digi-layout-columns
                    af-variation={state.responsiveTwoColumns}>
                    <div lang="en" class="col-flex">
                      <div class="color-text-flex">
                        <div
                          lang="en"
                          class="profile-color-flex-box profile-color-flex-box--af-blue"></div>
                        <div>
                          <span>stratos-500</span>
                          <p>#00005A</p>
                        </div>
                      </div>
                    </div>
                    <div lang="en" class="col-flex">
                      <div class="color-text-flex">
                        <div class="profile-color-flex-box profile-color-flex-box--af-green"></div>
                        <div>
                          <span>leaf-500</span>
                          <p>#95C23E</p>
                        </div>
                      </div>
                      <div class="color-text-flex">
                        <div class="profile-color-flex-box profile-color-flex-box--af-light-green"></div>
                        <div>
                          <span>leaf-200</span>
                          <p>#DDEBC1</p>
                        </div>
                      </div>
                      <div class="color-text-flex">
                        <div class="profile-color-flex-box profile-color-flex-box--af-green-lighter"></div>
                        <div>
                          <span>leaf-100</span>
                          <p>#F1F7E5</p>
                        </div>
                      </div>
                    </div>
                  </digi-layout-columns>
                </div>
                <div class="big-color-column">
                  <div class="big-color-box big-blue-box">
                    <span class="big-box-text">Aa</span>
                  </div>
                  <div class="green-box-container">
                    <div class="big-color-box big-green-box"></div>
                    <div class="big-color-box big-light-green-box"></div>
                    <div class="big-color-box big-very-light-green-box"></div>
                  </div>
                  <div class="big-color-grey-strip-container">
                    <span class="strip-text">Aa</span>
                    <div class="big-color-box big-neutral-strip"></div>
                    <div class="strip-volume-text-container">
                      <div class="big-color-box big-neutral-strip">
                        <div class="volume-water-strip"></div>
                        <div class="volume-rosie-strip"></div>
                        <div class="volume-hajdu-strip"></div>
                      </div>
                    </div>
                  </div>
                  <p class="volume-example-text">
                    Volymexempel på hur pass dominerande färgerna skall vara i
                    förhållande till varandra. Ungefärligt exempel.
                  </p>
                </div>
              </digi-layout-columns>

              <h3>Komplementfärger</h3>
              <p class="profile-paragraphs">
                Dessa används för att bryta av, komplettera och markera ett
                grafiskt objekt. Det kan till exempel vara en del av en
                interaktion, ett informationsblock eller liknande. Dessa färger
                skall användas med stor försiktighet och inte vara dominerande.
                Den blå är ny i sammanhanget som vi fortfarande håller på att
                utvärdera, så stäm av med designsystemteamet om du är osäker.{' '}
              </p>
              <p>
                Den rosa och turkosa är två komplementfärger som inte längre
                används i designsystemets komponenter, men finns fortfarande
                kvar i vissa sammanhang. De har blivit ersatta med
                funktionsfärger där grön ersatt den turkosa och röd ersatt den
                rosa.
              </p>
              <digi-layout-columns af-variation={state.responsiveFourColumns}>
                <div class="color-text-flex">
                  <div
                    lang="en"
                    class="profile-color-flex-box profile-color-flex-box--af-comp"></div>
                  <div>
                    <span>water-500</span>
                    <p>#BDD3E7</p>
                  </div>
                </div>
                <div class="color-text-flex">
                  <div
                    lang="en"
                    class="profile-color-flex-box profile-color-flex-box--af-comp-2"></div>
                  <div>
                    <span>rose-500</span>
                    <p>#D43372</p>
                  </div>
                </div>
                <div class="color-text-flex">
                  <div
                    lang="en"
                    class="profile-color-flex-box profile-color-flex-box--af-comp-3"></div>
                  <div>
                    <span>jade-500</span>
                    <p>#058470</p>
                  </div>
                </div>
                <div></div>
              </digi-layout-columns>

              <h2 class="extra-top-padding">Neutrala toner</h2>
              <p>
                En stor del av Arbetsförmedlingens identitet i färg utspelar sig
                på en neutral yta. Utgångspunkten är vit{' '}
                <span lang="en">(grayscale-0)</span>. Ibland behöver man bryta
                av den och dela in i sektioner. För bakgrund använder vi då en
                lätt grå ton <span lang="en">(grayscale-100)</span>. För
                avdelare och liknande grafik används mellanskiftet i den gråa
                skalan. Den primära textfärgen{' '}
                <span lang="en">(grayscale-900)</span> innehåller en liten
                gråton för att vara lättare att läsa på skärmar. Det finns en
                alternativ ljusare textfärg som fortfarande uppfyller
                kontrastkraven mot vit bakgrund{' '}
                <span lang="en">(grayscale-700)</span>.
              </p>
              <digi-layout-columns af-variation={state.responsiveFourColumns}>
                <div class="col-flex">
                  <div class="color-text-flex">
                    <div class="profile-color-flex-box profile-color-flex-box--af-grayscale"></div>
                    <div>
                      <span>grayscale-900</span>
                      <p>#333333</p>
                    </div>
                  </div>
                  <div class="color-text-flex">
                    <div class="profile-color-flex-box profile-color-flex-box--af-grayscale-2"></div>
                    <div>
                      <span>grayscale-700</span>
                      <p>#757575</p>
                    </div>
                  </div>
                </div>
                <div class="col-flex">
                  <div class="color-text-flex">
                    <div class="profile-color-flex-box profile-color-flex-box--af-grayscale-3 border"></div>
                    <div>
                      <span>grayscale-0</span>
                      <p>#FFFFFF</p>
                    </div>
                  </div>
                  <div class="color-text-flex">
                    <div class="profile-color-flex-box profile-color-flex-box--af-bg-sec"></div>
                    <div>
                      <span>grayscale-100</span>
                      <p>#F4F4F4</p>
                    </div>
                  </div>
                </div>
                <div></div>
                <div></div>
              </digi-layout-columns>
            </digi-layout-container>
            <br />
            <digi-layout-block
              af-variation="secondary"
              afVerticalPadding
              afMarginTop>
              <h2>Funktionsfärger</h2>
              <div>
                <h3>Text och länkar</h3>
                <p>
                  För textbaserade länkar i rubriker, löpande text och
                  länkknappar så använder vi en specifik länkblå{' '}
                  <span lang="en">(sapphire-500)</span>. För vanliga texter
                  används <span lang="en">grayscale-900</span>.
                </p>
                <digi-layout-columns af-variation={state.responsiveFourColumns}>
                  <div>
                    <div class="val-item">
                      <div class="profile-color-flex-box profile-color-flex-box--nav-button"></div>
                      <div>
                        <span>text/link</span>
                        <p>#1616B2</p>
                      </div>
                    </div>
                    <div class="val-item">
                      <div class="profile-color-flex-box profile-color-flex-box--af-btn-hover"></div>
                      <div>
                        <span>text/link-visited</span>
                        <p>#822998</p>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="val-item">
                      <div class="profile-color-flex-box profile-color-flex-box--nav-button-secondary"></div>
                      <div>
                        <span>text/primary</span>
                        <p>#333333</p>
                      </div>
                    </div>
                    <div></div>
                    <div></div>
                    {/* <div class="val-item">
                      <div class="profile-color-flex-box profile-color-flex-box--nav-button-secondary-hover"></div>
                      <div>
                        <span></span>
                        <p>#88BB44</p>
                      </div>
                    </div> */}
                  </div>
                  <div></div>
                  <div></div>
                </digi-layout-columns>
                <h2 class="extra-top-padding">Status</h2>
                <p>
                  Vi använder globala standardfärger som informationsfärger.
                  Uppmaning/information (blå), framgång/lyckad interaktion
                  (grön), varning (gul) och fel/systemfel (röd). Syftet med
                  dessa färger är att de skall avvika från Arbetsförmedlingens
                  profilfärger. Dessa färger ska ENDAST användas i syfte att
                  upplysa användaren i en validering/bekräftelse, status i ett
                  ärende eller i viktiga informationsmeddelanden. Profilfärger
                  får inte nyttjas för dessa ändamål, till exempel att den
                  profilgröna används för att validera en checkbox eller
                  liknande.
                </p>
                <digi-layout-columns af-variation={state.responsiveFourColumns}>
                  <div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-info"></div>
                      <div>
                        <span>info/primary</span>
                        <p>#0000FF</p>
                      </div>
                    </div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-info-bg border"></div>
                      <div>
                        <span>info/background</span>
                        <p>#E9E9FF</p>
                      </div>
                    </div>
                  </div>

                  <div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-success"></div>
                      <div>
                        <span>success/primary</span>
                        <p>#009322</p>
                      </div>
                    </div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-success-bg border"></div>
                      <div>
                        <span>success/background</span>
                        <p>#E9F6EC</p>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div></div>
                    <div></div>
                  </div>
                  <div></div>
                </digi-layout-columns>
                <br />
                <digi-layout-columns af-variation={state.responsiveFourColumns}>
                  <div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-warning"></div>
                      <div>
                        <span>warning/primary</span>
                        <p>#FFE200</p>
                      </div>
                    </div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-warning-bg border"></div>
                      <div>
                        <span>warning/background</span>
                        <p>#FFFBDD</p>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-error">
                      </div>
                      <div>
                        <span>error/primary</span>
                        <p>#DA0000</p>
                      </div>
                    </div>
                    <div class="val-item">
                      <div
                        lang="en"
                        class="profile-color-flex-box profile-color-flex-box--af-validate-error-bg border"></div>
                      <div>
                        <span>error/background</span>
                        <p>#FDEDED</p>
                      </div>
                  </div>
                  </div>
                  <div></div>
                  <div></div>
                </digi-layout-columns>
                <h2 class="extra-top-padding">Diagram</h2>
                <digi-layout-columns af-variation={state.responsiveFourColumns}>
                  <div>
                    <p>
                      I första hand används profilfärgerna för att visualisera
                      data i diagram. Om det finns behov av fler nyanser eller
                      färger finns det lite olika vägar att gå. Konsultera då
                      designsystemteamet för att hitta en passande lösning.
                      Ibland har färgen en innebörd i ett diagram, till exempel
                      vid heatmaps eller något som är rätt eller fel. Då kan
                      funktionsfärgerna användas med fördel.
                    </p>
                  </div>
                  <div>
                    <div class="heatmap"></div>
                    <p class="legend-paragraph">
                      Exempel på en <span lang="en">heatmap</span> där tre
                      färger ur funktionspaletten används.
                    </p>
                  </div>
                </digi-layout-columns>
              </div>
            </digi-layout-block>
          </digi-docs-page-layout>
          <digi-layout-block>
            <h2 class="extra-top-padding">
              Namngivning, implementation och designtokens
            </h2>
            <p>
              För att kunna systematisera vår färgpalett och skapa designtokens
              har vi givit varje färg ett unikt namn som identifierar just den
              färgen inklusive dess nyanser (ljust till mörkt). Den profilblåa
              heter till exempel Stratos-500.
            </p>
            <p>
              Varje färg har en skala som går från 0-1000, där 0 innebär att den
              inte innehåller någon färg alls (vit), medan 1000 betyder att det
              är den allra mörkaste (svart). I mitten, det vill säga 500 är den
              primära färgen.
            </p>
            <p>
              Vi använder en separat färgpalett för att guida våra användare
              vilka färger som är specifikt avsedda för olika ändamål. Paletten
              är begränsad där vi har pekat ut exakt var en specifik färg
              används. Ett exempel är <span lang="en">“text-primary”</span> som
              då är kopplad till en specifik färg och toning, i detta fall{' '}
              <span lang="en">“grayscale-900”</span>.
            </p>
            <digi-link-button af-size={LinkButtonSize.MEDIUM} afHref="oversikt">
              Färgtokens översikt
            </digi-link-button>

            <h2 class="extra-top-padding">Webbtillgänglighet med färger</h2>
            <p>
              Arbetsförmedlingens samtliga system omfattas av Lagen om
              tillgänglighet för digital offentlig service, förkortat
              "DOS-lagen" i vardagligt språk. Vi använder riktlinjerna i WCAG
              2.2 AA som verktyg för att upprätthålla detta.
            </p>
            <p>
              Paletten som presenteras i designsystemet är framtagen för att i
              största möjliga mån upprätthålla god kontrast i de avseenden som
              färgerna är ämnade att användas. De kan alltid kombineras i
              oförutsägbara kombinationer. Säkerställ därför att kontrastvärdena
              är godkända med hjälp av lämpligt verktyg för detta, exempelvis{' '}
              <a href="https://webaim.org/resources/contrastchecker/">
                Web AIM - Contrast checker
              </a>
              .
            </p>
            <p>
              WCAG nivå AA kräver ett kontrastförhållande på minst 4.5:1 för
              normal text och 3:1 för stor text, grafik och
              användargränssnittskomponenter (exempelvis ram på inmatningsfält).
              För högsta kontrast WCAG nivå AAA krävs ett kontrastförhållande på
              minst 7:1 för normal text och 4.5:1 för stor text. Stor text
              definieras som 14 punkter (18,66 pixlar) och fetstil eller större.
            </p>

            <div class="splash-flex">
              <div>
                <img src={splashOne} alt="" />
                <div class="splash-icon-paragraph">
                  <digi-icon-check-circle-solid
                    style={{
                      '--digi--icon--width': '14px',
                      '--digi--icon--height': '14px',
                      '--digi--icon--color': '#009322'
                    }}></digi-icon-check-circle-solid>
                  <p class="splash-paragraph">
                    Textfärg på profilgrön håller en godkänd kontrast på 6.05:1.
                  </p>
                </div>
              </div>

              <div>
                <img src={splashTwo} alt="" />
                <div class="splash-icon-paragraph">
                  <digi-icon-validation-error
                    style={{
                      '--digi--icon--width': '14px',
                      '--digi--icon--height': '14px',
                      '--digi--icon--color': '#DA0000'
                    }}></digi-icon-validation-error>
                  <p class="splash-paragraph">
                    Vit text på profilgrön uppnår inte ett godkänt kontrastvärde
                    2.08:1.
                  </p>
                </div>
              </div>

              <div>
                <img src={splashThree} alt="" />
                <div class="splash-icon-paragraph">
                  <digi-icon-check-circle-solid
                    style={{
                      '--digi--icon--width': '14px',
                      '--digi--icon--height': '14px',
                      '--digi--icon--color': '#009322'
                    }}></digi-icon-check-circle-solid>
                  <p class="splash-paragraph">
                    Bakgrundsfärgen ett steg ljusare för att uppnå högkontrast
                    AAA 7.05:1.
                  </p>
                </div>
              </div>
            </div>
            <div class="splash-column-flex-container">
              <div>
                <img src={splashFour} alt="" />
                <div class="splash-icon-paragraph">
                  <digi-icon-info-circle-solid
                    style={{
                      '--digi--icon--width': '14px',
                      '--digi--icon--height': '14px',
                      '--digi--icon--color': '#1616B2'
                    }}></digi-icon-info-circle-solid>
                  <p class="splash-paragraph">
                    Exempel på hur profilgrön upplevs med den vanligaste
                    färgblindheten röd-grön (protanopi/deuteranopi).
                  </p>
                </div>
              </div>

              <div>
                <img src={splashFive} alt="" />
                <div class="splash-icon-paragraph">
                  <digi-icon-info-circle-solid
                    style={{
                      '--digi--icon--width': '14px',
                      '--digi--icon--height': '14px',
                      '--digi--icon--color': '#1616B2'
                    }}></digi-icon-info-circle-solid>
                  <p class="splash-paragraph">
                    Total färgblindhet tritanopi eller svartvit skärm.
                  </p>
                </div>
              </div>
            </div>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            afMarginTop
            afMarginBottom>
            <div>
              <h3>Färger</h3>
              <p>Se relaterade artiklar om färger</p>
              <digi-link-button
                afHref="url"
                af-size="medium"
                af-variation="secondary">
                Grafisk profil - Färger
              </digi-link-button>
            </div>
          </digi-layout-block>
        </digi-typography>
      </host>
    );
  }
}
