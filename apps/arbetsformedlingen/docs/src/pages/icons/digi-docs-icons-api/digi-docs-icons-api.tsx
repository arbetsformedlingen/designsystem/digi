import { Component, h, State } from '@stencil/core';
import { iconsComponent } from '../../../data/_iconsData';
import { LayoutBlockContainer } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-icons-api',
  styleUrl: 'digi-docs-icons-api.scss'
})
export class DigiDocsIconsApi {
  // @State() digiDocsIconstApi: string;

  @State() componentName: string;

  render() {
    return (
      <div class="digi-docs-icons-api">
        <digi-docs-page-layout>
          {iconsComponent.props?.length > 0 && (
            <digi-layout-block
              afMarginBottom
              af-container={LayoutBlockContainer.STATIC}>
              <div>
                <digi-typography>
                  <h2>Attribut</h2>
                  <digi-table af-variation="tertiary">
                    <table>
                      <thead>
                        <tr>
                          <th>Attribut</th>
                          <th>Property</th>
                          <th>Beskrivning</th>
                          <th>Typ</th>
                        </tr>
                      </thead>
                      <tbody>
                        {iconsComponent.props.map((prop) => {
                          return (
                            <tr>
                              <td>
                                <digi-code
                                  af-variation="light"
                                  afCode={`${prop.attr}${
                                    prop.required ? ' (Obligatorisk)' : ''
                                  }`}></digi-code>
                              </td>
                              <td>
                                <digi-code
                                  af-variation="light"
                                  af-language="typescript"
                                  afCode={prop.name}></digi-code>
                              </td>
                              <td>{prop.docs || ''}</td>
                              <td>
                                {prop.type && (
                                  <digi-code
                                    af-variation="light"
                                    af-language="typescript"
                                    afCode={prop.type}></digi-code>
                                )}
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </digi-table>
                </digi-typography>
              </div>
            </digi-layout-block>
          )}
          {iconsComponent.methods?.length > 0 && (
            <digi-layout-block
              afMarginBottom
              af-container={LayoutBlockContainer.STATIC}>
              <div>
                <digi-typography>
                  <h2>Metoder</h2>
                  <digi-table af-variation="tertiary">
                    <table>
                      <thead>
                        <tr>
                          <th>Namn</th>
                          <th>Beskrivning</th>
                          <th>Returtyp</th>
                          <th>Signatur</th>
                        </tr>
                      </thead>
                      <tbody>
                        {iconsComponent.methods.map((method) => {
                          return (
                            <tr>
                              <td>
                                <digi-code
                                  af-variation="light"
                                  afCode={method.name}></digi-code>
                              </td>
                              <td>{method.docs}</td>
                              <td>
                                <digi-code
                                  afCode={method.returns?.type || 'void'}
                                  af-language="typescript"
                                  af-variation="light"></digi-code>
                              </td>
                              <td>
                                <digi-code
                                  afCode={method.signature}
                                  af-language="typescript"
                                  af-variation="light"></digi-code>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </digi-table>
                </digi-typography>
              </div>
            </digi-layout-block>
          )}
          {iconsComponent.styles?.length > 0 && (
            <digi-layout-block
              afMarginBottom
              af-container={LayoutBlockContainer.STATIC}>
              <div>
                <digi-typography>
                  <h2>Tokens</h2>
                  <digi-table af-variation="tertiary">
                    <table>
                      <thead>
                        <tr>
                          <th>Namn</th>
                          <th>Värde</th>
                        </tr>
                      </thead>
                      <tbody>
                        {iconsComponent.styles.map((token) => {
                          return (
                            <tr>
                              <td>
                                <digi-code
                                  afCode={token.name}
                                  af-variation="light"
                                  af-language="css"></digi-code>
                              </td>
                              <td>
                                <digi-code
                                  afCode={token.docs}
                                  af-variation="light"
                                  af-language="css"></digi-code>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </digi-table>
                </digi-typography>
              </div>
            </digi-layout-block>
          )}
          {iconsComponent.docsTags?.filter((tag) => tag.name === 'enums')
            .length > 0 && (
            <digi-layout-block
              afMarginBottom
              af-container={LayoutBlockContainer.STATIC}>
              <div>
                <digi-typography>
                  <h2>Enums</h2>
                  <digi-table af-variation="tertiary">
                    <table>
                      <thead>
                        <tr>
                          <th>Namn</th>
                          <th>Filnamn</th>
                        </tr>
                      </thead>
                      <tbody>
                        {iconsComponent.docsTags
                          .filter((tag) => tag.name === 'enums')
                          .map((enumb) => {
                            return (
                              <tr>
                                <td>
                                  <digi-code
                                    afCode={`enum ${
                                      enumb.text.split(' - ')[0]
                                    }`}
                                    af-variation="light"
                                    af-language="typescript"></digi-code>
                                </td>
                                <td>
                                  <digi-code
                                    afCode={enumb.text.split(' - ')[1]}
                                    af-variation="light"
                                    af-language="bash"></digi-code>
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </digi-table>
                </digi-typography>
              </div>
            </digi-layout-block>
          )}
        </digi-docs-page-layout>
      </div>
    );
  }
}
