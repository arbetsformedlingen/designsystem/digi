import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';
import { InfoCardMultiHeadingLevel } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-about-design-pattern-links',
  styleUrl: 'digi-docs-about-design-pattern-links.scss',
  scoped: true
})
export class AboutDesignPatternLinks {
  Router = router;
  @State() pageName = 'Länkar';

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-design-pattern-links/digi-docs-about-design-pattern-links.tsx">
          <span slot="preamble">
            Länkar är en grundläggande byggsten för att skapa webbplatser. Detta
            designmönster fokuserar på praxis för utformning och hantering av
            länkar, att använda en konsekvent färg- och stilhantering, och
            implementera rätt attribut för digital tillgänglighet. Genom att
            följa dessa riktlinjer kan vi stötta våra användare att enkelt
            navigera och interagera på våra tjänster.
          </span>
          <br />
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h2>Länkar och typografi</h2>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <digi-typography>
                <p>
                  Länkens syfte är att förflytta användaren från aktuell sida
                  till en annan sida. En ankarlänk kan även användas för att
                  förflytta användaren till en annan plats på samma sida. Länkar
                  skall inte användas för att genomföra en åtgärd. Vid dessa
                  situationer skall en knapp användas istället.
                </p>
                <p>
                  Länktexter ska vara begripliga och kunna tolkas även om länken
                  tas ur sitt sammanhang. Länktexter som “Läs mer” och “Klicka
                  här” ska inte användas. I vissa flöden är det ok att använda
                  länktexter som "Tillbaka" men helst ska det framgå vad
                  användaren går tillbaka till.
                </p>
                <p>
                  {' '}
                  Brödsmulor används som primärlösning för att navigera tillbaka
                  på våra webbplatser som hanterar många artikelsidor med
                  skriven information, som till exempel informationssidor med
                  djupa strukturer.
                </p>
                <p>
                  Exempel på situationer då vi inte använder brödsmulor är
                  tjänster som t.ex sökmotorer, enkät-flöden eller
                  ”steg-processer/snurror” som behöver optimera vyer för dess
                  huvudfunktion och/eller har många mobilanvändare kan välja
                  bort brödsmulor för att optimera sina vyer, och jobbar med
                  andra sätt att ta användaren tillbaka.
                </p>
                <p class="design-pattern-link-margin-bottom">
                  Grundutförandet av länkar är satt i vårt typsnitt Open Sans i
                  vikten{' '}
                  <span class="design-pattern-link-semibold" lang="en">
                    semibold
                  </span>
                  . Detta för att synas tillräckligt starkt i mycket varierade
                  grafiska miljöer. Dessutom signalerar länken att den är en
                  länk via färg och tillhörande ikon. Ikonen hjälper användare
                  som inte kan se färg att särskilja objektet som en länk. I
                  löpande texter där vi inte kan använda en ikon, så använder vi
                  färg och <span lang="en">underline</span> istället. Fontens
                  vikt sätts av texten den befinner sig i. Ligger länken t.ex i
                  en brödtext, är länken satt i <span lang="en">regular</span>.
                  Är länken en rubrik i en puff, ärver den vikten av
                  rubrik-stilen (<span lang="en">semibold</span>).
                </p>
              </digi-typography>
            </digi-layout-columns>
          </digi-layout-block>
        </digi-docs-page-layout>

        <digi-layout-block
          af-variation="primary"
          afVerticalPadding
          af-no-gutter
          afMarginTop>
          <hr />

          <digi-typography>
            <h2 class="design-pattern-link-margin-bottom">Länkvarianter</h2>
          </digi-typography>
          <digi-layout-columns
            af-column-gap="32"
            af-variation={state.responsive321Columns}>
            <div>
              <digi-typography>
                <h3 class="example-description-heading">Intern länk</h3>
                <p class="example-description">
                  Intern länk och grundutförandet av länkar är satt i vårt
                  typsnitt Open Sans <span lang="en">semibold</span>. Detta för
                  att synas tillräckligt starkt i mycket varierade grafiska
                  miljöer. Använd komponent{' '}
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-link-internal"></digi-code>{' '}
                  till interna länkar som inte ligger i en brödtext.
                </p>
                <div class="design-pattern-link-link-list">
                  <digi-link-internal afHref="#" hide-visited-color={true}>
                    <span lang="en">Semibold</span> Open Sans
                  </digi-link-internal>
                </div>
              </digi-typography>
            </div>
            <div>
              <digi-typography>
                <h3 class="example-description-heading">Länk i ny flik</h3>
                <p class="example-description">
                  Om en länk leder användaren till en extern sida framgår det av
                  länkens utformning, visuellt med etablerad ikon för extern
                  länk och med webbadress samt text om vart länken går inom
                  parantes, och i kod med relevant alt-text.
                </p>
                <p class="design-pattern-link-margin-bottom example-description">
                  Om intern länk leder användaren till en ny flik eller nytt
                  fönster framgår det visuellt med etablerad ikon för nytt
                  fönster eller flik. I båda fallen ska du använda dig av
                  komponenten
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-link-external"></digi-code>
                  .
                </p>
                <div class="design-pattern-link-link-list">
                  <digi-link-external afHref="#" hide-visited-color={true}>
                    Extern länk (webbadress.se, öppnas i egen flik)
                  </digi-link-external>
                </div>
                <div class="design-pattern-link-link-list">
                  <digi-link-external afHref="#" hide-visited-color={true}>
                    Intern länk (öppnas i egen flik)
                  </digi-link-external>
                </div>
              </digi-typography>
            </div>
            <div>
              <digi-typography>
                <h3 class="example-description-heading">Komponent med ikon</h3>
                <p class="example-description">
                  Om en länk leder användaren till exempel till ett dokument,
                  framgår det tydligt av länkens utformning, både visuellt, i
                  text och i kod. Komponenten kan ändra ikon efter behov. Använd
                  komponenten{' '}
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-link"></digi-code>
                  med ditt val av{' '}
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-icon-*"></digi-code>{' '}
                  för att uppnå detta resultat.
                </p>
                <div class="design-pattern-link-link-list">
                  <digi-link afHref="#" hide-visited-color={true}>
                    <digi-icon-file-pdf></digi-icon-file-pdf>
                    Välj lämplig ikon.pdf
                  </digi-link>
                </div>
                <div class="design-pattern-link-link-list">
                  <digi-link afHref="#" hide-visited-color={true}>
                    <digi-icon-file-word></digi-icon-file-word>
                    Dokument med dolores.wrd
                  </digi-link>
                </div>
                <div class="design-pattern-link-link-list">
                  <digi-link afHref="#" hide-visited-color={true}>
                    <digi-icon-envelope></digi-icon-envelope>
                    Kontakta oss via e-post
                  </digi-link>
                </div>
              </digi-typography>
            </div>
          </digi-layout-columns>

          <div class="design-pattern-link-margin-bottom-48"></div>

          <digi-layout-columns
            af-column-gap="32"
            af-variation={state.responsive321Columns}>
            <digi-typography hideVisitedColor={true}>
              <h3 class="example-description-heading">Länk i löpande text</h3>
              <p class="example-description">
                I löpande texter (där man inte kan använda en ikon), så använder
                vi färg och <span lang="en">underline</span> istället. Fontens
                vikt sätts av texten den befinner sig i. Ligger länken t.ex i en
                brödtext, är länken satt i <span lang="en">regular</span>.
                Ligger länken i vår typografikomponent{' '}
                <digi-code
                  af-lang="en"
                  af-variation="light"
                  af-language="typescript"
                  afCode="digi-typography"></digi-code>{' '}
                ger den länken rätt utseende och beteende.
              </p>
              <p>
                Länk i löpande text, satt i (Open Sans){' '}
                <a href="#">
                  <span lang="en">regular</span> med{' '}
                  <span lang="en">underline</span>{' '}
                </a>
                för att harmonisera med textens grundtypografi och visuella
                uttryck.
              </p>
            </digi-typography>
            <digi-typography hide-visited-color={true}>
              <h3 class="example-description-heading">Länk i ny flik </h3>
              <p class="example-description">
                Om länken öppnar en egen flik på vår egna webbplats skriver vi
                med parantes att den öppnas i egen flik.
              </p>
              <p>
                Länk i löpande text, som är en{' '}
                <a href="#">intern länk som då (öppnas i egen flik)</a> satt i
                Open Sans <span lang="en">regular</span> för att harmonisera med
                textens grundtypografi.
              </p>
            </digi-typography>
            <digi-typography hide-visited-color={true}>
              <h3 class="example-description-heading">
                Länk i ny flik till annan webbplats
              </h3>
              <p class="example-description">
                Om länken öppnas i egen flik på annan webbplats, skriver vi ut
                webbadressen inom parantes enligt exemplet.
              </p>
              <p>
                Länk i löpande text, satt i{' '}
                <a href="#">
                  regular med underline (webbadress.se, öppnas i egen flik)
                </a>{' '}
                för att harmonisera med textens grund-typografi.
              </p>
            </digi-typography>
          </digi-layout-columns>

          <hr />

          <digi-typography>
            <h2 class="design-pattern-link-margin-bottom" lang="en">
              States
            </h2>
          </digi-typography>
          <digi-layout-columns
            af-column-gap="32"
            af-variation={state.responsive321Columns}>
            <div>
              <digi-typography>
                <p class="design-pattern-link-margin-bottom example-description">
                  Länk med tillhörande ikon har inte{' '}
                  <span lang="en">underline</span> i vilande läge. Ikonen
                  hjälper användaren att förstå att det är en länk hen tittar
                  på, utöver den fastställda länkfärgen.
                </p>
                <div class="design-pattern-link-margin-bottom design-pattern-link-link-list">
                  <img
                    aria-hidden="true"
                    src="/assets/images/pattern/design-monster-lankar/link-default.svg"
                  />
                </div>
                <br />
                <p class="example-description design-pattern-link-margin-bottom">
                  Länk med tillhörande ikon har enbart{' '}
                  <span lang="en">underline</span> på{' '}
                  <span lang="en">hover</span>. Om länken ligger på mörk
                  bakgrund, har den samma beteende med undantaget att länken är
                  vit.
                </p>
                <div class="design-pattern-link-link-list">
                  <img
                    aria-hidden="true"
                    src="/assets/images/pattern/design-monster-lankar/link-hover.svg"
                  />
                </div>
              </digi-typography>
            </div>
            <div>
              <digi-typography>
                <p class="design-pattern-link-margin-bottom example-description">
                  Om du vill använda inställningen så att länken får utseendet
                  som besökt länk ska den använda denna färg och utseende.
                  Använd besökt länk med måtta då det får puffar, länkade
                  rubriker osv att se trasiga ut. Undvik besökt-länk på mörkt
                  bakgrund.
                </p>
                <div class="design-pattern-link-margin-bottom design-pattern-link-link-list">
                  <img
                    aria-hidden="true"
                    src="/assets/images/pattern/design-monster-lankar/link-visited.svg"
                  />
                </div>
                <br />
                <p class="design-pattern-link-margin-bottom example-description">
                  Om en besökt länk används ser den ut såhär på{' '}
                  <span lang="en">hover</span>. Om länken ligger på mörk
                  bakgrund, bör ”besökt-länk-färg” undvikas pga kontrastproblem.
                </p>
                <div class="design-pattern-link-link-list">
                  <img
                    aria-hidden="true"
                    src="/assets/images/pattern/design-monster-lankar/link-hover-visited.svg"
                  />
                </div>
              </digi-typography>
            </div>
            <div></div>
          </digi-layout-columns>

          <hr />
          <digi-typography>
            <h2 class="design-pattern-link-margin-bottom">Länkade rubriker</h2>
          </digi-typography>
          <digi-layout-columns
            af-column-gap="32"
            af-variation={state.responsive321Columns}>
            <div>
              <digi-typography hide-visited-color={true}>
                <p class="example-description">
                  En länkad rubrik ärver vikten visuellt av fonten från
                  rubrikfonten. Rubriken är länkblå och{' '}
                  <span lang="en">underline</span> i vilande läge. Använd
                  typografikomponenten{' '}
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-typography"></digi-code>
                  för detta behov.
                </p>
                <h3 class="example-description-heading" lang="en">
                  Hover
                </h3>
                <p class="example-description">
                  Rubriken behåller sin länkfärg, men{' '}
                  <span lang="en">hover</span> förstärks med en tyngre{' '}
                  <span lang="en">underline</span>. Rubriker har{' '}
                  <span class="design-pattern-link-semibold">inte</span>{' '}
                  ”besökt-länk-färg” efter användning.
                </p>
                <div>
                  <h1 style={{ 'margin-top': '0' }}>
                    <a href="#">
                      Länkad rubrik i <span lang="en">semibold</span>
                    </a>
                  </h1>
                </div>
              </digi-typography>
            </div>
            <div>
              <digi-typography>
                <h3 class="example-description-heading">
                  Exempel med länkad rubrik
                </h3>
                <p class="example-description">
                  Ett exempel med länkad rubrik i ett infokort. Länken ärver
                  vikten visuellt från infokortets rubrikstorlek. Exemplet nedan
                  görs med hjälp av komponenten{' '}
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-info-card-multi"></digi-code>
                  .
                </p>

                <div class="design-pattern-link-card-multi design-pattern-link-margin-bottom">
                  <digi-info-card-multi
                    afHeading="Länkrubrik i Infokort"
                    afHeadingLevel={InfoCardMultiHeadingLevel.H4}
                    af-type="entry"
                    af-link-href="/">
                    <p class="example-description">
                      Länk i ett infokort till fördjupad information om kortets
                      valda ämne.
                    </p>
                  </digi-info-card-multi>
                </div>
              </digi-typography>
            </div>
            <div></div>
          </digi-layout-columns>

          <hr />

          <digi-layout-columns
            af-column-gap="32"
            af-variation={state.responsive321Columns}>
            <div>
              <digi-typography>
                <h2 class="design-pattern-link-margin-bottom">Knapplänkar</h2>

                <p class="design-pattern-link-margin-bottom example-description">
                  Knapplänk är en länk som är stylad som en knapp. Den används
                  för att framhäva en länk extra tydligt, som till exempel vid
                  ingångar till våra digitala tjänster. Använd vår komponent{' '}
                  <digi-code
                    af-lang="en"
                    af-variation="light"
                    af-language="typescript"
                    afCode="digi-link-button"></digi-code>{' '}
                  för detta behov. Exempel på <strong> primär</strong> knapp på{' '}
                  <strong>ljus</strong> bakgrund.{' '}
                </p>
                <div class="design-pattern-link-margin-bottom">
                  <digi-link-button
                    afHref={'#'}
                    af-size="medium"
                    af-variation="primary">
                    En primär knapplänk
                  </digi-link-button>
                </div>
                <br />
                <p class="example-description">
                  Knapplänk är en länk som är stylad som en knapp. Den används
                  för att framhäva en länk extra tydligt, som till exempel vid
                  ingångar till digitala tjänster. Exempel på{' '}
                  <strong>sekundär</strong> knapplänk på <strong>ljus</strong>{' '}
                  bakgrund.
                </p>
                <div class="design-pattern-link-margin-bottom">
                  <digi-link-button
                    afHref={'#'}
                    af-size="medium"
                    af-variation="secondary">
                    En sekundär knapplänk
                  </digi-link-button>
                </div>
              </digi-typography>
            </div>
            <div>
              <digi-typography>
                <h2 class="design-pattern-link-margin-bottom">
                  På mörk bakgrund
                </h2>
                <p class="design-pattern-link-margin-bottom example-description">
                  Knapplänk är en länk som är stylad som en knapp. Den används
                  för att framhäva en länk extra tydligt, Exempel på{' '}
                  <strong> primär</strong> knapplänk på <strong>mörk</strong>{' '}
                  bakgrund.{' '}
                </p>
                <div class="design-pattern-link-margin-bottom">
                  <digi-link-button
                    afHref={'#'}
                    af-size="medium"
                    af-variation="primary-alt">
                    Primär mot mörk bakgrund
                  </digi-link-button>
                </div>
                <br />
                <p class="example-description">
                  Knapplänk är en länk som är stylad som en knapp. Den används
                  för att framhäva en länk extra tydligt. Exempel på{' '}
                  <strong>sekundär </strong>
                  knapplänk på <strong>mörk</strong> bakgrund.{' '}
                </p>
                <div class="design-pattern-link-margin-bottom">
                  <digi-link-button
                    afHref={'#'}
                    af-size="medium"
                    af-variation="secondary-alt">
                    Sekundär mot mörk bakgrund
                  </digi-link-button>
                </div>
              </digi-typography>
            </div>
            <div></div>
          </digi-layout-columns>

          <hr />

          <digi-layout-columns
            af-column-gap="32"
            af-variation={state.responsive321Columns}>
            <digi-typography>
              <h2 class="design-pattern-link-margin-bottom">Brödsmulor</h2>
              <p class="design-pattern-link-margin-bottom example-description">
                Brödsmulor används som primärlösning för att navigera tillbaka
                på våra webbplatser som hanterar många artikelsidor och
                informationssidor. Men vissa tjänster som behöver optimera sina
                vyer och flöden mår ofta bättre av att välja bort brödsmulor.
              </p>
              <div class="design-pattern-link-margin-bottom">
                <digi-navigation-breadcrumbs afCurrentPage="Nuvarande sida">
                  <a href="/">Start</a>
                  <a href="/contact-us">Undersida 1</a>
                  <a href="#">Undersida 2</a>
                </digi-navigation-breadcrumbs>
              </div>
            </digi-typography>
            <div>
              <digi-typography hide-visited-color={true}>
                <h2 class="design-pattern-link-margin-bottom">Undantag</h2>
                <p class="example-description">
                  Som undantag går även att använda en länk med enbart{' '}
                  <span lang="en">underline</span>
                  som fristående länk, utan att den ligger inne i löpande text,
                  men använd hellre det tänkta grundutförandet med chevron-ikon
                  och <span lang="en">semibold</span>.
                </p>
                <div class="design-pattern-link-margin-bottom">
                  <a href="#" style={{ 'font-weight': '600' }}>
                    Undantag med fristående länk utan ikon med{' '}
                    <span lang="en">underline</span> och fetare linje på{' '}
                    <span lang="en">hover</span>.
                  </a>
                </div>
              </digi-typography>
            </div>
            <div></div>
          </digi-layout-columns>
        </digi-layout-block>
        <digi-layout-block
          af-variation="secondary"
          afVerticalPadding
          af-no-gutter
          afMarginTop>
          <digi-typography hide-visited-color={true}>
            <h2>En länk är ett löfte</h2>
            <p class="design-pattern-link-margin-bottom">
              När vi skapar en länk, lovar vi att den kommer att leda användaren
              till en specifik destination eller resurs. En bruten länk bryter
              besökarens flöde och får användaren att tvivla på webbplatsens
              pålitlighet och kvalitet. Det är viktigt att länkar alltid är
              aktuella och fungerar som förväntat för att behålla en positiv
              användarupplevelse. Då det är svårt att undvika att länkar ofta
              blir inaktuella över tid, så det är viktigt att regelbundet
              övervaka och uppdatera dem. Detta gäller särskilt för länkar till
              externa resurser, där målwebbplatser kan ändras eller tas bort. Se
              till att ersätta brutna länkar med relevanta, fungerande
              alternativ eller meddelanden som hjälper användaren att hitta vad
              de söker.{' '}
              <a href="/designmonster/felmeddelandesidor">
                Läs mer i designmönster för felmeddelanden
              </a>
              .
            </p>{' '}
            <p>
              Använd verktyg och tjänster som kan automatisera länkövervakningen
              och meddela dig om brutna länkar omedelbart. Om du gör ändringar i
              URL-strukturen eller flyttar resurser på din webbplats, se till
              att informera användarna om dessa ändringar och vidta åtgärder för
              att säkerställa att gamla länkar fortfarande fungerar, till
              exempel genom att använda omdirigeringar. Om flera personer
              arbetar med webbplatsinnehåll och länkar, se till att alla är
              medvetna om vikten av att upprätthålla fungerande länkar.
            </p>
            <h2>Tillgänglighet</h2>
            <p class="design-pattern-link-margin-bottom">
              En länktext ska vara tydlig så att användaren förstår vart en länk
              leder även om länken lyfts ur sitt sammanhang. Undvik länkar som
              endast heter "Tillbaka" eller "Läs mer". Skriv länktexten så kort
              som möjligt. Behöver du förtydliga länkens syfte eller ge länken
              ett unikt namn så kan attributet aria-label användas. Det är svårt
              att säga när attributet behövs, men om flera länkar i t.ex. ett
              sökresultat är identiska så kan aria-label användas. Attributet
              aria-label används av skärmläsare och andra hjälpmedel. Attributet
              skriver över länktexten. Därför ska texten i aria-labeln inledas
              med den synliga texten. Därefter läggs förtydligandet till. Ett
              exempel kan vara om användaren söker jobb och får ett antal
              träffar. Det finns möjlighet att spara intressanta träffar. Den
              synliga texten är ”spara annons” och i aria labeln står ”spara
              annons” + annonsens namn. Om det finns länkade bilder är det
              viktigt att skilja på alt-text och länknamn. Alt-texten ska
              beskriva bilden och länknamnet beskriva länkens syfte. Länknamn
              som innehåller alt-texter blir både långa och otydliga.{' '}
            </p>
            <h2>Säkerhet och länkar</h2>
            <p>
              Se till att länkar inom ditt användargränssnitt använder säker
              HTTPS-anslutning. Detta skyddar användarnas data genom kryptering
              och visar att din webbplats är pålitlig. HSTS är en{' '}
              <span lang="en">header</span> som påtvingar krypterad HTTPS för
              att förhindra attacker som degraderar till okrypterad
              kommunikation. Läggs antingen in i webbservern eller
              lastbalanseraren, läs mer på den{' '}
              <a
                href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security"
                rel="noreferrer noopener"
                target="_blank">
                officiella dokumentationen på Mozilla (developer.mozilla.org,
                öppnas i egen flik)
              </a>
              .
            </p>
            <p>
              En annan bra resurs att rekommendera är{' '}
              <a
                rel="noreferrer noopener"
                href="https://portswigger.net/web-security/all-topics"
                target="_blank">
                Portswiggers artiklar om webbsäkerhet (portswigger.net, öppnas i
                egen flik)
              </a>
              .
            </p>
            <p>
              Försök att undvika att inkludera långa och komplexa URL-parametrar
              i dina länkar. De kan vara svåra att läsa och skapa förvirring
              bland användarna. Undvik också känslig data som parameter i URL
              (även kallad <span lang="en">query</span>) t ex{' '}
              <span lang="en">client secret</span> även om det är krypterat.
              Detta eftersom känslig data hamnar då i logg-filer, webbläsare,
              historik och <span lang="en">referer header</span> får åtkomst
              till denna.
            </p>
            <p>
              Använd <span lang="en">"noopener"</span> (en del av HTML och
              JavaScript som effektivt förebygger{' '}
              <span lang="en">"tabnapping"</span>-attacker och andra oönskade
              beteenden som kan uppstå) när du länkar till externa webbplatser,
              eftersom du inte har kontroll över säkerheten på dessa platser.
              Genom att öppna länkar i nya flikar eller fönster kan du hjälpa
              användarna att hålla sin nuvarande session intakt och samtidigt
              undvika potentiella faror.
            </p>
            <p>
              Designsystemets länk-komponenter lägger automatiskt till
              <span lang="en">rel="noopener noreferrer"</span> om man anger
              target till <span lang="en">"_blank"</span>. Tänk på att detta
              gäller inte om du själv skapar upp ett a-element.
            </p>{' '}
            <p class="design-pattern-link-margin-bottom">
              Det är viktigt att förstå att webbsäkerhet är en kontinuerlig
              process, och hoten utvecklas ständigt. Därför är det avgörande att
              hålla din webbplats och dess komponenter uppdaterade och att vara
              medveten om de senaste säkerhetshoten och bästa praxis för att
              förhindra dem. Glöm inte ansluta till IT-säks{' '}
              <strong>sårbarhetsscanner</strong> för att upptäcka komponenter
              som behöver uppdateras.{' '}
            </p>
          </digi-typography>
        </digi-layout-block>
      </host>
    );
  }
}
