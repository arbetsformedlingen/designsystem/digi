import { Component, h, State } from '@stencil/core';

import { TokenFormat } from '../../../components/design-tokens/token-format.enum';

import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-colors-tokens',
  styleUrl: 'digi-docs-colors-tokens.scss',
  scoped: true
})
export class DigiDocsColorsTokens {
  @State() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;

  tokenFormatChangeHandler(e) {
    this.tokenFormat = e.detail.target.value;
  }

  render() {
    return (
      <div class="digi-docs-colors-tokens">
        <digi-layout-block afMarginTop afMarginBottom>
          <digi-typography-preamble>
            Tokens är ett gemensamt språk för att beskriva struktur, kod och
            design. Här hittar du ett urval av färger som är kopplade till den
            grafiska profilen. Det vill säga färger som förekommer i
            komponenter, grafik, illustrationer och text.
          </digi-typography-preamble>
          <p>
            Det är viktigt att färgerna används på rätt sätt. Om du är osäker på
            hur du skall använda en färg diskutera gärna det med en insatt
            kollega eller hör av dig till oss i designsystemet så kan vi guida
            er.
          </p>
          <p>Kontrastvärdet vid varje färg har genererats via verktyget WebAIM Contrast Checker, som testar kontraster för normal och stor text samt grafiska objekt och användargränssnittskomponenter. Om kontrasten på ett värde inte är uppfylld inom alla dessa kategorier så kommenteras detta med "ej samtliga krav uppfyllda".</p>
             <digi-link
                af-target="_blank"
                af-variation="small"
                afHref="https://webaim.org/resources/contrastchecker/">
                <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                WebAIM Contrast Checker (öppnas i egen flik)
              </digi-link>
          {/* <p>
						Har ni kopplat designsystemet Digi UI-kit till ert projekt, så har ni
						motsvarande färgpalett speglad direkt i Figma.
					</p> */}
          {/* <div class="digi-docs-colors-tokens__filter-wrapper">
            <div class="digi-docs-colors-tokens__filter-format">
              <digi-form-select
                afLabel="Format"
                afId="tokenFormat"
                afDisableValidation={true}
                onAfOnChange={(e) => this.tokenFormatChangeHandler(e)}>
                <option value="custom-property">CSS-variabel </option>
                <option value="sass">Sass </option>
              </digi-form-select>
            </div>
          </div> */}
        </digi-layout-block>

        <digi-layout-block
          class="digi-docs-colors-tokens__bottom"
          afVariation={LayoutBlockVariation.SECONDARY}>
          <div class="digi-docs-colors-tokens__tokens-wrapper">
            <h2>Profilfärger</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'stratos-500',
                  heading: 'stratos-500',
                  subHeading: '#00005A',
                  displayColor: '#00005A',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '18.29:1'
                    }
                  ],
                  description:
                    'Primärfärg för varumärket. Används i grafik, bakgrund, avdelare, knappar, text och diagram med mera.',
                  tokenName: '--digi--stratos-500'
                },
                {
                  id: 'stratos-600',
                  heading: 'stratos-600',
                  subHeading: '#000048',
                  displayColor: '#000048',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '19.2:1'
                    }
                  ],
                  description:
                    'Används i väldigt få undantagsfall där man vill uppnå en skuggning av primärfärgen, exempelvis nyanser i diagram. Ett alternativ till svart bakgrund.',
                  tokenName: '--digi--stratos-600'
                },
                {
                  id: 'stratos-400',
                  heading: 'stratos-400',
                  subHeading: '#33337B',
                  displayColor: '#33337B',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '11.05:1'
                    }
                  ],
                  description:
                    'Används i väldigt få undantagsfall där man vill uppnå en ljus ton av primärfärgen, exempelvis nyanser i diagram.',
                  tokenName: '--digi--stratos-400'
                },
                {
                  id: 'stratos-100',
                  heading: 'stratos-100',
                  subHeading: '#DDDDE9',
                  displayColor: '#DDDDE9',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '1.34:1 (ej samtliga krav uppfyllda)'
                    }
                  ],
                  description:
                    'Används som en toning av primärfärgen i vissa typer av illustrationer. Bör inte användas i grafik, bakgrund eller komponenter.',
                  tokenName: '--digi--stratos-100'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'leaf-500',
                  heading: 'leaf-500',
                  subHeading: '#95C23E',
                  displayColor: '#95C23E',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '2.08:1 (ej samtliga krav uppfyllda)'
                    }
                  ],
                  description:
                    'Sekundärfärg för varumärket. Används i mindre volymer för att skapa en kontrast mot den blå. Används framförallt i grafik då den har låg kontrast och inte skall användas i text eller som bärande del i navigationselement.',
                  tokenName: '--digi--leaf-500'
                },
                {
                  id: 'leaf-600',
                  heading: 'leaf-600',
                  subHeading: '#779B32',
                  displayColor: '#779B32',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '3.22:1 (ej samtliga krav uppfyllda)'
                    }
                  ],
                  description:
                    'En något mörkare variant av profilgröna som uppnår godkänd kontrast för grafik. Kan användas i navigeringselement men saknar fortfarande tillräcklig hög kontrast för text.',
                  tokenName: '--digi--leaf-600'
                },
                {
                  id: 'leaf-400',
                  heading: 'leaf-400',
                  subHeading: '#AACE65',
                  displayColor: '#AACE65',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '1.79:1 (ej samtliga krav uppfyllda)'
                    }
                  ],
                  description: 'Används i grafik och illustrationer.',
                  tokenName: '--digi--leaf-400'
                },
                {
                  id: 'leaf-200',
                  heading: 'leaf-200',
                  subHeading: '#DDEBC1',
                  displayColor: '#DDEBC1',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '1.2:1 (ej samtliga krav uppfyllda)'
                    }
                  ],
                  description:
                    'Används som bakgrund för olika element samt i grafik och illustrationer. Markör i menykomponent.',
                  tokenName: '--digi--leaf-200'
                },
                {
                  id: 'leaf-100',
                  heading: 'leaf-100',
                  subHeading: '#DDEBC1',
                  displayColor: '#DDEBC1',
                  pairs: [
                    {
                      key: 'Kontrastvärde mot vit:',
                      value: '1.07:1 (ej samtliga krav uppfyllda)'
                    }
                  ],
                  description: 'Används i grafik och illustrationer.',
                  tokenName: '--digi--leaf-100'
                }
              ]}></digi-docs-color-token-cards>

            <h2>Komplementfärger</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'water-500',
                  heading: 'water-500',
                  subHeading: '#BDD3E7',
                  displayColor: '#BDD3E7',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.54:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Hoverfärg för fyllnad av sekundär knapp.',
                  tokenName: '--digi--water-500'
                },
                {
                  id: 'water-700',
                  heading: 'water-700',
                  subHeading: '#0058A3',
                  displayColor: '#0058A3',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '7.18:1' }],
                  description: 'Hoverfärg för fyllnad av sekundär knapp.',
                  tokenName: '--digi--water-700'
                },
                {
                  id: 'water-400',
                  heading: 'water-400',
                  subHeading: '#DFECF8',
                  displayColor: '#DFECF8',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.2:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Används i väldigt få undantagsfall där man vill uppnå en ljus ton av primärfärgen, exempelvis nyanser i diagram.',
                  tokenName: '--digi--water-400'
                },
                {
                  id: 'water-200',
                  heading: 'water-200',
                  subHeading: '#F1F8FE',
                  displayColor: '#F1F8FE',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.07:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Används i grafik och illustrationer.',
                  tokenName: '--digi--water-200'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'jade-500',
                  heading: 'jade-500',
                  subHeading: '#058470',
                  displayColor: '#058470',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '4.62:1' }],
                  description:
                    'Används som bakgrund i informationsgrafik, samt i diagram. Denna färg har tidigare används som “attentionfärg” men är numera ersatt med den röda statusfärgen.',
                  tokenName: '--digi--jade-500'
                },
                {
                  id: 'jade-700',
                  heading: 'jade-700',
                  subHeading: '#035548',
                  displayColor: '#035548',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '8.76:1 ' }],
                  description: 'Används för diagram.',
                  tokenName: '--digi--jade-700'
                },
                {
                  id: 'jade-200',
                  heading: 'jade-200',
                  subHeading: '#7FC1B6',
                  displayColor: '#7FC1B6',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '2.05:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Används i grafik och illustrationer.',
                  tokenName: '--digi--jade-200'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'rose-500',
                  heading: 'rose-500',
                  subHeading: '#D43372',
                  displayColor: '#D43372',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '4.65:1' }],
                  description:
                    'Används som bakgrund i informationsgrafik, samt i diagram. Denna färg har tidigare används som “attentionfärg” men är numera ersatt med den röda statusfärgen.',
                  tokenName: '--digi--rose-500'
                },
                {
                  id: 'rose-700',
                  heading: 'rose-700',
                  subHeading: '#882149',
                  displayColor: '#882149',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '8.89:1' }],
                  description: 'Används för diagram.',
                  tokenName: '--digi--rose-700'
                },
                {
                  id: 'rose-200',
                  heading: 'rose-200',
                  subHeading: '#E997B7',
                  displayColor: '#E997B7',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '2.19:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Används i grafik och illustrationer.',
                  tokenName: '--digi--rose-200'
                }
              ]}></digi-docs-color-token-cards>
            <h2>Neutrala toner</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'grayscale-1000',
                  heading: 'grayscale-1000',
                  subHeading: '#000000',
                  displayColor: '#000000',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '21:1' }],
                  description:
                    'Används som bakgrund på playtjänster som kräver helt svart bakgrund samt där högsta kontrast måste uppnås mot vit.',
                  tokenName: '--digi--grayscale-1000'
                },
                {
                  id: 'grayscale-900',
                  heading: 'grayscale-900',
                  subHeading: '#333333',
                  displayColor: '#333333',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '12.63:1' }],
                  description:
                    'Textfärg med gråton för att inte skapa en allt för skarp kontrast mot vit som då annars kan vara jobbig för ögonen då den visas på en dator eller mobilskärm. ',
                  tokenName: '--digi--grayscale-900'
                },
                {
                  id: 'grayscale-800',
                  heading: 'grayscale-800',
                  subHeading: '#5C5C5C',
                  displayColor: '#5C5C5C',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '6.68:1' }],
                  description: 'Alternativ textfärg.',
                  tokenName: '--digi--grayscale-800'
                },
                {
                  id: 'grayscale-700',
                  heading: 'grayscale-700',
                  subHeading: '#757575',
                  displayColor: '#757575',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '4.6:1' }],
                  description:
                    'Den ljusaste textfärgen som uppfyller godkända kontrastkrav. Eventuellt för separata stycken eller bildtexter.',
                  tokenName: '--digi--grayscale-700'
                },
                {
                  id: 'grayscale-600',
                  heading: 'grayscale-600',
                  subHeading: '#ACACAC',
                  displayColor: '#ACACAC',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '2.27:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Inget specificerat användningsområde.',
                  tokenName: '--digi--grayscale-600'
                },
                {
                  id: 'grayscale-500',
                  heading: 'grayscale-500',
                  subHeading: '#CACACA',
                  displayColor: '#CACACA',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.63:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Inget specificerat användningsområde.',
                  tokenName: '--digi--grayscale-500'
                },
                {
                  id: 'grayscale-400',
                  heading: 'grayscale-400',
                  subHeading: '#DDDDDD',
                  displayColor: '#DDDDDD',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.35:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Inget specificerat användningsområde.',
                  tokenName: '--digi--grayscale-400'
                },
                {
                  id: 'grayscale-300',
                  heading: 'grayscale-300',
                  subHeading: '#E4E4E4',
                  displayColor: '#E4E4E4',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.27:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Inget specificerat användningsområde.',
                  tokenName: '--digi--grayscale-300'
                },
                {
                  id: 'grayscale-200',
                  heading: 'grayscale-200',
                  subHeading: '#EDEDED',
                  displayColor: '#EDEDED',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.17:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Sekundär bakgrundsfärg för att bryta av mot vit.',
                  tokenName: '--digi--grayscale-200'
                },
                {
                  id: 'grayscale-100',
                  heading: 'grayscale-100',
                  subHeading: '#F4F4F4',
                  displayColor: '#F4F4F4',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.09:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Primär bakgrundsfärg för att bryta av mot vit.',
                  tokenName: '--digi--grayscale-100'
                },
                {
                  id: 'grayscale-0',
                  heading: 'grayscale-0',
                  subHeading: '#FFFFFF',
                  displayColor: '#FFFFFF',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Vit färg, används bland annat som bakgrund och text mot mörk bakgrund.',
                  tokenName: '--digi--grayscale-0'
                }
              ]}></digi-docs-color-token-cards>

            <h2>Funktionsfärger</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'sapphire-500',
                  heading: 'sapphire-500',
                  subHeading: '#1616B2',
                  displayColor: '#1616B2',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '11.72:1' }],
                  description:
                    'Länkfärg för text samt tillhörande ikon i komponent.',
                  tokenName: '--digi--sapphire-500'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'amethyst-500',
                  heading: 'amethyst-500',
                  subHeading: '#822998',
                  displayColor: '#822998',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '7.72:1' }],
                  description: 'Länkfärg vid besökt textlänk.',
                  tokenName: '--digi--amethyst-500'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'pear-500',
                  heading: 'pear-500',
                  subHeading: '#B7EE4A',
                  displayColor: '#B7EE4A',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.36:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Hoverfärg för fyllnad av primär alternativ länkknapp.',
                  tokenName: '--digi--pear-500'
                },
                {
                  id: 'pear-600',
                  heading: 'pear-600',
                  subHeading: '#A7D943',
                  displayColor: '#A7D943',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.17:1 (ej samtliga krav uppfyllda)' }],
                  description: '',
                  tokenName: '--digi--pear-600'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'water-500',
                  heading: 'water-500',
                  subHeading: '#BDD3E7',
                  displayColor: '#BDD3E7',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.54:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Hoverfärg för fyllnad av sekundär knapp.',
                  tokenName: '--digi--water-500'
                },
                {
                  id: 'water-700',
                  heading: 'water-700',
                  subHeading: '#0058A3',
                  displayColor: '#0058A3',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '7.18:1' }],
                  description: 'Hoverfärg för fyllnad av primär knapp.',
                  tokenName: '--digi--water-500'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'leaf-500',
                  heading: 'leaf-500',
                  subHeading: '#95C23E',
                  displayColor: '#95C23E',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '2.08:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Markeringsfärg i meny och flikar.',
                  tokenName: '--digi--leaf-500'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'indigo-500',
                  heading: 'indigo-500',
                  subHeading: '#1616B2',
                  displayColor: '#1616B2',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '11.72:1' }],
                  description:
                    'Länkfärg för text samt tillhörande ikon i komponent.',
                  tokenName: '--digi--indigo-500'
                },
                {
                  id: 'indigo-700',
                  heading: 'indigo-700',
                  subHeading: '#0058A3',
                  displayColor: '#0058A3',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '7.18:1' }],
                  description: 'Hoverfärg för fyllnad av primär knapp.',
                  tokenName: '--digi--indigo-700'
                },
                {
                  id: 'indigo-200',
                  heading: 'indigo-200',
                  subHeading: '#E9E9FF',
                  displayColor: '#E9E9FF',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.19:1 (ej samtliga krav uppfyllda)' }],
                  description: '',
                  tokenName: '--digi--indigo-200'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'forest-500',
                  heading: 'forest-500',
                  subHeading: '#009322',
                  displayColor: '#009322',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '4.04:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Bakgrundsfärg samt grafik i komponenter som vill förmedla en lyckad interaktion. ',
                  tokenName: '--digi--forest-500'
                },
                {
                  id: 'forest-700',
                  heading: 'forest-700',
                  subHeading: '#02703C',
                  displayColor: '#02703C',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '6.2:1' }],
                  description:
                    'Bakgrundsfärg samt grafik i komponenter som vill förmedla en lyckad interaktion. En dovare variant som inte tar lika mycket “attention” och samspelar med profilfärgerna. ',
                  tokenName: '--digi--forest-700'
                },
                {
                  id: 'forest-200',
                  heading: 'forest-200',
                  subHeading: '#E9F6EC',
                  displayColor: '#E9F6EC',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.11:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Bakgrundsfärg i inmatningsfält.',
                  tokenName: '--digi--forest-200'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'ruby-500',
                  heading: 'ruby-500',
                  subHeading: '#DA0000',
                  displayColor: '#DA0000',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '5.27:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Bakgrundsfärg samt grafik i komponenter som vill förmedla att något är fel.',
                  tokenName: '--digi--ruby-500'
                },
                {
                  id: 'ruby-700',
                  heading: 'ruby-700',
                  subHeading: '#C7311A',
                  displayColor: '#C7311A',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '5.4:1' }],
                  description:
                    'Bakgrundsfärg samt grafik i komponenter som vill förmedla att något är fel. En dovare variant som inte tar lika mycket “attention” och samspelar med profilfärgerna.',
                  tokenName: '--digi--ruby-700'
                },
                {
                  id: 'ruby-200',
                  heading: 'ruby-200',
                  subHeading: '#FDEDED',
                  displayColor: '#FDEDED',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.13:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Bakgrundsfärg i inmatningsfält.',
                  tokenName: '--digi--ruby-200'
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'amber-500',
                  heading: 'amber-500',
                  subHeading: '#FFE200',
                  displayColor: '#FFE200',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.3:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Bakgrundsfärg samt grafik i komponenter som vill förmedla en varning.',
                  tokenName: '--digi--amber-500'
                },
                {
                  id: 'amber-900',
                  heading: 'amber-900',
                  subHeading: '#664D00',
                  displayColor: '#664D00',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '7.99:1' }],
                  description:
                    'Textfärg för sekundär statusindikator. Linje på varningsmeddelande.',
                  tokenName: '--digi--amber-900'
                },
                {
                  id: 'amber-700',
                  heading: 'amber-700',
                  subHeading: '#FFC108',
                  displayColor: '#FFC108',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.62:1 (ej samtliga krav uppfyllda)' }],
                  description:
                    'Bakgrundsfärg samt grafik i komponenter som vill förmedla en varning. En dovare variant som inte tar lika mycket “attention” och samspelar med profilfärgerna.',
                  tokenName: '--digi--amber-700'
                },
                {
                  id: 'amber-200',
                  heading: 'amber-200',
                  subHeading: '#FFF398',
                  displayColor: '#FFF398',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.13:1 (ej samtliga krav uppfyllda)' }],
                  description: 'Bakgrund för sekundär statusindikator.',
                  tokenName: '--digi--amber-200'
                },
                {
                  id: 'amber-50',
                  heading: 'amber-50',
                  subHeading: '#FFFBDD',
                  displayColor: '#FFFBDD',
                  pairs: [{ key: 'Kontrastvärde mot vit:', value: '1.04:1 (ej samtliga krav uppfyllda) ' }],
                  description: 'Bakgrundsfärg i inmatningsfält.',
                  tokenName: '--digi--amber-50'
                }
              ]}></digi-docs-color-token-cards>
          </div>
        </digi-layout-block>
      </div>
    );
  }
}