import { Component, h, State } from '@stencil/core';
import state from '../../store/store';
import { router } from '../../global/router';
import { LayoutBlockContainer } from '@digi/arbetsformedlingen';
import { match, Route } from 'stencil-router-v2';

@Component({
  tag: 'digi-docs-colors',
  styleUrl: 'digi-docs-colors.scss',
  scoped: true
})
export class DigiDocsColors {
  Router = router;

  @State() isMobile: boolean;
  @State() isOpen: boolean;
  @State() activeTab: string;

  getTabId(): string {
    const tabName = state.router.activePath;

    switch (tabName) {
      case '/grafisk-profil/farger/farg-tokens':
        return 'farg-tokens';
      case '/grafisk-profil/farger/tryck-farger':
        return 'tryck-farger';
      case '/grafisk-profil/farger/oversikt':
        return 'oversikt'
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    this.Router.push(`${state.routerRoot}grafisk-profil/farger/${tag}`);
  }

  navigateTo(tabId: string) {
    this.Router.push(`${state.routerRoot}grafisk-profil/farger/${tabId}`);
  }

  componentWillLoad() {
    this.activeTab = this.getTabId();
  }

  get tabs() {
    return [
      { id: 'oversikt', title: 'Översikt' },
      { id: 'farg-tokens', title: 'Färgtokens' },
      { id: 'tryck-farger', title: 'Tryckfärger' }
    ];
  }

  render() {
    return (
      <div class="digi-docs-colors">
        <digi-docs-page-layout>
          <digi-layout-block>
            <digi-typography-heading-jumbo af-text="Färger"></digi-typography-heading-jumbo>
            <digi-tablist
              afTabs={this.tabs}
              afActiveTab={this.activeTab}
              onAfTabChange={(event) => {
                const tab = event.detail;
                this.navigateTo(tab.id);
              }}>
              {this.tabs.map((tab) => (
                <digi-tablist-panel tab={tab.id} />
              ))}
            </digi-tablist>
          </digi-layout-block>

          <digi-layout-block af-container={LayoutBlockContainer.NONE}>
            <this.Router.Switch>
              <Route
                path={match('/grafisk-profil/farger/farg-tokens')}
                render={() => <digi-docs-colors-tokens />}
              />
              <Route
                path={match('/grafisk-profil/farger/tryck-farger')}
                render={() => <digi-docs-colors-print />}
              />
              <Route
                path={/\/grafisk-profil\/farger\/?/}
                render={() => <digi-docs-colors-description />}
              />
            </this.Router.Switch>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
