import { Component, getAssetPath, h, Prop } from '@stencil/core';
import { RouterHistory } from '@stencil/router';
import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-colors-description',
  styleUrl: 'digi-docs-colors-description.scss',
  scoped: true
})
export class DigiDocsColorsDescription {
  @Prop() history: RouterHistory;

  render() {
    return (
      <div class="digi-docs-colors-description">
        <digi-layout-block
          class="digi-docs-colors-description__layout-block"
          afMarginTop
          afMarginBottom>
          <digi-typography-preamble>
            Arbetsförmedlingens färger är en viktig pusselbit i vår identitet.
            Vi använder oss av flertalet olika färger för specifika ändamål. Här
            presenterar vi de olika indelningarna samt hur och när du skall
            använda dem.
          </digi-typography-preamble>
          <br />
          <p>
            Våra färgkartor skiljer sig något mellan tryck och webb eftersom
            behoven skiftar och tekniken är olika. Det kan handla om nyanser,
            namnsättning och användning. I flikmenyn ovan kan du välja mellan
            färgtokens som ger dig allt du behöver veta om färgerna i digitala
            sammanhang. Under tryckfärger hittar du färgkartor och vägledning
            för tryck.
          </p>
          <p>
            Arbetsförmedlingens samtliga system omfattas av lagen om
            tillgänglighet för digital offentlig service. Vi använder
            riktlinjerna i WCAG 2.1 AA som verktyg för att upprätthålla detta.
            Därför är det av stor vikt att du som använder våra färger gör det
            enligt de färgkartor vi presenterar här. För att upprätthålla rätt
            kontraster och nyanser så att de är tillgängliga.
          </p>
          <h2>Profilfärger</h2>
          <p>
            Arbetsförmedlingens profilfärger är blått och grönt. Tillsammans med
            den vita färgen står de för trygghet (profilblå) och växtkraft
            (profilgrön). Dessa färger ska genomsyra all vår kommunikation. Vi
            kan också komplettera med ett flertal toningar och skuggningar av
            dessa tre färger för att kunna anpassa vid olika situationer.
          </p>
          <digi-media-image
            afUnlazy
            afSrc={getAssetPath('/assets/images/farger-profil-grafik.png')}
            afAlt="Exempel på användning av profilfärger"
            class="digi-docs-colors-description__img"></digi-media-image>
          <h2>Komplementfärger</h2>
          <p>
            Arbetsförmedlingens komplementerande fokusfärger är rosa och
            turkost. De används för att markera och framhäva något. Tex. viktig
            information, webbpuffar eller interaktion. Men de ska användas med
            stor försiktighet och inte vara dominerande.
          </p>
          <digi-media-image
            afUnlazy
            afSrc={getAssetPath('/assets/images/farger-komplement-grafik.png')}
            afAlt="Exempel på användning av komplementfärger"
            class="digi-docs-colors-description__img"></digi-media-image>
          <h2>
            Markeringsfärger <span lang="en">(call to action)</span>
          </h2>
          <p>
            Dessa används endast på webben för att markera en knapp eller länk.
            Samt de olika tillstånd som de kan befinna sig i. Samt länkfärgerna
            som endast används på en ljus yta. En starkare nyans av profilgrön
            med hover används då en knapp eller länk ligger på en profilblå
            bakgrund.{' '}
          </p>
          <digi-media-image
            afUnlazy
            afSrc={getAssetPath('/assets/images/farger-markering-grafik.png')}
            afAlt="Exempel på användning av markeringsfärger"
            class="digi-docs-colors-description__img"></digi-media-image>

          <h2>Neutral skala</h2>
          <p>
            Vi använder en gråskala från svart till vitt. Mörkaste grå används i
            första hand i brödtext eller annan text i mindre storlek på webben.
            Svart används för brödtext för tryck. Ljusgrått används som
            alternativ bakgrundston och ersätter i dessa fall vit bakgrund.
            Alternativa bakgrundsfärger är ljus blå och grön.
          </p>
          <digi-media-image
            afUnlazy
            afSrc={getAssetPath('/assets/images/farger-neutral-grafik.png')}
            afAlt="Exempel på användning av neutrala skalan"
            class="digi-docs-colors-description__img"></digi-media-image>

          <h2>Färger i grafik och illustrationer</h2>
          <p>
            I vår infografik används förutom svart och vit framförallt
            profilgrönt i sin originalnyans samt en tonad ljusare variant.
            Diagram använder sig av en mängd olika nyanser av både profil- och
            fokusfärger beroende på i vilket sammanhang och betydelsen av
            tillgänglighet. Tex. för att det skall vara synbara skillnader även
            för dem med färgblindhet. Våra illustrationer livas upp med tonade
            färgnyanser av profilblå och profilgrön samt fokusrosa.
          </p>
          <p>
            För komponenter används en mängd olika nyanser och originalfärger.
            Beroende på vilken effekt man vill uppnå eller i vilket sammanhang
            de förekommer.{' '}
          </p>
          <digi-media-image
            afUnlazy
            afSrc={getAssetPath(
              '/assets/images/farger-illustration-grafik.png'
            )}
            afAlt="Exempel på användning av färger i grafik och illustrationer"
            class="digi-docs-colors-description__img"></digi-media-image>
        </digi-layout-block>

        <digi-layout-block
          class="digi-docs-colors-description__layout-block"
          afVariation={LayoutBlockVariation.SECONDARY}
          afVerticalPadding>
          <h2>Funktionsfärger*</h2>
          <p>
            Vi använder globala standardfärger som informationsärger.
            Uppmaning/information (blå), framgång/lyckad interaktion (grön),
            varning (gul) och fel/systemfel (röd). Syftet med dessa färger är
            att de skall avvika från Arbetsförmedlingens profilfärger. *Dessa
            skall <strong>INTE</strong> användas på annat sätt än att förmedla
            information i en interaktion.
          </p>
          <digi-media-image
            afUnlazy
            afSrc={getAssetPath('/assets/images/farger-funktion-grafik.png')}
            afAlt="Exempel på användning av funktionsfärger"
            class="digi-docs-colors-description__img"></digi-media-image>
        </digi-layout-block>
      </div>
    );
  }
}
