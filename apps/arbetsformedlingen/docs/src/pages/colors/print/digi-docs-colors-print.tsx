import { Component, h, State } from '@stencil/core';

import { TokenFormat } from '../../../components/design-tokens/token-format.enum';

import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-colors-print',
  styleUrl: 'digi-docs-colors-print.scss',
  scoped: true
})
export class DigiDocsColorsPrint {
  @State() tokenFormat: TokenFormat = TokenFormat.RGB;

  render() {
    return (
      <div class="digi-docs-colors-print">
        <digi-layout-block afMarginTop afMarginBottom>
          <digi-typography-preamble digi-typography-preamble>
            För tryck använder vi färre varianter av färger än för webben. Det
            är framförallt profilfärgerna (med toningar), komplementfärger och
            en gråskala.
          </digi-typography-preamble>
          <br />
          <p>
            Dessa beskrivs på olika sätt beroende på vilken tryckteknik som
            används.
          </p>
          <p>
            CMYK står för cyan (blått), magenta (rött), yellow (gult) och key
            (svart). CMYK använd vid fyrfärgstryck i annonser, broschyrer,
            affischer med mera. Toningar anges som procentangivelse för varje
            färg.
          </p>
          <p>
            RGB står för Röd, Grön och Blå. Det är RGB-färger som återges på en
            dataskärm och anges med ett värde mellan 0 och 255.
          </p>
          <p>
            Viktigt att tänka på är att den profilblå innehåller mer än 100
            procent färg och kan ibland upplevas som svart. Beroende på underlag
            så kan den behövas justeras något.
          </p>
        </digi-layout-block>
        <digi-layout-block afVariation={LayoutBlockVariation.SECONDARY}>
          <div class="digi-docs-colors-print__colors-wrapper">
            <h2>Profilfärger</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'print-profile-blue',
                  heading: 'Profilblå',
                  description:
                    'Primärfärg för varumärket. Används i grafik, bakgrund, avdelare, text, diagram.',
                  displayColor: 'var(--digi--stratos-500)',
                  pairs: [
                    { key: 'CMYK', value: '100-95-0-45' },
                    { key: 'RGB (print)', value: '0-0-90' }
                  ]
                }
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'print-profile-green',
                  heading: 'Profilgrön',
                  description:
                    'Sekundärfärg för varumärket. Används i grafik, bakgrund, avdelare, diagram.',
                  displayColor: '#94c33f',
                  pairs: [
                    { key: 'CMYK', value: '50-0-90-0' },
                    { key: 'RGB (print)', value: '126-193-61' }
                  ]
                }
              ]}></digi-docs-color-token-cards>
            <h2>Komplementfärger</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'print-profile-turquoise-20',
                  heading: 'Turkos',
                  displayColor: '#058470',
                  description:
                    'Primär komplementfärg. Bakgrund, grafik, komponenter.',
                  pairs: [
                    { key: 'CMYK', value: '100-15-65-4' },
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
              ]}></digi-docs-color-token-cards>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'print-profile-pink',
                  heading: 'Rosa',
                  displayColor: '#d43273',
                  description:
                  'Sekundär komplementfärg. Bakgrund, grafik, komponenter.',
                  pairs: [
                    { key: 'CMYK', value: '0-89-23-0' },
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
              ]}></digi-docs-color-token-cards>
            <h2>Bakgrundsplattor</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'print-profile-green-20',
                  heading: 'Profilgrön 20%',
                  displayColor: '#ebf3da',
                  pairs: [
                    { key: 'CMYK', value: '11-0-21-0' },
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-green-40',
                  heading: 'Profilgrön 40%',
                  displayColor: '#d5e6b1',
                  pairs: [
                    { key: 'CMYK', value: '11-0-21-0' },
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-green-10',
                  heading: 'Profilgrön 10%',
                  displayColor: '#f4f8eb',
                  pairs: [
                    { key: 'CMYK', value: '-' },
                    { key: 'RGB (print)', value: '-' }
                  ]
                }
              ]}></digi-docs-color-token-cards>
            <h2>Gråskala</h2>
            <digi-docs-color-token-cards
              colors={[
                {
                  id: 'print-profile-black-100',
                  heading: 'Svart 100%',
                  displayColor: '#000000',
                  pairs: [
                    { key: 'CMYK', value: '100-0-0-0' },
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-black-85',
                  heading: 'Svart 85%',
                  displayColor: '#262626',
                  pairs: [
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-black-65',
                  heading: 'Svart 65%',
                  displayColor: '#595959',
                  pairs: [
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-black-40',
                  heading: 'Svart 40%',
                  displayColor: '#999999',
                  pairs: [
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-black-20',
                  heading: 'Svart 20%',
                  displayColor: '#CCCCCC',
                  pairs: [
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
                {
                  id: 'print-profile-black-10',
                  heading: 'Svart 10%',
                  displayColor: '#E6E6E6',
                  pairs: [
                    { key: 'RGB (print)', value: '-' }
                  ]
                },
              ]}></digi-docs-color-token-cards>
          </div>
        </digi-layout-block>
      </div>
    );
  }
}
