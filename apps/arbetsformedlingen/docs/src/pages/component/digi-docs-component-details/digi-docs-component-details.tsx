import { Component, Prop, h, Watch, State } from '@stencil/core';

@Component({
  tag: 'digi-docs-component-details',
  styleUrl: 'digi-docs-component-details.scss',
  scoped: true
})
export class DigiDocsComponentDetails {
  @State() componentDetailTag: string;

  @Prop() componentTag: string;

  @Watch('componentTag')
  setComponentDetailTag() {
    this.componentDetailTag = `${this.componentTag}-details`;
  }

  componentWillLoad() {
    this.setComponentDetailTag();
  }

  render() {
    return (
      <div class="digi-docs-component-details">
        <this.componentDetailTag></this.componentDetailTag>
      </div>
    );
  }
}
