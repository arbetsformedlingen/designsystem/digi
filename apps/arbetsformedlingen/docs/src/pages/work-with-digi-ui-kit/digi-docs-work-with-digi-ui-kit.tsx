import { Component, getAssetPath, h, State } from '@stencil/core';
import {
  InfoCardHeadingLevel,
  InfoCardVariation,
  LayoutColumnsVariation
} from '@digi/arbetsformedlingen';
import { router } from '../../global/router';

@Component({
  tag: 'digi-docs-work-with-digi-ui-kit',
  styleUrl: 'digi-docs-work-with-digi-ui-kit.scss',
  scoped: true
})
export class DigiDocsWorkWithDigiUiKit {
  @State() pageName = 'Jobba med Digi UI Kit';
  Router = router;
  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }
  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/work-with-digi-ui-kit/digi-docs-work-with-digi-ui-kit.tsx">
          <span slot="preamble">
            Om du designar gränssnitt för Arbetsförmedlingens digitala tjänster
            är Digi UI Kit (“UI-kittet”) grunden för det arbetet. I UI-kittet
            hittar du färdiga komponenter som uppfyller kraven på tillgänglighet
            och är baserade på Arbetsförmedlingens grafiska profil. Digi Core är
            en spegling av motsvarande komponenter, fast i kod. Tillsammans
            utgör UI-kittet och Digi Core kärnan i designsystemets
            komponentbibliotek.
          </span>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Bakgrund och syfte</h2>
              <p>
                UI-kittet består av Arbetsförmedlingens samlade komponenter som
                tagits fram och byggts upp under flera år. Syftet med UI-kittet
                är att förenkla för team som bygger tjänster till den svenska
                arbetsmarknaden. Det första UI-kittet var uppbyggt i Sketch och
                Abstract, men sedan hösten 2024 är det Figma som gäller.
              </p>
              <p>
                UI-kittet består av komponenter som de flesta tjänster har nytta
                av. Därför innehåller UI-kittet inte alla komponenter som finns
                i Arbetsförmedlingens tjänster.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Utveckla UI-kittet tillsammans</h2>
              <p>
                Vi gör kontinuerligt förbättringar av såväl UI-kittet som
                kodkomponenterna. Förbättringarna kommer från att vi lärt oss
                något nytt om användbarhet eller tillgänglighet som kräver en
                förändring av en befintlig komponent. Det kan också vara
                feedback från er som använder komponenterna och fått feedback
                från era användare som kräver justeringar av funktioner,
                tillgänglighet eller design.
              </p>
              <p>
                Alla uppdateringar av UI-kittet får du tillgång till automatiskt
                via Figma.{' '}
              </p>
              <h2>Licenser</h2>
              <p>
                För att använda UI-kittet som designer behöver du ha tillgång
                till Figma. Det finns som desktop-applikation eller i
                webbläsaren. Licensfrågor hanteras internt av Licensstöd. Har du
                problem med behörigheter kan du kontakta oss.
              </p>
              <h3>Tillgång till Figma</h3>
              <p>
                Instruktioner för hur du som medarbetar på arbetsförmedlingen
                får tillgång till Figma finns på designsystemets intranät grupp.
              </p>
              <digi-link
                af-target="_blank"
                af-variation="small"
                afHref="https://start.arbetsformedlingen.se/grupper/grupp/designsystem/designverktyg">
                <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                Instruktioner för Figma (öppnas i egen flik)
              </digi-link>
              <h3>Typsnitt</h3>
              <p>
                Du behöver också ladda ner typsnittet OpenSans för att
                typografin skall stämma i UI-kittets designfiler.
              </p>
              <digi-link
                afHref="https://space.arbetsformedlingen.se/sites/designpaarbetsformedlingen/Delade%20dokument/open-sans.zip?csf=1&e=FBgguD"
                afDownload={'Open Sans'}
                afTarget={'_blank'}>
                <digi-icon-download></digi-icon-download>
                Ladda ner OpenSans (Zip, 2,53 MB)
              </digi-link>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Hitta UI-kittet</h2>
              <p>
                UI-kittet finns per automatik tillgängligt i dina design-filer
                när dessa ligger inom ramen för vår organisation. UI-kittet är
                uppdelat i två bibliotek, ett ikon-bibliotek och ett
                komponentbibliotek.
              </p>
              <p>
                Är du en extern leverantör eller aktör, så kan du hitta
                arbetsförmedlingens UI kit på Figmas{' '}
                <span lang="eng">community</span> sida.
              </p>
              <digi-link-external
                afHref="https://www.figma.com/@sweden"
                af-target="_blank"
                af-variation="primary">
                UI-kittet i Figma (figma.com, öppnas i egen flik)
              </digi-link-external>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>UI-kittets uppbyggnad</h2>
              <p>
                Komponenterna i UI-kittet kan konfigureras för att visa
                komponentens olika versioner.
              </p>
              <digi-media-image
                afWidth="500"
                afUnlazy
                afSrc={getAssetPath('/assets/images/ui-kit/props.png')}
                afAlt="Bild som visar panelen där man konfigurera en markerad komponent."></digi-media-image>
              <p>
                Markerar du komponenten i Figma så dyker det upp en panel till
                höger där dom vanligaste konfigurationerna finns representerade.
                Dessa är samma i kodbiblioteket som i UI-kittet, så vi får ett
                så likt språk som möjligt mellan design och utveckling.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Att använda komponenterna i UI-kittet</h2>
              <h3>Komponenter</h3>
              <p>
                När du öppnar ditt projekt finner du UI-kittet under{' '}
                <span lang="en">assets</span> i vänstra panelen. Du kan lägga
                till komponenterna direkt därifrån, eller om du vill se dem
                först så kan du välja att se en överblick med alla komponenter i
                översiktsvyn.
              </p>
              <digi-media-image
                afWidth="500"
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/ui-kit/using-components.png'
                )}
                afAlt="Lägger till en komponent"></digi-media-image>
              <p>
                I bildexemplet ovan så visas hur du förhandsgranskar en
                komponent genom att klicka på den. Där kan du börja justera den
                till ditt behov, eller lägga till den i ditt projekt genom att
                klicka på “<span lang="en">Insert instance</span>”-knappen. Du
                kan också dra ut komponenten ur listan och justera den efter du
                placerat ut den.
              </p>
              <digi-info-card
                class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
                afHeading="Undvik"
                afHeadingLevel={InfoCardHeadingLevel.H4}
                afVariation={InfoCardVariation.SECONDARY}>
                <p>
                  Bryt inte loss komponenter och gör egna modifieringar. Det
                  leder till att om komponenten kommer i en ny variation i
                  UI-kittet så kommer din implementation inte att uppdateras
                  till den senaste versionen och du missar således att ta del av
                  förändringen. Behöver du bygga custom-komponenter till just
                  din applikation så bygg hellre upp en egen utav UI-kittets
                  minsta beståndsdelar.
                </p>
              </digi-info-card>
              <h3>
                <span lang="eng">Modes</span>
              </h3>
              <p>
                Typografi eller komponenter kan se olika beroende på
                skärmstorlek. För att enklare ta fram design för dessa
                skärmstorlekar så kan du ställa in <span lang="eng">mode</span>{' '}
                på en ram/<span lang="eng">frame</span>. Det gör att
                komponenterna från UI-kittet anpassar sitt utseende till det{' '}
                <span lang="eng">mode</span>:et.
              </p>
              <digi-media-image
                afWidth="500"
                afUnlazy
                afSrc={getAssetPath('/assets/images/ui-kit/using-modes.png')}
                afAlt="applicerar modes på en frame"></digi-media-image>
              <p>
                För att kunna välja <span lang="eng">mode</span> på en{' '}
                <span lang="eng">frame</span>, måste det finnas en komponent i
                den valda framen. I exemplet ovan finns bara responsiva lägen
                som alternativ, men det går att kombinera flera olika{' '}
                <span lang="eng">modes</span> i en och samma{' '}
                <span lang="eng">frame</span>. Till exempel kan det bli aktuellt
                med olika färg-teman, ljust och mörkt tema. Designsystemet
                kommer isåfall att lägga till ett nytt mode för ljust och mörkt
                tema.
              </p>
              <digi-info-card
                class="digi-docs-work-with-digi-ui-kit__infocard-narrow"
                afHeading="Tänk på"
                afHeadingLevel={InfoCardHeadingLevel.H4}
                afVariation={InfoCardVariation.SECONDARY}>
                <p>
                  Undvik att justera <span lang="eng">mode</span> på enskilda
                  komponenter i en vy. <span lang="eng">Modes</span>
                  är menade att appliceras på det yttersta lagret så det kan
                  påverka innehållet rätt. Det vill säga, justera bara mode på
                  din <span lang="eng">frame</span>.
                </p>
              </digi-info-card>
              <h3>Texter och textstilar</h3>
              <p>
                För att få rätt utseende och spacing på ett textblock i din
                design så kan du göra på två olika sätt. Du kan lägga till
                aktuell typografi som en komponent från UI-kittet.
              </p>
              <digi-media-image
                afWidth="500"
                afUnlazy
                afSrc={getAssetPath('/assets/images/ui-kit/text-component.png')}
                afAlt="Text som komponenter"></digi-media-image>
              <p>
                Du kan också markera text-objektet och i högermenyn ändra till
                utseendet du vill ha, se exempel nedan.
              </p>
              <digi-media-image
                afWidth="500"
                afUnlazy
                afSrc={getAssetPath('/assets/images/ui-kit/text-styles.png')}
                afAlt="Text med en text stil applicerade"></digi-media-image>
              <p>
                Med text som komponent så ingår utrymme under själva texten likt
                hur det gör i koden. Det kommer inte med textstilar, men dessa
                kan du använda om du snabbt vill komma igång.
              </p>
              <h2>Överblick av komponenter</h2>
              <p>
                Under rubriken Komponenter här på designsystemets
                dokumentationssida får du en bra överblick och indexering över
                vilka komponenter som finns tillgängliga. Här får du också
                möjlighet att testa komponenten live, se varianter, eventuella
                riktlinjer och designmönster.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Lägga till nya komponenter </h2>
              <p>
                Nya komponenter läggs till allt eftersom behov uppstår och görs
                tillsammans med teamet som har behovet. Vi gör det med hjälp av
                vår samarbetsmodell.
              </p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Komplettera, justera eller har du hittat en bugg?</h2>
              <p>
                Saknar du en färg, storlek eller någon annan del i en komponent
                och vill bidra med en komplettering eller justering? eller vill
                du dela med dig om hur du löst en bugg eller annan fråga? <br />
                Skriv i gruppen på intranätet eller kontakta oss via vår
                funktionsbrevlåda.
              </p>
              <digi-link
                afHref={`mailto:designsystem@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan`}
                class="digi-docs__accessibility-link">
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Mejla designsystem@arbetsformedlingen.se
              </digi-link>
              <br />
              <digi-link
                af-target="_blank"
                afHref="https://start.arbetsformedlingen.se/grupper/grupp/designsystem/">
                <digi-icon-external-link-alt slot="icon"></digi-icon-external-link-alt>
                Designsystemet på intranätet (öppnas i egen flik)
              </digi-link>
            </article>
          </digi-layout-container>
          <digi-layout-block af-variation="symbol" af-container="static">
            <digi-info-card
              afHeading="Fördjupande information och resurser för dig som jobbar med Arbetsförmedlingens design"
              afHeadingLevel={InfoCardHeadingLevel.H2}>
              <digi-layout-columns
                af-variation={LayoutColumnsVariation.THREE}
                class="digi-docs-work-with-digi-ui-kit__columns">
                <div>
                  <h3>Grafiska profilen</h3>
                  <p>
                    Lär dig mer om varumärket Arbetsförmedlingen. Logotyp,
                    färger och typografi. Här finner du också riktlinjer om
                    grafik, illustrationer och bildspråk.
                  </p>
                  <digi-link-internal
                    afHref="/grafisk-profil/om-grafisk-profil"
                    af-target="_self"
                    af-variation="large"
                    onAfOnClick={(e) => this.linkHandler(e)}>
                    Vår grafiska profil
                  </digi-link-internal>
                </div>
                <div>
                  <h3>Tillgänglighet</h3>
                  <p>
                    Vilka krav behöver Arbetsförmedlingen uppfylla? Verktyg för
                    att kunna sammanställa en tillgänglighetsredogörelse.
                  </p>
                  <digi-link-internal
                    afHref="/tillganglighet-och-design/om-digital-tillganglighet"
                    af-target="_self"
                    af-variation="large"
                    onAfOnClick={(e) => this.linkHandler(e)}>
                    Om tillgänglighet
                  </digi-link-internal>
                </div>
                <div>
                  <h3>Språk</h3>
                  <p>
                    Hur skriver man klarspråk? Hur namnger vi våra tjänster?
                    Microcopy och översättningar.
                  </p>
                  <digi-link-internal
                    afHref="/sprak/digi-docs-about-copy-languages"
                    af-target="_self"
                    af-variation="large"
                    onAfOnClick={(e) => this.linkHandler(e)}>
                    Om vårt språk
                  </digi-link-internal>
                </div>
              </digi-layout-columns>
            </digi-info-card>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
