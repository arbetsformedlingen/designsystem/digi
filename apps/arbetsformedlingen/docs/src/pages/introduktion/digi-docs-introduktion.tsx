import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-introduktion',
  styleUrl: 'digi-docs-introduktion.scss',
  scoped: true
})
export class Introduktion {
  render() {
    return (
      <div class="digi-docs-introduktion">
        <digi-docs-page-layout af-edit-href="pages/introduktion/digi-docs-introduktion.tsx">
          <digi-layout-block afMarginBottom>
            <digi-typography-heading-jumbo af-text="Introduktion"></digi-typography-heading-jumbo>
            <digi-typography-preamble>
              Arbetsförmedlingens designsystem finns för att utvecklare,
              designers och produktledare ska kunna skapa bättre tjänster för
              den svenska arbetsmarknaden.
            </digi-typography-preamble>
            <digi-typography>
              <div>
                <p>
                  Designsystemet innehåller teknik, design och verktyg som
                  används av utvecklingsteam i deras arbete. Genom att samla
                  resurser och kunskap på ett ställe underlättas utvecklingen av
                  produkter och tjänster, vilket resulterar i en mer
                  kostnadseffektiv process. Det gemensamma förvaltandet frigör
                  tid för utvecklingsteam, vilket i sin tur möjliggör fokus på
                  att förstå användarnas problem som skapar värdefulla
                  lösningar.
                </p>
                <p>
                  En viktig del av Arbetsförmedlingens arbete är att driva
                  utvecklingen utifrån uppdrag och behov, detsamma gäller för
                  teamet bakom vårt designsystem. Därför arbetar vi nära många
                  team och har kompetenser som sträcker sig över flera
                  discipliner - arkitektur, frontend, UX- och grafisk design,
                  tillgänglighet och användbarhet, vilket gör att många aspekter
                  av produktutvecklingen beaktas.
                </p>
                <p>
                  För att lyckas behöver vi attrahera de mest kunniga inom
                  teknik, design och produktutveckling och nyttja kraften med
                  öppen källkod.
                </p>
                <p>
                  Arbetsförmedlingens designsystem består av de här delarna:
                </p>

                <digi-list>
                  <li>
                    En väletablerad samarbetsmodell: Vi ska utveckla
                    designsystemet tillsammans och för detta har byggstenen en
                    väletablerad samarbetsmodell som gör det enkelt att både
                    komma igång och hitta sätt att bidra in till designsystemet.
                  </li>
                  <li>
                    Designmönster: Design- och interaktionsmönster för hur
                    gränssnitt bör utformas för att lösa specifika behov.
                  </li>
                  <li>
                    Grafisk profil: Verktyg och riktlinjer för hur du jobbar med
                    Arbetsförmedlingens varumärke och grafiska profil.
                  </li>
                  <li>
                    Komponentbibliotek: Komponentbiblioteket är uppdelat i
                    design och kod.
                  </li>
                  <li>
                    Språk och översättningar: Riktlinjer för hur vi förhåller
                    oss och arbetar med andra språk än svenska.
                  </li>
                  <li>
                    Praktisk copy: Systemtexter och mikrocopy kommuniceras i
                    komponenter och designmönster.
                  </li>
                  <li>
                    Tillgänglighetsriktlinjer: Kunskapsstöd och verktyg för hur
                    produkter skapar lösningar för alla.
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">
                      Dokumentationsplattform:{' '}
                    </span>
                    Designsystemets webbplats och dokumentationsplattform på{' '}
                    <digi-link
                      af-variation="small"
                      afHref="https://designsystem.arbetsformedlingen.se/"
                      af-href="https://designsystem.arbetsformedlingen.se/">
                      designsystem.arbetsformedlingen.se
                    </digi-link>
                  </li>
                </digi-list>
              </div>
            </digi-typography>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
