import { Component, h } from '@stencil/core';
import { router } from '../../../global/router';
import arkitekturbild from '../../../assets/images/startpage/arkitekturbild.svg';

@Component({
  tag: 'digi-docs-home-architecture',
  styleUrl: 'digi-docs-home-architecture.scss',
  scoped: true
})
export class Architecture {
  Router = router;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-layout-container class="digi-docs-home-architecture-container">
          <digi-typography>
            <h2 >Så fungerar designsystemet</h2>
            <figure>
              <div class="architect-image-container">
                <img class="architect-image" src={arkitekturbild} alt="" />
              </div>
              <figcation class="screen-reader">
                Illustrationen visar hur designsystemet fungerar och flödet av
                komponenter och resurser.
                <p>
                  I GitLab har vi våran <span lang={'en'}>Issue-tracker</span>.
                  Där samlar vi behov och förändringar, samt följer upp vårt
                  arbete.
                </p>
                <p>
                  <span lang={'en'}>Designtokens</span> definieras av teamet och
                  används i både Figma och våra webbkomponenter.
                </p>
                <p>
                  I Figma använder vi <span lang={'en'}>designtokens</span> när
                  vi bygger vårt UI-kit.
                </p>
                <p>Stencil används för att skapa webbkomponenter.</p>
                <p>
                  Webbkomponenterna paketeras och distribueras som npm-paket.
                </p>
                <p>
                  All källkod för designsystemet och dess dokumentationswebb
                  finns i GitLab som öppen källkod.
                </p>
                <p>
                  UI-kit och webbkomponenter kan användas i ramverk som Angular,
                  React och liknande,
                </p>
              </figcation>
            </figure>
          </digi-typography>
        </digi-layout-container>
      </host>
    );
  }
}
