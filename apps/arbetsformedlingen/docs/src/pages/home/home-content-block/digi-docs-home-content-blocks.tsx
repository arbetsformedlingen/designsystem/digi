import { Component, getAssetPath, h } from '@stencil/core';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-docs-home-content-blocks',
  styleUrl: 'digi-docs-home-content-blocks.scss',
  scoped: true
})
export class HomeLinkBlocks {
  Router = router;

  //linkClickHandler(e) {
  //	e.detail.preventDefault();
  //	this.Router.push(e.target.afHref);
  //}

  render() {
    return (
      <host>
        <digi-layout-block af-variation="secondary">
          <div class="digi-docs-home-content-blocks digi-docs-home-content-blocks--media-right">
            <div class="digi-docs-home-content-blocks__media">
              <digi-media-image
                class="digi-docs-home-content-blocks__media-image"
                afUnlazy
                afSrc={getAssetPath('/assets/images/collage-accessibility.svg')}
                afAlt="En illustration är en visualisering i form av en ritning, skiss, målning, fotografi eller annat verk som belyser eller dikterar information med hjälp av en"></digi-media-image>
            </div>
            <div class="digi-docs-home-content-blocks__text">
              <h2>
                Utforska vad vi menar med <br /> inkluderande design
              </h2>
              <p class="digi-docs-home-content-blocks__preamble">
                Designsystemet finns för dig som vill ta fram tillgängliga och
                användbara digitala produkter. Det bidrar till att vi
                tillsammans uppfyller aktuella riktlinjer och lagkrav gällande
                tillgänglighet. Arbetet med tillgänglighet sker integrerat med
                allt annat vi gör, genom användbarhetsforskning,
                användarcentrerad design, tillgängliga komponenter och
                användningstester med olika användare. Vi hjälper gärna dig som
                utvecklingsteam med en kognitiv genomgång som är en lätt,
                effektiv och billig metod att utvärdera användbarhet och
                tillgänglighet i din produkt.
              </p>
              <digi-link-internal
                afHref="/tillganglighet-och-design/om-digital-tillganglighet"
                af-variation="primary">
                Till tillgänglighetsmatrisen
              </digi-link-internal>
              <br />
              <digi-link-internal
                afHref="/tillganglighet-och-design/kognitiv-genomgang"
                af-variation="primary">
                Boka Kognitiv genomgång
              </digi-link-internal>
            </div>
          </div>
        </digi-layout-block>
        <div class="digi-docs-home-quote-block">
          <digi-layout-block>
            <digi-quote-multi-container
              af-heading="Det här säger våra användare"
              af-heading-level="h2">
              <digi-quote-single
                af-quote-author-name="Krister Dackland"
                af-quote-author-title="IT-direktör"
                af-variation="primary">
                Designsystemet är ett sätt för oss på Arbetsförmedlingen att
                hjälpa utvecklingsteam att skapa tillgängliga och användbara
                tjänster för arbetsgivare, arbetssökande och medarbetare. Att
                kunden känner igen sig och att tjänsterna är enkla att använda,
                oavsett förmåga, är en viktig bidragande faktor för att vi ska
                lyckas med vårt uppdrag.
              </digi-quote-single>
              <digi-quote-single
                af-quote-author-name="Johan Magnusson"
                af-quote-author-title="UX-designer"
                af-variation="primary">
                Designsystemet är en pålitlig partner för UX-designers, erbjuder
                effektiva verktyg för användarvänliga gränssnitt. Det förbättrar
                arbetsprocessen, minskar repetition och sparar tid, ökar
                produktiviteten. Med en stark grund kan UX-designern fokusera på
                att leverera en sammanhängande användarupplevelse som uppfyller
                krav och förväntningar.
              </digi-quote-single>
              <digi-quote-single
                af-quote-author-name="Anna Sondér"
                af-quote-author-title="Tillgänglighetsspecialist"
                af-variation="primary">
                I Designsystemet är tillgänglighet en naturlig del av
                designprocessen. Tillgänglighet handlar om att redan från början
                skapa inkluderande design. Målet är att alla ska ha samma
                möjlighet att använda webbplatser och digitala tjänster.
                Designsystemet stöttar dagligen utvecklingsteam i deras
                tillgänglighetsarbete.
              </digi-quote-single>
            </digi-quote-multi-container>
          </digi-layout-block>
        </div>

        <digi-layout-block af-variation="secondary">
          <div class="digi-docs-home-content-blocks digi-docs-home-content-blocks--media-right">
            <div class="digi-docs-home-content-blocks__media ">
              <digi-media-image
                class="digi-docs-home-content-blocks__media-image"
                afUnlazy
                afSrc={getAssetPath(
                  '/assets/images/Startsida-oppenkallkod.png'
                )}
                afAlt="Kollegor på kontor jobbar tillsammans"></digi-media-image>
            </div>
            <div class="digi-docs-home-content-blocks__text">
              <h2>Vem som helst kan vara med och bidra i öppna system</h2>
              <p class="digi-docs-home-content-blocks__preamble">
                Arbetsförmedlingen arbetar med Figma och Gitlab som plattformar
                för öppen källkod, vilket innebär att vem som helst kan delta i
                skapandet och förbättringen av designsystemet. Vår licens Apache
                2.0 möjliggör användning inom både offentlig och privat sektor.
              </p>
              <div>
                <digi-link-external
                  afHref="https://gitlab.com/arbetsformedlingen/designsystem/digi"
                  af-target="_blank"
                  af-variation="primary">
                  Utvecklarplattformen (gitlab.com, öppnas i egen flik)
                </digi-link-external>
              </div>
              <digi-link-external
                afHref="https://www.figma.com/@sweden"
                af-target="_blank"
                af-variation="primary">
                UI-kit (figma.com, öppnas i egen flik)
              </digi-link-external>
            </div>
          </div>
        </digi-layout-block>
      </host>
    );
  }
}
