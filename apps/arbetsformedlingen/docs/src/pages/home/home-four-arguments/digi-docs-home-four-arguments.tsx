import { Component, h } from '@stencil/core';
import { router } from '../../../global/router';
import illustrationFirst from '../../../assets/images/startpage/illustrationFirst.svg';
import illustrationSecond from '../../../assets/images/startpage/illustrationSecond.svg';
import illustrationThird from '../../../assets/images/startpage/illustrationThird.svg';
import illustrationFourth from '../../../assets/images/startpage/illustrationFourth.svg';

@Component({
  tag: 'digi-docs-home-four-arguments',
  styleUrl: 'digi-docs-home-four-arguments.scss',
  scoped: true
})
export class FourArguments {
  Router = router;

  linkHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-layout-container>
          <digi-typography>
            <h2 class="title-container">
              Designsystemet är en produkt för att skapa produkter
            </h2>
          </digi-typography>
          
          {/* Nästla in allt i sassen under logo-paragraph-container */}
          <div class="logo-paragraph-container">
            <div class="image-paragraph">
              <img
                class="illustration-first-image"
                src={illustrationFirst}
                alt=""
              />
              <p>
                <b>Bygg produkter snabbare</b>
                <br />
                Ett designsystem ger dig färdiga komponenter och riktlinjer som
                gör det lättare att gå från idé till färdig produkt.
              </p>
            </div>

            <div class="image-paragraph">
              <img
                class="illustration-second-image"
                src={illustrationSecond}
                alt=""
              />
              <b>Bättre användarupplevelse</b>
              <p>
                Designsystem bygger på beprövade lösningar och samlad
                erfarenhet, vilket ger produkter som fungerar bättre för alla.
              </p>
            </div>

            <div class="image-paragraph">
              <img
                class="illustration-third-image"
                src={illustrationThird}
                alt=""
              />
              <p>
                <b>Spara tid och resurser</b>
                <br />
                Genom att använda ett designsystem slipper du uppfinna hjulet på
                nytt. Vanliga problem är redan lösta, så du kan fokusera på det
                som är unikt för din produkt.
              </p>
            </div>

            <div class="image-paragraph">
              <img
                class="illustration-fourth-image"
                src={illustrationFourth}
                alt=""
              />
              <p>
                <b>Smidigare samarbete</b>
                <br />
                <p>
                  Designers och utvecklare får gemensamma verktyg och arbetssätt
                  som både förenklar samarbetet och gör arbetet mer effektivt
                  och meningsfullt.
                </p>
              </p>
            </div>
          </div>

          {/* Mobile Icons */}
          <div class="mobile-wrapper">


          <div class="mobile-image-container">
            <img class="mobile-first-image" src={illustrationFirst} alt="" />

            <img class="mobile-second-image" src={illustrationSecond} alt="" />

            <img class="mobile-third-image" src={illustrationThird} alt="" />

            <img class="mobile-fourth-image" src={illustrationFourth} alt="" />
                          </div>
            <div class="mobile-paragraph-container">
            <p>
              <b>Skapa produkter snabbare och enklare</b>
              <br />
              Ett designsystem ger dig färdiga komponenter och riktlinjer som
              gör det lättare att gå från idé till färdig produkt.
            </p>

            <b>Bättre kvalitet och användarupplevelse</b>
              <p>
                Designsystem bygger på beprövade lösningar och samlad
                erfarenhet, vilket ger produkter som fungerar bättre för alla.
              </p>

              <p>
                <b>Spara tid och resurser</b>
                <br />
                Genom att använda ett designsystem slipper du uppfinna hjulet på
                nytt. Vanliga problem är redan lösta, så du kan fokusera på det
                som är unikt för din produkt.
              </p>

              <p>
                <b>Effektivare samarbete</b>
                <br />
                <p>
                  Designers och utvecklare får gemensamma verktyg och arbetssätt
                  som både förenklar samarbetet och gör arbetet mer effektivt
                  och meningsfullt.
                </p>
              </p>
              </div>

          </div>
        </digi-layout-container>
      </host>
    );
  }
}
