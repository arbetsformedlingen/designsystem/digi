import { Component, getAssetPath, h, Host, Prop } from '@stencil/core';
import { RorligtLjudExampleType } from './rorligt-ljud-example-type.enum';

@Component({
  tag: 'digi-docs-rorligt-ljud-example',
  styleUrl: 'digi-docs-rorligt-ljud-example.scss',
  scoped: true
})
export class GoodBadExample {
  @Prop() exampleType: RorligtLjudExampleType = RorligtLjudExampleType.GOOD;

  get exampleIcon() {
    return this.exampleType === RorligtLjudExampleType.GOOD ? (
      <img
        src={getAssetPath(
          '/assets/images/graphics-example/example-icon-good.svg'
        )}
      />
    ) : (
      <img
        src={getAssetPath(
          '/assets/images/graphics-example/example-icon-bad.svg'
        )}
      />
    );
  }

  get cssModifiers() {
    return {
      'digi-docs-rorligt-ljud-example--good':
        this.exampleType === RorligtLjudExampleType.GOOD,
      'digi-docs-rorligt-ljud-example--bad':
        this.exampleType === RorligtLjudExampleType.BAD
    };
  }

  render() {
    return (
      <Host
        class={{
          'digi-docs-rorligt-ljud-example': true,
          ...this.cssModifiers
        }}>
        <div class="digi-docs-rorligt-ljud-example__content">
          <div class="digi-docs-rorligt-ljud-example__icon">
            {this.exampleIcon}
          </div>
          <slot></slot>
        </div>
        <div class="digi-docs-rorligt-ljud-example__footer">
          <slot name="footer"></slot>
        </div>
      </Host>
    );
  }
}
