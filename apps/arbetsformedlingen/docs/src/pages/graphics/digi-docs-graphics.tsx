import { Component, h, getAssetPath } from '@stencil/core';
import {
  InfoCardHeadingLevel,
  InfoCardVariation
} from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-graphics',
  styleUrl: 'digi-docs-graphics.scss',
  scoped: true
})
export class DigiDocsGraphics {
  render() {
    return (
      <host>
        <digi-docs-page-layout af-edit-href="pages/digi-docs-graphics/digi-docs-graphics.tsx">
          <digi-typography>
            <digi-layout-container>
              <digi-typography-heading-jumbo af-text="Grafik"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Vi använder grafik för att förstärka, kommunicera och beskriva.
                Utöver den unika grafik som görs för de olika tjänsterna så har
                vi egen infografik, ikonspråk och diagram som knyter allt samman
                och bygger Arbetsförmedlingens varumärke.
              </digi-typography-preamble>
            </digi-layout-container>
            <br />
            <digi-layout-container>
              <p>
                Huvudfärg för grafikkomponenter är densamma som textfärgen eller
                profilblått. För bakgrunder och utfyllnad i grafiken använder vi
                i första hand våra profilfärger i dess ursprungsnyans. När det
                inte passar och grafiken tar för mycket fokus använder vi våra
                ljusare nyanser av profilfärgerna och i andra hans ljusare
                nyanser av komplementfärgerna.
              </p>
              <h2>Infografik</h2>
              <p>
                Vi har ett stort bibliotek med infografik framtagen för att
                beskriva eller förstärka. Det kan vara uppmaningar,
                upplysningar, flödesbeskrivningar, situationer, aktivitet och
                föremål.
              </p>
              <p>
                Saknar du någon typ av infografik finns möjlighet att göra en
                beställning på att ta fram ny grafik. Anpassat efter era behov.
                Kontakta kommunikationsavdelningen för att göra en beställning.
              </p>
              <p>
                <digi-link
                  af-variation="small"
                  afHref={`mailto:kommunikationsavdelningen@arbetsformedlingen.se`}>
                  <digi-icon-envelope
                    slot="icon"
                    aria-hidden="true"></digi-icon-envelope>
                  Kontakta kommunikationsavdelningen
                </digi-link>
              </p>
              <div class="digi-docs__graphics-infographics">
                <digi-media-image
                  afUnlazy
                  afSrc={getAssetPath('/assets/images/infographics.png')}
                  afAlt="Exempel på infografik"></digi-media-image>
              </div>
              <br />
              <h2>Diagram</h2>
              <p>
                Vi använder flera typer av diagram. De vi använder främst är
                stapeldiagram och cirkeldiagram. När du producerar diagram
                oavsett om det är för webb eller tryck är det viktigt att du
                tänker på att de är lätta att läsa av. Kontrasten mellan text,
                bakgrund och grafik ska följa de tillgänglighetskrav vi har.
                Exempel för att öka kontrasten är att ge staplarna eller
                tårtbitarna en kantlinje eller att använda sig av mörk bakgrund.
                För att underlätta för färgblinda använd ett mönster i
                fyllnadsfärgen då det är möjligt. Tänk ockspå på att ett diagram
                även skall kunna läsas av med en skärmläsare. Då kan en tabell
                eller liknande vara en lösning.
              </p>
              <div class="digi-docs__graphics-diagrams">
                <digi-media-image
                  afUnlazy
                  class="digi-docs__graphics-diagrams"
                  afSrc={getAssetPath('/assets/images/diagrams.png')}
                  afAlt="Exempel på diagram enligt Arbetsförmedlingens grafiska"></digi-media-image>
              </div>
              <h2>Funktionsikoner </h2>
              <p>
                Arbetsförmedlingens ikonbibliotek heter Af Icons och finns
                representerat visuellt i Figma och kodmässigt i Digi Core. Målet
                är att det ska innehålla så neutrala och traditionsenliga ikoner
                som möjligt. Därför har vi valt att rita alla ikoner själva och
                inte använda färdiga ikonbibliotek. Biblioteket fylls
                kontinuerligt på med nya eller omarbetade ikoner.
              </p>
              <digi-link-external
                afHref="https://www.figma.com/community/file/1415694540311931262/af-icons"
                afTarget="_blank"
                afVariation="small">
                Ikonbiblioteket AF Icons i Figma (figma.com, öppnas i egen flik)
              </digi-link-external>
              <br />
              <br />
            </digi-layout-container>
            <digi-layout-container afNoGutter afMarginBottom>
              <digi-info-card
                afHeading="Riktlinjer"
                afHeadingLevel={InfoCardHeadingLevel.H3}>
                <digi-list>
                  <li>
                    {' '}
                    Använd endast den grafik, ikoner och diagram som finns i
                    våra resursbibliotek. Eftersom de uppfyller kraven vi har på
                    tillgänglighet samt följer Arbetsförmedlingens
                    varumärkesriktlinjer.
                  </li>
                  <li>
                    Använd dig endast av Arbetsförmedlingens profilfärger.{' '}
                  </li>
                  <li>
                    All typ av grafik används primärt ihop med en förklarande
                    text eller rubrik. I de fall det inte är möjligt är det
                    viktigt att det finns en alt-text eller div-tag som
                    förklarar grafikens innehåll.
                  </li>
                </digi-list>
                <br />
              </digi-info-card>
              <br />
              <digi-info-card
                afHeading="Då det blir fel"
                afHeadingLevel={InfoCardHeadingLevel.H3}
                afVariation={InfoCardVariation.SECONDARY}>
                <digi-list>
                  <li>
                    Du får inte ändra eller modifiera en ikon eller
                    infografikkomponent som sedan kan misstolkas eller blandas
                    ihop med annan.{' '}
                  </li>
                  <li>
                    Valet av ikon skall vara starkt visuellt kopplat till dess
                    funktion. Det är exempelvis inte lämpligt att ha en ikon med
                    en penna för att symbolisera en länk.
                  </li>
                  <li>
                    Funktionsikonerna är att betrakta som en visuell förstärkare
                    och komplement i tex. knappar eller menyer. De skall inte
                    användas som utsmyckade illustration.{' '}
                  </li>
                </digi-list>
                <br />
              </digi-info-card>
            </digi-layout-container>
          </digi-typography>
        </digi-docs-page-layout>
      </host>
    );
  }
}
