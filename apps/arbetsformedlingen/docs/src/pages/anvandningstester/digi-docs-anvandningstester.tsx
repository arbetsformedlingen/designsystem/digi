import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-anvandningstester',
  styleUrl: 'digi-docs-anvandningstester.scss'
})
export class DigiDocsAnvandningstester {
  render() {
    return (
      <div class="digi-docs-anvandningstester">
        <digi-docs-page-layout af-edit-href="pages/about-digi/digi-docs-anvandningstester/digi-docs-anvandningstester.tsx">
          <digi-layout-block afMarginBottom>
            <digi-typography-heading-jumbo af-text="Användningstester och användarutvärderingar"></digi-typography-heading-jumbo>
            <digi-typography-preamble>
              Arbetsförmedlingen ska verka för inkluderande design. Det innebär
              att system, tjänster och produkter ska vara tillgängliga för alla.
              Genom användningstester kan vi upptäcka brister tidigt, göra
              förbättringar som en del i processen och slippa dyra anpassningar
              efteråt.
            </digi-typography-preamble>
            <p>
              Ungefär 20% av världens befolkning har någon form av
              funktionsnedsättning. För att förstå deras upplevelse och kunna
              förbättra våra tjänster är det viktigt att låta användare med
              olika funktionsnedsättningar vara med och testa. Om man utgår
              ifrån de med permanenta funktionsnedsättningar så blir det oftast
              bättre även för alla andra.
            </p>
            <h2>Använd Arbetsförmedlingens användargrupp</h2>
            <p>
              Användargruppen på Arbetsförmedlingen består av användare med
              olika funktionsnedsättningar. De arbetar med våra kunder,
              arbetssökande och arbetsgivare och ger värdefulla kunskaper om det
              som utvecklas och tas fram.
            </p>
            <h2>Olika typer av tester</h2>
            <p>
              Det finns olika sätt att utvärdera tillgänglighet med
              användargruppen: Referensgrupp och användarutvärderingar.
            </p>
            <h3>Referensgrupp</h3>
            <p>
              Användare ingår i en referensgrupp för en produkt, tjänst eller
              system, Det kan handla om allt från att svara på frågor och
              fungera som referensgrupp till att faktiskt testa olika system och
              ge återkoppling.
            </p>
            <h3>Användarutvärderingar</h3>
            <p>
              Användarutvärderingar är när man vill få synpunkter på en produkt,
              ett system eller en tjänst. Testpersonen får en länk till
              produkten som personen sedan får testa i lugn och ro. Personen
              lämnar sedan sina synpunkter om den testade versionen.
            </p>
            <h3>Så går användningstesterna med användargruppen till</h3>
            <p>
              Användningstester görs efter förutbestämda scenarion som tas fram
              av testledaren eller teamet. Scenarion är baserade användarens
              typiska arbetsuppgifter på produkten som ska testas.
            </p>
            <p>
              Användningstester kan göras både på en prototyp och på den redan
              utvecklade produkten, till exempel i testmiljön. Det är däremot
              alltid bra att testa designen redan på prototypen för att tidigt
              kunna säkerställa att designen är inkluderande.
            </p>
            <h3>Var utförs testerna?</h3>
            <p>
              Det går att genomföra testerna antingen digitalt, eller på plats i
              verksamheten. Testledaren tar hand om de praktiska detaljerna.
              Användningstester leds av en testledare som genomför testerna och
              en observatör som noterar användarens reflektioner och
              interaktioner. Produktägare och medlemmar i teamet är också med
              och noterar det som man tycker är värdefullt.
            </p>
            <h3>Hur lång tid tar det?</h3>
            <p>
              Hur lång tid det tar att testa beror alldeles på hur mycket som
              ska testas eller hur detaljerat man vill testa. Vanligast är att
              användningstestet tar ungefär en timme.
            </p>
            <h3>Vad är resultatet?</h3>
            <p>
              Användningstester och utvärderingar resulterar i en åtgärdslista
              som produktägaren tar med i sin planering för att ständigt kunna
              förbättra produktens inkludering av alla. Det är också viktigt med
              användningstestresultatet för att bland annat:
            </p>
            <digi-list>
              <li>
                Bli mer medvetna om vilka man exkluderar - och därmed få större
                möjlighet att förbättra produkten och inkludera alla
              </li>
              <li>
                Ge större nytta för organisationen, både rent affärsmässigt och
                ur ett arbetsmiljöperspektiv
              </li>
              <li>
                Göra det som är nödvändigt för en och på det sättet göra det
                bättre för alla
              </li>
            </digi-list>
            <h2>Boka</h2>
            <p>Bokningen sker via mejllådan</p>
            <p>
              <digi-link
                af-variation="small"
                afHref="mailto:designsystem@arbetsformedlingen.se">
                <digi-icon-envelope slot="icon"></digi-icon-envelope>Mejla
                designsystem@arbetsformedlingen.se
              </digi-link>
            </p>
            <p>Skriv:</p>
            <p>
              <digi-list>
                <li>Vad som ska testas</li>
                <li>Önskemål på när testet ska genomföras</li>
                <li>
                  Idé om hur mång användare som behövs (annars kommer förslag)
                </li>
                <li>Hur lång tid det uppskattningsvis tar (per användare)</li>
                <li>
                  Vem som anordnar användningstesterna, eller om man önskar att
                  någon leder eller assisterar med detta
                </li>
              </digi-list>
            </p>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
