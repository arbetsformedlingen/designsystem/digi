import { Component, h, State } from '@stencil/core';
import { changelogData } from '../../data/changelogData';

@Component({
  tag: 'digi-docs-release-notes',
  styleUrl: 'digi-docs-release-notes.scss',
  shadow: true
})
export class DigiDocsReleaseNotes {
  @State() pageName = 'Release notes';

  render() {
    return (
      <div class="digi-docs-release-notes">
        <digi-docs-page-layout af-page-heading={this.pageName}>
          <digi-layout-container af-margin-top af-margin-bottom>
            <article>
              <div innerHTML={changelogData}></div>
            </article>
          </digi-layout-container>
        </digi-docs-page-layout>
      </div>
    );
  }
}
