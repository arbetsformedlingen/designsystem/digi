import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';
import { href } from 'stencil-router-v2';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-about-design-pattern-button',
  styleUrl: 'digi-docs-about-design-pattern-button.scss',
  scoped: true
})
export class AboutDesignPatternButton {
  @State() pageName = 'Knappar';
  Router = router;

  linkClickHandler(e) {
    e.detail.preventDefault();
    this.Router.push(e.target.afHref);
  }

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-design-pattern-button/digi-docs-about-design-pattern-button.tsx">
          <span slot="preamble">
            Knappar är klickbara gränssnittskomponenter som initierar en
            handling och utför en åtgärd. De hänvisar användaren till att
            slutföra viktiga mål inom en upplevelse. De kommunicerar uppmaningar
            och tillåter användare att interagera med tänkt webbplats.
          </span>
          <br />
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h3>Varianter av knappar</h3>
            <p>
              Med hjälp av olika varianter av knappar kan man stötta och leda
              användarna till deras nästa interaktion. Vi erbjuder några olika
              variationer för att möta olika användningsfall. Vi kategoriserar
              våra knappar i tre huvudgrupper: primära, sekundära och
              funktionsknappar. Primära knappar är utformade för att framhäva
              huvudåtgärder och uppmuntra användaren att utföra viktiga
              handlingar.
            </p>
            <br />
          </digi-layout-block>
          <digi-layout-block
            af-variation="primary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <digi-layout-columns
              af-variation={state.responsive321Columns}
              class="example-zone">
              <digi-docs-grid-list-item cardVariant="variant2">
                <div slot="image">
                  <digi-button
                    af-size="medium"
                    af-variation="primary"
                    af-full-width="false">
                    Primär knapp
                  </digi-button>
                </div>
                <div slot="content">
                  <h4>Primär knapp</h4>
                  <p>
                    Ska alltid vara mest framträdande i relation till andra
                    knappar. Används alltid om det endast finns en knapp på
                    sidan och ligger alltid i vänsterkant om den är själv på
                    sidan. Vi använder den primära knappen för att signalera den
                    primära interaktionen på sidan.
                  </p>
                </div>
              </digi-docs-grid-list-item>
              <digi-docs-grid-list-item cardVariant="variant2">
                <div slot="image">
                  <digi-button
                    af-size="medium"
                    af-variation="secondary"
                    af-full-width="false">
                    Sekundär knapp
                  </digi-button>
                </div>
                <div slot="content">
                  <h4>Sekundär knapp</h4>
                  <p>
                    Använd den sekundära knappen bredvid primär knapp för att
                    indikera alternativa eller mindre (i jämnförelse) viktiga
                    åtgärder. En sekundär interaktion användas om det finns mer
                    än en knapp på sidan.
                  </p>
                </div>
              </digi-docs-grid-list-item>
              <digi-docs-grid-list-item cardVariant="variant2">
                <div slot="image">
                  <div class="f-button-example">
                    <digi-button af-size="medium" af-variation="function">
                      <digi-icon-envelope slot="icon" aria-hidden />
                      Funktionsknapp
                    </digi-button>
                  </div>
                </div>
                <div slot="content">
                  <h4>Funktionsknapp</h4>
                  <p>
                    Funktionsknappar används för specifika handlingar som
                    "rensa" eller "ändra" och till skillnad från de andra
                    knappvarianter så måste funktionknappar ha en ikon.
                  </p>
                </div>
              </digi-docs-grid-list-item>
            </digi-layout-columns>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h3>Grundprinciper och användning</h3>
            <p>
              En knapp utför en handling eller åtgärd, till skillnad mot till
              exempel en knapplänk som har till uppgift att länka användaren
              vidare. En knapp låter användarna veta vad som kommer att hända
              härnäst,{' '}
            </p>
            <digi-list af-list-type="bullet">
              <li>
                Knappar finns i tre olika storlekar: stor (
                <span lang="en">"large"</span>
                ), mellan (<span lang="en">"medium"</span>) och liten (
                <span lang="en">"small"</span>). Mellanstorleken är förvald och
                passar bra för vanliga användningsfall och ger en balans mellan
                framträdande och utrymmes-användning.{' '}
                <p>
                  Den största storleken kan vara fördelaktig för att maximera
                  synligheten och uppmärksamheten för viktiga åtgärder eller för
                  användning i gränssnitt med mycket utrymme. Den minsta
                  storleken kan vara idealisk för kompakta gränssnitt där
                  utrymmet är begränsat.
                </p>
              </li>
            </digi-list>
            <digi-list af-list-type="bullet">
              <li>
                Använd kortfattade knapptexter som är lätta att skanna för att
                beskriva handlingen.
              </li>
              <li>
                Använd aktiva verb eller fraser som tydligt indikerar handling.
              </li>
              <li>
                Använd konsekvent språk för knappen och annan text som beskriver
                samma åtgärd.
              </li>
              <li>Texten ska alltid vara centrerad i knappen</li>
              <li>
                Inaktiva knappar ska inte användas, alla knappar ska alltid gå
                att interagera med.{' '}
              </li>
              <li>
                Applicering och kodimplementation måste följa
                webbtillgänglighetsriktlinjerna, läs mer om{' '}
                <a
                  {...href(
                    '/tillganglighet-och-design/om-digital-tillganglighet'
                  )}>
                  tillgänglighet
                </a>{' '}
                {/* <digi-link
									afHref="/tillganglighet-och-design/om-digital-tillganglighet"
									af-href="/tillganglighet-och-design/om-digital-tillganglighet"
									af-variation="small"
								>
									tillgänglighet
								</digi-link>{' '} */}
                och använd våra verktyg.
              </li>
            </digi-list>
          </digi-layout-block>
          <digi-layout-block
            af-variation="primary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            {/* <digi-docs-grid-list> */}
            <digi-layout-columns af-variation={state.responsive321Columns}>
              <digi-docs-grid-list-item cardVariant="variant2">
                <div slot="image">
                  <digi-media-image
                    afUnlazy
                    afSrc="/assets/images/pattern/button-pattern/button-pattern-size-comparison.svg"
                    afAlt="Bild på tre primära knappar i de tre storlekar"></digi-media-image>
                </div>
                <div slot="content">
                  <h4>Storlekar</h4>
                  <p>
                    De finns i tre olika storlekar: stor (
                    <span lang="en">"large"</span>
                    ), mellan (<span lang="en">"medium"</span>) och liten (
                    <span lang="en">"small"</span>). Mellanstorleken är förvald
                    och det är den som rekommenderas att användas.
                  </p>
                </div>
              </digi-docs-grid-list-item>
              <digi-docs-grid-list-item cardVariant="variant2">
                <div slot="image">
                  <digi-media-image
                    afUnlazy
                    afSrc="/assets/images/pattern/button-pattern/button-pattern-logiskt-flode.svg"
                    afAlt="Exempel på korrekt ordning av en primär och sekundär knapp med den primära till höger och den sekundära till vänster."></digi-media-image>
                </div>
                <div slot="content">
                  <h4>Logiskt flöde på knapparna</h4>
                  <p>
                    Placera alltid den primära knappen sist i läsordningen, till
                    höger om den sekundära knappen. Båda knapparna ligger i
                    vänsterkant på sidan
                  </p>
                </div>
              </digi-docs-grid-list-item>
              <digi-docs-grid-list-item cardVariant="variant2">
                <div slot="image">
                  <digi-media-image
                    afUnlazy
                    afSrc="/assets/images/pattern/button-pattern/button-pattern-mobile-button.svg"
                    afAlt="Bild på knappar på en mobil enhet, båda har full bredd och den sekundära är ovanför den primära knappen"></digi-media-image>
                </div>
                <div slot="content">
                  <h4>Knappar i mobil</h4>
                  <p>
                    Mobila enheter är smala och har per automatik inte utrymme
                    för knappar bredvid varandra. Vi förespråkar då full bredd
                    på ytan. Det gör det enklare för användaren att följa flödet
                    rakt nedåt.
                  </p>
                </div>
              </digi-docs-grid-list-item>
            </digi-layout-columns>
            {/* </digi-docs-grid-list> */}
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h3>Logiskt flöde</h3>
            <p>
              Vi läser från vänster till höger, och vi förespråkar att knappar i
              desktop alltid ligger i vänsterkant på en rad. Det gör det enklare
              för användaren när hen följer en rak linje från vänster till
              höger, (jämnfört med layouter där ögat hoppar från texten i
              vänsterkant, till knappar i mitten eller höger om en kolumn).
              Placera alltid den primära knappen sist i läsordningen.
            </p>
            <p>
              Mobila enheter är smala och har per automatik ofta inte utrymme
              för knappar bredvid varandra. Vi förespråkar då full bredd på
              knappytan. Det gör det enklare för användaren att följa flödet
              rakt nedåt.
            </p>
            <p>
              Denna design hjälper oss att samordna det visuella uttrycket för
              knappar i mobil och desktop- storlekar. Det blir även enklare för
              skärmläsare att navigera genom en sådan struktur.
            </p>
            <h3>Förloppssteg och knappar</h3>
            <p>
              I många flöden som till exempel enkäter och formulär kan det vara
              lämpligt att dela upp flödet med hjälp av fler sidor. Då kan du
              använda{' '}
              <a {...href('/komponenter/digi-progress-list/oversikt')}>
                komponenten förloppssteg
              </a>{' '}
              som hjälper användaren att se och få öveblick när användaren går
              vidare eller går tillbaka med hjälp av primära och sekundära
              knappar. Genom att visa på flera steg, guidar vi användaren att
              bryta ner uppgiften till mindre hanterbara delmoment, för att
              förstå vad som redan är gjort och vad som är kvar att göra.
            </p>
          </digi-layout-block>
          <digi-layout-block af-variation="secondary" afVerticalPadding>
            <br />
            <hr></hr>
            <br />
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h4>Designmönster</h4>
                <p>Se relaterade designmönster</p>
                <digi-docs-related-links>
                  <digi-link-button
                    afHref="/designmonster/formular"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Formulär
                  </digi-link-button>
                  <digi-link-button
                    afHref="/designmonster/spacing"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Spacing
                  </digi-link-button>
                </digi-docs-related-links>
              </div>
              <div>
                <h4>Komponenter</h4>
                <p>Se relaterade komponenter</p>
                <digi-docs-related-links>
                  <digi-link-button
                    afHref="/komponenter/digi-link-button/oversikt"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Länkknappar
                  </digi-link-button>
                  <digi-link-button
                    afHref="/komponenter/digi-badge-notification/oversikt"
                    af-target="_blank"
                    af-size="medium"
                    af-variation="secondary"
                    onAfOnClick={(e) => this.linkClickHandler(e)}>
                    Notifikationsindikator
                  </digi-link-button>
                </digi-docs-related-links>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
