import { Component, h, State, getAssetPath } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { InfoCardHeadingLevel } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-about-digital-accessibility',
  styleUrl: 'digi-docs-about-digital-accessibility.scss',
  scoped: true
})
export class DigiDocsAboutDigitalAccessibility {
  @State() pageName = 'Om digital tillgänglighet';

  render() {
    return (
      <div class="digi-docs-about-digital-accessibility">
        <Helmet>
          <meta
            property="rek:pubdate"
            content="Thu Nov 29 2022 08:06:16 GMT+0100 (CET)"
          />
        </Helmet>
        <digi-typography>
          <digi-docs-page-layout
            af-page-heading={this.pageName}
            af-edit-href="pages/digi-docs-about-digital-accessibility/digi-docs-about-digital-accessibility.tsx">
            <span slot="preamble">
              Arbetsförmedlingens användare har olika förutsättningar och behov.
              Vårt mål är att allt digitalt som produceras på Arbetsförmedlingen
              ska vara lätt att förstå och enkelt att använda, oavsett vem
              användaren är.
            </span>

            <digi-layout-block af-margin-bottom>
              <h2>Vad innebär digital tillgänglighet?</h2>
              <p>
                Digital tillgänglighet innebär att vi utgår från användarnas
                behov. Både de tjänster vi utvecklar och vår digitala
                arbetsmiljö ska vara lätt att förstå och enkelt att använda.
              </p>
              <h2>Tillgängligt för vem?</h2>
              <p>
                Våra digitala tjänster ska kunna användas av både arbetssökande,
                arbetsgivare och medarbetare, vare sig mobil eller dator
                används. De ska vara tillgängliga oavsett om användaren har en
                funktionsnedsättning, är ny i Sverige eller saknar teknikvana.
              </p>
              <h3>Enkelt för våra användare</h3>
              <p>
                Vi strävar efter att våra användare i första hand ska kunna
                använda våra digitala kanaler. De ska kunna utföra sina ärenden
                digitalt utan besvär, vare sig det är att hitta information,
                använda en tjänst eller fylla i ett formulär. Det avlastar också
                våra kontor och telefonsupport så vi kan hjälpa användarna där
                snabbare.
              </p>
              <h3>En inkluderande arbetsmiljö</h3>
              <p>
                Arbetsförmedlingen ska spegla Sveriges mångfald. God digital
                tillgänglighet hjälper våra medarbetare att arbeta smidigt och
                effektivt. Det skapar en inkluderande arbetsmiljö som lockar och
                inspirerar kompetenta medarbetare.
              </p>
              <h2>Designsystemet stöttar tillgänglighetsarbetet</h2>
              <p>
                Designsystemet samlar återanvändbara verktyg och riktlinjer. De
                hjälper oss att följa lagkrav och riktlinjer för digital
                tillgänglighet. Dessutom får allt som utvecklas med hjälp av
                designsystemet ett enhetligt utseende. Det gör våra tjänster
                lättare att känna igen och enklare att använda.
              </p>
            </digi-layout-block>
            <digi-layout-block
              af-variation="secondary"
              af-no-gutter
              af-vertical-padding>
              <digi-docs-grid-list>
                <digi-docs-grid-list-item class="variant1">
                  <div slot="image">
                    <digi-media-image
                      afUnlazy
                      afWidth="455"
                      afSrc={getAssetPath(
                        '/assets/images/about-digital-accessibility/komponenter.svg'
                      )}
                      afAlt=""></digi-media-image>
                  </div>
                  <div slot="content">
                    <h3>Komponenter och Digi Core</h3>
                    <p>
                      Digi Core är vårt komponentbibliotek för kod. De hjälper
                      oss att snabbt och effektivt bygga digitala produkter.
                    </p>
                  </div>
                  <div slot="footer">
                    <ul>
                      <li>
                        <digi-link-internal
                          afHref="/komponenter/om-komponenter"
                          af-variation="small">
                          Komponenter
                        </digi-link-internal>
                      </li>
                      <li>
                        <digi-link-internal
                          afHref="/om-designsystemet/digi-core"
                          af-variation="small">
                          Digi Core
                        </digi-link-internal>
                      </li>
                    </ul>
                  </div>
                </digi-docs-grid-list-item>
                <digi-docs-grid-list-item class="variant1">
                  <div slot="image">
                    <digi-media-image
                      afUnlazy
                      afWidth="455"
                      afSrc={getAssetPath(
                        '/assets/images/about-digital-accessibility/designmonster.svg'
                      )}
                      afAlt=""></digi-media-image>
                  </div>
                  <div slot="content">
                    <h3>Designmönster och Digi UI Kit</h3>
                    <p>
                      Digi UI Kit samlar designkomponenter för skisser och
                      prototyper. Designmönster beskriver hur vi löser olika
                      designproblem.
                    </p>
                  </div>
                  <div slot="footer">
                    <ul>
                      <li>
                        <digi-link-internal
                          afHref="/designmonster/introduktion"
                          af-variation="small">
                          Designmönster
                        </digi-link-internal>
                      </li>
                      <li>
                        <digi-link-internal
                          afHref="/om-designsystemet/digi-ui-kit"
                          af-variation="small">
                          Digi UI Kit
                        </digi-link-internal>
                      </li>
                    </ul>
                  </div>
                </digi-docs-grid-list-item>
                <digi-docs-grid-list-item class="variant1">
                  <div slot="image">
                    <digi-media-image
                      afUnlazy
                      afWidth="455"
                      afSrc={getAssetPath(
                        '/assets/images/about-digital-accessibility/matrispuff.svg'
                      )}
                      afAlt=""></digi-media-image>
                  </div>
                  <div slot="content">
                    <h3>Tillgänglighetsmatrisen</h3>
                    <p>
                      Tillgänglighetsmatrisen ger exempel på användargrupper med
                      olika förutsättningar och behov.
                    </p>
                  </div>
                  <div slot="footer">
                    <digi-link-internal
                      afHref="/tillganglighet-och-design/om-tillganglighetsmatrisen"
                      af-variation="small">
                      Tillgänglighetsmatrisen
                    </digi-link-internal>
                  </div>
                </digi-docs-grid-list-item>
                <digi-docs-grid-list-item class="variant1">
                  <div slot="image">
                    <digi-media-image
                      afUnlazy
                      afWidth="455"
                      afSrc={getAssetPath(
                        '/assets/images/about-digital-accessibility/listpuff.svg'
                      )}
                      afAlt=""></digi-media-image>
                  </div>
                  <div slot="content">
                    <h3>Tillgänglighetslistan</h3>
                    <p>
                      Tillgänglighetslistan är ett stöd både under
                      utvecklingsprocessen och för att ta fram
                      tillgänglighetsredogörelsen.
                    </p>
                  </div>
                  <div slot="footer">
                    <digi-link-internal
                      afHref="/tillganglighet-och-design/tillganglighet-checklista"
                      af-variation="small">
                      Tillgänglighetslistan
                    </digi-link-internal>
                  </div>
                </digi-docs-grid-list-item>
                <digi-docs-grid-list-item class="variant1">
                  <div slot="image">
                    <digi-media-image
                      afUnlazy
                      afWidth="455"
                      afSrc={getAssetPath(
                        '/assets/images/about-digital-accessibility/redogorpuff.svg'
                      )}
                      afAlt=""></digi-media-image>
                  </div>
                  <div slot="content">
                    <h3>Tillgänglighetsredogörelse </h3>
                    <p>
                      Avsnittet “Process för tillgänglighetsredogörelse” samlar
                      instruktioner och mallar för att skapa en
                      tillgänglighetsredogörelse.
                    </p>
                  </div>
                  <div slot="footer">
                    <digi-link-internal
                      afHref="/tillganglighet-och-design/tillganglighetsredogorelse"
                      af-variation="small">
                      Tillgänglighetsredogörelse
                    </digi-link-internal>
                  </div>
                </digi-docs-grid-list-item>
                <digi-docs-grid-list-item class="variant1">
                  <div slot="image">
                    <digi-media-image
                      class="cta-block__img"
                      afUnlazy
                      afWidth="455"
                      afSrc={getAssetPath(
                        '/assets/images/about-digital-accessibility/lagkravpuff.svg'
                      )}
                      afAlt=""></digi-media-image>
                  </div>
                  <div slot="content">
                    <h3>Lagkrav och riktlinjer</h3>
                    <p>
                      Lagkrav och riktlinjer beskriver hur tillgänglighet är
                      lagstadgat och länkar vidare till fördjupad information.
                    </p>
                  </div>
                  <div slot="footer">
                    <digi-link-internal
                      afHref="/tillganglighet-och-design/lagkrav-och-riktlinjer"
                      af-variation="small">
                      Lagkrav och riktlinjer
                    </digi-link-internal>
                  </div>
                </digi-docs-grid-list-item>
              </digi-docs-grid-list>
            </digi-layout-block>

            <digi-layout-block af-no-gutter af-vertical-padding>
              <digi-info-card
                afHeading="Kognitiv genomgång"
                af-heading-level="h2"
                afHeadingLevel={InfoCardHeadingLevel.H2}
                af-type="tip"
                af-link-href="/tillganglighet-och-design/kognitiv-genomgang"
                af-link-text="Kognitiv genomgång"
                af-variation="primary"
                af-size="standard">
                <h3>- ett tillgänglighetserbjudande til utvecklingsteam</h3>
                <p>
                  Designsystemsteamet erbjuder utvecklingsteam en manuell
                  granskning av digitala tjänster. Genomgången ger konkret
                  återkoppling på användbarhet, tillgänglighet och design.
                </p>
              </digi-info-card>
            </digi-layout-block>
          </digi-docs-page-layout>
        </digi-typography>
      </div>
    );
  }
}
