import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
  tag: 'digi-docs-about-design-pattern-loaders',
  styleUrl: 'digi-docs-about-design-pattern-loaders.scss',
  scoped: true
})
export class AboutDesignPatternLoaders {
  Router = router;
  //@Prop() pageName ='Laddare och platshållare (skeleton loader och loading spinner)';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          //af-page-heading={this.pageName}
          af-edit-href="pages/about-design-pattern-loaders/digi-docs-about-design-pattern-loaders.tsx">
          <Helmet>
            <meta
              property="rek:pubdate"
              content="Wed Oct 18 2023 09:11:07 GMT+0200 (Central European Summer Time)"
            />
          </Helmet>
          <digi-pattern-loaders-intro></digi-pattern-loaders-intro>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <digi-layout-columns af-variation={state.responsiveTwoColumns}>
              <div>
                <h2>Visuell återkoppling</h2>
                <p>
                  Användare engageras mer om applikationen verkar snabb och
                  responsiv. Laddningsindikatorer visar att applikationen
                  arbetar med att ladda innehåll. En{' '}
                  <span lang="en">skeleton loader</span> liknar det faktiska
                  innehållets layout och struktur, vilket hjälper användare att
                  förstå och hålla reda på vad som kommer att visas. Dessutom
                  ger laddningsindikatorer en enhetlig visuell stil och layout
                  över olika vyer och skärmar inom en eller flera tjänster,
                  vilket gör det enklare för användare att förstå hur
                  gränssnittet fungerar och hur innehållet är strukturerat.
                </p>
                <h2>Tänk på prestanda i underliggande system</h2>
                <p>
                  Utgångspunkten är att tjänster som erbjuds på myndigheten ska
                  erbjuda en så pass god prestanda att olika lösningar för att
                  hantera laddtider ska hållas till ett minimum. Men det är
                  klokt att vara beredd på att användarens egna
                  internetuppkoppling är långsam eller vid perioder där systemen
                  utsätts för större påfrestningar. Ha alltid en dialog med
                  ansvariga för de system du utvecklar mot hur de föredrar anrop
                  mot API m.m. så att de ges möjlighet att dimensionera systemen
                  efter hur de är tänkta att användas. Två riktlinjer för när
                  laddare och platshållare ofta är mer relevanta:
                </p>
                <digi-list af-list-type="bullet">
                  <li>
                    Vid <span lang="en">dashboards</span> som hämtar,
                    sammanställer eller beräknar data från flera system med
                    varierande prestanda.
                  </li>
                  <li>
                    En användare väljer att ändra vilken data som ska visas i en
                    tjänst, där vi vill göra denne uppmärksam på att datan
                    förändras.
                  </li>
                </digi-list>
                <h2>Hålltider för vad som är lång tid</h2>
                <digi-list af-list-type="bullet">
                  <li>Mindre än 1s uppfattas som god prestanda.</li>
                  <li>Mellan 1s och 3s uppfattas som uthärdligt.</li>
                  <li>
                    Mer än 3s är ofta problematiskt i kommersiella sammanhang,
                    men hos en myndighet är förväntningarna i regel inte lika
                    höga och det finns ofta inget alternativ till att vänta.
                    Dock påverkar det generellt sett upplevelsen negativt.
                  </li>
                  <li>
                    Det finns viss forskning som pekar på att avgörande och
                    viktiga händelser i en tjänst, t ex en beräkning inför ett
                    besked, kan gynnas av en viss fördröjning då det ökar
                    trovärdigheten. Det här bör beaktas i användningstester.
                  </li>
                </digi-list>
                <h2>Använd rätt variant och komponent vid rätt tillfälle</h2>
                <p>
                  För olika situationer så finns olika varianter framtagna.
                  Försök att matcha visuell form och även antalet objekt efter
                  det förväntade svaret på förfrågan. I den mån det går att
                  förutse.
                </p>
              </div>
              <div>
                <br />
                <digi-layout-block af-variation="primary" afVerticalPadding>
                  <digi-loader-skeleton af-variation="section"></digi-loader-skeleton>
                  <h3>
                    <span lang="en">Skeleton loader</span>
                  </h3>
                  <p>
                    En <span lang="en">skeleton loader</span> liknar det
                    faktiska innehållets layout och struktur, vilket hjälper
                    användare att förstå och hålla reda på vad som kommer att
                    visas, när väl innehållet har laddat färdigt.
                  </p>
                </digi-layout-block>
                <br />
                <digi-layout-block af-variation="primary" afVerticalPadding>
                  <digi-loader-spinner
                    af-size="large"
                    af-text="Laddar innehåll"></digi-loader-spinner>
                  <h3>
                    <span lang="en">Spinner</span>
                  </h3>
                  <p>
                    <span lang="en">Spinner</span> används när tjänsten väntar
                    på att användaren ska göra något eller att data har skickats
                    eller sparats av användaren. Till exempel vid uppladdning av
                    en fil eller vid en inloggning
                  </p>
                </digi-layout-block>
              </div>
            </digi-layout-columns>
          </digi-layout-block>
          <digi-layout-container af-margin-bottom>
            <div class="loaders__table__container" id="loaders__table">
              <div class="loaders__table__item">
                <h3>Variant</h3>
                <h3>Situation</h3>
                <h3>Exempel</h3>
              </div>

              <div class="loaders__table__item">
                <div class="loaders__table__item__first-column">
                  <div class="loaders__table__item__text_title">
                    <h4>Skeleton för helsida eller dominerande objekt</h4>
                  </div>
                </div>
                <div class="loaders__table__item__second-column">
                  <div class="loaders__table__item__text_paragraph">
                    <p>När en hel sida/tjänst laddas in för första gången</p>
                  </div>
                </div>
                <div class="loaders__table__item__third-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/Skeleton_example1.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
                <div class="loaders__table__item__fourth-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/Skeleton_example2.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
              </div>

              <div class="loaders__table__item">
                <div class="loaders__table__item__first-column">
                  <div class="loaders__table__item__text_title">
                    <h4>Område eller modul</h4>
                  </div>
                </div>
                <div class="loaders__table__item__second-column">
                  <div class="loaders__table__item__text_paragraph">
                    <p>
                      I vissa fall finns det ett ”kort” eller annan liknande
                      modul som är en del av en sida, som uppdateras efter
                      interaktion av användare. Hurdant mönster vill vi ha för
                      när ett enda ”kort” på en sida uppdateras?
                    </p>
                  </div>
                </div>
                <div class="loaders__table__item__third-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/modul_example1.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
                <div class="loaders__table__item__fourth-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/modul_example2.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
              </div>

              <div class="loaders__table__item">
                <div class="loaders__table__item__first-column">
                  <div class="loaders__table__item__text_title">
                    <h4>Tabell</h4>
                  </div>
                </div>
                <div class="loaders__table__item__second-column">
                  <div class="loaders__table__item__text_paragraph">
                    <p>
                      <span lang="en">Skeleton</span> vid första inläsning eller
                      när data uppdateras på begäran av användaren.
                    </p>
                  </div>
                </div>
                <div class="loaders__table__item__third-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/tabell_example1.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
                <div class="loaders__table__item__fourth-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/tabell_example2.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
              </div>

              <div class="loaders__table__item">
                <div class="loaders__table__item__first-column">
                  <div class="loaders__table__item__text_title">
                    <h4>Text</h4>
                  </div>
                </div>
                <div class="loaders__table__item__second-column">
                  <div class="loaders__table__item__text_paragraph">
                    <p>
                      <span lang="en">Skeleton</span> vid första inläsning,
                      överanvänd inte efter det utan låt rubriker m.m. stå kvar
                      om de inte behöver uppdateras. Om det är till kundens
                      namn, så bör detta absolut stå kvar vid eventuell
                      omladdning
                    </p>
                  </div>
                </div>
                <div class="loaders__table__item__third-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/text_example1.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
                <div class="loaders__table__item__fourth-column">
                  <div class="loaders__table__item__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/text_example2.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
              </div>

              <div class="loaders__table__item__last">
                <div class="loaders__table__item__first-column">
                  <div class="loaders__table__item__text_title">
                    <h4>Uppladdning</h4>
                  </div>
                </div>
                <div class="loaders__table__item__second-column">
                  <div class="loaders__table__item__text_paragraph">
                    <p>
                      Använd en laddindikator/
                      <span lang="en">loading spinner</span>, när tjänsten
                      väntar på att användaren ska göra något eller att data har
                      skickats eller sparats av användaren. Till exempel vid
                      uppladdning av en fil, eller vid en inloggning. Om det tar
                      mer än 10 sekunder kan det vara lämpligt att inkludera ett
                      meddelande.
                    </p>
                  </div>
                </div>
                <div class="loaders__table__item__third-column">
                  <div class="loaders__table__item__last__image">
                    <digi-media-image
                      afUnlazy
                      afFullwidth
                      afSrc="/assets/images/pattern/loading-example/Upload_example.png"
                      afAlt=""></digi-media-image>
                  </div>
                </div>
              </div>
            </div>
          </digi-layout-container>
          <digi-layout-block
            af-variation="secondary"
            afVerticalPadding
            af-no-gutter
            afMarginTop>
            <h2>Tillgänglighet och laddningsindikator</h2>
            <p>
              När vi implementerar <span lang="en">skeleton loader</span> och{' '}
              <span lang="en">loading spinner</span> med tanke på en tillgänglig
              användarupplevelse finns det flera aspekter att beakta.
            </p>
            <h3>Textbaserad information</h3>
            <p>
              Se till att varje laddare och spinner har tillräcklig textbaserad
              information. Använd <digi-code af-code="<span>"></digi-code>
              -element eller ARIA-rollen "status" för att kommunicera dess syfte
              och status till användaren. Till exempel kan du använda
              "Laddar..." eller "Väntar på data..." för att beskriva vad som
              händer.
            </p>
            <h3>Tangentbordstöd</h3>
            <p>
              Se till att användare kan interagera med laddaren och spinnern med
              tangentbordet. Använd lämpliga HTML-element som knappar eller
              länkar och tillhandahåll fokusstyrning och aktiveringsmöjligheter
              med tangentbordet.
            </p>
            <h3>Tillgängliga ARIA-attribut</h3>
            <p>
              Använd ARIA{' '}
              <span lang="en">(Accessible Rich Internet Applications)</span>{' '}
              -attribut när det är nödvändigt för att förbättra
              tillgängligheten. Till exempel, om din laddare är en del av en
              förväntad uppdatering, kan du använda `aria-live` för att meddela
              skärmläsare om ändringar i innehållet.
            </p>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            af-vertical-padding
            af-margin-bottom>
            <h2>
              Mät prestanda, åtgärda flaskhalsar och tänk på klimatavtrycket
            </h2>
            <p>
              Upprätthåll en process för att övervaka och optimera prestanda
              över tiden. Prestanda kan variera beroende på användning och
              trafikbelastning. Överväg att använda tekniker för progressiv
              förbättring för att säkerställa att din webbplats eller
              applikation fungerar även på äldre webbläsare eller enheter med
              långsammare prestanda.
            </p>
            <h3>Tjänstens klimatavtryck</h3>
            <p>
              Internet, serverhallar, telekommunikationsnätverk och
              slutanvändarprodukter som telefoner och bärbara datorer - använder
              mycket el. Det slukar ungefär samma mängd el som Storbritannien,
              en av världens största ekonomier. Medan vi fortsätter att
              konsumera allt mer data i våra dagliga liv, ökar mängden el som
              internet förbrukar snabbt. Denna enorma elförbrukning har ett
              enormt koldioxidavtryck, och det är något som vi behöver hantera.
              De flesta saker som vi, som jobbar med webbutveckling kan göra för
              att förbättra prestanda och användarupplevelse, gynnar också
              energieffektiviteten och därigenom koldioxidavtrycket.
            </p>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}
