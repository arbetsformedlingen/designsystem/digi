import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
import typescript from '@rollup/plugin-typescript';
import nodeResolve from '@rollup/plugin-node-resolve';
// https://stenciljs.com/docs/config

console.log('docs!');

const dev: boolean = process.argv && process.argv.indexOf('--dev') > -1;
const apiEnv: string = dev ? 'dev' : 'prod';

export const config: Config = {
  namespace: 'digi-arbetsformedlingen-docs',
  globalStyle: 'src/global/app.scss',
  globalScript: 'src/global/app.ts',
  taskQueue: 'async',
  enableCache: false,
  env: {
    apiEnv: apiEnv
  },
  devServer: {
    reloadStrategy: 'pageReload',
    port: 4444
  },
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'www',
      baseUrl: 'http://.local/',
      serviceWorker: {
        swSrc: './src/service-worker.js',
        swDest:
          '../../../../dist/apps/arbetsformedlingen/docs/www/service-worker.js'
      },
      copy: [
        {
          src: './robots.txt',
          dest: './robots.txt'
        },
        {
          src: './_redirects',
          dest: './_redirects'
        },
        {
          src: './sitemap.xml',
          dest: './sitemap.xml'
        },
        {
          src: '../../../../libs/arbetsformedlingen/fonts/src/assets/fonts/**/*',
          dest: './assets/fonts'
        }
      ]
    },
    {
      type: 'www',
      dir: 'www-netlify',
      serviceWorker: null,
      baseUrl: 'http://.local/',
      copy: [
        {
          src: '../../../../libs/arbetsformedlingen/fonts/src/assets/fonts/**/*',
          dest: './assets/fonts'
        }
      ]
    }
  ],
  plugins: [sass()],
  rollupPlugins: {
    before: [
      nodeResolve({ browser: true }),
      typescript({ tsconfig: `${__dirname}/tsconfig.json` })
    ]
  }
};
