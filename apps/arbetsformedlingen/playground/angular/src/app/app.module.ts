import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

import appRoutes from './app.router';

import { DigiArbetsformedlingenAngularModule } from 'arbetsformedlingen-angular-dist';

import { ArticleDemoComponent } from './components/article-demo/article-demo.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { ChartsComponent } from './components/charts/charts.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FooterComponent } from './components/framework/footer/footer.component';
import { FormComponent } from './components/form/form.component';
import { FormcontrolInvalidPipe } from './shared/pipes/control-invalid.pipe';
import { FormReceiptComponent } from './components/receipt/receipt.component';
import { GridComponent } from './components/grid/grid.component';
import { HeaderComponent } from './components/framework/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { LanguagepickerComponent } from './components/languagepicker/languagepicker.component';
import { LinkButtonsComponent } from './components/link-buttons/link-buttons.component';
import { LinksComponent } from './components/links/links.component';
import { MultifilterPocComponent } from './components/multifilter-poc/multifilter-poc.component';
import { NavigationDesignsystemComponent } from './components/navigation-demo/navigation-designsystem/navigation-designsystem.component';
import { NavigationExternalComponent } from './components/navigation-demo/navigation-external/navigation-external.component';
import { NavigationIntranetComponent } from './components/navigation-demo/navigation-intranet/navigation-intranet.component';
import { NavigationPageComponent } from './components/navigation-demo/navigation-page/navigation-page.component';
import { NyaIkonerComponent } from './components/nya-ikoner/nya-ikoner.component';
import { SharedNavigationComponent } from './components/navigation-demo/shared-navigation/shared-navigation.component';
import { SidenavComponent } from './components/framework/sidenav/sidenav.component';
import { StepsComponent } from './components/steps/steps.component';
import { TimepickerComponent } from './components/timepicker/timepicker.component';
import { ValidationComponent } from './components/validering/validation.component';
import { ToolsLanguagepickerComponent } from './components/tools-languagepicker/tools-languagepicker.component';
import { WheelOfFortuneComponent } from './components/wheel-of-fortune/wheel-of-fortune.component';
import { TestFormSelectComponent } from './components/test-form-select/test-form-select.component';
import { ForloppsstegarenComponent } from './components/forloppsstegare/forloppsstegaren.component';
import { DatepickerValueAccessorComponent } from './components/datepicker-value-accessor/datepicker-value-accessor.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticleDemoComponent,
    CalendarComponent,
    ChartsComponent,
    DatepickerComponent,
    FileUploadComponent,
    FooterComponent,
    FormComponent,
    FormcontrolInvalidPipe,
    FormReceiptComponent,
    GridComponent,
    HeaderComponent,
    HomeComponent,
    LanguagepickerComponent,
    LinkButtonsComponent,
    LinksComponent,
    MultifilterPocComponent,
    NavigationDesignsystemComponent,
    NavigationExternalComponent,
    NavigationIntranetComponent,
    NavigationPageComponent,
    NyaIkonerComponent,
    SharedNavigationComponent,
    SidenavComponent,
    StepsComponent,
    TimepickerComponent,
    LanguagepickerComponent,
    FormReceiptComponent,
    ValidationComponent,
    LinkButtonsComponent,
    ChartsComponent,
    ToolsLanguagepickerComponent,
    DatepickerComponent,
    MultifilterPocComponent,
    WheelOfFortuneComponent,
    TestFormSelectComponent,
    ForloppsstegarenComponent,
    DatepickerValueAccessorComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    DigiArbetsformedlingenAngularModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
