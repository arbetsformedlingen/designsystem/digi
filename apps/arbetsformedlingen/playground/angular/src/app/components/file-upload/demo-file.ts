const MyFiles = [
  {
    id: 'file-5leom',
    status: 'OK',
    base64: '',
    name: 'kvitto-tunnelbaneresa-2023.09.01.jpg',
    size: 1229695,
    type: 'application/doc'
  },
  {
    id: 'file-oiz6p',
    status: 'OK',
    base64: '',
    name: 'kvitto-för-bussresa-mellan-fridhemsplan-och-rådmansgatan.jpg',
    size: 1149459,
    type: 'application/pdf'
  },
  {
    id: 'file-eiz7p',
    status: 'OK',
    base64: '',
    name: 'kvittotest.pdf',
    size: 2377736,
    type: 'application/pdf'
  }
];

export default MyFiles;
