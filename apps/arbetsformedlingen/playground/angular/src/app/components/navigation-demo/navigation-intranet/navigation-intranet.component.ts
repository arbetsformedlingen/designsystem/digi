import { Component, HostListener, Input } from '@angular/core';

import {
  UtilBreakpointObserverBreakpoints,
  NavigationSidebarVariation,
  NavigationVerticalMenuVariation
} from 'arbetsformedlingen-dist';

import IntranetNavigation from '../intranetNavigation.json';

@Component({
  selector: 'digi-ng-navigation-intranet',
  templateUrl: './navigation-intranet.component.html',
  styleUrls: ['./navigation-intranet.component.scss']
})
export class NavigationIntranetComponent {
  NavigationSidebarVariation = NavigationSidebarVariation;
  NavigationVerticalMenuVariation = NavigationVerticalMenuVariation;

  @Input() navLayout = 'primary';

  private _isMobile: boolean;

  navData = IntranetNavigation;

  get sideNavHeader() {
    return this._isMobile ? 'false' : 'true';
  }

  @HostListener('document:afOnChange', ['$event'])
  onAfOnChange(event) {
    if (event.target.tagName == 'DIGI-UTIL-BREAKPOINT-OBSERVER') {
      this._isMobile =
        event.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }
}
