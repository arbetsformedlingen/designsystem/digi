import { Component } from '@angular/core';

@Component({
  selector: 'digi-ng-navigation-page',
  templateUrl: './navigation-page.component.html',
  styleUrls: ['./navigation-page.component.scss']
})
export class NavigationPageComponent {}
