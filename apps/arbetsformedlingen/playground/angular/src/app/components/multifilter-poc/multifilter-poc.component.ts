import { Component, OnInit } from '@angular/core';
import { CategoryM, getData, ItemM } from './multifilter-poc.functions';

@Component({
  selector: 'digi-multifilter-poc',
  templateUrl: './multifilter-poc.component.html',
  styleUrls: ['./multifilter-poc.component.scss']
})
export class MultifilterPocComponent implements OnInit {
  data: Array<ItemM> = getData(1000);

  allHits = 'Alla träffar';

  /*** Search ***/
  updateSearchResult(evt: Event) {
    const element = evt.target as HTMLInputElement;

    this.searchPhrase = element.value;
    this.searchResult = JSON.parse(JSON.stringify(this.data)).filter(
      (item: ItemM) => item.name.includes(this.searchPhrase)
    );
    this.setDisplayCategories();
  }

  searchPhrase = '';
  searchResult: Array<ItemM> = [];
  /*** Category filter ***/
  displayCategories: Array<CategoryM> = [];

  setDisplayCategories() {
    const _categories: Array<string> = this.searchResult.map(
      (item) => item.category
    );
    this.displayCategories = [
      ..._categories
        .filter((category, i) => _categories.indexOf(category) === i)
        .map((category) => {
          return {
            name: category,
            hits: _categories.filter((c) => c === category).length,
            selected: false
          };
        })
        .sort((a, b) => b.hits - a.hits)
    ];
    this.selectedCategories = this.displayCategories.map(
      (category) => category.name
    );
    this.categoryResult = this.searchResult.filter((item) =>
      this.selectedCategories.includes(item.category)
    );
    this.setPagination();
  }

  updateSelectedCategories(evt: CustomEvent) {
    this.selectedCategories = evt.detail;
    if (this.selectedCategories[0] === 'Alla kategorier') {
      this.categoryResult = [...this.searchResult];
    } else {
      this.categoryResult = this.searchResult.filter((item) =>
        this.selectedCategories.includes(item.category)
      );
    }
    this.setPagination();
  }

  selectedCategories: Array<string> = [];

  get formattedSelectedCategories() {
    return this.selectedCategories.length === this.displayCategories.length
      ? this.allHits
      : this.selectedCategories.join(', ');
  }

  categoryResult: Array<ItemM> = [];

  updateFilter(evt: Event) {
    console.log(evt);
  }

  filterResult: Array<ItemM> = [];

  /*** Pagination ***/
  pagiHitsPerPage = 25;
  pagiTotalPages = 0;
  pagiTotalHits = 0;
  pagiCurrentStart = 0;
  pagiCurrentEnd = 0;

  setPagination() {
    this.pagiTotalHits = this.categoryResult.length;
    this.pagiTotalPages = Math.ceil(this.pagiTotalHits / this.pagiHitsPerPage);
    this.setPaginationIndex(0);
  }

  updatePagination(evt: CustomEvent) {
    this.setPaginationIndex(evt.detail - 1);
  }

  setPaginationIndex(index: number) {
    this.pagiCurrentStart = index * this.pagiHitsPerPage + 1;
    this.pagiCurrentEnd = (index + 1) * this.pagiHitsPerPage;
    if (this.pagiCurrentEnd > this.pagiTotalHits)
      this.pagiCurrentEnd = this.pagiTotalHits;
    this.setFilterResult();
  }

  /*** Sort ***/
  updateSort(evt) {
    this.sortResult = parseInt(evt.target.activeListItemIndex) - 1;
    this.setFilterResult();
  }
  sortResult = 0;

  /*** Result ***/
  setFilterResult() {
    if (
      this.sortResult === 0 &&
      this.searchPhrase &&
      this.searchPhrase.length > 0
    ) {
      const result: Array<ItemM> = JSON.parse(
        JSON.stringify(this.categoryResult)
      );
      result.forEach(
        (item) => (item.order = item.name.indexOf(this.searchPhrase))
      );
      this.multifilterResult = result
        .sort((a, b) => a.order - b.order)
        .slice(this.pagiCurrentStart - 1, this.pagiCurrentEnd)
        .map((item) => item.name);
    } else
      this.multifilterResult = this.categoryResult
        .slice(this.pagiCurrentStart - 1, this.pagiCurrentEnd)
        .map((item) => item.name);
  }
  multifilterResult: Array<string> = [];

  ngOnInit(): void {
    this.searchResult = this.data;
    this.setDisplayCategories();
    this.categoryResult = this.searchResult.filter((item) =>
      this.selectedCategories.includes(item.category)
    );
    this.setPagination();
    this.setFilterResult();
  }
}
