const names: Array<string> = [
  'Apple',
  'Banana',
  'Cherry',
  'Dragonfruit',
  'Eggplant',
  'Fig',
  'Grape',
  'Honeydew',
  'Iris',
  'Jasmine',
  'Kiwi',
  'Lemon',
  'Mango',
  'Nectarine',
  'Orange',
  'Pineapple',
  'Quince',
  'Raspberry',
  'Strawberry',
  'Tomato',
  'Ugli fruit',
  'Vanilla',
  'Watermelon',
  'Xigua',
  'Yuzu',
  'Zucchini',
  'Almond',
  'Butter',
  'Cinnamon',
  'Doughnut',
  'Eclair',
  'Flan',
  'Gingerbread',
  'Honey',
  'Ice cream',
  'Jelly',
  'Ketchup',
  'Lollipop',
  'Marshmallow',
  'Nougat',
  'Oreo',
  'Pancake',
  'Quesadilla',
  'Raisin',
  'Scone',
  'Tiramisu',
  'Udon',
  'Vegetable',
  'Waffle',
  'Xiaolongbao',
  'Yogurt',
  'Ziti',
  'Avocado',
  'Barley',
  'Carrot',
  'Dill',
  'Egg',
  'Fennel',
  'Garlic',
  'Hibiscus',
  'Iced tea',
  'Jalapeno',
  'Kale',
  'Lettuce',
  'Mushroom',
  'Nutmeg',
  'Oat',
  'Parsley',
  'Quinoa',
  'Radish',
  'Spinach',
  'Tofu',
  'Ube',
  'Vinegar',
  'Wheat',
  'Xacuti',
  'Yam',
  'Zest',
  'Apricot',
  'Blueberry',
  'Cantaloupe',
  'Durian',
  'Elderberry',
  'Fruit punch',
  'Grapefruit',
  'Huckleberry',
  'Iced coffee',
  'Jicama',
  'Kumquat',
  'Lime',
  'Mandarin',
  'Nashi pear',
  'Olive',
  'Papaya',
  'Quince',
  'Rhubarb',
  'Star fruit',
  'Tangerine',
  'Ugli fruit',
  'Vanilla',
  'Watermelon',
  'Xigua',
  'Yuzu',
  'Zucchini'
];
const categories: Array<string> = [
  'Outdoor Gear',
  'Office Supplies',
  'Musical Instruments',
  'Board Games',
  'Pet Accessories',
  'Candles & Scents',
  'Handbags & Purses',
  'Party Supplies',
  'Car Accessories',
  'Vintage Clothing',
  'Clothing',
  'Electronics',
  'Beauty',
  'Home & Garden',
  'Food & Beverage',
  'Books',
  'Sports & Outdoors',
  'Toys & Games',
  'Health & Wellness',
  'Arts & Crafts'
]
  .sort(() => Math.random() - 0.5)
  .slice(0, 10);

const used_ids: Array<string> = [];
function getNewID(): string {
  let id: string = Math.random().toString(36).slice(2, 7);
  while (used_ids.includes(id)) id = Math.random().toString(36).slice(2, 7);
  used_ids.push(id);
  return id;
}
const used_names: Array<string> = [];
function getNewName(): string {
  let name: string = getRandomIndexes(3, names.length)
    .map((index) => names[index])
    .join(' ');
  while (used_names.includes(name))
    name = getRandomIndexes(3, names.length)
      .map((index) => names[index])
      .join(' ');
  used_names.push(name);
  return name;
}
export interface ItemM {
  id: string;
  name: string;
  category: string;
  order?: number;
}
export interface CategoryM {
  name: string;
  hits?: number;
  selected?: boolean;
}
export function getData(amount_of_items: number): Array<ItemM> {
  const data: Array<ItemM> = [];
  for (let i = 0; i < amount_of_items; i++) data.push(getItem());
  return data;
}
function getItem(): ItemM {
  return {
    id: getNewID(),
    name: getNewName(),
    category: categories[Math.floor(Math.random() * categories.length)]
  };
}
function getRandomIndexes(amount: number, max_value: number): Array<number> {
  const numbers: Array<number> = [];
  for (let i = 0; i < amount; i++) {
    let number: number = Math.floor(Math.random() * max_value);
    while (numbers.includes(number))
      number = Math.floor(Math.random() * max_value);
    numbers.push(number);
  }
  return numbers;
}
