import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'digi-ng-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent {
  currentDate = '';

  bookings = [
    '2022-06-12',
    '2022-05-12',
    '2022-05-16',
    '2022-05-09',
    '2022-06-20'
  ];

  get hasBookings() {
    return JSON.stringify(this.bookings);
  }

  get headingText() {
    return this.currentDate;
  }

  @HostListener('document:afOnDateChange', ['$event'])
  onAfOnDateChange(event) {
    this.currentDate = event.detail;
  }
}
