import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'digi-wheel-of-fortune',
  templateUrl: './wheel-of-fortune.component.html',
  styleUrls: ['./wheel-of-fortune.component.scss']
})
export class WheelOfFortuneComponent implements OnInit {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  spinning = false;
  winner: string | undefined = undefined;
  options: Array<string> = [
    'David',
    'Elliot',
    'Fredrik',
    'Jonas',
    'Magnus',
    'Marko',
    'Rosie',
    'Sandra',
    'Sandra',
    'Sean',
    'Sebastian',
    'Supreet',
    'Viyan'
  ];
  colors: Array<string> = [
    '#2f9fb3',
    '#aef6f7',
    '#f9f8f3',
    '#e8cdc7',
    '#7a4d4a',
    '#ffcfb3',
    '#fee9ea',
    '#ebc0e4',
    '#e690bc',
    '#886eaa'
  ];

  ngOnInit(): void {
    this.canvas = document.getElementById(
      'playground--wheel-of-fortune--canvas'
    ) as HTMLCanvasElement;
    this.ctx = this.canvas.getContext('2d');
    this.drawWheel();
  }

  updateOption(evt, i) {
    this.options[i] = evt.target.value;
    this.drawWheel();
  }
  addOption() {
    this.options = [...this.options, ''];
    this.drawWheel();
  }
  removeOption(i) {
    this.options = [...this.options.slice(0, i), ...this.options.slice(i + 1)];
    this.drawWheel();
  }
  updateColor(evt, i) {
    this.colors[i] = evt.target.value;
    this.drawWheel();
  }
  addColor() {
    this.colors = [...this.colors, 'white'];
    this.drawWheel();
  }
  removeColor(i) {
    this.colors = [...this.colors.slice(0, i), ...this.colors.slice(i + 1)];
    this.drawWheel();
  }
  drawWheel() {
    const wheelRadius = this.canvas.width / 2;
    const segmentAngle = (2 * Math.PI) / this.options.length;

    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.translate(wheelRadius, wheelRadius);

    this.options.forEach((option, index) => {
      this.ctx.beginPath();
      this.ctx.moveTo(0, 0);
      this.ctx.arc(
        0,
        0,
        wheelRadius,
        index * segmentAngle,
        (index + 1) * segmentAngle
      );
      this.ctx.lineTo(0, 0);
      this.ctx.closePath();
      this.ctx.fillStyle = this.colors[index % this.colors.length];
      this.ctx.fill();
      this.ctx.stroke();
      this.ctx.save();
      this.ctx.rotate((index + 0.5) * segmentAngle);
      this.ctx.fillStyle = 'black';
      this.ctx.font = '16px Arial';
      this.ctx.fillText(option, wheelRadius / 2, 0);
      this.ctx.restore();
    });

    this.ctx.translate(-wheelRadius, -wheelRadius);
  }
  spinWheel() {
    this.animateWheel(
      this.canvas.width / 2,
      Math.random() * 20 + 20,
      0,
      Date.now(),
      0.2
    );
    this.spinning = true;
  }
  animateWheel(wheelRadius, startSpeed, currentAngle, startTime, friction) {
    const elapsedTime = Date.now() - startTime;
    const speedFraction = 1 - (friction * elapsedTime) / 1000;
    const speed = startSpeed * speedFraction;

    if (speed > 0) {
      const frameDuration = 1000 / 60;
      const angleDelta = (speed * frameDuration) / 1000;
      currentAngle += angleDelta;
      this.ctx.setTransform(1, 0, 0, 1, wheelRadius, wheelRadius);
      this.ctx.rotate(currentAngle);
      this.ctx.translate(-wheelRadius, -wheelRadius);
      this.drawWheel();
      requestAnimationFrame(() => {
        this.animateWheel(
          wheelRadius,
          startSpeed,
          currentAngle,
          startTime,
          friction
        );
      });
    } else {
      this.spinning = false;
      this.setWinner(currentAngle);
    }
  }
  setWinner(currentAngle) {
    const segmentAngle = (2 * Math.PI) / this.options.length;
    const finalAngle = currentAngle % (2 * Math.PI);
    const reversedAngle = 2 * Math.PI - finalAngle;
    const offsetAngle = reversedAngle + (3 * Math.PI) / 2;
    const winningIndex =
      Math.floor(offsetAngle / segmentAngle) % this.options.length;
    this.winner = this.options[winningIndex];
  }
  resetWinner() {
    this.winner = undefined;
  }
}
