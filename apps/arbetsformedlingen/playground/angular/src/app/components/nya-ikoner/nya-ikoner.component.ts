import { Component } from '@angular/core';

@Component({
  selector: 'digi-nya-ikoner',
  templateUrl: './nya-ikoner.component.html',
  styleUrls: ['./nya-ikoner.component.scss']
})
export class NyaIkonerComponent {
  iconStates: { [key: string]: boolean } = {
    video: true,
    mic: true,
    phone: true,
    screen1: true,
    screen: true,
    screen2: true
  };

  toggle(icon: string) {
    this.iconStates[icon] = !this.iconStates[icon];
  }
}
