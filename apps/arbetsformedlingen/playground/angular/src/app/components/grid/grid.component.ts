import { Component } from '@angular/core';
import { LayoutBlockContainer } from 'arbetsformedlingen-dist';

@Component({
  selector: 'digi-ng-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent {
  LayoutBlockContainer = LayoutBlockContainer;
}
