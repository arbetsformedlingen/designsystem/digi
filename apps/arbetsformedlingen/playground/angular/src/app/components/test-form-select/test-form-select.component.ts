import { Component } from '@angular/core';

@Component({
  selector: 'digi-test-form-select',
  templateUrl: './test-form-select.component.html',
  styleUrls: ['./test-form-select.component.scss']
})
export class TestFormSelectComponent {
  listA: Array<string> = ['One', 'Two', 'Three'];
  listB: Array<string> = ['A', 'B', 'C'];

  listAchange(evt) {
    console.log(evt);
    console.log(evt.target.value);
    switch (evt.target.value) {
      case 'Two':
        this.listB = ['AA', 'BB', 'CC'];
        break;
      case 'Three':
        this.listB = ['AAA', 'BBB', 'CCC'];
        break;
      default:
        this.listB = ['A', 'B', 'C'];
        break;
    }
  }
  listBchange(evt) {
    console.log(evt);
    console.log(evt.target.value);
  }
}
