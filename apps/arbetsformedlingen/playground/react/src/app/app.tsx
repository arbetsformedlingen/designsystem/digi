import { Outlet } from 'react-router-dom';
import './app.scss';
import { DigiBadgeStatus, DigiTypography } from 'arbetsformedlingen-react-dist';
import {
  BadgeStatusSize,
  BadgeStatusVariation,
  BadgeStatusType
} from 'arbetsformedlingen-dist';

import Header from './components/header/Header';
import Footer from './components/footer/Footer';

export function App() {
  return (
    <DigiTypography>
      <div className="app">
        <Header />
        <main className="app__main">
          <DigiBadgeStatus
            afType={BadgeStatusType.APPROVED}
            afVariation={BadgeStatusVariation.PRIMARY}
            afText="Godkänd"
            afSize={BadgeStatusSize.LARGE}></DigiBadgeStatus>
          <Outlet />
        </main>
        <Footer />
      </div>
    </DigiTypography>
  );
}

export default App;
