import {
  NavigationSidebarPosition,
  NavigationSidebarVariation,
  NavigationVerticalMenuActiveIndicatorSize,
  NavigationVerticalMenuVariation
} from 'arbetsformedlingen-dist';
import {
  DigiNavigationSidebar,
  DigiNavigationVerticalMenu,
  DigiNavigationVerticalMenuItem,
  DigiUtilBreakpointObserver
} from 'arbetsformedlingen-react-dist';
import { useState, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './Navigation.module.scss';

interface navItem {
  active: boolean;
  activeSubnav?: boolean;
  href?: string;
  overrideLink?: boolean;
  text: string;
  id: string;
  subNavs?: navItem[];
}

const navItems: navItem[] = [
  {
    text: 'Ekonomi, inköp och upphandling (1)',
    href: '/ekonomi',
    active: false,
    id: 'ekonomi-inkop-och-upphandling',
    overrideLink: true,
    subNavs: [
      {
        text: 'Ekonomi (2)',
        href: '/ekonomi',
        active: false,
        id: 'ekonomi',
        overrideLink: true,
        subNavs: [
          {
            text: 'Ekonomi (3)',
            href: '/ekonomi',
            active: false,
            id: 'ekonomi',
            overrideLink: true,
            subNavs: [
              {
                text: 'Ekonomi (4)',
                href: '/ekonomi',
                active: false,
                id: 'ekonomi',
                overrideLink: true
              },
              {
                text: 'Inköp och upphandling (4)',
                href: '/inkop-och-upphandling',
                active: true,
                id: 'inkop',
                overrideLink: true
              },
              {
                text: 'Beställ varor och tjänster (4)',
                href: '/ekonomi',
                active: false,
                id: 'ekonomi-inkop-och-upphandling',
                overrideLink: true
              }
            ]
          },
          {
            text: 'Inköp och upphandling (3)',
            href: '/inkop-och-upphandling',
            active: false,
            id: 'inkop',
            overrideLink: true
          },
          {
            text: 'Beställ varor och tjänster (3)',
            href: '/ekonomi',
            active: false,
            id: 'ekonomi-inkop-och-upphandling',
            overrideLink: true
          }
        ]
      },
      {
        text: 'Inköp och upphandling (2)',
        href: '/inkop-och-upphandling',
        active: false,
        id: 'inkop',
        overrideLink: true
      },
      {
        text: 'Beställ varor och tjänster (2)',
        href: '/ekonomi',
        active: false,
        id: 'ekonomi-inkop-och-upphandling',
        overrideLink: true
      }
    ]
  },
  {
    text: 'Förebygga säkerhetsrisker (1)',
    href: '/forebygga-sakerhetsrisker',
    active: false,
    id: 'forebygga-sakerhetsrisker',
    overrideLink: true,
    subNavs: [
      {
        text: 'Din personliga säkerhet (2)',
        href: '/din-personliga-sakerhet',
        active: false,
        id: 'din-personliga-sakerhet',
        overrideLink: true
      },
      {
        text: 'Hantera och skydda information (2)',
        href: '/hantera-och-skydda-information',
        active: false,
        id: 'hantera-och-skydda-information',
        overrideLink: true
      },
      {
        text: 'Så här förebygger vi säkerhetsrisker (2)',
        href: '/sa-har-forebygger-vi-sakerhetsrisker',
        active: false,
        id: 'sa-har-forebygger-vi-sakerhetsrisker',
        overrideLink: true
      }
    ]
  }
];

const subNavItems: navItem[] = [
  {
    text: 'Ekonomi',
    href: '/article',
    active: false,
    id: 'ekonomi',
    overrideLink: true
  },
  {
    text: 'Inköp och upphandling',
    href: '/article2',
    active: false,
    id: 'inkop',
    overrideLink: true
  },
  {
    text: 'Beställ varor och tjänster',
    href: '/article3',
    active: false,
    id: 'ekonomi-inkop-och-upphandling',
    overrideLink: true
  }
];

const Navigation = () => {
  const navigate = useNavigate();

  const navigationRef = useRef<React.ElementRef<
    typeof DigiNavigationVerticalMenu
  > | null>(null);

  const [activeLink, setActiveLink] = useState('/');

  const [subNav, setSubNav] = useState<navItem[]>([]);

  function navItemClickHandler(e: CustomEvent) {
    const el = e.detail.target;
    if (subNav.length === 0) {
      setSubNav([...subNavItems]);

      setTimeout(() => {
        !!navigationRef &&
          !!navigationRef.current &&
          navigationRef?.current.afMSetCurrentActiveLevel(el);
      }, 0);
    }
    if (el.tagName === 'A') {
      navigate(el.href.replace('http://localhost:4200', ''));
      setActiveLink(el.href.replace('http://localhost:4200', ''));
    }
  }

  function linkHandler(e: CustomEvent) {
    navigate(e.detail.target.href.replace('http://localhost:4200', ''));
    setActiveLink(e.detail.target.href.replace('http://localhost:4200', ''));
  }

  return (
    <DigiUtilBreakpointObserver>
      <DigiNavigationSidebar
        id="sideNav"
        className={styles['sidebar']}
        af-active={true}
        af-variation={NavigationSidebarVariation.STATIC}
        af-position={NavigationSidebarPosition.END}
        af-hide-header={true}
        af-sticky-header="true"
        af-close-button-text="Stäng">
        <DigiNavigationVerticalMenu
          ref={navigationRef}
          id="mainNav"
          af-id="mainNavigation"
          af-aria-label="Huvudmeny"
          af-active-indicator-size={
            NavigationVerticalMenuActiveIndicatorSize.PRIMARY
          }
          af-variation={NavigationVerticalMenuVariation.PRIMARY}>
          <ul className="navigation__root">
            {navItems.map((item: navItem) => {
              return (
                <li key={item.id}>
                  <DigiNavigationVerticalMenuItem
                    afText={item.text}
                    afHref={item?.href}
                    afOverrideLink={item?.overrideLink}
                    afActive={item?.active}
                    afHasSubnav={!!item.subNavs && true}
                    afActiveSubnav={!!item.subNavs && false}
                    afId={item.id}></DigiNavigationVerticalMenuItem>

                  {item.subNavs && item.subNavs?.length > 0 && (
                    <ul>
                      {item.subNavs.map((subItem: navItem) => {
                        return (
                          <li key={subItem.id}>
                            {!!subItem.subNavs && (
                              <DigiNavigationVerticalMenuItem
                                afText={subItem.text}
                                afHref={subItem?.href}
                                afOverrideLink={subItem?.overrideLink}
                                afActive={subItem?.active}
                                afHasSubnav={true}
                                afActiveSubnav={!!subItem.subNavs && false}
                                afId={
                                  subItem.id
                                }></DigiNavigationVerticalMenuItem>
                            )}

                            {!subItem.subNavs && (
                              <DigiNavigationVerticalMenuItem
                                afText={subItem.text}
                                afHref={subItem?.href}
                                afOverrideLink={subItem?.overrideLink}
                                afActive={subItem?.active}
                                afId={
                                  subItem.id
                                }></DigiNavigationVerticalMenuItem>
                            )}

                            {subItem.subNavs && subItem.subNavs?.length > 0 && (
                              <ul>
                                {subItem.subNavs.map((subItem2: navItem) => {
                                  return (
                                    <li key={subItem2.id}>
                                      <DigiNavigationVerticalMenuItem
                                        afText={subItem2.text}
                                        afHref={subItem2?.href}
                                        afOverrideLink={subItem2?.overrideLink}
                                        afActive={subItem2?.active}
                                        afId={
                                          subItem2.id
                                        }></DigiNavigationVerticalMenuItem>
                                    </li>
                                  );
                                })}
                              </ul>
                            )}
                          </li>
                        );
                      })}
                    </ul>
                  )}
                </li>
              );
            })}

            <li>
              <DigiNavigationVerticalMenuItem
                afText={'Dynamiska undernivåer'}
                afHref={'/'}
                afOverrideLink={true}
                afActive={'/' === activeLink}
                afHasSubnav={true}
                afActiveSubnav={false}
                afId={'testItem'}
                onAfOnClick={
                  navItemClickHandler
                }></DigiNavigationVerticalMenuItem>

              {subNav.length > 0 && (
                <ul>
                  {subNav.map((item: navItem) => {
                    function setActive() {
                      return (
                        item?.href?.replace('http://localhost:4200', '') ===
                        activeLink
                      );
                    }
                    return (
                      <li key={item.id}>
                        <DigiNavigationVerticalMenuItem
                          afText={item.text}
                          afHref={item?.href}
                          afOverrideLink={item?.overrideLink}
                          afActive={setActive()}
                          afId={item.id}
                          onAfOnClick={
                            linkHandler
                          }></DigiNavigationVerticalMenuItem>
                      </li>
                    );
                  })}
                </ul>
              )}
            </li>
          </ul>
        </DigiNavigationVerticalMenu>
      </DigiNavigationSidebar>
    </DigiUtilBreakpointObserver>
  );
};

export default Navigation;
