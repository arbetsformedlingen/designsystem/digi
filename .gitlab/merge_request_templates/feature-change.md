## Checklista

- [ ] Jag har synkat förändringen med designsystemsteamet
- [ ] Koden följer våra standarder
- [ ] Ändringen finns i dokumentationen
- [ ] Ändringen finns med i UI-kitet
- [ ] Jag har testat förändringen i
  - Vanilla HTML (Web Components)
  - Angular
  - React
- [ ] Jag har bett intressenter testa ändringen (vid behov)
  - Andra i teamet
  - Med hjälpmedel
  - Team som efterfrågat förändringen
