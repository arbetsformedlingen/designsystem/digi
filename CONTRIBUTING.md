# Contribution Manual

Allt som krävs för att du ska kunna komma igång och utveckla för designsystemet finns dokumenterat här.

## Komma igång (Utvecklare)

- Installera [Git ↗](https://git-scm.com/downloads)
- Installera [Volta ↗](https://docs.volta.sh/guide/getting-started) (Versionshanterare för Javascript) - Detta är valfritt, men verifiera annars att du har rätt version av Node installerad.

- Kör följande kommandon

```sh
# installera rätt version av Node
volta install node@18

# klona ner designsystemet
git clone git@gitlab.com:arbetsformedlingen/designsystem/digi.git
cd digi

# installera beroenden
npm install
```

### Starta utvecklingsmiljö

För att starta Komponentbiblioteket test-sida så kör du `npm run af`

Utvecklingsmiljön finns tillgänglig på `http://localhost:3333`

För att starta Dokumentationswebben test-sida så kör du `npm run af-docs`

Utvecklingsmiljön finns tillgänglig på `http://localhost:4444`

## Publicera testsidor för utvecklingsmiljöer

För att testa under ett projekts gång så använder vi Netlify för att publicera test-sidor som vi kan dela med teamet för mer ingående test.

**🔐 Innan du fortsätter:** Be någon i teamet om autentiseringsuppgifter till Netlify.

Första gången du kör kommandot kommer du att bli ombedd att logga in och godkänna tillgång till ditt Netlify-konto. Godkänn för att gå vidare.

Publicera Komponentbiblioteket (`libs/arbetsformedlingen/package`)

- `npm run deploy-af`, _skapar ny sida_, bygger och laddar upp
- `npm run update-af`, bygger och laddar upp till senaste sida du skapade

Publicera Dokumentationswebben (`apps/arbetsformedlingen/docs`)

- `npm run deploy-af-docs`, _skapar ny sida_, bygger och laddar upp
- `npm run update-af-docs`, bygger och laddar upp till senaste sida du skapade

## Verktyg för att skapa nytt

När du skapar en ny komponent eller undersida i dokumentationswebben så finns det verktyg som automatiskt hjälper dig att skapa nödvändiga filer på rätt ställe.

Observera att vissa kommandon kräver manuellt arbete efteråt. Läs noga vad kommandot skriver ut.

`TODO 📝` indikerar manuellt arbete.

🚨 Vid fel så kan det bero på att komponenten eller undersidan du försöker skapa redan finns. Se till att namn och tag är unika.

### Skapa ny komponent

⚙️ `npm run new-component -- --tag form-filter --name "Multifilter"`

`form-filter` i kommandot ovan blir komponentens tag-nam. "Multifilter" blir dens svenska namn.

När komponenten är skapad behöver den flyttas från "libs\arbetsformedlingen\package\src\components\form-filter" till passande kategori-mapp.
Exempel: Flytta till mappen "\_form" om det är en formulärkomponent.

### Skapa exempelsida för komponent

⚙️ `npm run new-component-page -- --tag form-filter --title "Multifilter"`

Kommandot meddelar sökvägen till exempelsidan. 

Lägg till komponenten i "categoryData.ts" kopplad till passande kategori. 
Detta gör att komponenten visas i navigation och sök.

### Skapa ny ikon

⚙️ `npm run new-icon -- --tag chevron-upside-down`

Kommandot meddelar sökvägen till ikonen.

### Skapa undersida i dokumentationswebb

_Exempel: Designmönster_

⚙️ `npm run new-docs-page -- --id about-filters --title "Filtrering och sortering"`

Kommandot meddelar sökvägen till undersidan.
Lägg in "Route" nedan i "digi-docs.tsx"
Uppdatera path så att det följer namngivning för relaterade sidor
`<Route path="/about-filters">
  <digi-docs-about-filters />
</Route>`

## Kör tester

Vi skriver e2e-tester när vi testar våra komponenter som körs med hjälp av `Stencil`, `Puppeteer` och `Jest`. E2E-tester har filändelsen `.e2e.ts`.

⚙️ `npx nx test arbetsformedlingen`

Använd `watch`-läge när du vill att testerna körs kontinuerligt när du gör ändringar i källkoden.

⚙️ `npx nx test arbetsformedlingen -- --watch`

Vi skriver enhetstester med hjälp av `Stencil` och `Jest`. Enhetstester har filändelsen `.spec.ts`.

⚙️ `npx nx run-many -t test --exclude arbetsformedlingen`

Du kan även köra dessa i `watch`-läge med 

⚙️ `npx nx run-many -t test --exclude arbetsformedlingen --watch`

Kör alla tester

⚙️ `npm test`

**ℹ️ Tips & Tricks**

* Om du vill se webbläsaren för att kunna felsöka ett test kan du uppdatera konfigurationens `testing` i `libs/arbetsformedlingen/package/stencil.prod.ts` och lägga till fältet `browserHeadless: false`. Lägg sedan till `await page.waitForSelector('dummy', { timeout: 1000000 })` i ditt test för att kunna använda webinspektorn som vanligt.

## Release av Beta

* Skapa MR på Gitlab med beta-förändringar
* Kör `npm run version-arbetsformedlingen:beta`
* Uppdatera peerDependencies så att dom stämmer överens med ny version
* Pusha upp förändringar
* Öppna MR i Gitlab och öppna fliken `Pipelines`
* Tryck på siffrorna under översta pipelinen och vänta tills den har körts färdigt
* Starta `publish-beta-packages` som är grupperade under `deploy-beta`. Paketeten får då en beta-tag.
* Efter publicering kan paketet installeras i externa projekt med `npm i @digi/arbetsformedlingen@beta`

## Lint

Vi skiljer på lint och formatering. Vi använder eslint för att linta projektets filer och prettier för att formatera
dom enligt vår standard. Linten är kopplad till prettiers regler, men formaterar inte alla filer. Nästan all config
för lint ligger i projektets rot. Vissa projektspecifika lintregler finns antingen definierade i `project.json`
eller projektets `.eslintrc.json`

För att linta hela projektet kan du köra

`npm run lint`

Vill du bara linta lib-et du jobbar på kan du köra `nx run <lib>:lint` eller
`nx run <lib>:lint --fix` för att linten ska försökta automatiskt linta filerna rätt.

Beroende på vilken ändring du ska göra i designsystemet så kan du behöva starta upp olika delar.

### Undantag

Just nu lintar vi inte HTML-filer, utan formaterar dom endast.

## Prettier

För att formatera filerna enligt våra regler så kör `npm run format`

Du kan också aktivera prettier i ditt IDE och peka på vår `.prettierrc` fil.

## Exempel

### Skapa en komponent

> 💭 Vi skapar en ny komponent till designsystemet. Den ska heta `input-example`.

Du startar utvecklingsmiljön genom att köra följande i roten av projektet.

⚙️ `npm run af`

Skapa en ny komponent med följande kommando i roten av monorepot

⚙️ `npm run new-component -- --tag "input-example" --name "Exempel på inmatningsfält"`

Sedan följer du stegen i ` TODO 📝` som kommandot meddelar efter att kommandot är slutfört.

Relevanta filer ligger i följande mapp

🗂️ `/libs/arbetsformedlingen/package/src`

Lägg till taggen `<digi-input-example af-variation="primary"></digi-input-example>` i filen `src/index.html`. Din utvecklingsmiljö uppdateras automatiskt efter ändringar. Du ska se följande i din utvecklingsmiljö.

```
Hej Världen! 👋
Jag är digi-input-example
```

Nu är det fritt fram att implementera logik och styling i `input-example.scss` samt `input-example.tsx`.

**ℹ️ Tips & Tricks**

- Kod för `digi-form-input` ligger i filen `form-input.tsx` under `components/_form/form-input`.
- Om du letar efter kod för `digi-form-input` så är det lättare att söka efter `form-input.tsx`.
- Källkodsfiler för komponenten **namnges inte** med prefix `digi-`

### Skapa sida för komponent i dokumentationswebben

> 💭 Vi har skapat en komponent till designsystemet. Den ska dokumenteras och publiceras på dokumentationswebben. Komponenten heter `input-example`.

Du startar utvecklingsmiljön genom att köra följande i roten av projektet

⚙️ `npm run af-docs`

Utvecklingsmiljön finns tillgänglig på `http://localhost:4444`

Skapa en ny undersida med följande kommando i roten av monorepot

⚙️ `npm run new-component-page -- --tag "input-example" --title "Exempel på inmatningsfält"`

Sedan följer du stegen i ` TODO 📝` som kommandot meddelar efter att kommandot är slutfört.

Relevanta filer ligger i följande mapp

🗂️ `/apps/arbetsformedlingen/docs/src`

Öppna din webbläsare och navigera till nya komponenten i navigeringsmenyn. Observera att det är `@swedishName` för `digi-input-example` som visas i navigeringsmenyn.

Nu är det fritt fram att implementera logik och styling i `digi-input-example-details.scss` samt `digi-input-example-details.tsx`.

**ℹ️ Tips & Tricks**

- Kolla hur andra komponenter har dokumenterats och utvecklats för att ta inspiration om hur du kan följa samma standard
- Dokumentationssidan för `digi-form-input` ligger i filen `digi-form-input-details.tsx` under `details/core/digi-form-input-details`.
- Alla komponentsidor namnges med prefix `digi-` och suffix `-details`.

**⚠️ Tänk på**

- Utvecklingsmiljön måste startas om manuellt när komponenter i Komponentbiblioteket uppdateras. `Ctrl+C` och kör kommandot för utvecklingsmiljön igen.
  - Arbeta i utvecklingsmiljön för komponentbiblioteket istället om det är mycket ändringar planerat.

## Utvecklingsmiljöer

### React Playground

Utvecklingsmiljö för att testa designsystemet lokalt i React-miljö.

Starta miljön genom att köra följande i roten av projektet

⚙️ `npm run af-pg-react`

Sökväg till projektets filer

🗂️ `/apps/arbetsformedlingen/playgrounds/react`

**ℹ️ Tips & Tricks**

- Du måste manuellt navigera till `http://localhost:4201` efter miljön har startat
- Utvecklingsmiljön måste startas om manuellt när komponenter i Komponentbiblioteket uppdateras. `Ctrl+C` och kör kommandot för utvecklingsmiljön igen.

### Angular Playground

Utvecklingsmiljö för att testa designsystemet lokalt i Angular-miljö.

Starta miljön genom att köra följande i roten av projektet

⚙️ `npm run af-pg-angular`

Sökväg till projektets filer

🗂️ `/apps/arbetsformedlingen/playgrounds/angular`

**⚠️ Tänk på**

- Du måste manuellt navigera till `http://localhost:4200` efter miljön har startat
- Utvecklingsmiljön måste startas om manuellt när komponenter i Komponentbiblioteket uppdateras. `Ctrl+C` och kör kommandot för utvecklingsmiljön igen.

## Design Tokens

Alla design tokens finns tillgängliga under `themes/`, ett exempel är Arbetsförmedlingens tokens som finns i

`/themes/arbetsformedlingen/tokens.json`

Filen byggs tills css, scss och json när dokumentationswebben eller komponenterna byggs med hjälp av `style-dictionary-export`-verktyget som ligger under `tools`.

### Referera till en annan token

I exemplet nedan refererar `new-alias` till en redan existerande färg `new-color-500`.

```json
{
  "tokens": {
    "new-color-500": "#f0f0f0",
    "new-alias": "{new-color-500}"
  }
}
```

**⚠️ Tänk på**

En token kan endast innehålla ett värde. Behöver du flera värden så skapar du flera tokens alternativt en css-variabel lokalt i ditt projekt. 


## Tematisering

För att skapa ett nytt brand för designsystemet

1. Skapa en ny mapp under `themes` med ett förklarande namn, exempelvis `skolverket`. 
2. Kopiera över en existerande `tokens.json` från exempelvis `themes/arbetsformedlingen/tokens.json` till `skolverket`-mappen
3. Uppdatera byggsteget `build-tokens` i `project.json` till att använda `skolverket/tokens.json` istället för `arbetsformedlingen/tokens.json`

-

## Verktyg och Ramverk

Designsystemet är uppbyggt i en monorepo-arkitektur. Kortfattat betyder det att alla dokumentationssidor, paket, verktyg, byggsteg och beroenden finns på ett och samma ställe. Vi använder [NX ↗](https://nx.dev/getting-started/intro) och [NPM Workspaces ↗](https://docs.npmjs.com/cli/v10/using-npm/workspaces) för att göra det arbetet enklare.

Vi använder **inte** NX Cloud.

---

Med hjälp av [Stencil ↗](https://stenciljs.com/docs/getting-started#my-first-component) kan vi implementera komponenter i designsystemet och sedan exportera dessa till ramverk som React och Angular utan större handpåläggning.

---

Versionshantering sker via Git och vi använder GitLab som tjänst. Just nu har vi stöd för att göra följande via GitLab

- Publicera Dokumentationswebben
- Publicera NPM-paket
- Köra tester vid Merge Requests

Alla byggsteg finns definierade i `.gitlab-ci.yml` och kan visualiseras i [Pipeline Editor](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/ci/editor)

---

Vi bestämmer nytt versionsnummer med hjälp av [Semantic Versioning ↗](https://semver.org/spec/v2.0.0.html) när designsystemet uppdateras och publiceras.

Versionen bestäms för hela designsystemet. **Inte** per komponent.

**⚠️ Tänk på**

- MAJOR gäller även för visuella förändringar om det kan påverka sidlayout eller liknande.

## Filstruktur

**libs/arbetsformedlingen/package**

- Alla komponenter i designsystemet
- Publiceras som 📦 `@digi/arbetsformedlingen`
- Använder sig av Stencil

**themes/arbetsformedlingen/tokens.json**

- Alla design tokens knytna till Arbetsförmedlingen
- Transformeras till "Style Dictionary" format vid bygge med hjälp av `style-dictionary-export flat-to-style-dictionary`
- Använder sedan style-dictionary för att konvertera ut till `css`, `scss` och `json`. Detta för att användas på dokumentationswebben och i enskilda komponenter.

**libs/arbetsformedlingen/fonts**

- Innehåller typsnitt för designsystemet
- Kopieras till `package` under byggsteg

**libs/arbetsformedlingen/angular**

- Angular-specifik konfiguration för Stencil
- Publiceras som 📦 `@digi/arbetsformedlingen-angular`

**libs/arbetsformedlingen/react**

- React-specifik konfiguration för Stencil
- Publiceras som 📦 `@digi/arbetsformedlingen-react`

**libs/arbetsformedlingen/styles**

- SASS-relaterade funktioner och mixins som kan användas i komponenters styling
- Kopieras till `package` under byggsteg

## Tips & Tricks

- Största delen av byggstegen sker i en kombination av `project.json`, `stencil.config.*.ts` och `stencil.config.ts`
- Vid felsökning av byggsteg, se till att använda `npm run flush` för att ta bort byggfiler och cache ordentligt
- Kör `build` task för alla projekt med `npx nx run-many -t build` i roten av vårt repo. Bra sätt att se om en förändring påverkade ett annat paket.

## Resurser

[📦 Designsystemet publicerade paket ↗](https://nexus.jobtechdev.se/#browse/browse:arbetsformedlingen-npm)

[🔓 Release Manual (Låst länk)](https://gitlab.com/arbetsformedlingen/designsystem/digi/-/issues/131)

---

<details>
  <summary>Äldre version av Contribution Guidelines</summary>

Digi förvaltas av Arbetsförmedlingen, men strävar efter att kunna bli ett designsystem som används av flera olika myndigheter.
Här finns bland annat komponentbibliotek, design tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma
riktlinjer. Repot är ett monorepo genererat med [Nx](https://nx.dev).
Besök [Arbetsförmedlingens dokumentationssida](https://designsystem.arbetsformedlingen.se/) för att se vilka komponenter
som ingår.

## Kom igång

### Förutsättningar

- [node (och npm)](https://nodejs.org/en/) i din utvecklingsmiljö. Aktuell nodeversion finns i `.nvmrc`

### Installera

- `nvm use` om du använder nvm. Annars får byta till rätt nodeversion på annat sätt.
- `npm i` i roten av projektet.

## Arbeta med bibliotek och applikationer

Nx gör skillnad på bibliotek (libs) och applikationer (apps). Dessa hittar du under respektive mapp i projektet. Digi
innehåller flera olika bibliotek och applikationer med tydligt definierade syften.

## Versionshantering

Versionshantering hanteras per designsystem och versionen sätt för hela designsystemet. Vi versionshanterar inte per
komponent, utan per system.

### Semantic versioning

Designsystemen följer [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

- MAJOR version ändras när en eller flera komponenter inte längre är bakåtkompatibla i sin version. Det kan bero på
  ändringar av komponentens API
  eller [visuella breaking changes](https://medium.com/eightshapes-llc/visual-breaking-change-in-design-systems-1e9109fac9c4).
- MINOR version ändras när vi lägger till nya features eller komponenter som är bakåtkompatibla.
- PATCH version ändras när vi endast gjort buggfixar, eller förändringar av dokumentationen.

### Struktur

Varje lib är grupperad efter ägandeskap:

- `arbetsformedlingen` är specifikt för Arbetsförmedlingen. Publiceras som `@digi/arbetsformedlingen`

Dessa libs innehåller några av följande bibliotek:

- `package` - Detta är ett Stencil-projekt med komponenter.
- `design-tokens` - Design tokenskonfigurationer (använder sig av Style Dictionary) för respektive myndighet
- `fonts` - Typsnittsfiler och css-definitioner
- `angular` - Angularkomponenter från Stencils Angular output target
- `react` - Reactkomponenter från Stencils React output target

Utöver allt ovan finns även ett lib som heter `shared`. Där finns funktioner och annat som används av alla andra
tjänster. I `Shared\Styles` ligger
all css som genereras automatiskt vid byggen.

## Börja jobba

### Starta enskilt bibliotek (lib)

Du kan starta olika delar av repot genom `npm run start <lib>`.
Aktuella ´lib´ är;

- arbetsformedlingen

Kommandot öppnar en `index.html`-fil som finns i just det bibliotekets src-mapp. Där kan du lägga in komponenten du
jobbar med. Det körs en watch i bakgrunden på det biblioteket.

### Starta dokumentations-webbarna (apps)

Du kan även starta de olika dokumentations-webbarna eller test-applikationerna genom att först bygga
dom `npm run build <apps>` och sedan `npm run start <apps>` `<apps>` motsvarar namnet på appen som finns specificerat i
dens `project.json`.

## Tooling (scripts)

Alla generella scripts ligger i rotens `package.json`. Dom dokumenteras här, eller i REDAME.md för respektive myndighet.

### Versions

- Alla `version-` script så som `version-arbetsformedlingen` används för att öka versionsnumret på specificerat
  designsystem. Detta kommando körs manuellt vid release.
  - Kom ihåg att specificera vilken semantic version som ska justeras, `version-arbetsformedlingen patch|minor|major`
  - Obs! Kommandot ändrar inte peer-dependencies för dom ramverks-specifika paketen, så dessa måste hanteras manuellt.
    Exempelvis `@digi/arbetsformedlingen-react` har `@digi/arbetsformedlingen` som peerDependency. Så efter att du
    kört versions-kommandot behöver du gå in i React-lib:ets `package.json` och justera peerDependency-versionen
    av `@digi/arbetsformedlingen` till den senaste.
- Behöver vi släppa ett paket som `beta` kan vi använda oss av `version-arbetsformedlingen:beta`. Vi behöver även där
  manuellt uppdatera peer-dependencies som beskrivet ovan.
  - För att en tjänst ska kunna hämta beta-paketet kör `release-arbetsformedlingen:beta`
- Kommandot `version-arbetsformedlingen:prerelease` används sällan, men fungerar på liknande sätt som `beta`.
- Kommandona `version:prerelease` eller `version:beta` påverkar alla som finns definierade som "workspaces" i rotens
  package.json

### Release

- Release-scripten bygger och publicerar definierat projekt. Projektens build och publish script finns i deras
  `project.json` filer.

### NX

Vi har NX och kan köra alla script som kommer med NX, även om de inte finns dokumenterade här.
De körs genom `npx nx ` - namn på script.

Bra kommandon att komma ihåg:

- `npx nx migrate latest` - Håller koll på alla paket i repot så alla beroenden ska fungera med varandra. Så om vi ska
  gå från Angular X till Y som i sin tur är beroende av Typescript Z så har NX koll på detta.
- `npx nx dep-graph` - Visar en utförlig dependency-graf.

### Övrigt

- `sw-precache:docs:arbetsformedlingen` - Behöver undersökas om och hur denna fortfarande behövs.

## Arbetsförmedlingen

Mer dokumentation för hur du jobbar med specifikt arbetsförmedlingens designsystem hittar
du [i README för apps/arbetsförmedlingen](./apps/arbetsformedlingen/README.md). Där finns bland annat instruktioner för
hur du skapar upp en ny komponent.

## Publicera paket

Publicering av paket görs separat för dom olika myndigheterna.

- Arbetsförmedlingens process finns dokumenterad internt på myndigheten.

</details>
