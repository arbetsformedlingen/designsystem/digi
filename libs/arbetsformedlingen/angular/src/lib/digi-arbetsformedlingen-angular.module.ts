import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BooleanValueAccessor } from './stencil-generated/boolean-value-accessor';
import { NumericValueAccessor } from './stencil-generated/number-value-accessor';
import { RadioValueAccessor } from './stencil-generated/radio-value-accessor';
import { SelectValueAccessor } from './stencil-generated/select-value-accessor';
import { TextValueAccessor } from './stencil-generated/text-value-accessor';

import { DIRECTIVES } from './stencil-generated/proxies-list';

const VALUEACCESSORS = [
  BooleanValueAccessor,
  NumericValueAccessor,
  RadioValueAccessor,
  SelectValueAccessor,
  TextValueAccessor
];

@NgModule({
  imports: [CommonModule, DIRECTIVES],
  declarations: [VALUEACCESSORS],
  exports: [DIRECTIVES, VALUEACCESSORS]
})
export class DigiArbetsformedlingenAngularModule {}
