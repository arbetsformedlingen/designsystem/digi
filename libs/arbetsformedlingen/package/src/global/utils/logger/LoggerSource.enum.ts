export enum LoggerSource {
  CORE = '@af/digi-core',
  NG = '@af/digi-ng'
}
