export enum LinkExternalVariation {
  SMALL = 'small',
  LARGE = 'large'
}
