export enum ProgressListItemType {
  CIRCLE = 'circle',
  ICON = 'icon'
}
