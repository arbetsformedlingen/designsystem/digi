export enum ProgressListItemStatus {
  CURRENT = 'current',
  UPCOMING = 'upcoming',
  DONE = 'done'
}
