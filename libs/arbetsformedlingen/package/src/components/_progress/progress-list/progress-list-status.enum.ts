export enum ProgressListStatus {
  CURRENT = 'current',
  UPCOMING = 'upcoming',
  DONE = 'done'
}
