/* eslint-disable @typescript-eslint/no-explicit-any */

import { _t } from '../../../text';
import {
  Component,
  Element,
  Prop,
  State,
  h,
  Watch,
  Listen
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { UtilBreakpointObserverBreakpoints } from '../../_util/util-breakpoint-observer/util-breakpoint-observer-breakpoints.enum';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ProgressIndicatorRole } from './progress-indicator-role.enum';
import { ProgressIndicatorVariation } from './progress-indicator-variation.enum';

/**
 * @enums ProgressIndicatorRole - progress-indicator-role.enum.ts
 * @enums ProgressIndicatorVariation - progress-indicator-variation.enum.ts
 * @swedishName Förloppsstegaren
 */
@Component({
  tag: 'digi-progress-indicator',
  styleUrls: ['progress-indicator.scss'],
  scoped: true
})
export class ProgressIndicator {
  headingElement!: HTMLHeadingElement;
  @Element() hostElement: HTMLStencilElement;

  private _observer;

  @State() completedSteps: number;
  @State() totalSteps: number[] = [];
  @State() listItems: any[];
  @State() hasSlottedItems: boolean;
  @State() isMobile: boolean;

  @Prop() radius = 42;
  @Prop() stroke = 6;

  /**
   * Sätter antal färdiga steg
   * @en Set finished amount of steps
   */
  @Prop() afCompletedSteps: number;

  /**
   * Sätter ett steg där det krävs ytterligare åtgärder
   * @en Set a step where something requires additional actions
   */
  @Prop() afWarningStep: number;

  /**
   * Sätter ett steg där något gick fel
   * @en Set a step that failed
   */
  @Prop() afErrorStep: number;

  /**
   * Sätter totala antalet steg
   * @en Set amount of total steps
   */
  @Prop() afTotalSteps: number;

  /**
   * Text emellan antal färdiga och totala antalet steg.
   * Som standard används "av", men kan ändras för exempelvis annat språk.
   * @en Separator text between current steps and total steps.
   * Default is "av", but can be changed e.g. for different language.
   */
  @Prop() afStepsSeparator = _t.of;

  /**
   * Text som visar "Steg" innan det antalet steg visas
   * Som standard används "Steg", men kan ändras för exempelvis annat språk.
   * @en Text that shows "Steg" before the current step
   * Default is "Steg", but can be changed e.g. for different language.
   */
  @Prop() afStepText = _t.step;

  /**
   * Beskrivande text om vad som kommer härnäst
   * @en Descriptive text of what comes in the next step.
   */
  @Prop() afStepNextText = _t.progress.questions_next;

  /**
   * Sätter en beskrivande rubrik för förloppsmätaren
   * @en Adds a descriptive header for the progress-indicator
   */
  @Prop() afStepHeading: string;

  /**
   * Sätter en beskrivande rubrik för förloppsmätaren
   * @en Adds a descriptive header for the progress-indicator
   */
  @Prop() afStepNextLabel: string;

  /**
   * Beskrivande text om något går fel i processen
   * @en Descriptive text that tells the user something went wrong in the process
   */
  @Prop() afValidationLabel: string;

  /**
   * Sätter attributet 'role'. Förvalt är 'status'.
   * Använder du komponenten både före och efter ett formulär så
   * ange bara den ena som 'status' för att undvika att skärmläsare läser upp texten två gånger
   * @en Set role attribute. Defaults to 'status'.
   * If you apply the progress-indicator both before and after a form,
   * set only role 'status' on one of them to avoid screen readers to read the text twice
   */
  @Prop() afRole: ProgressIndicatorRole = ProgressIndicatorRole.STATUS;

  /**
   * Sätter variant. Kan vara 'primary' och 'secondary'.
   * @en Set variation of progress-indicator. Can be 'primary' and 'secondary'.
   */
  @Prop() afVariation: ProgressIndicatorVariation =
    ProgressIndicatorVariation.PRIMARY;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-progress-indicator');

  componentWillLoad() {
    this.changeTotalStepsHandler(this.afTotalSteps);
    this.getItems();
  }

  componentDidLoad() {
    this.getItems();
  }

  @Watch('afTotalSteps')
  changeTotalStepsHandler(value: number) {
    const arr = [];
    let n = 0;
    while (n < value) {
      n++;
      arr.push(n);
    }
    this.totalSteps = arr;
  }

  @Watch('afCompletedSteps')
  changeCompletedStepsHandler(value: number) {
    this.completedSteps = value;
  }

  getItems(update?: boolean) {
    let items: any;

    if (update) {
      items = this._observer.children;
    } else {
      items = this.hostElement.querySelectorAll(
        '.digi-progress-indicator__slot li'
      );
    }

    if (items.length > 0) {
      this.hasSlottedItems = true;

      this.listItems = [...items]
        .filter((element) => element.tagName.toLowerCase() === 'li')
        .map((element) => {
          const el = element as HTMLElement;
          const checked = el.matches('.completed');

          return {
            text: element.textContent,
            checked: checked
          };
        });
    }
  }

  @Listen('afOnChange')
  breakpointChange(e: any) {
    const type = e.target.nodeName;
    if (type.toLowerCase() === 'digi-util-breakpoint-observer') {
      this.isMobile =
        e.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }

  @Listen('focus')
  focusOnHeading() {
    this.headingElement.focus();
  }

  renderHeading() {
    return (
      <h2
        class={{
          'digi-progress-indicator__heading': true,
          'digi-progress-indicator__heading--mobile': this.isMobile
        }}
        tabIndex={-1}
        ref={(el) => (this.headingElement = el as HTMLHeadingElement)}>
        {this.afStepHeading}
      </h2>
    );
  }

  get cssModifiers() {
    return {
      'digi-progress-indicator': true,
      'digi-progress-indicator--primary':
        this.afVariation === ProgressIndicatorVariation.PRIMARY,
      'digi-progress-indicator--secondary':
        this.afVariation === ProgressIndicatorVariation.SECONDARY,
      'digi-progress-indicator--linear': !this.hasSlottedItems
    };
  }

  get slottedContent() {
    return;
  }

  render() {
    return (
      <div
        class={this.cssModifiers}
        role={
          this.afRole === ProgressIndicatorRole.STATUS
            ? ProgressIndicatorRole.STATUS
            : null
        }>
        <digi-util-breakpoint-observer
          onAfOnChange={(e) =>
            (this.isMobile =
              e.detail.value === UtilBreakpointObserverBreakpoints.SMALL)
          }></digi-util-breakpoint-observer>
        {this.isMobile ? (
          <div class="digi-progress-indicator__circle-wrapper">
            <div class="digi-progress-indicator__circle">
              <p
                class="digi-progress-indicator__circle-label"
                aria-label={`${this.afCompletedSteps} ${this.afStepsSeparator} ${this.afTotalSteps}`}>
                {this.afCompletedSteps} {this.afStepsSeparator}{' '}
                {this.afTotalSteps}{' '}
              </p>
              <svg width={this.radius * 2} height={this.radius * 2}>
                <defs>
                  <mask id={this.afId}>
                    <circle
                      cx={this.radius}
                      cy={this.radius}
                      r={this.radius - this.stroke * 2}
                      fill="none"
                      stroke="white"
                      stroke-width={this.stroke}
                      stroke-dasharray={
                        (1000 - this.afTotalSteps * 15) / this.afTotalSteps +
                        ' 15'
                      }
                      pathLength="1000"
                    />
                  </mask>
                </defs>
                <g>
                  <circle
                    class="digi-progress-indicator__circle-step-unfinished"
                    mask={`url(#${this.afId})`}
                    cx={this.radius}
                    cy={this.radius}
                    r={this.radius - this.stroke * 2}
                    fill="none"
                    stroke-width={this.stroke}
                  />
                  <circle
                    class="digi-progress-indicator__circle-step-completed"
                    mask={`url(#${this.afId})`}
                    cx={this.radius}
                    cy={this.radius}
                    r={this.radius - this.stroke * 2}
                    fill="none"
                    stroke-width={this.stroke}
                    stroke-dasharray={
                      (1000 / this.totalSteps.length) * this.afCompletedSteps +
                      ' 1000'
                    }
                    pathLength="1000"
                  />
                  {this.afWarningStep && (
                    <circle
                      class="digi-progress-indicator__circle-step-warning"
                      mask={`url(#${this.afId})`}
                      cx={this.radius}
                      cy={this.radius}
                      r={this.radius - this.stroke * 2}
                      fill="none"
                      stroke-width={this.stroke}
                      stroke-dasharray={
                        (1000 / this.totalSteps.length) * 1 + ' 1000'
                      }
                      stroke-dashoffset={
                        -(1000 / this.totalSteps.length) *
                        (this.afCompletedSteps - 1)
                      }
                      pathLength="1000"
                    />
                  )}
                  {this.afErrorStep && (
                    <circle
                      class="digi-progress-indicator__circle-step-error"
                      mask={`url(#${this.afId})`}
                      cx={this.radius}
                      cy={this.radius}
                      r={this.radius - this.stroke * 2}
                      fill="none"
                      stroke-width={this.stroke}
                      stroke-dasharray={
                        (1000 / this.totalSteps.length) * 1 + ' 1000'
                      }
                      stroke-dashoffset={
                        -(1000 / this.totalSteps.length) *
                        (this.afCompletedSteps - 1)
                      }
                      pathLength="1000"
                    />
                  )}
                </g>
              </svg>
            </div>
            <div class="digi-progress-indicator__content">
              {this.renderHeading()}

              {this.afCompletedSteps === this.afTotalSteps && (
                <p
                  class="digi-progress-indicator__next digi-progress-indicator__next--mobile"
                  style={{
                    visibility: 'hidden'
                  }}>{`${this.afStepNextText} ${this.afStepNextLabel}`}</p>
              )}

              {this.afCompletedSteps != this.afTotalSteps && (
                <p
                  class="digi-progress-indicator__next digi-progress-indicator__next--mobile"
                  style={{
                    visibility:
                      this.afStepNextLabel !== null &&
                      this.afStepNextLabel !== undefined
                        ? 'visible'
                        : 'hidden'
                  }}>
                  {`${this.afStepNextText} ${this.afStepNextLabel}`}
                </p>
              )}
            </div>
          </div>
        ) : (
          <div>
            {this.afStepNextLabel !== undefined ? (
              <div class="digi-progress-indicator__content">
                {this.renderHeading()}

                {this.afCompletedSteps != this.afTotalSteps && (
                  <p
                    class="digi-progress-indicator__next"
                    style={{
                      visibility:
                        this.afStepNextLabel !== null &&
                        this.afStepNextLabel !== undefined
                          ? 'visible'
                          : 'hidden'
                    }}>
                    {`${this.afStepNextText} ${this.afStepNextLabel}`}
                  </p>
                )}

                {this.afCompletedSteps === this.afTotalSteps && (
                  <p
                    class="digi-progress-indicator__next"
                    style={{
                      visibility: 'hidden'
                    }}>{`${this.afStepNextText} ${this.afStepNextLabel}`}</p>
                )}
              </div>
            ) : (
              <div class="digi-progress-indicator__content">
                {this.renderHeading()}
                <p class="digi-progress-indicator__label">
                  <span>{`${this.afStepText}`} </span>
                  <span class="digi-progress-indicator__label--steps">{`${this.afCompletedSteps} ${this.afStepsSeparator} ${this.afTotalSteps}`}</span>
                </p>
              </div>
            )}

            {!this.hasSlottedItems ? (
              <ol class="digi-progress-indicator__list" aria-hidden="true">
                {this.totalSteps.map((item, i) => {
                  return (
                    <li
                      class={{
                        'digi-progress-indicator__step': true,
                        'digi-progress-indicator__step--active':
                          this.afCompletedSteps > i,
                        'digi-progress-indicator__step--current':
                          this.afCompletedSteps === i + 1,
                        'digi-progress-indicator__step--warning':
                          this.afWarningStep === i + 1,
                        'digi-progress-indicator__step--error':
                          this.afErrorStep === i + 1
                      }}>
                      <span class="digi-progress-indicator__step-indicator">
                        {this.afVariation ===
                          ProgressIndicatorVariation.SECONDARY &&
                          this.afCompletedSteps > i + 1 && (
                            <digi-icon-check class="digi-progress-indicator__icon"></digi-icon-check>
                          )}
                        {this.afWarningStep === i + 1 && (
                          <digi-icon-exclamation-triangle-warning class="digi-progress-indicator__icon--warning"></digi-icon-exclamation-triangle-warning>
                        )}
                        {this.afErrorStep === i + 1 && (
                          <digi-icon-validation-error class="digi-progress-indicator__icon--error"></digi-icon-validation-error>
                        )}
                        <span class="digi-progress-indicator__step-number">
                          {item}
                        </span>
                      </span>
                    </li>
                  );
                })}
              </ol>
            ) : (
              <ol class="digi-progress-indicator__list" aria-hidden="true">
                {this.listItems.map((item, index) => {
                  return (
                    <li
                      class={{
                        'digi-progress-indicator__step': true,
                        'digi-progress-indicator__step--active': item.checked
                      }}>
                      <span class="digi-progress-indicator__step-indicator">
                        <span class="digi-progress-indicator__step-number">
                          {index + 1}
                        </span>
                      </span>
                    </li>
                  );
                })}
              </ol>
            )}
            {this.afStepNextLabel !== undefined && (
              <p class="digi-progress-indicator__label">
                <span>{`${this.afStepText}`} </span>
                <span class="digi-progress-indicator__label--steps">{`${this.afCompletedSteps} ${this.afStepsSeparator} ${this.afTotalSteps}`}</span>
              </p>
            )}
          </div>
        )}
        {this.afValidationLabel && this.afWarningStep > 0 && (
          <p class="digi-progress-indicator__validation-label digi-progress-indicator__validation-label--warning">
            <digi-icon-validation-warning class="digi-progress-indicator__icon--warning"></digi-icon-validation-warning>
            <span>{this.afValidationLabel}</span>
          </p>
        )}

        {this.afValidationLabel && this.afErrorStep > 0 && (
          <p class="digi-progress-indicator__validation-label digi-progress-indicator__validation-label--error">
            <digi-icon-validation-error class="digi-progress-indicator__icon--error"></digi-icon-validation-error>
            <span>{this.afValidationLabel}</span>
          </p>
        )}

        <div hidden class="digi-progress-indicator__slot">
          <digi-util-mutation-observer
            ref={(el) => (this._observer = el)}
            onAfOnChange={() => this.getItems()}>
            <slot></slot>
          </digi-util-mutation-observer>
        </div>
      </div>
    );
  }
}
