export enum ProgressIndicatorVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
