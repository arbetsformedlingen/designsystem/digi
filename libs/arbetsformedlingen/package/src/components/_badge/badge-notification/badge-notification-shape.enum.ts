export enum BadgeNotificationShape {
  CIRCLE = 'circle',
  OVAL = 'oval',
  PILL = 'pill'
}
