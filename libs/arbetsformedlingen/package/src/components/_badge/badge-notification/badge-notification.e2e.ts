import { newE2EPage } from '@stencil/core/testing';

describe('form-file-upload', () => {
  it('should render', async () => {
    const page = await newE2EPage({
      html: `<digi-badge-notification></digi-badge-notification>`
    });

    expect(await page.evaluate(() => document.body.innerHTML)).toContain('digi-badge-notification')
  });
})