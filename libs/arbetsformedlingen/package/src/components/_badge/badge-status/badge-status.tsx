import { Component, Prop, h, Host } from '@stencil/core';
import { BadgeStatusType } from './badge-status-type.enum';
import { BadgeStatusVariation } from './badge-status-variation.enum';
import { BadgeStatusSize } from './badge-status-size.enum';

/**
 *
 * @enums BadgeStatusVariation - badge-status-variation.enum.ts
 * @enums BadgeStatusType - badge-status-type.enum.ts
 * @enums BadgeStatusSize - badge-status-size.enum.ts
 *
 * @swedishName Statusindikator
 */
@Component({
  tag: 'digi-badge-status',
  styleUrl: 'badge-status.scss',
  scoped: true
})
export class BadgeStatus {
  /**
   * Sätter text i statusindikatorn.
   * @en Set status badge text.
   */
  @Prop() afText!: string;
  /**
   * Sätter attributet 'type'.
   * @en Set status badge `type` attribute.
   */
  @Prop() afType: BadgeStatusType = BadgeStatusType.NEUTRAL;
  /**
   * Sätter storleken.
   * @en Set status badge size
   */
  @Prop() afSize: BadgeStatusSize = BadgeStatusSize.LARGE;
  /**
   * Sätter variation. Kan vara primär eller sekundär
   * @en Set variation. Can be 'primary' or 'secondary'
   */
  @Prop() afVariation: BadgeStatusVariation = BadgeStatusVariation.PRIMARY;

  /**
   * Sätter attributet 'aria-label'.
   * @en Set badge  `aria-label` attribute.
   */
  @Prop() afAriaLabel: string;

  get cssModifiers() {
    return {
      [`digi-badge-status--${this.afType}--${this.afVariation}`]:
        !!this.afVariation,
      [`digi-badge-status--${this.afSize}`]: !!this.afSize
    };
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-badge-status': true,
            ...this.cssModifiers
          }}
          aria-label={this.afAriaLabel}>
          <span class="digi-badge-status__text">{this.afText}</span>
        </div>
      </Host>
    );
  }
}
