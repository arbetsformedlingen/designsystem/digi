import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-arrow-sign-out',
  styleUrl: 'icon-arrow-sign-out.scss',
  scoped: true
})
export class IconArrowSignOut {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-arrow-sign-out"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-arrow-sign-out__shape"
          d="M26.5,11.2540632 L26.5,15.8221892 L17.5,15.8221892 C15.015625,15.8221892 13,17.725575 13,20.0716087 L13,27.915329 C13,30.2613627 15.015625,32.1647485 17.5,32.1647485 L26.5,32.1647485 L26.5,36.7328745 C26.5,40.5042344 31.346875,42.4164732 34.178125,39.7340271 L47.678125,26.9946214 C49.43125,25.3391184 49.43125,22.6478193 47.678125,20.9834634 L34.178125,8.23520472 C31.35625,5.58817046 26.5,7.47385038 26.5,11.2540632 Z M44.5,24.0023218 L31,36.7505805 L31,27.9241819 L17.5,27.9241819 L17.5,20.0804617 L31,20.0804617 L31,11.2540632 L44.5,24.0023218 Z M9.23076923,6 L17.3076923,6 C17.9423077,6 18.4615385,6.49326923 18.4615385,7.09615385 L18.4615385,9.28846154 C18.4615385,9.89134615 17.9423077,10.3846154 17.3076923,10.3846154 L9.23076923,10.3846154 C6.68269231,10.3846154 4.61538462,12.3485577 4.61538462,14.7692308 L4.61538462,32.3076923 C4.61538462,34.7283654 6.68269231,36.6923077 9.23076923,36.6923077 L17.3076923,36.6923077 C17.9423077,36.6923077 18.4615385,37.1855769 18.4615385,37.7884615 L18.4615385,39.9807692 C18.4615385,40.5836538 17.9423077,41.0769231 17.3076923,41.0769231 L9.23076923,41.0769231 C4.13461538,41.0769231 2.84217094e-14,37.1490385 2.84217094e-14,32.3076923 L2.84217094e-14,14.7692308 C2.84217094e-14,9.92788462 4.13461538,6 9.23076923,6 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
