import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-phone',
  styleUrls: ['icon-phone.scss'],
  scoped: true
})
export class IconPhone {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-phone"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g id="Pågående" stroke="none" stroke-width="1" fill-rule="evenodd">
          <g
            id="Artboard"
            transform="translate(-1185.000000, -887.000000)"
            fill-rule="nonzero">
            <g
              id="phone-45-degree-copy-2"
              transform="translate(1185.000000, 887.000000)">
              <path
                d="M29.3151176,29.2282295 C32.6450904,25.9852436 34.9040719,21.2212644 34.7090735,19.1122736 L29.02112,13.5592979 C28.9161209,13.4242985 27.9561287,13.0133003 30.5001079,8.12032164 C30.5001079,8.12032164 34.2050777,-0.648640079 36.7940565,0.0383569218 L45.0649889,2.93934426 C49.3309541,4.24733855 50.8759415,20.396268 35.9630633,35.1862035 C35.9270636,35.2222033 35.2250693,35.9242002 35.1890696,35.9602001 C20.3961905,50.876135 4.24732239,49.3311417 2.93933308,45.0651603 L0.038356776,36.7941964 C-0.648637612,34.2052078 8.12029075,30.5002239 8.12029075,30.5002239 C13.0132508,27.956235 13.4242474,28.9132309 13.5592463,29.0212304 L19.112201,34.7092056 C21.2181838,34.9072047 25.9851448,32.6482145 29.2281183,29.3152291"
                id="Path"></path>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
