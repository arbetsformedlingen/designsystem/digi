import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-calendar-alt',
  styleUrls: ['icon-calendar-alt.scss'],
  scoped: true
})
export class IconCalendarAlt {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-calendar-alt"
        width="22"
        height="26"
        viewBox="0 0 22 26"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-calendar-alt__shape"
          d="M21.41 8.125H.59a.601.601 0 01-.59-.61V5.689C0 4.341 1.056 3.25 2.357 3.25h2.357V.61c0-.336.265-.61.59-.61h1.964c.324 0 .59.274.59.61v2.64h6.285V.61c0-.336.265-.61.59-.61h1.963c.325 0 .59.274.59.61v2.64h2.357C20.944 3.25 22 4.342 22 5.688v1.828c0 .335-.265.609-.59.609zM.59 9.75h20.82c.325 0 .59.274.59.61v13.203C22 24.907 20.944 26 19.643 26H2.357C1.056 26 0 24.908 0 23.562V10.36c0-.335.265-.609.59-.609zm5.696 10.36a.601.601 0 00-.59-.61H3.732a.601.601 0 00-.59.61v2.03c0 .336.266.61.59.61h1.964c.325 0 .59-.274.59-.61v-2.03zm0-6.5a.601.601 0 00-.59-.61H3.732a.601.601 0 00-.59.61v2.03c0 .336.266.61.59.61h1.964c.325 0 .59-.274.59-.61v-2.03zm6.285 6.5a.601.601 0 00-.589-.61h-1.964a.601.601 0 00-.59.61v2.03c0 .336.266.61.59.61h1.964c.324 0 .59-.274.59-.61v-2.03zm0-6.5a.601.601 0 00-.589-.61h-1.964a.601.601 0 00-.59.61v2.03c0 .336.266.61.59.61h1.964c.324 0 .59-.274.59-.61v-2.03zm6.286 6.5a.601.601 0 00-.59-.61h-1.963a.601.601 0 00-.59.61v2.03c0 .336.265.61.59.61h1.964c.324 0 .59-.274.59-.61v-2.03zm0-6.5a.601.601 0 00-.59-.61h-1.963a.601.601 0 00-.59.61v2.03c0 .336.265.61.59.61h1.964c.324 0 .59-.274.59-.61v-2.03z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
