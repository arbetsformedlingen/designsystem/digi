import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-bookmark-solid',
  styleUrl: 'icon-bookmark-solid.scss',
  scoped: true
})
export class IconBookmarkSolid {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-bookmark-solid"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-bookmark-solid__shape"
          d="M8.15,0 C5.862,0 4,1.861 4,4.149 L4,45.092 C4,46.038 4.555,46.94 5.401,47.364 C7.024,48.179 7.437,48.391 24.292,35.835 C37.543,45.705 40.634,47.6851639 42.138,47.6851639 C42.547,47.684 42.838,47.538 43.185,47.364 C44.031,46.94 44.585,46.038 44.585,45.092 L44.585,4.149 C44.585,1.861 42.723,0 40.435,0 L8.15,0 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
