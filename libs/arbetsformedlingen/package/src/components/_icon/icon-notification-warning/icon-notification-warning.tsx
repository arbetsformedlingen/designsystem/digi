import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-notification-warning',
  styleUrls: ['icon-notification-warning.scss'],
  scoped: true
})
export class IconExclamationCircle {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-notification-warning"
        width="42"
        height="42"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g fill="currentColor" class="digi-icon-notification-warning__shape">
          <path
            d="m24 0c-13.254 0-24 10.75-24 24 0 13.258 10.746 24 24 24s24-10.742 24-24c0-13.25-10.746-24-24-24zm0 43.355c-10.697 0-19.355-8.6546-19.355-19.355 0-10.693 8.6586-19.355 19.355-19.355 10.693 0 19.355 8.6585 19.355 19.355 0 10.696-8.6546 19.355-19.355 19.355zm4.0645-10.065c0 2.2412-1.8233 4.0645-4.0645 4.0645s-4.0645-1.8233-4.0645-4.0645c0-2.2412 1.8233-4.0645 4.0645-4.0645s4.0645 1.8233 4.0645 4.0645zm-7.8745-20.458 0.65806 13.161c0.030871 0.6181 0.54106 1.1033 1.1598 1.1033h3.9842c0.61877 0 1.129-0.48523 1.1598-1.1033l0.65806-13.161c0.033194-0.66329-0.49568-1.2193-1.1598-1.2193h-5.3003c-0.66416 0-1.193 0.55597-1.1598 1.2193z"
            fill="currentColor"
            fill-rule="nonzero"
          />
        </g>
      </svg>
    );
  }
}
