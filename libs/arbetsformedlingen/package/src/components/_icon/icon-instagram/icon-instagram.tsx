import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-instagram',
  styleUrls: ['icon-instagram.scss'],
  scoped: true
})
export class IconInstagram {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-instagram"
        width="23"
        height="23"
        viewBox="0 0 23 23"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-instagram__shape"
          d="M11.505 5.635a5.87 5.87 0 00-5.873 5.885 5.87 5.87 0 005.873 5.886 5.87 5.87 0 005.873-5.886 5.87 5.87 0 00-5.873-5.885zm0 9.712a3.826 3.826 0 010-7.653 3.826 3.826 0 010 7.653zm7.483-9.953c0 .763-.614 1.373-1.37 1.373-.762 0-1.37-.615-1.37-1.373s.614-1.373 1.37-1.373c.756 0 1.37.615 1.37 1.373zm3.89 1.393c-.088-1.839-.507-3.468-1.85-4.81-1.34-1.342-2.965-1.762-4.8-1.854-1.891-.108-7.56-.108-9.45 0-1.83.087-3.456.507-4.8 1.85C.634 3.313.22 4.942.128 6.782c-.108 1.895-.108 7.575 0 9.47.087 1.84.506 3.469 1.85 4.81 1.344 1.343 2.964 1.763 4.8 1.855 1.89.108 7.559.108 9.45 0 1.835-.087 3.46-.507 4.8-1.854 1.338-1.342 1.757-2.971 1.85-4.81.107-1.896.107-7.571 0-9.467zm-2.444 11.5a3.87 3.87 0 01-2.177 2.182c-1.508.6-5.086.462-6.752.462-1.666 0-5.249.133-6.752-.462a3.87 3.87 0 01-2.177-2.182c-.598-1.51-.46-5.097-.46-6.767s-.133-5.26.46-6.766A3.87 3.87 0 014.753 2.57c1.508-.599 5.086-.46 6.752-.46 1.666 0 5.25-.134 6.752.46a3.87 3.87 0 012.177 2.183c.598 1.51.46 5.097.46 6.766 0 1.67.138 5.261-.46 6.767z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
