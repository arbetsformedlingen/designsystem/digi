import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-online-meeting',
  styleUrls: ['icon-online-meeting.scss'],
  scoped: true
})
export class IconOnlineMeeting {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-online-meeting"
        width="62px"
        height="63px"
        viewBox="0 0 62 63"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 0 62 0 62 9.21298912 0 9.21298912" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-543, -890)">
            <g transform="translate(543, 896.4583)">
              <g transform="translate(0, 0)">
                <path
                  d="M8.78313943,7.5511486 C8.78313943,6.84208031 9.3379027,6.26282418 10.0151303,6.26282418 L51.9848697,6.26282418 C52.6620973,6.26282418 53.2168606,6.84208031 53.2168606,7.5511486 L53.2168606,34.4369605 L58.8881822,34.4369605 L58.8881822,2.89872993 C58.8881822,1.30424477 57.6414956,0 56.1168151,0 L5.88318486,0 C4.35850436,0 3.1118178,1.30424477 3.1118178,2.89872993 L3.1118178,34.4369605 L8.78313943,34.4369605 L8.78313943,7.5511486 Z"
                  fill="currentColor"
                />
                <g transform="translate(0, 39.3695)">
                  <mask fill="white">
                    <use href="#path-1" />
                  </mask>
                  <g />
                  <path
                    d="M43.6913426,0 C39.7639125,1.36670156 37.9232741,3.2171371 31.0003674,3.2171371 C24.0774606,3.2171371 22.2368222,1.36670156 18.3093922,0 L-0.000244928595,0 L-0.000244928595,4.37809864 C-0.000244928595,7.03802319 1.44115986,9.21298912 4.70850732,9.21298912 L57.2922275,9.21298912 C60.5595749,9.21298912 62.0009797,7.03802319 62.0009797,4.37809864 L62.0009797,0 L43.6913426,0 Z"
                    fill="currentColor"
                  />
                </g>
                <path
                  d="M31.0002449,23.0051633 C33.9993956,23.0051633 36.4303119,20.5742469 36.4303119,17.5750963 C36.4303119,14.574721 33.9993956,12.1438047 31.0002449,12.1438047 C28.0010943,12.1438047 25.570178,14.574721 25.570178,17.5750963 C25.570178,20.5742469 28.0010943,23.0051633 31.0002449,23.0051633"
                  fill="currentColor"
                />
                <path
                  d="M18.6328204,24.3085508 C21.031896,24.3085508 22.9778537,22.3638177 22.9778537,19.9635175 C22.9778537,17.5644419 21.031896,15.6197089 18.6328204,15.6197089 C16.2337448,15.6197089 14.2877871,17.5644419 14.2877871,19.9635175 C14.2877871,22.3638177 16.2337448,24.3085508 18.6328204,24.3085508"
                  fill="currentColor"
                />
                <path
                  d="M43.367547,24.3085508 C45.7666226,24.3085508 47.7125802,22.3638177 47.7125802,19.9635175 C47.7125802,17.5644419 45.7666226,15.6197089 43.367547,15.6197089 C40.9684714,15.6197089 39.0225137,17.5644419 39.0225137,19.9635175 C39.0225137,22.3638177 40.9684714,24.3085508 43.367547,24.3085508"
                  fill="currentColor"
                />
                <path
                  d="M35.8877951,24.0912991 L34.9191025,24.0912991 C32.5090051,25.5412764 29.4914848,25.5400518 27.0813874,24.0912991 L26.1126948,24.0912991 C24.0136567,24.0912991 22.311403,25.7935528 22.311403,27.8925909 L22.311403,31.6938827 C22.311403,32.4139728 22.8943331,32.9981275 23.6144231,32.9981275 L38.3860667,32.9981275 C39.1061568,32.9981275 39.6890869,32.4139728 39.6890869,31.6938827 L39.6890869,27.8925909 C39.6890869,25.7935528 37.9868331,24.0912991 35.8877951,24.0912991"
                  fill="currentColor"
                />
                <path
                  d="M14.7227803,25.177435 C13.0425702,25.177435 11.6819918,26.539238 11.6819918,28.2182235 L11.6819918,29.9559919 C11.6819918,30.6760819 12.2649219,31.2602366 12.985012,31.2602366 L18.6330654,31.2602366 L18.6330654,27.8924684 C18.6330654,27.242183 18.7457325,26.6176151 18.9526972,26.038359 C17.7623442,26.1008158 16.5560709,25.8142493 15.4979793,25.177435 L14.7227803,25.177435 Z"
                  fill="currentColor"
                />
                <path
                  d="M47.2777095,25.177435 L46.5025105,25.177435 C45.4431943,25.8142493 44.2381457,26.1008158 43.0477927,26.038359 C43.2596559,26.6335355 43.3686491,27.2605527 43.3674245,27.8924684 L43.3674245,31.2602366 L49.0154779,31.2602366 C49.735568,31.2602366 50.318498,30.6760819 50.318498,29.9559919 L50.318498,28.2182235 C50.318498,26.539238 48.956695,25.177435 47.2777095,25.177435"
                  fill="currentColor"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
