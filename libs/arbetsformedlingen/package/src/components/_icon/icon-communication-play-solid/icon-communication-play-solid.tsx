import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-communication-play-solid',
  styleUrl: 'icon-communication-play-solid.scss',
  scoped: true
})
export class IconCommunicationPlaySolid {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-communication-play-solid"
        width="62px"
        height="62px"
        viewBox="0 0 62 62"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          d="M42.8345738,32.713506 L24.3520889,43.3836955 C24.0459958,43.5605835 23.7045252,43.6497966 23.3630545,43.6497966 C23.0215838,43.6497966 22.6801131,43.5605835 22.37402,43.3836955 C21.763372,43.0314578 21.3849856,42.3777414 21.3849856,41.6717277 L21.3849856,20.3298105 C21.3849856,19.6237968 21.763372,18.9700804 22.37402,18.6163045 C22.9862062,18.2640667 23.7399027,18.2640667 24.3520889,18.6163045 L42.8345738,29.2880322 C43.4467599,29.6402699 43.8236082,30.2939863 43.8236082,31 C43.8236082,31.7075519 43.4467599,32.3597301 42.8345738,32.713506 M31,-3.67113747e-14 C13.9064702,-3.67113747e-14 0,13.9064702 0,31 C0,48.095068 13.9064702,62.0015382 31,62.0015382 C48.0935298,62.0015382 62,48.095068 62,31 C62,13.9064702 48.0935298,-3.67113747e-14 31,-3.67113747e-14"
          fill="currentColor"></path>
      </svg>
    );
  }
}
