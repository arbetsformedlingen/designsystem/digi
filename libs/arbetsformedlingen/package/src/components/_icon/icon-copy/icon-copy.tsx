import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-copy',
  styleUrls: ['icon-copy.scss'],
  scoped: true
})
export class IconCopy {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-copy"
        width="22"
        height="26"
        viewBox="0 0 22 26"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-copy__shape"
          d="M15.714 22.75v2.031c0 .673-.527 1.219-1.178 1.219H1.179C.528 26 0 25.454 0 24.781V6.094c0-.673.528-1.219 1.179-1.219h3.535v15.031c0 1.568 1.234 2.844 2.75 2.844h8.25zm0-17.469V0h-8.25c-.65 0-1.178.546-1.178 1.219v18.687c0 .673.527 1.219 1.178 1.219h13.357c.651 0 1.179-.546 1.179-1.219V6.5h-5.107c-.648 0-1.179-.548-1.179-1.219zm5.94-1.575L18.418.356A1.16 1.16 0 0017.583 0h-.297v4.875H22v-.308a1.24 1.24 0 00-.345-.861z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
