import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-film',
  styleUrl: 'icon-film.scss',
  scoped: true
})
export class IconFilm {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-film"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-film__shape"
          d="M8.36363636,7.70190745 L36.8118717,23.7050367 L3.63659067,42.3672949 L6.72704569,48 L44.9088639,26.5213893 C47.0909848,25.2938698 47.0909848,22.1162036 44.9088639,20.8886842 L6.72704569,-0.58992654 C4.60626464,-1.7829402 2,-0.230139443 2,2.22642603 L2,25.8864581 L8.36363636,25.8864581 L8.36363636,7.70190745 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
