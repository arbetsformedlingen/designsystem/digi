import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-bullseye',
  styleUrls: ['icon-bullseye.scss'],
  scoped: true
})
export class IconBullseye {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-bullseye"
        width="62px"
        height="63px"
        viewBox="0 0 62 63"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 0 62 0 62 62.0004533 0 62.0004533" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-459, -890)">
            <g transform="translate(459, 890)">
              <g transform="translate(-0, 0)">
                <path
                  d="M39.2803927,31.0001133 C39.2803927,35.573451 35.5727711,39.2810726 30.9994334,39.2810726 C26.4260958,39.2810726 22.7207404,35.573451 22.7207404,31.0001133 C22.7207404,26.4267756 26.4260958,22.719154 30.9994334,22.719154 C35.5727711,22.719154 39.2803927,26.4267756 39.2803927,31.0001133"
                  fill="currentColor"
                />
                <path
                  d="M30.9996601,17.8391897 C23.7430559,17.8391897 17.8394163,23.7428293 17.8394163,30.9994334 C17.8394163,38.2560376 23.7430559,44.1619434 30.9996601,44.1619434 C38.2562642,44.1619434 44.1621701,38.2560376 44.1621701,30.9994334 C44.1621701,23.7428293 38.2562642,17.8391897 30.9996601,17.8391897 M30.9996601,50.9607606 C19.9946414,50.9607606 11.0405992,42.0067184 11.0405992,30.9994334 C11.0405992,19.9944147 19.9946414,11.0403725 30.9996601,11.0403725 C42.0046788,11.0403725 50.9609872,19.9944147 50.9609872,30.9994334 C50.9609872,42.0067184 42.0046788,50.9607606 30.9996601,50.9607606"
                  fill="currentColor"
                />
                <g>
                  <mask fill="white">
                    <use href="#path-1" />
                  </mask>
                  <g />
                  <path
                    d="M30.9996601,6.79881715 C17.6558483,6.79881715 6.79813727,17.6565281 6.79813727,31.0003399 C6.79813727,44.3441517 17.6558483,55.2018627 30.9996601,55.2018627 C44.3457381,55.2018627 55.2011828,44.3441517 55.2011828,31.0003399 C55.2011828,17.6565281 44.3457381,6.79881715 30.9996601,6.79881715 M30.9996601,62.0006799 C13.9074337,62.0006799 -0.000679881715,48.0948325 -0.000679881715,31.0003399 C-0.000679881715,13.9058473 13.9074337,0 30.9996601,0 C48.0941527,0 62,13.9058473 62,31.0003399 C62,48.0948325 48.0941527,62.0006799 30.9996601,62.0006799"
                    fill="currentColor"
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
