import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-check-circle-reg-alt',
  styleUrls: ['icon-check-circle-reg-alt.scss'],
  scoped: true
})
export class IconCheckCircleRegAlt {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-check-circle-reg-alt"
        width="48px"
        height="48px"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon id="path-1" points="0 0 48 0 48 48.0016481 0 48.0016481" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-1038, -215)">
            <g transform="translate(1038, 215)">
              <path
                d="M37.7978941,16.6176103 L36.1955745,15.0152907 C35.5436594,14.3633756 34.4760568,14.3633756 33.8241416,15.0152907 L21.4469098,27.3943537 L15.7609644,21.7084084 C15.1090493,21.0564932 14.0414467,21.0564932 13.3895315,21.7084084 L11.7890432,23.3107279 C11.1352968,23.9626431 11.1352968,25.0302457 11.7890432,25.6821608 L18.6579582,32.5529071 L18.6579582,32.5529071 L20.2602777,34.1552266 C20.9121929,34.8071418 21.9797955,34.8071418 22.6317107,34.1552266 L37.7978941,18.9890432 C38.4498092,18.337128 38.4498092,17.2695254 37.7978941,16.6176103"
                fill="currentColor"
              />
              <g>
                <mask id="mask-2" fill="white">
                  <use href="#path-1" />
                </mask>
                <g />
                <path
                  d="M24,5.49366702 C13.7964291,5.49366702 5.49366702,13.7964291 5.49366702,24 C5.49366702,34.2054021 13.7964291,42.5081642 24,42.5081642 C34.2054021,42.5081642 42.506333,34.2054021 42.506333,24 C42.506333,13.7964291 34.2054021,5.49366702 24,5.49366702 M24,48.0018312 C10.7657561,48.0018312 0,37.2342439 0,24 C0,10.7657561 10.7657561,0 24,0 C37.2342439,0 48,10.7657561 48,24 C48,37.2342439 37.2342439,48.0018312 24,48.0018312"
                  fill="currentColor"
                />
              </g>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
