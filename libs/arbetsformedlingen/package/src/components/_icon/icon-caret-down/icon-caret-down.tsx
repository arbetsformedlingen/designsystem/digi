import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-caret-down',
  styleUrls: ['icon-caret-down.scss'],
  scoped: true
})
export class IconCaretDown {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-caret-down"
        width="12"
        height="6"
        viewBox="0 0 12 6"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-caret-down__shape"
          d="M11.777.198A.608.608 0 0112 .667c0 .18-.074.336-.223.468l-5.25 4.667A.768.768 0 016 6a.768.768 0 01-.527-.198L.223 1.135A.608.608 0 010 .667C0 .487.074.33.223.197A.768.768 0 01.75 0h10.5c.203 0 .379.066.527.198z"
          fill="currentColor"
          fill-rule="evenodd"
        />
      </svg>
    );
  }
}
