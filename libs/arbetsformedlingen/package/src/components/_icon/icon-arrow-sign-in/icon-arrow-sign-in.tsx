import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-arrow-sign-in',
  styleUrl: 'icon-arrow-sign-in.scss',
  scoped: true
})
export class IconArrowSignIn {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-arrow-sign-in"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-arrow-sign-in__shape"
          d="M13.5,11.2540632 L13.5,15.8221892 L4.5,15.8221892 C2.015625,15.8221892 0,17.725575 0,20.0716087 L0,27.915329 C0,30.2613627 2.015625,32.1647485 4.5,32.1647485 L13.5,32.1647485 L13.5,36.7328745 C13.5,40.5042344 18.346875,42.4164732 21.178125,39.7340271 L34.678125,26.9946214 C36.43125,25.3391184 36.43125,22.6478193 34.678125,20.9834634 L21.178125,8.23520472 C18.35625,5.58817046 13.5,7.47385038 13.5,11.2540632 Z M31.5,24.0023218 L18,36.7505805 L18,27.9241819 L4.5,27.9241819 L4.5,20.0804617 L18,20.0804617 L18,11.2540632 L31.5,24.0023218 Z M39,41 L31.125,41 C30.50625,41 30,40.5219403 30,39.9376451 L30,37.8129353 C30,37.2286402 30.50625,36.7505805 31.125,36.7505805 L39,36.7505805 C41.484375,36.7505805 43.5,34.8471946 43.5,32.5011609 L43.5,15.5034827 C43.5,13.157449 41.484375,11.2540632 39,11.2540632 L31.125,11.2540632 C30.50625,11.2540632 30,10.7760035 30,10.1917083 L30,8.06699853 C30,7.48270334 30.50625,7.00464364 31.125,7.00464364 L39,7.00464364 C43.96875,7.00464364 48,10.8114153 48,15.5034827 L48,32.5011609 C48,37.1932283 43.96875,41 39,41 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
