import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-comunication-graduate',
  styleUrl: 'icon-comunication-graduate.scss',
  scoped: true
})
export class IconComunicationGraduate {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-comunication-graduate"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 0 47.6387971 0 47.6387971 18.3470365 0 18.3470365" />
          <polygon points="0 0 4.19452822 0 4.19452822 6.2203013 0 6.2203013" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-376, -386)">
            <g transform="translate(376, 394)">
              <path
                d="M23.8194363,21.2065913 C22.9282778,21.2065913 22.0673025,21.0692578 21.3949721,20.8202466 L7.60503029,15.7147611 L7.60503029,20.0483117 C10.8980156,22.2848855 19.7756443,27.1436234 23.8194363,27.1436234 C28.4268992,27.1436234 35.8406436,22.7089593 40.0338423,20.0483117 L40.0338423,15.7147611 L26.2439005,20.8202466 C25.5715701,21.0692578 24.7105948,21.2065913 23.8194363,21.2065913"
                fill="currentColor"
              />
              <g>
                <mask id="mask-2" fill="white">
                  <use href="#path-1" />
                </mask>
                <g />
                <path
                  d="M47.1845402,8.30961897 C46.9966499,8.16172138 46.7551844,8.03117911 46.4646713,7.92327423 L25.9680262,0.334467399 C24.7629626,-0.111489133 22.8757591,-0.111489133 21.6706955,0.334467399 L1.17480495,7.92327423 C0.883537229,8.03117911 0.642071764,8.16172138 0.454181449,8.30961897 C0.160649992,8.5412749 -7.54579579e-05,8.81292355 -7.54579579e-05,9.10192753 C-7.54579579e-05,9.51166424 0.322130022,9.88895403 0.894855923,10.1628664 C0.982387154,10.2043683 1.07520044,10.2436064 1.17480495,10.2805808 L7.60533212,12.6612794 L7.60533212,12.7495652 L21.9468716,18.0595417 C22.4471579,18.2451683 23.1119425,18.3470365 23.8197381,18.3470365 C24.5267792,18.3470365 25.1915638,18.2451683 25.6918501,18.0595417 L40.0341441,12.7495652 L46.1884952,10.4707349 C46.4797629,10.36283 46.6480341,10.2496431 46.741602,10.163621 C47.3158371,9.89046319 47.6387971,9.51241882 47.6387971,9.10192753 C47.6387971,8.81292355 47.4788263,8.5412749 47.1845402,8.30961897"
                  fill="currentColor"
                />
              </g>
              <path
                d="M47.6502667,23.0794578 C47.6502667,24.0445651 46.8677677,24.8270641 45.9026604,24.8270641 C44.9375531,24.8270641 44.1550541,24.0445651 44.1550541,23.0794578 C44.1550541,22.1143505 44.9375531,21.3318515 45.9026604,21.3318515 C46.8677677,21.3318515 47.6502667,22.1143505 47.6502667,23.0794578"
                fill="currentColor"
              />
              <g transform="translate(43.8055, 26.2016)">
                <mask id="mask-4" fill="white">
                  <use href="#path-3" />
                </mask>
                <g />
                <path
                  d="M4.17575356,5.68424797 L2.49153194,0.289758558 C2.43795679,0.117714414 2.27798592,0 2.09688682,0 C1.9165423,0 1.75657143,0.117714414 1.70299628,0.289758558 L0.0187746601,5.68424797 C-0.0310275921,5.84421884 0.0202838193,6.01777215 0.148562348,6.12492245 C0.277595456,6.23207275 0.457939975,6.25093724 0.605837573,6.17246096 L2.09688682,5.38467988 L3.58869065,6.17246096 C3.64905702,6.20490788 3.71546002,6.21999947 3.78186302,6.21999947 C3.87618547,6.21999947 3.96975334,6.18755255 4.04596588,6.12492245 C4.1742444,6.01777215 4.22555582,5.84421884 4.17575356,5.68424797"
                  fill="currentColor"
                />
              </g>
              <path
                d="M45.9026604,20.1162238 L45.9026604,20.1162238 C45.661195,20.1162238 45.4627405,19.918524 45.4627405,19.6763039 L45.4627405,12.8707507 C45.4627405,12.6285307 45.661195,12.4308308 45.9026604,12.4308308 C46.1441259,12.4308308 46.3425803,12.6285307 46.3425803,12.8707507 L46.3425803,19.6763039 C46.3425803,19.918524 46.1441259,20.1162238 45.9026604,20.1162238"
                fill="currentColor"
              />
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
