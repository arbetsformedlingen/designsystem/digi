import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-check-circle-solid',
  styleUrls: ['icon-check-circle-solid.scss'],
  scoped: true
})
export class IconCheckCircleSolid {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-check-circle-solid"
        width="48px"
        height="48px"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <polygon points="0 0 48 0 48 48.0005539 0 48.0005539" />
        </defs>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-1122, -215)">
            <g transform="translate(1122, 215)">
              <mask id="mask-2" fill="white">
                <use href="#path-1" />
              </mask>
              <g id="Clip-2" />
              <path
                d="M38.2695118,19.0969092 L22.8594286,34.5069924 C22.1966492,35.1697718 21.1129402,35.1697718 20.4501608,34.5069924 L18.821828,32.8786597 L18.821828,32.8786597 L11.8414129,25.8982446 C11.1786335,25.2354652 11.1786335,24.14991 11.8414129,23.4871306 L13.4678995,21.860644 C14.1306789,21.1978646 15.2162341,21.1978646 15.8790135,21.860644 L21.6538716,27.6373483 L34.2319113,15.0593086 C34.8946907,14.3965292 35.9783997,14.3965292 36.6411791,15.0593086 L38.2695118,16.6876413 C38.9322913,17.3504208 38.9322913,18.4341298 38.2695118,19.0969092 M24.0003692,0 C10.7447807,0 0,10.7447807 0,24.0003692 C0,37.2559578 10.7447807,48.0007385 24.0003692,48.0007385 C37.2541116,48.0007385 48.0007385,37.2559578 48.0007385,24.0003692 C48.0007385,10.7447807 37.2541116,0 24.0003692,0"
                fill="currentColor"
              />
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
