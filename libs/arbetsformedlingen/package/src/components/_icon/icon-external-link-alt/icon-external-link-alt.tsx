import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-external-link-alt',
  styleUrls: ['icon-external-link-alt.scss'],
  scoped: true
})
export class IconExternalLinkAlt {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-external-link-alt"
        width="29"
        height="26"
        viewBox="0 0 29 26"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-external-link-alt__shape"
          d="M22.556 12.28v11.283A2.427 2.427 0 0120.139 26H2.417A2.427 2.427 0 010 23.562V5.688A2.427 2.427 0 012.417 3.25h17.117c.538 0 .807.656.427 1.04L18.752 5.51a.602.602 0 01-.427.178H2.72a.303.303 0 00-.302.305v17.266c0 .168.135.305.302.305h17.118a.303.303 0 00.302-.305v-9.76c0-.16.064-.316.177-.43l1.208-1.219a.604.604 0 011.032.431zM28.396 0h-6.847c-.536 0-.807.657-.427 1.04l2.426 2.448L9.844 17.311a.613.613 0 000 .862l1.139 1.149a.6.6 0 00.854 0L25.542 5.499l2.427 2.447A.604.604 0 0029 7.516V.608C29 .273 28.73 0 28.396 0z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
