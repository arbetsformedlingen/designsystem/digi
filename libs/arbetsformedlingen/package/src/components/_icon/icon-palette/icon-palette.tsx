import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-palette',
  styleUrl: 'icon-palette.scss',
  scoped: true
})
export class IconPalette {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-palette"
        width="64"
        height="64"
        viewBox="0 0 64 64"
        fill="none"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          d="M50.5759 39.1509C54.3693 39.2601 59.4665 38.6612 59.8531 34.8859C59.9502 33.9372 60 32.9744 60 32C60 16.536 47.464 4 32 4C16.536 4 4 16.536 4 32C4 47.464 16.536 60 32 60C32.9744 60 33.9372 59.9502 34.8859 59.8531C38.6612 59.4665 39.1684 54.4467 38.9695 50.6569C38.6574 44.7081 44.5944 38.9788 50.5759 39.1509Z"
          stroke="currentColor"
          stroke-width="8"
        />
        <circle cx="40.5" cy="22.5" r="5.5" fill="currentColor" />
        <circle cx="25.5" cy="21.5" r="5.5" fill="currentColor" />
        <circle cx="19.5" cy="35.5" r="5.5" fill="currentColor" />
      </svg>
    );
  }
}
