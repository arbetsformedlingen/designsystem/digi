import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-window',
  styleUrls: ['icon-window.scss'],
  scoped: true
})
export class IconWindow {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-window"
        width="68px"
        height="68px"
        viewBox="0 0 68 68"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g transform="translate(-1216, -884)" fill="currentColor">
            <g transform="translate(1218.8333, 886.8333)">
              <path d="M62.3333333,0 L29.0888889,4.80358639 L29.0888889,30.4740741 L62.3333333,30.4740741 L62.3333333,0 Z M0,9.09650148 L0,30.4740741 L24.9333333,30.4740741 L24.9333333,5.54074074 L0,9.09650148 Z M29.0888889,33.2444444 L29.0888889,57.7006941 L62.3333333,62.3333333 L62.3333333,33.2444444 L29.0888889,33.2444444 Z M0,33.2444444 L0,53.3919745 L24.9333333,56.7925926 L24.9333333,33.2444444 L0,33.2444444 Z"></path>
            </g>
          </g>
        </g>
      </svg>
    );
  }
}
