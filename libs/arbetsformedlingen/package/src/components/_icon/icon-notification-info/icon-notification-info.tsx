import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-notification-info',
  styleUrls: ['icon-notification-info.scss'],
  scoped: true
})
export class IconNotificationInfo {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-notification-info"
        width="42"
        height="42"
        viewBox="0 0 48 48"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g
          class="digi-icon-notification-info__shape"
          fill="currentColor"
          fill-rule="nonzero">
          <path
            d="m24 0c-13.254 0-24 10.75-24 24 0 13.258 10.746 24 24 24s24-10.742 24-24c0-13.25-10.746-24-24-24zm0 43.355c-10.697 0-19.355-8.6546-19.355-19.355 0-10.693 8.6586-19.355 19.355-19.355 10.693 0 19.355 8.6585 19.355 19.355 0 10.696-8.6546 19.355-19.355 19.355zm0-32.71c2.2448 0 4.0645 1.8197 4.0645 4.0645s-1.8197 4.0645-4.0645 4.0645-4.0645-1.8197-4.0645-4.0645 1.8197-4.0645 4.0645-4.0645zm5.4194 24.581c0 0.64132-0.51997 1.1613-1.1613 1.1613h-8.5161c-0.64132 0-1.1613-0.51997-1.1613-1.1613v-2.3226c0-0.64132 0.51997-1.1613 1.1613-1.1613h1.1613v-6.1935h-1.1613c-0.64132 0-1.1613-0.51997-1.1613-1.1613v-2.3226c0-0.64132 0.51997-1.1613 1.1613-1.1613h6.1935c0.64132 0 1.1613 0.51997 1.1613 1.1613v9.6774h1.1613c0.64132 0 1.1613 0.51997 1.1613 1.1613v2.3226z"
            fill="currentColor"
            fill-rule="nonzero"
          />
        </g>
      </svg>
    );
  }
}
