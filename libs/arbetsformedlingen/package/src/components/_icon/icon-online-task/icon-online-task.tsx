import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-online-task',
  styleUrls: ['icon-online-task.scss'],
  scoped: true
})
export class IconOnlineTask {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-online-task"
        width="62px"
        height="62px"
        viewBox="0 0 62 62"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <defs>
          <path
            d="M40.322.348 14.356 26.405 4.814 16.83a1.18 1.18 0 0 0-1.675 0L.347 19.632a1.192 1.192 0 0 0 0 1.682l13.17 13.215a1.179 1.179 0 0 0 1.675 0L44.79 4.831a1.193 1.193 0 0 0 0-1.681L41.996.348a1.178 1.178 0 0 0-1.675 0Z"
            id="a"
          />
        </defs>
        <g fill="none" fill-rule="evenodd">
          <g transform="translate(0 47.327)" />
          <path
            d="M43.14 47.327c-3.878 1.35-5.696 3.177-12.531 3.177-6.836 0-8.653-1.828-12.53-3.177H-.002v4.323c0 2.626 1.424 4.774 4.65 4.774h51.92c3.226 0 4.649-2.148 4.649-4.774v-4.323H43.14Z"
            fill="currentColor"
          />
          <g transform="translate(16.864 6.458)">
            <mask id="b" fill="#fff">
              <use href="#a" />
            </mask>
            <g />
            <path
              fill="currentColor"
              mask="url(#b)"
              d="M-.705 35.699h46.546V-.705H-.705z"
            />
          </g>
          <path
            d="M5.808 8.434c-1.505 0-2.736 1.288-2.736 2.86v31.142h5.6V15.89c0-.7.547-1.273 1.217-1.273h31.417l6.706-6.183H5.808Zm46.737 34.002h5.6V24.328l-5.6 5.748z"
            fill="currentColor"
          />
        </g>
      </svg>
    );
  }
}
