import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-x-button-outline',
  styleUrl: 'icon-x-button-outline.scss',
  scoped: true
})
export class IconXButtonOutline {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till andra element på sidan för att definiera ett tillgängligt namn.
   * @en Reference other elements on the page to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-x-button-outline"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={
          this.afSvgAriaLabelledby
            ? this.afSvgAriaLabelledby
            : this.afTitle
              ? this.titleId
              : undefined
        }>
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-x-button-outline__shape"
          d="M24,0 C10.7419355,0 0,10.7419355 0,24 C0,37.2580645 10.7419355,48 24,48 C37.2580645,48 48,37.2580645 48,24 C48,10.7419355 37.2580645,0 24,0 Z M24,43.3548387 C13.3064516,43.3548387 4.64516129,34.6935484 4.64516129,24 C4.64516129,13.3064516 13.3064516,4.64516129 24,4.64516129 C34.6935484,4.64516129 43.3548387,13.3064516 43.3548387,24 C43.3548387,34.6935484 34.6935484,43.3548387 24,43.3548387 Z M27.8322581,24 L33.8516129,30.0193548 C34.3064516,30.4741935 34.3064516,31.2096774 33.8516129,31.6645161 L31.6645161,33.8516129 C31.2096774,34.3064516 30.4741935,34.3064516 30.0193548,33.8516129 L24,27.8322581 L17.9806452,33.8516129 C17.5258065,34.3064516 16.7903226,34.3064516 16.3354839,33.8516129 L14.1483871,31.6645161 C13.6935484,31.2096774 13.6935484,30.4741935 14.1483871,30.0193548 L20.1677419,24 L14.1483871,17.9806452 C13.6935484,17.5258065 13.6935484,16.7903226 14.1483871,16.3354839 L16.3354839,14.1483871 C16.7903226,13.6935484 17.5258065,13.6935484 17.9806452,14.1483871 L24,20.1677419 L30.0193548,14.1483871 C30.4741935,13.6935484 31.2096774,13.6935484 31.6645161,14.1483871 L33.8516129,16.3354839 C34.3064516,16.7903226 34.3064516,17.5258065 33.8516129,17.9806452 L27.8322581,24 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
