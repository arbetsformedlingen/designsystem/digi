import { _t } from '../../../text';
import {
  Component,
  h,
  Prop,
  Element,
  Watch,
  Event,
  EventEmitter
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { ButtonType, ButtonVariation } from '../../../enums';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ExpandableAccordionHeaderLevel } from './expandable-accordion-header-level.enum';
import { ExpandableAccordionVariation } from './expandable-accordion-variation.enum';

/**
 * @enums ExpandableAccordionHeaderLevel - expandable-accordion-header-level.enum.ts
 * @swedishName Utfällbar rubrik
 */
@Component({
  tag: 'digi-expandable-accordion',
  styleUrl: 'expandable-accordion.scss',
  scoped: true
})
export class ExpandableAccordion {
  private _button: HTMLButtonElement;

  @Element() hostElement: HTMLStencilElement;

  /**
   * Utfällbara ytans variation
   * @en Set component variation
   */
  @Prop() afVariation: ExpandableAccordionVariation =
    ExpandableAccordionVariation.PRIMARY;

  /**
   * Rubrikens Utfällbar yta
   * @en The heading text
   */
  @Prop() afHeading: string;

  /**
   * Sätter utfällbar rubrik. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: ExpandableAccordionHeaderLevel =
    ExpandableAccordionHeaderLevel.H2;

  /**
   * Sätter läge på rubriken om den ska vara expanderad eller ej.
   * @en Sets the state of the accordion to expanded or not.
   */
  @Prop() afExpanded = false;

  /**
   * Ställer in ifall komponenten ska ha en animation vid utfällning. Animation är påslagen som standard.
   * @en Sets the state of the animation for the accordion's expanded area. Animation is on as default.
   */
  @Prop() afAnimation = true;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-expandable-accordion');

  /**
   * Sätter texten som visas på knappen som expanderar komponenten. Om inget väljs så används "Visa"
   * @en Sets the text on the button that expands the component. Defaults to "Visa".
   */
  @Prop() afExpandText: string = _t.show;

  /**
   * Sätter texten som visas på knappen som kollapsar komponenten. Om inget väljs så används "Dölj"
   * @en Sets the text on the button that collapses the component. Defaults to "Dölj".
   */
  @Prop() afCollapseText: string = _t.hide;

  /**
   * Buttonelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter<MouseEvent>;

  get cssModifiers() {
    return {
      [`digi-expandable-accordion--expanded-${!!this.afExpanded}`]: true,
      [`digi-expandable-accordion--animation-${!!this.afAnimation}`]: true,
      [`digi-expandable-accordion--variation-${this.afVariation}`]:
        !!this.afVariation
    };
  }

  /**
   * Hämtar en höjden till innehållet i Utfällbar yta och animera innehållet.
   * @en Gets a height to the content for expandable and animate the content.
   */

  componentDidLoad() {
    const content = this.hostElement.children[0].querySelector(
      `#${this.afId}-content`
    ) as HTMLElement;

    if (!this.afExpanded) {
      content.setAttribute('style', 'visibility: hidden;');
    }

    if (this.afExpanded) {
      content.setAttribute('style', 'height: auto;');
    }

    content.addEventListener('transitionend', () => {
      if (!this.afExpanded) {
        content.setAttribute(
          'style',
          'visibility: hidden;  height: ' + 0 + 'px'
        );
      }
    });
  }

  @Watch('afExpanded')
  animateContentHeight() {
    const content = this.hostElement.children[0].querySelector(
      `#${this.afId}-content`
    ) as HTMLElement;
    const sectionHeight = (content.firstElementChild as HTMLElement)
      .offsetHeight;

    if (!this.afExpanded && content.style.height === 'auto') {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');

      setTimeout(() => {
        content.setAttribute('style', 'height: 0');
      });

      return;
    }

    if (this.afExpanded) {
      content.setAttribute(
        'style',
        'visibility: visible; height:' + sectionHeight + 'px'
      );
    } else {
      content.setAttribute('style', 'height: ' + 0 + 'px');
    }
  }

  clickToggleHandler(e: MouseEvent, resetFocus = false) {
    e.preventDefault();
    this.afExpanded = !this.afExpanded;
    if (resetFocus) {
      this._button.focus();
    }
    this.afOnClick.emit(e);
  }

  get toggleIcon () {
    if (this.afExpanded) {
      return <digi-icon-chevron-up slot="icon-secondary" />
    } else {
      return <digi-icon-chevron-down slot="icon-secondary" />;
    }
  }

  render() {
    return (
      <article
        aria-expanded={this.afExpanded ? 'true' : 'false'}
        class={{
          'digi-expandable-accordion': true,
          ...this.cssModifiers
        }}>
        <header class="digi-expandable-accordion__header">
          <this.afHeadingLevel class="digi-expandable-accordion__heading">
            <button
              type="button"
              class="digi-expandable-accordion__button"
              onClick={(e) => this.clickToggleHandler(e)}
              aria-expanded={this.afExpanded ? 'true' : 'false'}
              aria-controls={`${this.afId}-content`}
              ref={(el) => (this._button = el)}>
              {this.afVariation !== ExpandableAccordionVariation.SECONDARY && (
                <digi-icon-chevron-down class="digi-expandable-accordion__toggle-icon" />
              )}
              {this.afHeading}
              {this.afVariation === ExpandableAccordionVariation.SECONDARY && (
                <span class="digi-expandable-accordion__toggle-label">
                  {this.afExpanded ? this.afCollapseText : this.afExpandText}
                  {this.toggleIcon}
                </span>
              )}
            </button>
          </this.afHeadingLevel>
        </header>
        <section
          class="digi-expandable-accordion__content"
          id={`${this.afId}-content`}>
          <div>
            <digi-typography>
              <slot></slot>
            </digi-typography>
            {this.afVariation === ExpandableAccordionVariation.SECONDARY && (
              <digi-button
                class="digi-expandable-accordion__toggle-inside"
                afVariation={ButtonVariation.FUNCTION}
                afType={ButtonType.BUTTON}
                onAfOnClick={(e) => this.clickToggleHandler(e.detail, true)}
                afFullWidth
                afAriaExpanded={this.afExpanded}
                afAriaControls={`${this.afId}-content`}>
                {this.afExpanded ? this.afCollapseText : this.afExpandText}
                {this.toggleIcon}
              </digi-button>
            )}
          </div>
        </section>
      </article>
    );
  }
}
