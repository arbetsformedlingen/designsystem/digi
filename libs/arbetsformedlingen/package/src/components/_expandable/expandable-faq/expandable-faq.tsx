import { Component, h, Prop, State, Element } from '@stencil/core';
import { ExpandableFaqHeadingLevel } from './expandable-faq-heading-level.enum';
import { ExpandableFaqVariation } from './expandable-faq-variation.enum';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { logger } from '../../../global/utils/logger';

/**
 * @enums ExpandableFaqHeadingLevel- expandable-faq-heading-level.enum.ts
 * @enums ExpandableFaqVariation - expandable-faq-variation.enum.ts
 * @swedishName FAQ
 */
@Component({
  tag: 'digi-expandable-faq',
  styleUrl: 'expandable-faq.scss',
  scoped: true
})
export class ExpandableFaq {
  @Element() hostElement: HTMLStencilElement;
  @State() isExpanded: boolean;
  /**
   * Rubrikens vanliga frågor
   * @en The heading of FAQ text
   */
  @Prop() afHeading: string;

  /**
   * Sätter rubriknivå. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: ExpandableFaqHeadingLevel =
    ExpandableFaqHeadingLevel.H2;

  /**
   * Sätter variant. Kan vara 'Primary' eller 'Secondary'
   * @en Set variation. Can be 'Primary' or 'Secondary'
   */
  @Prop() afVariation: ExpandableFaqVariation = ExpandableFaqVariation.PRIMARY;

  @State() listItems = [];

  componentWillLoad() {
    this.getListItems();
    this.setFaqItemProps();
  }

  componentWillUpdate() {
    this.getListItems();
    this.setFaqItemProps();
  }

  getListItems() {
    const menuItems = this.hostElement.querySelectorAll(
      'digi-expandable-faq-item'
    );

    if (!menuItems || menuItems.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }

    this.listItems = [...Array.from(menuItems)].map((faqItem) => {
      return {
        outerHTML: faqItem.outerHTML,
        ref: faqItem
      };
    });
  }

  setFaqItemProps() {
    this.listItems &&
      this.listItems.forEach((faqItem) => {
        faqItem.ref.afVariation = this.afVariation;

        try {
          const topLevel = Number.parseInt(this.afHeadingLevel.split('h')[1]);
          faqItem.ref.afHeadingLevel = 'h' + (topLevel + 1);
        } catch (e) {
          // Nothing
        }
      });
  }

  render() {
    return (
      <div
        class={{
          'digi-expandable-faq': true
        }}>
        <this.afHeadingLevel>{this.afHeading}</this.afHeadingLevel>
        <slot></slot>
      </div>
    );
  }
}
