import { E2EPage, newE2EPage } from '@stencil/core/testing';

describe('form-select-filter', () => {
  let page: E2EPage
  
  beforeEach(async () => {
    page = await newE2EPage({
      html: `<digi-form-select-filter
      af-filter-button-text-label="Välj språk"
      af-description="Beskrivande text"
      af-filter-button-text="Inget språk valt"
      af-name="Sök språk"
      af-submit-button-text="Filtrera"
      af-multiple-items="true"
      af-validation="neutral"
      
    >
    </digi-form-select-filter>
    
    <script>
      const element = document.querySelector('digi-form-select-filter');
    
      element.afListItems = [{"label":"Danska","value":"Danska"},{"label":"Franska","value":"Franska"},{"label":"Tyska","value":"Tyska"},{"label":"Ukrainska","value":"Ukrainska"},{"label":"العربية (Arabiska)","dir":"rtl","lang":"ar","value":"العربية (Arabiska)"}];
    </script`
    })
  })

  it('should output on query changes', async () => {

    const button = await page.find('button');
    await button.click();
    const input = await page.find('input');
    const changeEvent = await page.spyOnEvent('afOnQueryChanged');

    await input.press('a');
    expect(changeEvent).toHaveReceivedEventDetail('a');


  });


  it('should output submited filters for a given number of selections', async () => {
 
    const button = await page.find('button');
    await button.click();

    const checkboxes = await page.findAll('.digi-form-select-filter__item');

    await checkboxes[0].click();;
    const submitbutton = await page.find('.digi-form-select-filter__submit-button');
    await submitbutton.click();

    const changeEvent = await page.spyOnEvent('afOnSubmitFilters');

    await button.click();
    await checkboxes[1].click();
    await submitbutton.click();
    

    expect(changeEvent).toHaveReceivedEventDetail([ { label: "Danska", "selected": true, value: "Danska" },
    { label: "Franska", "selected": true, value: "Franska" }]);
  });
});
