export enum FormInputButtonVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
