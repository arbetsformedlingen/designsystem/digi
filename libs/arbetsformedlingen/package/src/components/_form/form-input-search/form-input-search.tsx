import {
  Component,
  Element,
  Event,
  EventEmitter,
  Listen,
  Method,
  Prop,
  h,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormInputSearchVariation } from './form-input-search-variation.enum';
import { FormInputType } from '../form-input/form-input-type.enum';
import { ButtonType } from '../../_button/button/button-type.enum';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { FormInputVariation } from '../form-input/form-input-variation.enum';
import { ButtonSize } from '../../_button/button/button-size.enum';
import { FormInputButtonVariation } from '../form-input/form-input-button-variation.enum';
import { DigiFormInputCustomEvent } from '../../../components';

/**
 * @enums FormInputSearchVariation - form-input-search-variation.enum.ts
 * @swedishName Sökfält
 */
@Component({
  tag: 'digi-form-input-search',
  styleUrls: ['form-input-search.scss'],
  scoped: true
})
export class FormInputSearch {
  @Element() hostElement: HTMLStencilElement;

  private _input?;

  /**
   * Texten till labelelementet
   * @en The label text
   */
  @Prop() afLabel!: string;

  /**
   * Valfri beskrivande text
   * @en A description text
   */
  @Prop() afLabelDescription: string;

  /**
   * Sätter attributet 'type'.
   * @en Input type attribute
   */
  @Prop() afType: FormInputType = FormInputType.SEARCH;

  /**
   * Layout för hur inmatningsfältet ska visas då det finns en knapp
   * @en Input layout when there is a button
   */
  @Prop() afButtonVariation: FormInputButtonVariation =
    FormInputButtonVariation.PRIMARY;

  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  @Prop() afName: string;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-form-input-search');

  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   * @ignore
   */
  @Prop() value: string;
  @Watch('value') onValueChanged(value) {
    this.afValue = value;
  }

  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  @Prop() afValue: string;
  @Watch('afValue') onAfValueChanged(value) {
    this.value = value;
  }

  /**
   * Sätter attributet 'autocomplete'.
   * @en Input autocomplete attribute
   */
  @Prop() afAutocomplete: string;

  /**
   * Sätter attributet 'aria-autocomplete'.
   * @en Input aria-autocomplete attribute
   */
  @Prop() afAriaAutocomplete: string;

  /**
   * Sätter attributet 'aria-activedescendant'.
   * @en Input aria-activedescendant attribute
   */
  @Prop() afAriaActivedescendant: string;

  /**
   * Sätter attributet 'aria-labelledby'.
   * @en Input aria-labelledby attribute
   */
  @Prop() afAriaLabelledby: string;

  /**
   * Sätter attributet 'aria-describedby'.
   * @en Input aria-describedby attribute
   */
  @Prop() afAriaDescribedby: string;

  /**
   * Sätter variant. Kan vara 'small', 'medium' eller 'large'
   * @en Set input variation.
   */
  @Prop() afVariation: FormInputSearchVariation =
    FormInputSearchVariation.MEDIUM;

  /**
   * Dölj knappen
   * @en Hide the button
   */
  @Prop() afHideButton: boolean;

  /**
   * Sätter knappelementets attribut 'type'. Bör vara 'submit' eller 'button'
   * @en Set button element's type attribute.
   */
  @Prop() afButtonType: ButtonType = ButtonType.SUBMIT;

  /**
   * Knappens text
   * @en The button text
   */
  @Prop() afButtonText: string;

  /**
   * Knappens aria label text
   * @en The buttons aria label text
   */
  @Prop() afButtonAriaLabel: string;

  /**
   * Button aria-labelledby attribute
   */
  @Prop() afButtonAriaLabelledby: string;

  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  @Prop() afAutofocus: boolean;

  /**
   * Vid fokus utanför komponenten.
   * @en When focus is outside component.
   */
  @Event() afOnFocusOutside: EventEmitter;

  /**
   * Vid fokus inuti komponenten.
   * @en When focus is inside component.
   */
  @Event() afOnFocusInside: EventEmitter;

  /**
   * Vid klick utanför komponenten.
   * @en When click outside component.
   */
  @Event() afOnClickOutside: EventEmitter;

  /**
   * Vid klick inuti komponenten.
   * @en When click inside component.
   */
  @Event() afOnClickInside: EventEmitter;

  /**
   * Inputelementets 'onchange'-event.
   * @en The input element's 'onchange' event.
   */
  @Event() afOnChange: EventEmitter;

  /**
   * Inputelementets 'onblur'-event.
   * @en The input element's 'onblur' event.
   */
  @Event() afOnBlur: EventEmitter;

  /**
   * Inputelementets 'onkeyup'-event.
   * @en The input element's 'onkeyup' event.
   */
  @Event() afOnKeyup: EventEmitter;

  /**
   * Inputelementets 'onfocus'-event.
   * @en The input element's 'onfocus' event.
   */
  @Event() afOnFocus: EventEmitter;

  /**
   * Inputelementets 'onfocusout'-event.
   * @en The input element's 'onfocusout' event.
   */
  @Event() afOnFocusout: EventEmitter;

  /**
   * Inputelementets 'oninput'-event.
   * @en The input element's 'oninput' event.
   */
  @Event() afOnInput: EventEmitter;

  /**
   * Knappelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  @Event() afOnClick: EventEmitter;

  /**
   * Komponent skickar detta Event när sökfältets knapp är klickat eller enter knapp är tryckt när man har fokus på sökfältet. Returnerar fältets value
   * @en Component sends this event when the search field button is clicked or enter button is pressed when focus is on the search field. Returns field value
   */
  @Event() afOnSubmitSearch: EventEmitter<string>;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  @Method()
  async afMGetFormControlElement() {
    const formControlElement = await this._input.afMGetFormControlElement();
    return formControlElement;
  }

  blurHandler(e: Event) {
    this.afOnBlur.emit(e);
  }

  changeHandler(e: Event) {
    const element = e.target as HTMLInputElement;
    this.afValue = this.value = element.value;
    this.afOnChange.emit(e);
  }

  focusHandler(e: Event) {
    e.stopPropagation();
    this.afOnFocus.emit(e);
  }

  focusoutHandler(e: Event) {
    this.afOnFocusout.emit(e);
  }

  keyupHandler(e: DigiFormInputCustomEvent<KeyboardEvent>) {
    this.afOnKeyup.emit(e);
    if (e.detail.key === 'Enter') {
      this.afOnSubmitSearch.emit(this.afValue);
    }
  }

  inputHandler(e: Event) {
    this.afOnInput.emit(e);
  }

  clickHandler(e: Event) {
    e.stopPropagation();
    this.afOnClick.emit(e);
    this.afOnSubmitSearch.emit(this.afValue);
  }

  @Listen('click', { target: 'window' })
  clickOutsideListener(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    } else {
      this.afOnClickInside.emit(e);
    }
  }

  @Listen('focusin', { target: 'document' })
  focusOutsideListener(e: FocusEvent): void {
    const target = e.target as HTMLElement;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    } else {
      this.afOnFocusInside.emit(e);
    }
  }

  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  componentDidLoad() {
    this.afOnReady.emit();
  }

  get cssModifiers() {
    return {
      'digi-form-input-search--small':
        this.afVariation === FormInputSearchVariation.SMALL,
      'digi-form-input-search--medium':
        this.afVariation === FormInputSearchVariation.MEDIUM,
      'digi-form-input-search--large':
        this.afVariation === FormInputSearchVariation.LARGE
    };
  }

  render() {
    return (
      <div
        class={{
          'digi-form-input-search': true,
          ...this.cssModifiers
        }}
        data-af-identifier={this.afId}>
        <digi-form-input
          class={{
            'digi-form-input-search__input': true,
            'digi-form-input-search__input--no-button': this.afHideButton
          }}
          ref={(el) => (this._input = el)}
          onAfOnBlur={(e) => this.blurHandler(e)}
          onAfOnChange={(e) => this.changeHandler(e)}
          onAfOnFocus={(e) => this.focusHandler(e)}
          onAfOnFocusout={(e) => this.focusoutHandler(e)}
          onAfOnKeyup={(e) => this.keyupHandler(e)}
          onAfOnInput={(e) => this.inputHandler(e)}
          afLabel={this.afLabel}
          afLabelDescription={this.afLabelDescription}
          afAriaActivedescendant={this.afAriaActivedescendant}
          afAriaDescribedby={this.afAriaDescribedby}
          afAriaLabelledby={this.afAriaLabelledby}
          afAriaAutocomplete={this.afAriaAutocomplete}
          afAutocomplete={this.afAutocomplete}
          afName={this.afName}
          afType={this.afType}
          afValue={this.afValue}
          afVariation={this.afVariation as unknown as FormInputVariation}
          afAutofocus={this.afAutofocus ? this.afAutofocus : null}
          afButtonVariation={this.afButtonVariation}
          afId={this.afId}>
          {!this.afHideButton && (
            <digi-button
              class="digi-form-input-search__button"
              onAfOnClick={(e) => this.clickHandler(e)}
              onAfOnFocus={(e) => e.stopPropagation()}
              afType={this.afButtonType}
              afAriaLabel={this.afButtonAriaLabel}
              afAriaLabelledby={this.afButtonAriaLabelledby}
              afSize={this.afVariation as unknown as ButtonSize}
              slot="button">
              <digi-icon-search slot="icon" />
              {this.afButtonText}
            </digi-button>
          )}
        </digi-form-input>
      </div>
    );
  }
}
