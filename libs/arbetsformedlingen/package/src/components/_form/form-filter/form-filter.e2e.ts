import { newE2EPage } from '@stencil/core/testing';

describe('form-filter', () => {

  beforeAll(() => {
    console.warn = () => null
  })

  it('should rerender when listItems updates', async () => {
    const page = await newE2EPage({
      html: `
  <digi-form-filter
    af-reset-button-text="Återställ"
    af-filter-button-text="Filtrera"
    af-submit-button-text="Filtrera"
    ></digi-form-filter>
    <script>
    const elem = document.querySelector('digi-form-filter')
    elem.afCheckItems = ['1', '3']
    elem.afListItems = [
      { id: '1', label: 'Första valet' },
      { id: '2', label: 'Andra valet' },
      { id: '3', label: 'Tredje valet' },
      { id: '4', label: 'Fjärde valet' }
    ]
    </script>
      `
    });

    const elem = await page.find('digi-form-filter');
    await elem.click();

    const checkboxes = () => page.findAll('digi-form-checkbox');
    const checked = async () => {
      const results = [];
      for (const cb of await checkboxes()) {
        if ((await cb.getProperty('afChecked')) === true) {
          results.push(cb);
        }
      }
      return results;
    };

    expect((await checkboxes()).length).toBe(4); // number of options
    expect((await checked()).length).toBe(2);

    await elem.setProperty('afListItems', []);
    await elem.setProperty('afCheckItems', []);
    await page.waitForChanges();

    expect((await checkboxes()).length).toBe(0);
    expect((await checked()).length).toBe(0);
  });

})