import {
  Component,
  Element,
  Event,
  EventEmitter,
  Prop,
  State,
  Listen,
  h,
  Host,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { _t } from '../../../text';
import {
  FilterItemId,
  FormFilterItem,
  RefIndex,
  FocusElement,
  FormFilterState,
  FilterChange
} from './form-filter.types';

/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-checkbox.
 * @swedishName Multifilter
 */
@Component({
  tag: 'digi-form-filter',
  styleUrl: 'form-filter.scss',
  scoped: true
})
export class FormFilter {
  @Element() hostElement: HTMLStencilElement;

  @State() listItems: FormFilterItem[] = [];
  @State() checkedItems: FilterItemId[] = [];
  @State() tabItems: FilterItemId[] = [];
  @State() isActive = false;
  @State() activeListItemIndex = 0;

  private refs: Partial<RefIndex<FocusElement>> = {};
  private slotRef: Element;

  /**
   * Prefixet för alla komponentens olika interna 'id'-attribut på dropdowns, filterlistor etc. Ex. 'my-cool-id' genererar 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' och så vidare. Ges inget värde så genereras ett slumpmässigt.
   * @en Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' and so on. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-form-filter');

  /**
   * Texten i filterknappen
   * @en Set the filter button text
   */
  @Prop() afFilterButtonText!: string;

  /**
   * Texten i submitknappen
   * @en Set the submit button text
   */
  @Prop() afSubmitButtonText!: string;

  /**
   * Skärmläsartext för submitknappen
   * @en Set the submit button aria-label text
   */
  @Prop() afSubmitButtonTextAriaLabel: string;

  /**
   * Texten i rensaknappen
   * @en Set the reset button text
   */
  @Prop() afResetButtonText: string = _t.clear;

  /**
   * Skärmläsartext för resetknappen
   * @en Set the reset button aria-label text
   */
  @Prop() afResetButtonTextAriaLabel: string;

  /**
   * Döljer rensaknappen när värdet sätts till true
   * @en Hides the reset button when set to true
   */
  @Prop() afHideResetButton = false;

  /**
   * Justera dropdown från höger till vänster
   * @en Align dropdown from right to left
   */
  @Prop() afAlignRight: boolean;

  /**
   * Namnet på filterlistans valda filter vid submit. Används även av legendelementet inne i filterlistan.
   * @en Binding name of filterlist selected filters on submit. Also used by the legend element.
   */
  @Prop() afName: string;

  /**
   * Sätter attributet 'aria-label' på filterknappen
   * @en Input aria-label attribute on filter button
   */
  @Prop() afFilterButtonAriaLabel: string;

  /**
   * Sätter attributet 'aria-describedby' på filterknappen
   * @en Input aria-describedby attribute on filter button
   */
  @Prop() afFilterButtonAriaDescribedby: string;

  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  @Prop() afAutofocus: boolean;

  /**
   * Lista med val
   * @en List with option
   */
  @Prop() afListItems: string | FormFilterItem[];

  /**
   * Definiera vilka kryssrutor som ska vara ikryssade, förväntar sig filtrets `id`
   * @en Defines which checkboxes are checked, expects the `id` of the filter
   */
  @Prop() afCheckItems: FilterItemId[] = [];

  @Watch('afCheckItems')
  syncCheckItems() {
    this.checkedItems = this.afCheckItems;
  }

  @Watch('afListItems')
  handleListItems() {
    if (this.afListItems) {
      // via props
      this.setupListProps(this.afListItems);
    } else {
      // child elements
      this.setupListElements(this.slotRef.children);
    }
  }

  @Watch('listItems')
  setupAccessibility() {
    // set default tabIndex to first filter
    if (this.tabItems.length === 0 && this.listItems.length > 0) {
      this.tabItems = [this.listItems.at(0).id];
    }
  }

  setupListProps(userItems: string | FormFilterItem[]) {
    try {
      if (Array.isArray(userItems)) {
        this.listItems = userItems;
      } else if (typeof userItems === 'string') {
        this.listItems = JSON.parse(userItems);
      } else {
        throw `Invalid type in "af-list-items" attribute`;
      }
    } catch (e) {
      logger.warn(
        `Invalid JSON in "af-list-items" attribute`,
        this.hostElement,
        e
      );
      return;
    }
  }

  /**
   * När fokus går över till ett annat element
   * @en When focus moves on to another element
   */
  @Event() afBlurFilter: EventEmitter<{ filterName?: string }>;

  /**
   * Vid uppdatering av filtret
   * @en At filter submit
   */
  @Event() afSubmitFilter: EventEmitter<
    FormFilterState & { filterName?: string }
  >;

  /**
   * När filtret stängs utan att valda alternativ bekräftats
   * @en When the filter is closed without confirming the selected options
   */
  @Event() afCloseFilter: EventEmitter<
    FormFilterState & { filterName?: string }
  >;

  /**
   * Vid reset av filtret
   * @en At filter reset
   */
  @Event() afResetFilter: EventEmitter<{ filterName?: string }>;

  /**
   * När ett filter ändras
   * @en When a filter is changed
   */
  @Event() afChangeFilter: EventEmitter<FilterChange & { filterName?: string }>;

  @Listen('click', { target: 'window' })
  clickHandler(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const rootId = this.refs.root?.id;
    if (rootId) {
      if (detectClickOutside(target, `#${rootId}`) && this.isActive) {
        this.hideMenu();
      }
    }
  }

  @Listen('focusin', { target: 'document' })
  focusoutHandler(e: FocusEvent): void {
    const target = e.target as HTMLElement;
    const rootId = this.refs.root?.id;
    const focusOutsideFilter = detectFocusOutside(target, `#${rootId}`);

    if (focusOutsideFilter && this.isActive) {
      this.hideMenu();
    } else {
      this.afBlurFilter.emit({
        filterName: this.afName
      });
    }
  }

  setupListElements(collection: HTMLCollection) {
    if (!collection || collection.length <= 0) {
      logger.warn(`The slot contains no filter elements.`, this.slotRef);
      return;
    }

    this.listItems = [...collection].map(
      (element: HTMLDigiFormCheckboxElement, i) => ({
        id: element.afId || i.toString(),
        label: element.afLabel
      })
    );
  }

  componentWillLoad() {
    this.syncCheckItems();
  }

  componentDidLoad() {
    this.handleListItems();
    this.setupSlot();
  }

  setupSlot() {
    if (!window.MutationObserver) { return } // for tests
    const observer = new window.MutationObserver(() => {
      this.setupListElements(this.slotRef.children);
    });
    observer.observe(this.slotRef, { childList: true });
  }

  incrementFocus() {
    if (this.activeListItemIndex < this.listItems.length - 1) {
      this.focusItem(this.activeListItemIndex + 1);
    }
  }

  decrementFocus() {
    if (this.activeListItemIndex > 0) {
      this.focusItem(this.activeListItemIndex - 1);
    }
  }

  @Listen('keydown')
  handleKey(e: KeyboardEvent) {
    switch (e.key) {
      case 'ArrowDown':
        return this.incrementFocus();
      case 'ArrowUp':
        return this.decrementFocus();
      case 'Home':
        return this.focusItem(0);
      case 'End':
        return this.focusItem(this.listItems.length - 1);
      case 'Enter':
        (e.target as HTMLInputElement).click();
        break;
      case 'Escape':
        return this.hideMenu();
    }
  }

  focusItem(idx: number) {
    this.activeListItemIndex = idx;
    const currentItem = this.listItems.at(idx);
    if (currentItem && this.refs.filter) {
      this.refs.filter[currentItem.id]?.focus();
    }
    this.tabItems = [currentItem?.id];
  }

  hideMenu() {
    this.isActive = false;
    (this.refs.toggle as HTMLButtonElement)?.focus();
    this.afCloseFilter.emit({
      filterName: this.afName,
      listItems: this.listItems,
      checked: this.checkedItems
    });
  }

  showMenu() {
    this.isActive = true;
    setTimeout(() => this.focusItem(0), 10);
  }

  handleToggle() {
    if (this.isActive) {
      this.hideMenu();
    } else {
      this.showMenu();
    }
  }

  handleCheckItem(item: FormFilterItem) {
    const isCurrentlyChecked = this.checkedItems.includes(item.id);
    if (isCurrentlyChecked) {
      this.checkedItems = this.checkedItems.filter((id) => id !== item.id);
    } else {
      this.checkedItems = [...this.checkedItems, item.id];
    }

    const idx = this.listItems.findIndex(({ id }) => id === item.id);
    this.focusItem(idx);

    this.afChangeFilter.emit({
      filterName: this.afName,
      id: item.id,
      isChecked: !isCurrentlyChecked
    });
  }

  handleSubmit() {
    this.hideMenu();
    this.afSubmitFilter.emit({
      filterName: this.afName,
      listItems: this.listItems,
      checked: this.checkedItems
    });
  }

  handleReset() {
    this.checkedItems = [];
    this.afResetFilter.emit({
      filterName: this.afName
    });
  }

  render() {
    return (
      <Host>
        <div
          id={`${this.afId}-identifier`}
          ref={(el) => (this.refs.root = el as HTMLElement)}
          class={{
            'digi-form-filter': true,
            'digi-form-filter--open': this.isActive
          }}>
          <digi-button
            id={`${this.afId}-toggle-button`}
            afInputRef={(el) => (this.refs.toggle = el as HTMLButtonElement)}
            class="digi-form-filter__toggle-button"
            onAfOnClick={() => this.handleToggle()}
            af-aria-haspopup="true"
            af-variation="secondary"
            af-aria-label={this.afFilterButtonAriaLabel}
            af-aria-labelledby={this.afFilterButtonAriaDescribedby}
            af-aria-controls={`${this.afId}-dropdown-menu`}
            af-aria-expanded={this.isActive}
            af-autofocus={this.afAutofocus ? this.afAutofocus : null}>
            <i
              class={{
                'digi-form-filter__toggle-button-indicator': true,
                'digi-form-filter__toggle-button-indicator--active':
                  this.checkedItems.length > 0
              }}></i>
            <span class="digi-form-filter__toggle-button-text">
              {this.afFilterButtonText}
            </span>
            <digi-icon-chevron-down
              class="digi-form-filter__toggle-icon"
              slot="icon-secondary"
            />
          </digi-button>

          <div
            id={`${this.afId}-dropdown-menu`}
            ref={(el) => (this.refs.menu = el as HTMLElement)}
            class={{
              'digi-form-filter__dropdown': true,
              'digi-form-filter__dropdown--right': this.afAlignRight,
              'digi-form-filter__dropdown--hidden': !this.isActive
            }}>
            <div class="digi-form-filter__scroll">
              <fieldset>
                {this.afName ? (
                  <legend class="digi-form-filter__legend">
                    {this.afName}
                  </legend>
                ) : (
                  <legend class="legend-hidden">
                    {this.afFilterButtonText}
                  </legend>
                )}
                <ul
                  class="digi-form-filter__list"
                  aria-label={this.afFilterButtonAriaLabel}
                  id={`${this.afId}-filter-list`}>
                  {this.listItems.map((item) => {
                    const isChecked = this.checkedItems.includes(item.id);
                    const isTabbable = this.tabItems.includes(item.id);
                    return (
                      <li class="digi-form-filter__item" key={item.id}>
                        <digi-form-checkbox
                          afInputRef={(ref) =>
                            (this.refs.filter = {
                              ...this.refs.filter,
                              [item.id]: ref
                            })
                          }
                          afTabIndex={isTabbable ? 0 : -1}
                          afLabel={item.label}
                          afValue={item.id}
                          afChecked={isChecked}
                          onClick={(e) => {
                            e.preventDefault();
                            this.handleCheckItem(item);
                          }}
                        />
                      </li>
                    );
                  })}
                </ul>
              </fieldset>
            </div>
            <div class="digi-form-filter__footer">
              <digi-button
                class="digi-form-filter__submit-button"
                af-variation="primary"
                af-aria-label={this.afSubmitButtonTextAriaLabel}
                onClick={() => this.handleSubmit()}
                afInputRef={(ref) =>
                  (this.refs.submit = ref as HTMLButtonElement)
                }>
                {this.afSubmitButtonText}
              </digi-button>
              {!this.afHideResetButton && (
                <digi-button
                  af-variation="secondary"
                  af-aria-label={this.afResetButtonTextAriaLabel}
                  onClick={() => this.handleReset()}
                  class="digi-form-filter__reset-button"
                  afInputRef={(ref) =>
                    (this.refs.reset = ref as HTMLButtonElement)
                  }>
                  {this.afResetButtonText}
                </digi-button>
              )}
            </div>
          </div>
        </div>
        <div
          ref={(ref) => (this.slotRef = ref)}
          id={`${this.afId}-children`}
          style={{ display: 'none' }}>
          <slot />
        </div>
      </Host>
    );
  }
}
