/* eslint-disable @typescript-eslint/no-explicit-any */

import {
  Component,
  Element,
  Event,
  EventEmitter,
  Fragment,
  h,
  Method,
  Prop,
  State,
  Watch
} from '@stencil/core';
import { _t } from '../../../text';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormFileUploadVariation } from './form-file-upload-variation.enum';
import { FormFileUploadValidation } from './form-file-upload-validation.enum';
import { FormFileUploadHeadingLevel } from './form-file-upload-heading-level.enum';
import { UtilBreakpointObserverBreakpoints } from '../../_util/util-breakpoint-observer/util-breakpoint-observer-breakpoints.enum';
import { FormFileUploadErrorMessage } from './form-file-upload-error-messages.type';
import { FormValidationMessageVariation } from '../form-validation-message/form-validation-message-variation.enum';

/**
 *	@enums FormInputFileType - form-input-file-type.enum.ts
 *  @enums FormInputFileVariation - form-input-file-variation.enum.ts
 * 	@swedishName Filuppladdare
 */
@Component({
  tag: 'digi-form-file-upload',
  styleUrl: 'form-file-upload.scss',
  scoped: true
})
export class FormFileUpload {
  @Element() hostElement: HTMLStencilElement;

  @State() _input: HTMLInputElement;
  @State() _fileList: HTMLUListElement;
  @State() _uploadButton: HTMLButtonElement;
  @State() _srTextContainer: HTMLParagraphElement;
  @State() files: File[] = [];
  @State() error = false;
  @State() errorMessages: FormFileUploadErrorMessage[] = [];
  @State() fileHover = false;

  /**
   * Texten till labelelementet
   * @en The label text
   */
  @Prop() afLabel: string = _t.form.file_upload;

  /**
   * Valfri beskrivande text
   * @en A description text
   */
  @Prop() afLabelDescription: string;

  /**
   * Sätter variant. Kan vara 'small', 'medium' eller 'large'. Oberoende på satt storlek så kommer komponenten bli 'small' på mobilskärmar.
   * @en Set input variation 'small', 'medium' or 'large'. On a mobile screen the variation will be automatically set to 'small'.
   */
  @Prop() afVariation: FormFileUploadVariation = FormFileUploadVariation.SMALL;
  private aforiginalVariation: FormFileUploadVariation;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-form-file-upload');

  /** Sätter attributet 'accept'. Använd för att limitera accepterade filtyper
   * @en Set input field's accept attribute. Use this to limit accepted file types.
   */
  @Prop() afFileTypes!: string;

  /**
   * Sätter en maximal filstorlek i MB, 10MB är standard.
   * @en Set file max size in MB
   */
  @Prop() afFileMaxSize = 10;

  /**
   * Sätter maximalt antal filer som kan laddas upp
   * @en Set maximum number of upload files
   */
  @Prop() afMaxFiles: number;

  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  @Prop() afName: string;

  /**
   * Sätter ladda upp knappens text.
   * @en Set upload buttons text
   */
  @Prop() afUploadBtnText: string;

  /**
   * Sätter attributet 'required'.
   * @en Input required attribute
   */
  @Prop() afRequired: boolean;

  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */
  @Prop() afRequiredText: string;

  /**
   * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
   * @en Set this to true if the form contains more required fields than optional fields.
   */
  @Prop() afAnnounceIfOptional = false;

  /**
   * Sätt rubrik för uppladdade filer.
   * @en Set heading for uploaded files.
   */
  @Prop() afHeadingFiles = _t.form.file_uploaded_files;

  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  @Prop() afAnnounceIfOptionalText: string;

  /**
   * Sätter text vid varning om överskriden filstorlek.
   * @en Set text for warning on file size exceeded.
   */
  @Prop() afFileMaxSizeText: string;

  /**
   * Visar miniatyrbild/ikon på uppladdade filer.
   * @en Shows thumbnail.
   */
  @Prop() afShowThumbnail = false;

  /**
   * Aktiverar förhandsgranskning.
   * @en Activates preview of uploaded files.
   */
  @Prop() afEnablePreview = false;

  /**
   * Sätter valideringsmeddelande. Kan exempelvis användas för att förtydliga obligatoriskt fält.
   * @en Set validation message
   */
  @Prop()
  afValidationText: string;
  @Watch('afValidationText')
  afValidationTextChange() {
    this.setValidationText();
  }

  /**
   * Sätter antivirus scan status. Kan vara 'enable' eller 'disabled'.
   * @en Set antivirus scan status. Enable is default.
   */
  @Prop() afValidation: FormFileUploadValidation =
    FormFileUploadValidation.DISABLED;

  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: FormFileUploadHeadingLevel =
    FormFileUploadHeadingLevel.H2;

  /**
   * Sänder ut fil som håller på att laddas upp, eller som har laddats upp - beroende på afValidation-inställning
   * @en Emits file on upload
   */
  @Event() afOnUploadFile: EventEmitter;

  /**
   * Sänder ut vilken fil som tagits bort
   * @en Emits which file that was deleted.
   */
  @Event() afOnRemoveFile: EventEmitter;

  /**
   * Sänder ut den fil vars uppladdning avbrutits
   * @en Emits which file that was canceled
   */
  @Event() afOnCancelFile: EventEmitter;

  /**
   * När komponenten och slotsen är laddade och initierade så skickas detta eventet.
   * @en When the component and slots are loaded and initialized this event will trigger.
   */
  @Event({
    bubbles: false,
    cancelable: true
  })
  afOnReady: EventEmitter;

  /**
   * När fönstret för att välja filer öppnas
   * @en When the file selection modal is activated
   */
  @Event() afShowModal: EventEmitter<void>;

  breakpointHandler(e: CustomEvent) {
    if (e.detail.value) {
      if (e.detail.value === UtilBreakpointObserverBreakpoints.SMALL) {
        this.afVariation = FormFileUploadVariation.SMALL;
      } else {
        this.afVariation = this.aforiginalVariation;
      }
    }
  }

  /**
   * Används för att skicka in en validerad fil tillbaks till komponenten. Förväntar sig att filen redan finns i komponenten med status "pending".
   * @en Send a validated file to the component. Expects file to exist in component with status "pending".
   */
  @Method()
  async afMValidateFile(file: File) {
    if (!this.fileExistInFiles(file)) {
      return;
    }

    this.removeFileFromList(file['id']);
    if (file['status'] !== 'error') {
      /* Måste göra en kopia av filen för att komponenten ska rendera om sig.
       * Annars kan den peka på samma referens och förstår inte att statusen har ändrats.  */
      const copiedFile = new File([file], file.name, { type: file.type });
      copiedFile['id'] = file['id'];
      copiedFile['status'] = file['status'];
      copiedFile['base64'] = await this.toBase64(file);
      this.files = [...this.files, copiedFile];
    } else {
      this.validateFile(file);
    }
  }

  /**
   * Få ut alla uppladdade filer
   * @en Get all uploaded files
   */
  @Method()
  async afMGetAllFiles() {
    return this.files;
  }

  /**
   * Importera lista med filer utan att trigga uppladdningsevent. Ett objekt måste innehålla id, status och filen själv.
   * @en Import array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
   */
  @Method()
  async afMImportFiles(files: File[]) {
    if (files && files.length > 0) {
      for (const importedFile of files) {
        this.validateFile(importedFile);
      }
    }

    const filesThatArOK =
      this.errorMessages.length === 0 ? files : this.getFilesThatAreOK(files);

    if (filesThatArOK.length === 0) {
      return;
    }
    this.files = this.files.concat(filesThatArOK);
  }

  @Method()
  async afMGetFormControlElement() {
    return this._input;
  }

  componentWillLoad() {
    this.aforiginalVariation = this.afVariation;
  }

  componentDidLoad() {
    this.afOnReady.emit();
    /* Vid första renderingen så fungerar inte propertyn korrekt om vi inte kallar på setValidationText härifrån.
     * Gissar att det är en tajmin-issue mellan prop och state, men hinner inte gräva i det mer nu.  */
    this.setValidationText();
  }

  get fileMaxSize() {
    const MEGABYTE = 1048576;
    return this.afFileMaxSize * MEGABYTE;
  }

  setValidationText() {
    if (this.afValidationText && this.afValidationText.length > 0) {
      let isDuplicated = false;
      if (this.errorMessages.length > 0) {
        this.errorMessages.forEach((error) => {
          if (error.message === this.afValidationText) {
            isDuplicated = true;
          }
        });
      }
      if (!isDuplicated) {
        this.errorMessages.push({
          fileName: '',
          message: this.afValidationText
        });
      }
      this.error = true;
    } else {
      this.resetErrors();
    }
  }

  fileExistInFiles(fileToCheck): boolean {
    let exist = false;
    if (this.files.length > 0) {
      this.files.forEach((file) => {
        if (file.name === fileToCheck.name) {
          exist = true;
        }
      });
    }
    return exist;
  }

  checkAcceptedTypes(file) {
    const acceptedTypes = this.afFileTypes.replace(/\s/g, '').split(',');
    const fileMimeType = file.type;
    const fileNameExtension = file.name
      .split('.')
      .slice(-1)
      .map((item) => '.' + item.toLowerCase())
      .toString();

    return acceptedTypes.some((item) => {
      // Checking against group of mime types
      if (item.includes('/*')) {
        const mimeTypeCategory = item.split('/').slice(0, 1).toString();
        const fileMimeTypeCategory = fileMimeType
          .split('/')
          .slice(0, 1)
          .toString();
        return mimeTypeCategory == fileMimeTypeCategory;
      }

      // Checking against mimetype and extension
      return (
        item === fileMimeType || item === fileNameExtension || item === '*'
      );
    });
  }

  resetErrors(handleFocus = false) {
    this.error = false;
    this._srTextContainer.textContent = '';
    this.errorMessages = [];

    /* Timeouten behövs för att state och element ska hinna renderas om innan fokus flyttas.
     Vi märkte att det blir problem för hjälpmedel om den inte är på plats.  */
    if (handleFocus) {
      setTimeout(() => {
        this._uploadButton.focus();
      }, 100);
    }
  }

  addErrorMessage(fileName: string, message: string) {
    this.errorMessages.push({
      fileName: fileName,
      message: message
    });
  }

  validateFileStatus(file: File) {
    if (
      file['status'] &&
      file['status'] === FormValidationMessageVariation.ERROR
    ) {
      const message = file['error']
        ? file['error']
        : _t.form.file_error_unknown_error;
      this.addErrorMessage(file.name, message);
    }
  }

  validateMaxSize(file: File) {
    if (file.size > this.fileMaxSize) {
      const message = this.afFileMaxSizeText
        ? this.afFileMaxSizeText
        : _t.form.file_error_too_large(file.name, this.afFileMaxSize);
      this.addErrorMessage(file.name, message);
    }
  }

  validateFileType(file: File) {
    if (!this.checkAcceptedTypes(file)) {
      this.addErrorMessage(
        file.name,
        _t.form.file_error_filetype_not_allowed(file.name, this.afFileTypes)
      );
    }
  }

  validateFile(file: File) {
    this.validateIfFileIsAlreadyUploaded(file);
    this.validateFileStatus(file);
    this.validateMaxSize(file);
    this.validateFileType(file);

    this.error = this.errorMessages.length > 0;
  }

  onCancelFileHandler(e: Event, id: string) {
    e.preventDefault();
    const fileFromList = this.removeFileFromList(id);
    fileFromList['status'] = 'error';
    fileFromList['error'] = _t.form.file_upload_canceled;

    this.afOnCancelFile.emit(fileFromList);
  }

  removeFileFromList(id: string) {
    const index = this.getIndexOfFile(id);

    if (index === -1) {
      return;
    }

    const removedFile = this.files[index];
    this.files = [
      ...this.files.slice(0, index),
      ...this.files.slice(index + 1)
    ];
    return removedFile;
  }

  getIndexOfFile(id: string) {
    const index = this.files.findIndex((file) => file['id'] == id);
    return index;
  }

  getFilesThatAreOK(files: File[]): File[] {
    const validFiles = [];

    files.forEach((file) => {
      let isValid = true;
      this.errorMessages.forEach((message) => {
        if (message.fileName === file.name) {
          isValid = false;
        }
      });
      if (isValid) {
        validFiles.push(file);
      }
    });
    return validFiles;
  }

  validateIfFileIsAlreadyUploaded(file: File) {
    for (const item of this.files) {
      if (item.name === file.name) {
        this.addErrorMessage(file.name, _t.form.file_error_exists(file.name));
      }
    }
  }

  handleFiles(e) {
    this.resetErrors();
    const fileList = e.dataTransfer ? e.dataTransfer?.files : e.target?.files;

    const filesToValidate = [];
    Array.from(fileList).forEach((file: File) => {
      filesToValidate.push(file);
    });

    if (!filesToValidate || filesToValidate.length === 0) {
      return;
    }

    const numberOfFiles = filesToValidate.length + this.files.length;
    if (numberOfFiles > this.afMaxFiles) {
      this.addErrorMessage('', _t.form.file_error_too_many);
      this.error = true;
      return;
    }

    filesToValidate.forEach((file: File) => {
      this.validateFile(file);
    });

    const filesThatArOK =
      this.errorMessages.length === 0
        ? filesToValidate
        : this.getFilesThatAreOK(filesToValidate);

    if (filesThatArOK.length === 0) {
      return;
    }

    const formData = new FormData();
    filesThatArOK.forEach((file) => {
      formData.append('file', file);
    });

    for (const obj of formData.entries()) {
      this.addFileStatusAndEmitFiles(obj[1]);
    }
    this._input.value = '';
  }

  onButtonUploadFileHandler(e) {
    e.preventDefault();
    if (!e.target.files) return;

    this.handleFiles(e);
  }

  onRemoveFileHandler(e: Event, id: string) {
    e.preventDefault();
    this.resetErrors();

    const fileToRemove = this.removeFileFromList(id);
    this.afOnRemoveFile.emit(fileToRemove);
    this._input.value = '';

    this.handleFocusAfterFileRemoved();
  }

  handleFocusAfterFileRemoved() {
    /* Timeouten behövs för att state och element ska hinna renderas om innan fokus flyttas.
       Vi märkte att det blir problem för hjälpmedel om den inte är på plats.  */
    setTimeout(() => {
      if (this.files.length > 0) {
        this._fileList.focus();
      } else {
        this._uploadButton.focus();
      }
    }, 100);
  }

  emitFile(file) {
    const index = this.getIndexOfFile(file['id']);
    if (this.afValidation === FormFileUploadValidation.ENABLED) {
      this.afOnUploadFile.emit(this.files[index]);
    } else {
      this.files[index]['status'] = 'OK';
      this.afOnUploadFile.emit(this.files[index]);
    }
  }

  async addFileStatusAndEmitFiles(file) {
    file['id'] = randomIdGenerator('file');
    file['status'] = 'pending';
    file['base64'] = await this.toBase64(file);

    this.files = [...this.files, file];
    this.emitFile(file);
  }

  toBase64 = (file: Blob) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
        resolve(encoded);
      };
      reader.onerror = (error) => reject(error);
    });

  handleDrop(e: DragEvent) {
    e.stopPropagation();
    e.preventDefault();
    this.fileHover = false;

    if (e.dataTransfer.files) {
      this.handleFiles(e);
    }
  }

  handleAllowDrop(e: DragEvent) {
    if (this.afVariation === FormFileUploadVariation.SMALL) return;
    e.stopPropagation();
    e.preventDefault();
  }

  handleOnDragEnterLeave(e: DragEvent) {
    this.handleAllowDrop(e);
    this.fileHover = !this.fileHover;
  }

  handleInputClick(e: Event) {
    e.stopPropagation();
    this.afShowModal.emit();
    this._input.click();
  }

  getFiletypeAndShortFileName(file: File): { type: string; name: string } {
    let fileType = null;
    let fileName = file.name;
    const fileExtension = fileName.split('.').pop();
    const fileWithoutExtension = fileName.replace('.' + fileExtension, '');
    const minChars = window.innerWidth < 769 ? 6 : fileName.length;
    const maxChars = window.innerWidth < 769 ? 14 : fileName.length;
    if (String(file['type']).split('/')[0] === 'image') {
      fileType = 'image';
    } else if (String(file['type']).split('/')[1] === 'pdf') {
      fileType = 'pdf';
    } else {
      fileType = 'default';
    }
    if (fileName.length > minChars) {
      fileName =
        fileWithoutExtension.substring(0, minChars) + '...' + fileExtension;
    } else if (fileName.length > maxChars) {
      fileName = fileWithoutExtension.substring(0, 6) + '...' + fileExtension;
    } else {
      fileName = file.name;
    }
    return { type: fileType, name: fileName };
  }

  // If its a pdf or an image the file is opened on another tab, any other file formats are downloaded
  async viewUploadedFile(e, file) {
    e.preventDefault();
    if (this.getFiletypeAndShortFileName(file).type === 'image') {
      const w = window.open();
      w.document.write(
        `<img src="data:${file['type']};base64,${file['base64']}" alt="${file.name}" />`
      );
      w.document.title = file['name'];
    } else if (this.getFiletypeAndShortFileName(file).type === 'pdf') {
      (async () => {
        const pdfstr = await fetch(
          `data:application/pdf;base64,${file['base64']}`
        );
        const blobFromFetch = await pdfstr.blob();
        const blob = new Blob([blobFromFetch], { type: 'application/pdf' });
        const blobUrl = URL.createObjectURL(blob);
        setTimeout(() => {
          window.open(blobUrl, '_blank');
        }, 30);
      })();
    } else {
      const fileURL = URL.createObjectURL(file);
      const a = document.createElement('a');
      a.href = fileURL;
      a.target = '_blank';
      a.download = file.name;
      a.click();
    }
  }

  get cssModifiers() {
    return {
      'digi-form-file-upload--small': this.afVariation == 'small',
      'digi-form-file-upload--medium': this.afVariation == 'medium',
      'digi-form-file-upload--large': this.afVariation == 'large',
      'digi-form-file-upload--has-errors': this.errorMessages.length > 0
    };
  }

  renderErrorMessage() {
    if (!this.error) {
      return;
    }
    this.readErrorMessagesToAssistiveTechnologies();

    const showBorder = this.files.length > 0 ? 'with-files' : '';
    return (
      <div class={'digi-form-file-upload__error-list-container ' + showBorder}>
        <ul class="digi-form-file-upload__error-list" aria-hidden="true">
          {this.renderErrorMessages()}
        </ul>
        <button
          aria-label={_t.form.file_upload_reset_errors_aria_label}
          class="digi-form-file-upload__button clear-button hidden-mobile"
          onClick={() => {
            this.resetErrors(true);
          }}>
          <span>{_t.form.file_upload_reset_errors}</span>
        </button>
        <button
          onClick={() => {
            this.resetErrors(true);
          }}
          class="digi-form-file-upload__button clear-button hidden-desktop"
          aria-label={_t.form.file_upload_reset_errors_aria_label}>
          <digi-icon-x-button-outline
            class="digi-form-file-upload__button-icon clear-icon"
            aria-hidden="true"></digi-icon-x-button-outline>
        </button>
      </div>
    );
  }

  readErrorMessagesToAssistiveTechnologies() {
    this._srTextContainer.textContent = '';
    const srTexts = this.errorMessages.map((error) => {
      return error.message + ' ';
    });

    /* Timeouten behövs för att state och element ska hinna renderas om innan innehållet ändras.
       Vi märkte att det blir problem för hjälpmedel om den inte är på plats.  */
    setTimeout(() => {
      this._srTextContainer.textContent = srTexts.toString();
    }, 200);
  }

  renderErrorMessages() {
    return this.errorMessages.map((error) => {
      return (
        <li>
          <digi-form-validation-message
            id="digi-form-file-upload__error"
            class="digi-form-file-upload__error"
            af-variation="error">
            {error.message}
          </digi-form-validation-message>
        </li>
      );
    });
  }

  renderPendingFile(file: File) {
    return (
      <div class="digi-form-file-upload__item-pending-container">
        <span class="info-wrapper">
          <digi-icon-spinner class="digi-form-file-upload__spinner"></digi-icon-spinner>
          <span style={{ margin: '0' }}>
            {_t.form.file_upload_progress} <span class="hidden-mobile">|</span>
          </span>
          <p class="digi-form-file-upload__item-name-pending hidden-mobile">
            {this.getFiletypeAndShortFileName(file).name}
          </p>
        </span>
        <span class="button-wrapper">
          <button
            onClick={(e) => this.onCancelFileHandler(e, file['id'])}
            class="digi-form-file-upload__button digi-form-file-upload__button-abort hidden-mobile"
            aria-label={_t.form.file_cancel_upload_long(file.name)}>
            {_t.form.file_cancel_upload}
          </button>
          <button
            onClick={(e) => this.onCancelFileHandler(e, file['id'])}
            class="digi-form-file-upload__button hidden-desktop"
            aria-label={_t.form.file_cancel_upload_long(file.name)}>
            <digi-icon-x-button-outline
              class="digi-form-file-upload__button-icon"
              aria-hidden="true"></digi-icon-x-button-outline>
          </button>
        </span>
      </div>
    );
  }

  renderImageThumbnail(file: File) {
    if (this.afShowThumbnail) {
      return (
        <figure
          class="digi-form-file-upload__item-text-container-figure"
          style={{ margin: '0' }}>
          <img
            aria-hidden="true"
            class="thumbnail"
            src={`data:${file['type']};base64,${file['base64']}`}
          />
          <figcaption class="visually-hidden">
            Miniatyr av {file.name}
          </figcaption>
        </figure>
      );
    }
    return this.renderCheckmark();
  }

  renderCheckmark() {
    return <span class="digi-form-file-upload__item-green-check"></span>;
  }

  renderPDFThumbnail(file: File) {
    if (this.afShowThumbnail) {
      return (
        <span
          class={`${this.afShowThumbnail ? 'digi-form-file-upload__item-pdf-preview-bis' : null}`}
          aria-label={file.name}>
          <svg
            style={{ scale: '1' }}
            width="23.76"
            height="32"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M22.888 6.124 17.696.88A2.955 2.955 0 0 0 15.6 0H2.97C1.33.006 0 1.35 0 3.006V29C0 30.657 1.33 32 2.97 32h17.82c1.64 0 2.97-1.343 2.97-3V8.249a3.03 3.03 0 0 0-.872-2.124Zm-2.34 1.88H15.84V3.25l4.709 4.756ZM2.97 29.002V3.006h9.9v6.498c0 .831.662 1.5 1.485 1.5h6.435v17.997H2.97Zm15.481-8.98c-.755-.75-2.908-.544-3.985-.406-1.064-.656-1.775-1.562-2.277-2.893.242-1.006.625-2.537.335-3.5-.26-1.637-2.34-1.474-2.636-.368-.273 1.006-.025 2.406.433 4.193-.619 1.493-1.54 3.499-2.19 4.649-1.238.643-2.909 1.637-3.156 2.887-.204.987 1.609 3.449 4.708-1.95 1.386-.462 2.896-1.031 4.233-1.256 1.17.637 2.537 1.062 3.452 1.062 1.578 0 1.733-1.762 1.083-2.418ZM6.194 24.883c.315-.856 1.516-1.844 1.88-2.187-1.175 1.893-1.88 2.23-1.88 2.187Zm5.049-11.91c.458 0 .414 2.005.111 2.549-.272-.869-.266-2.55-.111-2.55Zm-1.51 8.535c.6-1.056 1.114-2.312 1.528-3.418.514.944 1.17 1.7 1.863 2.219-1.287.268-2.407.818-3.391 1.2Zm8.143-.312s-.31.375-2.308-.488c2.172-.162 2.53.338 2.308.488Z"
              fill="#1616B2"
              fill-rule="evenodd"
            />
          </svg>
        </span>
      );
    }
    return this.renderCheckmark();
  }

  renderThumbnail(file: File) {
    const fileType = this.getFiletypeAndShortFileName(file).type;
    switch (fileType) {
      case 'pdf':
        return this.renderPDFThumbnail(file);
      case 'image':
        return this.renderImageThumbnail(file);
      default:
        return this.renderCheckmark();
    }
  }

  renderFileName(file: File) {
    if (this.afEnablePreview) {
      return (
        <a
          class="digi-form-file-upload__item-name"
          href="#"
          onClick={(e) => {
            this.viewUploadedFile(e, file);
          }}>
          {file.name}
        </a>
      );
    } else {
      return <span class="digi-form-file-upload__item-name">{file.name}</span>;
    }
  }

  renderOKFile(file: File) {
    return (
      <Fragment>
        <span class="digi-form-file-upload__item-text-and-thumbnail-container">
          {this.renderThumbnail(file)}
          {this.renderFileName(file)}
        </span>
        <button
          onClick={(e) => this.onRemoveFileHandler(e, file['id'])}
          class="digi-form-file-upload__button digi-form-file-upload__button--mobile"
          aria-label={_t.form.file_remove(file.name)}>
          <span class="hidden-mobile">{_t.form.file_remove()}</span>
          <digi-icon-trash class="digi-form-file-upload__button-icon hidden-desktop"></digi-icon-trash>
        </button>
      </Fragment>
    );
  }

  renderFile(file: File) {
    switch (file['status']) {
      case 'pending':
        return this.renderPendingFile(file);
      default:
        return this.renderOKFile(file);
    }
  }

  renderFiles() {
    const filesMap = this.files.map((file, index: number) => {
      {
        return (
          <li
            class="digi-form-file-upload__item-container"
            key={'file-' + index}>
            <div class="digi-form-file-upload__item">
              <div class="digi-form-file-upload__item-text-container">
                {this.renderFile(file)}
              </div>
            </div>
          </li>
        );
      }
    });
    return filesMap;
  }

  renderFileContainer() {
    if (this.files.length === 0) {
      return;
    }

    return (
      <Fragment>
        <this.afHeadingLevel
          class={{
            'digi-form-file-upload__files-heading': true,
            'digi-form-file-upload__files-heading--and-errors': this.error
          }}>
          {this.afHeadingFiles}
        </this.afHeadingLevel>
        <ul
          class="digi-form-file-upload__files-list"
          tabindex={-1}
          ref={(el) => {
            this._fileList = el as HTMLUListElement;
          }}>
          {this.renderFiles()}
        </ul>
      </Fragment>
    );
  }

  render() {
    return (
      <Fragment>
        <digi-util-breakpoint-observer
          onAfOnChange={(e) =>
            this.breakpointHandler(e)
          }></digi-util-breakpoint-observer>
        <div
          class={{
            'digi-form-file-upload': true,
            ...this.cssModifiers
          }}>
          <digi-form-label
            afFor={this.afId}
            afLabel={this.afLabel}
            afDescription={this.afLabelDescription}
            afRequired={this.afRequired}
            afAnnounceIfOptional={this.afAnnounceIfOptional}
            afRequiredText={this.afRequiredText}
            afAnnounceIfOptionalText={
              this.afAnnounceIfOptionalText
            }></digi-form-label>
          <div
            class={{
              'digi-form-file-upload__upload-area': true,
              'digi-form-file-upload__upload-area--hover': this.fileHover
            }}
            onDrop={(e) => this.handleDrop(e)}
            onDragOver={(e) => this.handleAllowDrop(e)}
            onDragEnter={(e) => this.handleOnDragEnterLeave(e)}
            onDragLeave={(e) => this.handleOnDragEnterLeave(e)}
            onClick={(e) => this.handleInputClick(e)}>
            {this.fileHover &&
              this.afVariation != FormFileUploadVariation.SMALL && (
                <div class="digi-form-file-upload__upload-area-overlay"></div>
              )}
            <div class="digi-form-file-upload__text-area">
              <button
                ref={(el) => {
                  this._uploadButton = el as HTMLButtonElement;
                }}
                onClick={(e) => this.handleInputClick(e)}
                class="digi-form-file-upload__button hidden_mobile"
                style={{
                  margin: '0'
                }}>
                <digi-icon-paperclip
                  aria-hidden="true"
                  class="digi-form-file-upload__button-icon paper-clip"
                  slot="icon"></digi-icon-paperclip>
                {this.afUploadBtnText
                  ? this.afUploadBtnText
                  : _t.form.file_choose}
              </button>
              <input
                ref={(el) => {
                  this._input = el as HTMLInputElement;
                }}
                onChange={(e) => this.onButtonUploadFileHandler(e)}
                class="digi-form-file-upload__input"
                multiple
                type="file"
                id={this.afId}
                accept={this.afFileTypes}
                name={this.afName ? this.afName : null}
                required={this.afRequired ? this.afRequired : null}
                onDrop={(e) => this.handleDrop(e)}
                onDragOver={(e) => this.handleAllowDrop(e)}
                aria-errormessage="digi-form-file-upload__error"
                aria-describedBy="digi-form-file-upload__error"
              />
              <span>
                {this.afVariation !== FormFileUploadVariation.SMALL &&
                  _t.form.file_dropzone}
              </span>
            </div>
          </div>
          <p
            class="visually-hidden"
            role="alert"
            aria-atomic="true"
            ref={(el) => {
              this._srTextContainer = el as HTMLParagraphElement;
            }}></p>
          {(this.files.length > 0 || this.error) && (
            <div class="digi-form-file-upload__files">
              {this.renderErrorMessage()}
              {this.renderFileContainer()}
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}
