export type FormFileUploadErrorMessage = {
  fileName: string;
  message: string;
};
