import { Component, Prop, h } from '@stencil/core';
import { LayoutColumnsElement } from './layout-columns-element.enum';
import { LayoutColumnsVariation } from './layout-columns-variation.enum';
import { LayoutColumnsGap } from './layout-columns-column-gap.enum';

/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutColumnsVariation - layout-columns-variation.enum.ts
 * @enums LayoutColumnsElement - layout-columns-element.enum.ts
 * @swedishName Kolumner
 */
@Component({
  tag: 'digi-layout-columns',
  styleUrls: ['layout-columns.scss'],
  scoped: true
})
export class LayoutColumns {
  /**
   * Sätter elementtypen på det omslutande elementet. Kan vara 'div', 'ul' eller 'ol'.
   * @en Set wrapper element type. can be 'div', 'ul' or 'ol'.
   */
  @Prop() afElement: LayoutColumnsElement = LayoutColumnsElement.DIV;

  /**
   * Sätter förvalda kolumnvarianter. Kan vara 'two' eller 'three'.
   * @en Set preset column variations. Can be 'two' or 'three'.
   */
  @Prop() afVariation: LayoutColumnsVariation;

  /**
   * Sätter CSS-propertyn grid-column-gap, default är 16px (1rem). Kan sättas till '32' eller default.
   * @en Set CSS-propety grid-column-gap, default is 16px (1rem).  Can be set to '32' or default.
   */
  @Prop() afColumnGap: LayoutColumnsGap = LayoutColumnsGap.DEFAULT;

  get cssModifiers() {
    return {
      'digi-layout-columns--one':
        this.afVariation === LayoutColumnsVariation.ONE,
      'digi-layout-columns--two':
        this.afVariation === LayoutColumnsVariation.TWO,
      'digi-layout-columns--three':
        this.afVariation === LayoutColumnsVariation.THREE,
      'digi-layout-columns--four':
        this.afVariation === LayoutColumnsVariation.FOUR,
      'digi-layout-columns--column-gap-32':
        this.afColumnGap === LayoutColumnsGap.GAP_32
    };
  }

  render() {
    return (
      <this.afElement
        class={{
          'digi-layout-columns': true,
          ...this.cssModifiers
        }}>
        <slot></slot>
      </this.afElement>
    );
  }
}
