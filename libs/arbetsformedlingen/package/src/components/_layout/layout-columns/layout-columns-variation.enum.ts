export enum LayoutColumnsVariation {
  FOUR = 'four',
  THREE = 'three',
  TWO = 'two',
  ONE = 'one',
  NONE = 'none'
}
