import {
  Component,
  h,
  Host,
  Prop,
  Element,
  EventEmitter,
  Event
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { DialogHeadingLevel } from './dialog-heading-level.enum';
import { DialogVariation } from './dialog-variation.enum';
import { DialogSize } from './dialog-size.enum';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonSize, ButtonVariation } from '../../../enums';

/**
 * @slot default - kan innehålla vad som helst
 * @slot over-heading - Innehåll som hamnar över den inbyggda rubriken
 * @enums DialogHeadingLevel - dialog-heading-level.enum.ts
 * @enums DialogSize - dialog-size.enum.ts
 * @enums DialogVariation - dialog-variation.enum.ts (beta)
 * @swedishName Modal
 */
@Component({
  tag: 'digi-dialog',
  styleUrl: 'dialog.scss',
  shadow: true,
  scoped: false
})
export class Dialog {
  @Element() hostElement: HTMLStencilElement;

  // Replace with prop and enums!
  private _host;
  private _closeButton;
  private _dialogHeading;
  private _wrapper: HTMLElement;
  private _focusableElementsSelectors =
    'a, input, select, textarea, button, button:not(hidden), iframe, object, [tabindex="0"]';

  private focusableElements = [];
  private fallbackElement: Element | null;
  private firstFocusableEl;
  private lastFocusableEl;
  private closeButtonEl;
  private _primaryButton: HTMLElement;
  private _secondaryButton: HTMLElement;
  private _buttonContainer: HTMLElement;
  private buttonContainerObserver: ResizeObserver;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-dialog');
  /**
   * Sätt variation på modalen. OBS! Sekundär variant är endast en beta.
   * @en Set variation to modal. Secondary is only in beta.
   */
  @Prop() afVariation: DialogVariation = DialogVariation.PRIMARY;
  /**
   * Sätt storlek på modalen
   * small = max-width: 450px,
   * medium = max-width: 675px (standard),
   * large = max-width: 900px
   * @en Set size of modal window.
   */
  @Prop({ mutable: true }) afSize: DialogSize = DialogSize.MEDIUM;
  /**
   * Sätt till true om modalen ska visas
   * @en Set to true to activate modal window.
   */
  @Prop({ mutable: true }) afShowDialog: boolean;
  /**
   * Visa stäng-knapp
   * @en Show close button
   */
  @Prop() afCloseButtonText = 'Stäng';
  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: DialogHeadingLevel = DialogHeadingLevel.H2;
  /**
   * Text till rubrik
   * @en The heading text
   */
  @Prop() afHeading: string;
  /**
   * Text på primär knapp
   * @en The primary button text
   */
  @Prop() afPrimaryButtonText: string;
  /**
   * Text på sekundär knapp
   * @en The secondary button text
   */
  @Prop() afSecondaryButtonText: string;
  /**
   * Om man vill ha annat element än sista aktiva elementet innan modalen öppnades
   * @en Use if you want to choose which element gets focused when modal is closed
   */
  @Prop() afFallbackElementSelector: string;
  /**
   * Primära knappens 'onclick'-event
   * @en The primary buttons 'onclick' event
   */
  @Event() afPrimaryButtonClick: EventEmitter<MouseEvent>;
  /**
   * Sekundära knappens 'onclick'-event
   * @en The secondary buttons 'onclick' event
   */
  @Event() afSecondaryButtonClick: EventEmitter<MouseEvent>;
  /**
   * Event när modalen stängs
   * @en Event fired when modal is closed
   */
  @Event() afOnClose: EventEmitter<MouseEvent>;

  primaryClickHandler(e) {
    this.afPrimaryButtonClick.emit(e);
  }
  secondaryClickHandler(e) {
    this.afSecondaryButtonClick.emit(e);
  }

  escHandler(e) {
    if (this._closeButton) {
      this.closeDialog(e);
    }
  }

  shiftHandler(e) {
    let el;

    if (document.activeElement.tagName === 'DIGI-DIALOG') {
      el = document.activeElement.shadowRoot.activeElement;
    } else {
      el = document.activeElement;
    }

    if (this.shouldUseFocusTrap) {
      if (el === this._dialogHeading && !!this.closeButtonEl) {
        e.detail.preventDefault();
        this.closeButtonEl.focus();
      } else if (el === this.firstFocusableEl && !!this.closeButtonEl) {
        e.detail.preventDefault();
        this.closeButtonEl.focus();
      } else if (el === this._dialogHeading && !this.closeButtonEl) {
        e.detail.preventDefault();
        this.lastFocusableEl.focus();
      } else if (el === this.firstFocusableEl && !this.closeButtonEl) {
        e.detail.preventDefault();
        this.lastFocusableEl.focus();
      } else if (el === this.closeButtonEl) {
        e.detail.preventDefault();
        this.lastFocusableEl.focus();
      }
    } else {
      e.detail.preventDefault();
    }
  }

  tabHandler(e) {
    let el;
    if (document.activeElement.tagName === 'DIGI-DIALOG') {
      el = document.activeElement.shadowRoot.activeElement;
    } else {
      el = document.activeElement;
    }

    if (this.shouldUseFocusTrap) {
      if (el === this.lastFocusableEl && !!this.closeButtonEl) {
        e.detail.preventDefault();
        this.closeButtonEl.focus();
      } else if (el === this.closeButtonEl || el === this.lastFocusableEl) {
        e.detail.preventDefault();
        this.firstFocusableEl.focus();
      }
    } else {
      e.detail.preventDefault();
    }
  }

  onActive() {
    document.body.style.overflow = 'hidden';

    if (!this.afCloseButtonText) {
      this._wrapper.style.paddingTop = '25px';
    }
    if (this._dialogHeading) {
      this._dialogHeading.style.marginTop = '0';
    }

    this.onResize();
    setTimeout(() => {
      this.setFallback();
      this.getFocusableItems();
      this.setFocus();
    }, 50);
  }

  buttonWidthMaxContent() {
    if (this._primaryButton) {
      this._primaryButton.style.width = 'max-content';
      this._primaryButton.querySelector('button').style.width = 'max-content';
    }
    if (this._secondaryButton) {
      this._secondaryButton.style.width = 'max-content';
      this._secondaryButton.querySelector('button').style.width = 'max-content';
    }
  }
  buttonWidth100Percent() {
    if (this._primaryButton) {
      this._primaryButton.style.width = '100%';
      this._primaryButton.querySelector('button').style.width = '100%';
    }
    if (this._secondaryButton) {
      this._secondaryButton.style.width = '100%';
      this._secondaryButton.querySelector('button').style.width = '100%';
    }
  }

  onResize() {
    this.buttonContainerObserver = new ResizeObserver(() => {
      if (this._buttonContainer.clientWidth <= 313) {
        this.buttonWidth100Percent();
      } else {
        this.buttonWidthMaxContent();
      }
    });

    this.buttonContainerObserver.observe(this._buttonContainer);
  }

  onInactive() {
    const el = this.fallbackElement as HTMLElement;

    if (el) {
      el.focus();
    }
    this.buttonContainerObserver.disconnect();
    document.body.style.overflow = '';
  }

  closeDialog(e) {
    this.afShowDialog = false;
    this.afOnClose.emit(e);
  }

  setFallback() {
    if (
      !!this.afFallbackElementSelector &&
      !!document.querySelector(this.afFallbackElementSelector)
    ) {
      this.fallbackElement = document.querySelector(
        this.afFallbackElementSelector
      );

      if (this.fallbackElement instanceof HTMLHeadingElement) {
        this.fallbackElement.setAttribute('tabindex', '-1');
      }
    } else {
      this.fallbackElement = document.activeElement;
    }
  }

  setFocus() {
    if (this.afShowDialog) {
      let el: HTMLElement;

      if (this.afHeading) {
        el = this._dialogHeading;
        this._dialogHeading.style.outline = 'none';
      } else if (this.firstFocusableEl) {
        el = this.firstFocusableEl;

        if (!el.getAttribute('tabindex')) {
          el.setAttribute('tabindex', '0');
        }
      } else if (this.closeButtonEl) {
        el = this.closeButtonEl;
      } else {
        el = this._wrapper;
        el.setAttribute('tabindex', '0');
      }

      if (el) {
        el.focus();
      }
    }
  }

  getFocusableItems() {
    let allElements;

    const slotElements = this._host.querySelectorAll(
      this._focusableElementsSelectors
    );
    const defaultElements = this._wrapper.querySelectorAll(
      this._focusableElementsSelectors
    );

    if (this._closeButton) {
      this.closeButtonEl = this._closeButton.children[0] as HTMLElement;

      const defaultElementsWithoutCloseButton =
        Array.prototype.slice.call(defaultElements);

      defaultElementsWithoutCloseButton.shift();

      allElements = Array.from(slotElements).concat(
        Array.from(defaultElementsWithoutCloseButton)
      );
    } else {
      allElements = Array.from(slotElements).concat(
        Array.from(defaultElements)
      );
    }

    this.focusableElements = allElements;

    if (this.focusableElements.length > 0) {
      this.firstFocusableEl = this.focusableElements[0] as HTMLElement;
      this.lastFocusableEl = this.focusableElements[
        this.focusableElements.length - 1
      ] as HTMLElement;
    }
  }

  componentDidLoad() {
    if (this.afShowDialog) {
      this.onActive();
    }
  }

  componentDidUpdate() {
    if (this.afShowDialog) {
      this.onActive();
    } else if (!this.afShowDialog && this.fallbackElement) {
      this.onInactive();
    }
  }

  get shouldUseFocusTrap() {
    return this.afShowDialog && this.focusableElements.length > 0;
  }

  get cssModifiers() {
    return {
      'digi-dialog--primary': this.afVariation == DialogVariation.PRIMARY,
      'digi-dialog--secondary': this.afVariation == DialogVariation.SECONDARY,
      'digi-dialog--small': this.afSize == DialogSize.SMALL,
      'digi-dialog--medium': this.afSize == DialogSize.MEDIUM,
      'digi-dialog--large': this.afSize == DialogSize.LARGE
    };
  }

  render() {
    return (
      <Host
        ref={(el) => {
          this._host = el;
        }}>
        {this.afShowDialog && (
          <div class={{ 'digi-dialog': true, ...this.cssModifiers }}>
            <digi-util-keydown-handler
              onAfOnEsc={(e) => this.escHandler(e)}
              onAfOnTab={(e) => this.tabHandler(e)}
              onAfOnShiftTab={(e) => this.shiftHandler(e)}
              class="digi-dialog__keydown-handler">
              <div
                ref={(el) => {
                  this._wrapper = el;
                }}
                role="dialog"
                aria-modal="true"
                class="digi-dialog__content">
                <digi-typography>
                  {this.afCloseButtonText && (
                    <digi-button
                      ref={(el) => (this._closeButton = el)}
                      class="digi-dialog__close-button"
                      afAriaLabel={this.afCloseButtonText}
                      afSize={ButtonSize.MEDIUM}
                      af-variation="function"
                      onAfOnClick={(e) => this.closeDialog(e)}>
                      {this.afCloseButtonText}
                      <digi-icon-x
                        class="digi-dialog__close-icon"
                        slot="icon-secondary"
                      />
                    </digi-button>
                  )}

                  <slot name="over-heading"></slot>

                  {this.afHeadingLevel && this.afHeading && (
                    <this.afHeadingLevel
                      tabindex="-1"
                      ref={(el) => (this._dialogHeading = el)}>
                      {this.afHeading}
                    </this.afHeadingLevel>
                  )}

                  <slot></slot>

                  <div
                    ref={(el) => (this._buttonContainer = el)}
                    class="digi-dialog__button-container">
                    {this.afSecondaryButtonText && (
                      <digi-button
                        ref={(el) => (this._secondaryButton = el)}
                        afVariation={ButtonVariation.SECONDARY}
                        onAfOnClick={(e) => this.secondaryClickHandler(e)}>
                        {this.afSecondaryButtonText}
                      </digi-button>
                    )}
                    {this.afPrimaryButtonText && (
                      <digi-button
                        ref={(el) => (this._primaryButton = el)}
                        afVariation={ButtonVariation.PRIMARY}
                        onAfOnClick={(e) => this.primaryClickHandler(e)}>
                        {this.afPrimaryButtonText}
                      </digi-button>
                    )}
                  </div>
                </digi-typography>
              </div>
            </digi-util-keydown-handler>
          </div>
        )}
      </Host>
    );
  }
}
