export enum DialogVariation {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
