export enum ToolsFeedbackBannerType {
  FULLWIDTH = 'fullwidth',
  GRID = 'grid'
}
