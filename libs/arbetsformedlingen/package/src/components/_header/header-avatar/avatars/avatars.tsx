/* eslint-disable max-len */

import { h } from '@stencil/core';

export const avatarPlaceholder = {
  'avatar-logged-in': {
    AvatarPlaceholderComponent(
      {
        afSvgAriaHidden,
        afSvgAriaLabelledby
      }: { afSvgAriaHidden: boolean; afSvgAriaLabelledby: string },
      children: HTMLElement
    ) {
      return (
        <svg
          width="32px"
          height="32px"
          viewBox="0 0 32 32"
          xmlns="http://www.w3.org/2000/svg"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
          aria-labelledby={afSvgAriaLabelledby}>
          {children}
          <defs>
            <polygon id="path-avatar-1" points="0 0 32 0 32 32 0 32"></polygon>
            <polygon
              id="path-avatar-3"
              points="0 0 22.7664777 0 22.7664777 11.9523581 0 11.9523581"></polygon>
          </defs>
          <g
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd">
            <g id="Artboard" transform="translate(-281, -653)">
              <g id="Group-9" transform="translate(281, 653)">
                <g id="Group-3">
                  <mask id="mask-avatar-2" fill="white">
                    <use xlinkHref="#path-avatar-1"></use>
                  </mask>
                  <g id="Clip-2"></g>
                  <path
                    d="M32.0002117,15.9999294 C32.0002117,24.8361768 24.8366065,32.0002822 16.0001058,32.0002822 C7.16360517,32.0002822 0,24.8361768 0,15.9999294 C0,7.16368206 7.16360517,-0.000423327086 16.0001058,-0.000423327086 C24.8366065,-0.000423327086 32.0002117,7.16368206 32.0002117,15.9999294"
                    id="Fill-1"
                    fill="#EDEDED"
                    mask="url(#mask-avatar-2)"></path>
                </g>
                <path
                  d="M9.6578493,11.5102635 C9.6578493,15.2821078 12.5704231,18.3399405 16.1624564,18.3399405 C19.7551953,18.3399405 22.6677691,15.2821078 22.6677691,11.5102635 C22.6677691,7.73841914 19.7551953,4.68058648 16.1624564,4.68058648 C12.5704231,4.68058648 9.6578493,7.73841914 9.6578493,11.5102635"
                  id="Fill-4"
                  fill="#00005A"></path>
                <g id="Group-8" transform="translate(4.7794, 20.0476)">
                  <mask id="mask-avatar-4" fill="white">
                    <use xlinkHref="#path-avatar-3"></use>
                  </mask>
                  <g id="Clip-7"></g>
                  <path
                    d="M17.8881988,0 L15.1125046,0 C12.7911945,1.11758351 10.0641844,1.16062176 7.65467867,0 L4.87827895,0 C2.18372481,0 0,2.29372726 0,5.12225774 L0,7.35460258 C2.88787905,10.197244 6.84821746,11.9526403 11.220606,11.9526403 C15.7602136,11.9526403 19.8546094,10.0589571 22.7664777,7.02229082 L22.7664777,5.12225774 C22.7664777,2.29372726 20.5827529,0 17.8881988,0"
                    id="Fill-6"
                    fill="#00005A"
                    mask="url(#mask-avatar-4)"></path>
                </g>
              </g>
            </g>
          </g>
        </svg>
      );
    }
  },
  'avatar-logged-out': {
    AvatarPlaceholderComponent(
      {
        afSvgAriaHidden,
        afSvgAriaLabelledby
      }: { afSvgAriaHidden: boolean; afSvgAriaLabelledby: string },
      children: HTMLElement
    ) {
      return (
        <svg
          width="32px"
          height="32px"
          viewBox="0 0 32 32"
          xmlns="http://www.w3.org/2000/svg"
          aria-hidden={afSvgAriaHidden ? 'true' : 'false'}
          aria-labelledby={afSvgAriaLabelledby}>
          {children}
          <defs>
            <polygon id="path-avatar-1" points="0 0 32 0 32 32 0 32"></polygon>
            <polygon
              id="path-avatar-3"
              points="0 0 22.7664777 0 22.7664777 11.9523581 0 11.9523581"></polygon>
          </defs>
          <g
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd">
            <g id="Artboard" transform="translate(-491, -742)">
              <g id="Group-9" transform="translate(491, 742)">
                <g id="Group-3">
                  <mask id="mask-avatar-2" fill="white">
                    <use xlinkHref="#path-avatar-1"></use>
                  </mask>
                  <g id="Clip-2"></g>
                </g>
                <path
                  d="M9.6578493,11.5102635 C9.6578493,15.2821078 12.5704231,18.3399405 16.1624564,18.3399405 C19.7551953,18.3399405 22.6677691,15.2821078 22.6677691,11.5102635 C22.6677691,7.73841914 19.7551953,4.68058648 16.1624564,4.68058648 C12.5704231,4.68058648 9.6578493,7.73841914 9.6578493,11.5102635"
                  id="Fill-4"
                  fill="#757575"></path>
                <g id="Group-8" transform="translate(4.7794, 20.0476)">
                  <mask id="mask-avatar-4" fill="white">
                    <use xlinkHref="#path-avatar-3"></use>
                  </mask>
                  <g id="Clip-7"></g>
                  <path
                    d="M17.8881988,0 L15.1125046,0 C12.7911945,1.11758351 10.0641844,1.16062176 7.65467867,0 L4.87827895,0 C2.18372481,0 0,2.29372726 0,5.12225774 L0,7.35460258 C2.88787905,10.197244 6.84821746,11.9526403 11.220606,11.9526403 C15.7602136,11.9526403 19.8546094,10.0589571 22.7664777,7.02229082 L22.7664777,5.12225774 C22.7664777,2.29372726 20.5827529,0 17.8881988,0"
                    id="Fill-6"
                    fill="#757575"
                    mask="url(#mask-avatar-4)"></path>
                </g>
              </g>
            </g>
          </g>
        </svg>
      );
    }
  }
} as const;

export type AvatarPlaceholderName = keyof typeof avatarPlaceholder;
