import { avatarPlaceholder } from './avatars';

export const headerAvatarNamesAsArray = Object.keys(avatarPlaceholder);
