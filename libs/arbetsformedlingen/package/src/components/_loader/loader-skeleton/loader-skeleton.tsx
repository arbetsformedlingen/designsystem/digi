import { Component, h, Host, Prop, Element, State } from '@stencil/core';
import { LoaderSkeletonVariation } from './loader-skeleton-variation.enum';
import type { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - loader-skeleton-myEnum.enum.ts
 *
 * @swedishName Platshållare
 */
@Component({
  tag: 'digi-loader-skeleton',
  styleUrl: 'loader-skeleton.scss',
  scoped: true
})
export class LoaderSkeleton {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Sätter antalet rader av skeleton loader
   * @en sets the amount of lines of skeleton loader
   */
  @Prop() afCount = 4;

  /**
   * Sätter varianten
   * @en Sets the variation.
   */
  @Prop() afVariation: `${LoaderSkeletonVariation}` =
    LoaderSkeletonVariation.LINE;

  @State() isLoading = true;

  get cssModifiers() {
    return {
      'digi-loader-skeleton--line':
        this.afVariation === LoaderSkeletonVariation.LINE,
      'digi-loader-skeleton--header':
        this.afVariation === LoaderSkeletonVariation.HEADER,
      'digi-loader-skeleton--text':
        this.afVariation === LoaderSkeletonVariation.TEXT,
      'digi-loader-skeleton--circle':
        this.afVariation === LoaderSkeletonVariation.CIRCLE,
      'digi-loader-skeleton--section':
        this.afVariation === LoaderSkeletonVariation.SECTION,
      'digi-loader-skeleton--rounded':
        this.afVariation === LoaderSkeletonVariation.ROUNDED
    };
  }

  render() {
    return (
      <Host>
        <div
          aria-busy="true"
          aria-hidden="true"
          class={{
            'digi-loader-skeleton': true,
            ...this.cssModifiers
          }}>
          {this.afVariation === LoaderSkeletonVariation.HEADER && (
            <div class="digi-loader-skeleton__item digi-loader-skeleton__item--header"></div>
          )}
          {this.afVariation === LoaderSkeletonVariation.LINE && (
            <div class="digi-loader-skeleton__item digi-loader-skeleton__item--line"></div>
          )}

          {this.afVariation === LoaderSkeletonVariation.TEXT &&
            Array(this.afCount)
              .fill('')
              .map(() => {
                return (
                  <div class="digi-loader-skeleton__item digi-loader-skeleton__item--text"></div>
                );
              })}
          {this.afVariation === LoaderSkeletonVariation.SECTION && [
            <div class="digi-loader-skeleton__item digi-loader-skeleton__item--section-header"></div>,
            ...Array(this.afCount)
              .fill('')
              .map(() => (
                <div class="digi-loader-skeleton__item digi-loader-skeleton__item--section-text"></div>
              ))
          ]}

          {this.afVariation === LoaderSkeletonVariation.CIRCLE && (
            <div class="digi-loader-skeleton__item digi-loader-skeleton__item--circle"></div>
          )}
          {this.afVariation === LoaderSkeletonVariation.ROUNDED && (
            <div class="digi-loader-skeleton__item digi-loader-skeleton__item--rounded"></div>
          )}
        </div>
      </Host>
    );
  }
}
