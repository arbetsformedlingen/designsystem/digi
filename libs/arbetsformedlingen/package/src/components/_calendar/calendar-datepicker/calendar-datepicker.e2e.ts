import { E2EPage, newE2EPage } from '@stencil/core/testing';

/*

digi-calendar-datepicker

* afOnDateChange
    - vid val av datum via kalendern så emittas en Date[] med valt datum
    - Tar du bort datum via kalendern så emittas en tom Date[]
    - skriver du i ett fungerande datum exempelvis "20250101" i inputen i komponenten så emittas datumet i en Date[]
    - Tar du bort ett datum via inputen emittas en String[] med en tom sträng i sig // lösning vi kan behöva se över
    - Skriver du ett felaktigt datum i inputen, exempelvis "abc" emittas en tom array och ett felmeddelande visas

 */

describe('calendar-datepicker', () => {

  let page: E2EPage

  beforeAll(() => {
    console.warn = () => null;
  });

  beforeEach(async () => {
    page = await newE2EPage({
      html: `<digi-calendar-datepicker></digi-calendar-datepicker>`
    });
  })

  it ('should show calendar', async () => {
    const el = await page.find('digi-calendar-datepicker');

    const calendar = await page.find('digi-calendar');
    expect(await calendar.getProperty('afActive')).toBeFalsy();

    const calendarButton = await el.find('button');
    await calendarButton.click();
    await page.waitForChanges();

    expect(await calendar.getProperty('afActive')).toBeTruthy();
  })

  it('should not skip a month (MR-1239)', async () => {
    /**
     * Problem vid test
     * - Fungerar inte att sätta af-init-selected-month som prop
     * - Fungerar inte att sätta af-init-selected-date som prop
     * - Renderingen uppdateras inte när prop `afSelectedDates` uppdateras
     */
    const date = '2022-01-31';

    page = await newE2EPage({
      html: `
        <digi-calendar></digi-calendar>
        <script>
          document.querySelector('digi-calendar').afInitSelectedDate = new Date("${date}")
        </script>
      `
    });
    

    const getMonthName = (monthIdx, lang = 'default') => {
      const date = new Date(2022, monthIdx, 1);
      const month = date.toLocaleString(lang, { month: 'long' });
      return month;
    };

    const detectedLanguage = await page.evaluate(() => navigator.languages[0]); // based on browser language

    // localized date names
    const january = getMonthName(0, detectedLanguage).toLowerCase();
    const february = getMonthName(1, detectedLanguage).toLowerCase();
    const feb = february.slice(0, 3);

    const getCalendarTitle = () => page.find('.digi-calendar__month');
    expect((await getCalendarTitle()).textContent.toLowerCase()).toBe(
      `${january} 2022`
    );

    const nextButton = await page.find(
      '.digi-calendar__header--month-step button:last-child'
    );
    expect(nextButton.textContent.toLowerCase()).toBe(feb);

    await nextButton.click();

    expect((await getCalendarTitle()).textContent.toLowerCase()).toBe(
      `${february} 2022`
    );
  }); 
  
  it('should dispatch expected event values', async () => {
  
    const input = await page.find('input');
    const changeEvent = await page.spyOnEvent('afOnDateChange');
  
    // invalid date
    await input.press('a');
    await input.press('Enter');
    expect(changeEvent).toHaveReceivedEventDetail([]);
  
    // no date or value
    await input.press('Backspace');
    await input.press('Enter');
    expect(changeEvent).toHaveReceivedEventDetail(['']);
  
    // correct singular date
    await input.type('2022-01-31');
    await input.press('Enter');
    expect(
      (changeEvent.lastEvent.detail[0] as string).startsWith('2022-01-31')
    ).toBe(true);
  })
})

