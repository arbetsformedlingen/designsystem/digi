import { newE2EPage } from '@stencil/core/testing';
import { expect, it } from '@jest/globals';

/*
digi-calendar

* Komponenten kan ta emot ett datum genom propertyn selectedDate av typen Date
*   - När selectedDate sätts så sätts även selectedDates om datumet är valid
* Komponenten kan ta emot flera datum genom propertyn selectedDates av typen Date[]

* Kalendern emittar alltid en array av datum via afOnDateSelectedChange eller en tom array
...
*/
describe('calendar', () => {

  beforeAll(() => {
    console.warn = () => null;
  });

  it('should render', async () => {
    const page = await newE2EPage({
      html: `
        <digi-calendar
          af-active="true"			 
        ></digi-calendar>
      `
    });
    expect(await page.evaluate(() => document.body.innerHTML)).toContain(
      'digi-calendar'
    );
  });
});
