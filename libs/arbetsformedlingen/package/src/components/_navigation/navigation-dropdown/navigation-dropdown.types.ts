export interface DropdownItem {
  id: string | number;
  title: string;
}

export type DropdownRole = 'menu' | 'listbox';

export type HTMLDir = 'rtl' | 'ltr' | 'auto';

export type ElementRef = 'host' | 'item'

export type RelativePosition = 'center-bottom' | 'left-bottom' | 'right-bottom';

export type DropdownPosition = RelativePosition;

export interface DropdownEventItem {
  item: DropdownItem;
  idx: number;
}

export interface DropdownAfRefEvent {
  id: ElementRef;
  ref: HTMLElement;
  payload?: DropdownItem;
}
