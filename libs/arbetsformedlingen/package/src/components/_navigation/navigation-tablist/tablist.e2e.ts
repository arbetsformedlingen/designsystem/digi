import { E2EPage, newE2EPage } from '@stencil/core/testing';

describe('tablist', () => {
  let page: E2EPage;

  beforeAll(() => {
    console.warn = () => null;
  });

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <digi-tablist 
          af-tabs='[
        {
          "id": "om-yrket",
          "title": "Om Yrket"
        },
        {
          "id": "arbetsuppgifter",
          "title": "Arbetsuppgifter"
        },
        {
          "id": "formagor",
          "title": "Förmågor"
        },
        {
          "id": "lon",
          "title": "Lön"
        },
        {
          "id": "formaner",
          "title": "Förmåner"
        },
        {
          "id": "ovrigt",
          "title": "Övrigt"
        }
        ]'
          af-aria-label="Detaljer om Yrke"
        >

          <digi-tablist-panel tab="om-yrket">
            <p>Innehåll som visas när flik "Om Yrket" är aktiv</p>
          </digi-tablist-panel>


          <digi-tablist-panel tab="arbetsuppgifter">
            <p>Innehåll som visas när flik "Arbetsuppgifter" är aktiv</p>
          </digi-tablist-panel>


          <digi-tablist-panel tab="formagor">
            <p>Innehåll som visas när flik "Förmågor" är aktiv</p>
          </digi-tablist-panel>


          <digi-tablist-panel tab="lon">
            <p>Innehåll som visas när flik "Lön" är aktiv</p>
          </digi-tablist-panel>


          <digi-tablist-panel tab="formaner">
            <p>Innehåll som visas när flik "Förmåner" är aktiv</p>
          </digi-tablist-panel>


          <digi-tablist-panel tab="ovrigt">
            <p>Innehåll som visas när flik "Övrigt" är aktiv</p>
          </digi-tablist-panel>

        </digi-tablist>
      `
    });
  });

  it('should display first tab content and change on click', async () => {
    const firstPanel = await page.find('#tabpanel-om-yrket')
    const anotherPanel = await page.find('#tabpanel-lon')
    expect(await firstPanel.isVisible()).toBe(true);
    expect(await anotherPanel.isVisible()).toBe(false)
    
    const anotherTab = await page.find('#tab-lon');
    await anotherTab.click()

    expect(await firstPanel.isVisible()).toBe(false);
    expect(await anotherPanel.isVisible()).toBe(true);
    
  });

  it('should set initial tab to not first', async () => {
    page = await newE2EPage({
      html: `
        <digi-tablist 
          af-initial-tab="arbetsuppgifter"
          af-tabs='[
        {
          "id": "om-yrket",
          "title": "Om Yrket"
        },
        {
          "id": "arbetsuppgifter",
          "title": "Arbetsuppgifter"
        }
        ]'
          af-aria-label="Detaljer om Yrke"
        >

          <digi-tablist-panel tab="om-yrket">
            <p>Innehåll som visas när flik "Om Yrket" är aktiv</p>
          </digi-tablist-panel>


          <digi-tablist-panel tab="arbetsuppgifter">
            <p>Innehåll som visas när flik "Arbetsuppgifter" är aktiv</p>
          </digi-tablist-panel>

        </digi-tablist>
      `
    });
    const firstPanel = await page.find('#tabpanel-om-yrket');
    const anotherPanel = await page.find('#tabpanel-arbetsuppgifter');
    expect(await firstPanel.isVisible()).toBe(false);
    expect(await anotherPanel.isVisible()).toBe(true);
  });

  it('should set tab with props', async () => {
    const firstPanel = await page.find('#tabpanel-om-yrket');
    const anotherPanel = await page.find('#tabpanel-lon');
    expect(await firstPanel.isVisible()).toBe(true);
    expect(await anotherPanel.isVisible()).toBe(false);

    const tablist = await page.find('digi-tablist')
    tablist.setProperty('afActiveTab', 'lon') // set active tab to "Lön"

    await page.waitForChanges()

    expect(await firstPanel.isVisible()).toBe(false);
    expect(await anotherPanel.isVisible()).toBe(true);
  });

});
