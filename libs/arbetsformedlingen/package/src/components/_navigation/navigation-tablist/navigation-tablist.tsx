import {
  Component,
  h,
  Host,
  Prop,
  State,
  Watch,
  Listen,
  Event,
  EventEmitter
} from '@stencil/core';
import { Direction, OverflowMode, TabItem } from './navigation-tablist.types';

/**
 * @slot default - <digi-tablist-panel /> för varje flik
 * @swedishName Flikfält
 */
@Component({
  tag: 'digi-tablist',
  styleUrl: 'navigation-tablist.scss',
  scoped: true
})
export class NavigationTablist {
  private tablistRef: HTMLElement;
  private tabRefs: {
    [key: string]: HTMLElement;
  } = {};
  private panelsRef: HTMLElement;

  private resizeObserver: ResizeObserver;

  @State() activeTab = 0;
  @State() selectedTab = 0;

  @State() windowTab = 0;
  @State() tabsPerWindow: number;

  @State() tabs: TabItem[] = [];

  @State() listOverflows = false;
  @State() showPrev = false;
  @State() showNext = false;

  /**
   * Bestämmer flikar
   * @en Set tabs
   */
  @Prop() afTabs: TabItem[] | string;

  /**
   * Bestämmer beteende då flikar överflödar
   * * @en Set overflow behaviour
   */
  @Prop() afOverflowMode: OverflowMode = OverflowMode.Controls;

  /**
   * Bestämmer aria-label på listan av flikar
   * @en Set aria-labelledby for tablist
   */
  @Prop() afAriaLabelledby: string;

  /**
   * Bestämmer aria-label på listan av flikar
   * @en Set aria-label for tablist
   */
  @Prop() afAriaLabel: string;

  /**
   * Flik som är aktiv vid start, använder flikens id
   * @en Active tab at start, using tab id
   */
  @Prop() afInitialTab?: string;

  /**
   * Flik som är aktiv, använder flikens id
   * @en Active tab, using tab id
   */
  @Prop() afActiveTab?: string;

  /**
   * Skickas när en flik aktiveras, dvs vid klick eller med space/enter‐tangentklick på markerad flik. Fliken som aktiveras finns under event.detail
   * @en Emitted when a tab is activated
   */
  @Event() afTabChange: EventEmitter<TabItem>;

  /**
   * Skickas när en flik fokusmarkeras samt vid klick. Fliken som markeras finns under event.detail
   * @en Emitted when a tab is focus‐marked
   */
  @Event() afTabFocusChange: EventEmitter<TabItem>;

  @Listen('keydown')
  handleKey(e: KeyboardEvent) {
    if (this.focusCaptured()) {
      let captureKey = true;
      switch (e.key) {
        case ' ':
        case 'Enter':
          return this.setActiveTabAndFocus(this.selectedTab);
        case 'ArrowLeft':
          if (e.shiftKey) {
            this.handleTabControl(Direction.Previous);
          } else {
            this.focusTab(this.selectedTab - 1);
          }
          break;
        case 'ArrowRight':
          if (e.shiftKey) {
            this.handleTabControl(Direction.Next);
          } else {
            this.focusTab(this.selectedTab + 1);
          }
          break;
        case 'Home':
          if (e.shiftKey) {
            return this.focusTab(0);
          } else {
            return this.moveWindow(0);
          }
        case 'End':
          if (e.shiftKey) {
            return this.focusTab(this.tabs.length - 1);
          } else {
            return this.moveWindow(this.tabs.length - 1);
          }
        default:
          captureKey = false;
      }
      if (captureKey) {
        e.preventDefault();
      }
    }
  }

  @Watch('activeTab')
  syncPanels() {
    // side-effect
    if (this.panelsRef != null) {
      const panels = this.panelsRef.querySelectorAll('digi-tablist-panel');
      for (let i = 0; i < panels.length; i++) {
        const panel = panels[i] as HTMLDigiTablistPanelElement;
        const showPanel = panel.tab === this.tabs[this.activeTab].id;
        panel.isVisible = showPanel;
      }
    }
  }

  @Watch('afActiveTab')
  handleTabPropChange(tabId) {
    this.setActiveTabById(tabId);
  }

  @Watch('windowTab')
  @Watch('listOverflows')
  syncControls() {
    const tabGroup = Math.floor(this.windowTab / this.tabsPerWindow);
    const numberOfGroups = Math.ceil(this.tabs.length / this.tabsPerWindow);
    this.showPrev = tabGroup > 0;
    this.showNext = tabGroup < numberOfGroups - 1;
  }

  @Watch('windowTab')
  syncWindow(tabIdx: number) {

    const tab = this.tabs[tabIdx];
    const tabRef = this.tabRefs[tab.id];

    const listBox = this.tablistRef.getBoundingClientRect();
    const tabBox = tabRef.getBoundingClientRect();

    const centeredOffset =
      tabRef.offsetLeft - listBox.width / 2 + tabBox.width / 2;
    const maxLeft = this.tablistRef.scrollWidth - listBox.width;
    const left = Math.max(Math.min(centeredOffset, maxLeft), 0);

    this.tablistRef.scrollTo({
      left,
      behavior: 'smooth'
    });
  }

  @Watch('selectedTab')
  syncFocus(tabIdx: number) {
    const tabRef = this.tabRefs[this.tabs[tabIdx].id];
    tabRef.focus({ preventScroll: true });

    this.windowTab = tabIdx;
  }

  @Watch('afTabs')
  setupTabs(tabs: TabItem[] | string) {
    if (typeof tabs === 'string') {
      this.tabs = JSON.parse(tabs);
    } else if (Array.isArray(tabs)) {
      this.tabs = tabs;
    } else {
      throw new Error('Invalid format or no tabs supplied');
    }
  }

  setActiveTabById(id: string) {
    const tabIdx = this.tabs.findIndex((tab) => tab.id === id);
    if (tabIdx > -1) {
      this.activeTab = tabIdx;
    }
  }

  setupInitialTab() {
    if (this.afActiveTab != null) {
      return this.setActiveTabById(this.afActiveTab);
    } else if (this.afInitialTab != null) {
      return this.setActiveTabById(this.afInitialTab);
    }
  }

  setupResizeHandler() {
    this.resizeObserver = new ResizeObserver(() => this.handleResize());
    this.resizeObserver.observe(this.tablistRef)
  }

  handleTabChange(tab: TabItem) {
    const idx = this.tabs.findIndex(({ id }) => id === tab.id);
    this.setActiveTabAndFocus(idx);

    this.afTabChange.emit(tab);
  }

  handleResize() {
    const tlRect = this.tablistRef.getBoundingClientRect();
    const contentWidth = this.tablistRef.scrollWidth;
    this.tabsPerWindow = Math.max(
      Math.floor(
        Math.min(
          (tlRect.width / contentWidth) * this.tabs.length,
          this.tabs.length
        )
      ),
      1
    );
    this.listOverflows =
      this.tablistRef.scrollWidth > this.tablistRef.clientWidth;
  }

  handleTabControl(direction: Direction) {
    this.moveWindow(this.windowTab + direction * this.tabsPerWindow);
  }

  setActiveTabAndFocus(tabIdx: number) {
    this.activeTab = tabIdx;
    this.focusTab(tabIdx);
  }

  focusTab(tabIdx: number) {
    this.selectedTab = this.nextTabIdx(tabIdx);
    const currentTab = this.tabs[this.selectedTab];
    this.afTabFocusChange.emit(currentTab);
  }

  moveWindow(tabIdx: number) {
    tabIdx = Math.max(0, Math.min(tabIdx, this.tabs.length - 1));
    this.windowTab = tabIdx;
  }

  nextTabIdx(tabIdx: number): number {
    if (tabIdx >= this.tabs.length) {
      return 0;
    } else if (tabIdx < 0) {
      return this.tabs.length - 1;
    }
    return tabIdx;
  }

  focusCaptured() {
    return this.tabs.some(
      (tab) => this.tabRefs[tab.id] === document.activeElement
    );
  }

  componentWillLoad() {
    this.setupTabs(this.afTabs);
    this.setupInitialTab();
  }
  
  componentDidLoad() {
    this.setupResizeHandler();
    this.handleResize();
    this.syncPanels();
    this.syncControls();
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'digi-tablist': true,
            'digi-tablist--overflow-scroll':
              this.afOverflowMode === OverflowMode.Scroll,
            'digi-tablist--overflow-controls':
              this.afOverflowMode === OverflowMode.Controls
          }}>
          <div
            class="control-wrapper"
            style={{
              left: '0',
              visibility: this.showPrev ? 'visible' : 'hidden'
            }}>
            <button
              tabIndex={-1}
              aria-hidden="true"
              class={{ 'tab-control-button': true }}
              onClick={() => this.handleTabControl(Direction.Previous)}>
              <digi-icon-chevron-left />
            </button>
          </div>
          <div
            role="tablist"
            ref={(ref) => (this.tablistRef = ref)}
            aria-labelledby={this.afAriaLabelledby}
            aria-label={this.afAriaLabel}>
            {this.tabs.map((tab) => {
              const focusable = tab.id === this.tabs[this.selectedTab].id;
              const isActive = tab.id === this.tabs[this.activeTab].id;
              return (
                <button
                  ref={(ref) => (this.tabRefs[tab.id] = ref)}
                  class={{
                    'digi-tab': true,
                    'is-active': isActive
                  }}
                  id={`tab-${tab.id}`}
                  role="tab"
                  tabindex={focusable ? '0' : '-1'}
                  aria-selected={isActive.toString()}
                  aria-controls={`tabpanel-${tab.id}`}
                  onClick={() => this.handleTabChange(tab)}>
                  <div class="content-wrapper">
                    <span class="title" data-text={tab.title}>
                      {tab.title}
                    </span>
                    <div class="indicator" />
                  </div>
                </button>
              );
            })}
          </div>
          <div
            class="control-wrapper"
            style={{
              right: '0',
              visibility: this.showNext ? 'visible' : 'hidden'
            }}>
            <button
              tabIndex={-1}
              aria-hidden="true"
              class={{ 'tab-control-button': true }}
              onClick={() => this.handleTabControl(Direction.Next)}>
              <digi-icon-chevron-right />
            </button>
          </div>
        </div>
        <div ref={(ref) => (this.panelsRef = ref)} role="presentation">
          <slot />
        </div>
      </Host>
    );
  }
}
