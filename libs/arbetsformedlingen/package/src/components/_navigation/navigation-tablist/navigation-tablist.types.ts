export interface TabItem {
  id: string;
  title: string;
}

export enum Direction {
  Previous = -1,
  Next = 1
}

export enum OverflowMode {
  Controls = 'controls',
  Scroll = 'scroll'
}
