import {
  Component,
  State,
  Host,
  Event,
  EventEmitter,
  Listen,
  Prop,
  Watch,
  h
} from '@stencil/core';

import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';

import {
  DropdownItem,
  HTMLDir,
  RelativePosition
} from '../navigation-dropdown/navigation-dropdown.types';
import { ContextMenuEventItem, ContextMenuItem } from './navigation-context-menu.types';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

function deserializeProp<T>(prop: string | T): T {
  if (typeof prop === 'string') {
    return JSON.parse(prop);
  }
  return prop;
}

/**
 * @enums ContextMenuHorizontalPosition - navigation-context-menu-horizontal-position.enum.ts
 * @swedishName Rullgardinsmeny
 */
@Component({
  tag: 'digi-context-menu',
  styleUrls: ['navigation-context-menu.scss']
})
export class ContextMenu {
  private buttonRef: HTMLElement;
  private itemRefs: { [key: string | number]: HTMLElement } = {};

  @Prop() afId: string = randomIdGenerator('digi-context-menu');
  @Prop() afIcon: string;
  @Prop() afTitle: string;

  @Prop() afMenuItems: string | ContextMenuItem[];
  @Prop() afDir?: HTMLDir;
  @Prop() afLang?: string;
  @Prop() afAriaLabel: string;

  /**
   * När menyalternativ är valt
   * @en When menu item is activated
   */
  @Prop() afMenuPosition?: RelativePosition = 'left-bottom';

  /**
   * När menyalternativ är valt
   * @en When menu item is activated
   */
  @Prop() afActiveItem: ContextMenuItem['id'];

  @State() items: ContextMenuItem[] = [];
  @State() visible = false;
  @State() activeItemIdx: number;

  /**
   * När menyalternativ är valt
   * @en When menu item is activated
   */
  @Event() afChangeItem: EventEmitter<ContextMenuEventItem>;

  /**
   * När fokus flyttar mellan menyalternativ
   * @en When focus change between menu items
   */
  @Event() afFocusItem: EventEmitter<ContextMenuEventItem>;

  /**
   * När menyn öppnas / stängs
   * @en When the menu opens / closes
   */
  @Event() afToggleMenu: EventEmitter<boolean>;

  /**
   * När fokus går över till ett annat element
   * @en When focus moves on to another element
   */
  @Event() afBlur: EventEmitter;

  @Listen('click', { target: 'window' })
  clickHandler(e: MouseEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-identifier`;

    if (detectClickOutside(target, selector) && this.visible) {
      this.hideMenu();
    }
  }

  @Listen('focusin', { target: 'document' })
  focusoutHandler(e: FocusEvent): void {
    const target = e.target as HTMLElement;
    const selector = `#${this.afId}-identifier`;

    if (detectFocusOutside(target, selector) && this.visible) {
      this.hideMenu();
      this.afBlur.emit();
    }
  }

  hideMenu() {
    this.visible = false;
    this.afToggleMenu.emit(this.visible);
  }

  showMenu() {
    this.visible = true;
    this.afToggleMenu.emit(this.visible);
  }

  handleToggle() {
    if (this.visible) {
      this.hideMenu();
      this.buttonRef.focus?.();
    } else {
      this.showMenu();
      const idx = this.activeItemIdx || 0;
      const item = this.items[idx];
      setTimeout(() => this.itemRefs[item.id].focus(), 25);
    }
  }

  handleChangeItem(item: ContextMenuItem, idx: number) {
    this.activeItemIdx = idx;
    this.handleToggle();
    this.afChangeItem.emit({ item, idx });
  }

  handleFocusItem(item: ContextMenuItem, idx: number) {
    this.afFocusItem.emit({ item, idx });
  }

  handleRef(ref: HTMLElement, refId: string, payload: DropdownItem) {
    if (refId === 'item') {
      this.itemRefs[payload.id] = ref;
    }
  }

  @Watch('afActiveItem')
  syncActiveItem() {
    if (this.afActiveItem != null) {
      this.activeItemIdx = this.items.findIndex(
        (item) => item.id.toString() === this.afActiveItem
      );
    }
  }

  componentWillLoad() {
    this.items = deserializeProp<ContextMenuItem[]>(this.afMenuItems);
    this.syncActiveItem();
  }

  get digiIcon() {
    if (this.afIcon != null) {
      return `digi-icon-${this.afIcon}`;
    }
  }

  render() {
    return (
      <Host id={`${this.afId}-identifier`}>
        <digi-navigation-dropdown
          afAriaLabelledby={`${this.afId}-trigger`}
          afItems={this.items as DropdownItem[]}
          afVisible={this.visible}
          afMenuPosition={this.afMenuPosition}
          afDir={this.afDir}
          afActiveItem={this.activeItemIdx}
          afLang={this.afLang}
          onAfRef={(e) =>
            this.handleRef(e.detail.ref, e.detail.id, e.detail.payload)
          }
          onAfChangeItem={(e) =>
            this.handleChangeItem(e.detail.item, e.detail.idx)
          }
          onAfFocusItem={(e) =>
            this.handleFocusItem(e.detail.item, e.detail.idx)
          }
          afRenderItem={(item, idx) => {
            const menuItem = this.items[idx];
            const Container = menuItem.type === 'link' ? 'a' : 'div';
            const containerArgs =
              menuItem.type === 'link'
                ? { href: menuItem.href, tabindex: -1 }
                : {};
            const showIcon = menuItem.type !== 'link';
            return (
              <Container class="content-wrapper" {...containerArgs}>
                {showIcon && (
                  <digi-icon-check
                    class="digi-navigation-context-menu__button-icon"
                    slot="icon"
                    style={{
                      visibility:
                        idx === this.activeItemIdx && this.visible
                          ? 'visible'
                          : 'hidden'
                    }}
                  />
                )}
                <p>{item.title}</p>
              </Container>
            );
          }}>
          <digi-button
            id={`${this.afId}-trigger`}
            afInputRef={(ref) => (this.buttonRef = ref)}
            onClick={() => this.handleToggle()}
            afVariation="function"
            afAriaControls={this.afId}
            afAriaExpanded={this.visible}
            afAriaLabel={this.afAriaLabel || null}
            class={{
              'digi-navigation-context-menu__trigger-button': true,
              'digi-navigation-context-menu--active': this.visible
            }}>
            {!!this.afIcon && <this.digiIcon slot="icon"></this.digiIcon>}
            {this.afTitle}
            <digi-icon-chevron-down
              class="digi-navigation-context-menu__toggle-icon"
              slot="icon-secondary"
            />
          </digi-button>
        </digi-navigation-dropdown>
      </Host>
    );
  }
}
