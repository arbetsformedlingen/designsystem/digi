import {
  Component,
  Element,
  Prop,
  h,
  Host,
  Event,
  EventEmitter,
  Listen,
  State,
  Watch
} from '@stencil/core';
import type { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { NavigationSidebarPosition } from './navigation-sidebar-position.enum';
import { NavigationSidebarVariation } from './navigation-sidebar-variation.enum';
import { NavigationSidebarMobilePosition } from './navigation-sidebar-mobile-position.enum';
import { NavigationSidebarMobileVariation } from './navigation-sidebar-mobile-variation.enum';
import { NavigationSidebarHeadingLevel } from './navigation-sidebar-heading-level.enum';
import { NavigationSidebarCloseButtonPosition } from './navigation-sidebar-close-button-position.enum';
import { UtilBreakpointObserverBreakpoints } from '../../_util/util-breakpoint-observer/util-breakpoint-observer-breakpoints.enum';

/**
 * @slot default - Ska innehålla navigationskomponent
 * @slot footer - Kan innehålla vad som helst
 *
 * @enums NavigationSidebarPosition - navigation-sidebar-position.enum.ts
 * @enums NavigationSidebarVariation - navigation-sidebar-variation.enum.ts
 * @enums NavigationSidebarMobileVariation - navigation-sidebar-mobile-variation.enum.ts
 * @enums NavigationSidebarHeadingLevel - navigation-sidebar-heading-level.enum.ts
 * @enums NavigationSidebarCloseButtonPosition - navigation-sidebar-close-button-position.enum.ts
 * @swedishName Sidofält
 */

@Component({
  tag: 'digi-navigation-sidebar',
  styleUrls: ['navigation-sidebar.scss'],
  scoped: true
})
export class NavigationSidebar {
  @Element() hostElement: HTMLStencilElement;

  @State() isMobile: boolean;
  @State() showHeader = true;

  private _hasFooter: boolean;
  private _wrapper: HTMLElement;
  private _closeFocusableElement: HTMLElement;
  private _heading;

  private _focusableElementsSelectors =
    'a, input, select, textarea, button, button:not(hidden), iframe, object, [tabindex="0"]';

  private focusableElements = [];
  private firstFocusableEl;
  private lastFocusableEl;

  /**
   * Positionerar menyn
   * @en Sets sidebar position
   */
  @Prop() afPosition: NavigationSidebarPosition =
    NavigationSidebarPosition.START;

  /**
   * Sätt variant
   * @en Sets sidebar variation
   */
  @Prop() afVariation: NavigationSidebarVariation =
    NavigationSidebarVariation.OVER;

  /**
   * Positionerar menyn i mobilen. Sätter du inget blir det samma som för större skärmar.
   * @en Sets sidebar position in mobile. Defaults to the same as for desktop.
   */
  @Prop() afMobilePosition: NavigationSidebarMobilePosition;

  /**
   * Sätt variant för mobilen.
   * @en Sets sidebar variation for mobile.
   */
  @Prop() afMobileVariation: NavigationSidebarMobileVariation =
    NavigationSidebarMobileVariation.DEFAULT;

  /**
   * Rubrikens text
   * @en The heading text
   */
  @Prop() afHeading: string;

  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: NavigationSidebarHeadingLevel =
    NavigationSidebarHeadingLevel.H2;

  /**
   * Justerar stäng-knappens position
   * @en Positions the close button, defaults to start
   */
  @Prop() afCloseButtonPosition: NavigationSidebarCloseButtonPosition =
    NavigationSidebarCloseButtonPosition.START;

  /**
   * Låser rubrikdelen mot toppen
   * @en Sticks the header area to the top
   */
  @Prop() afStickyHeader: boolean;

  /**
   * Stäng-knappens text
   * @en The close buttons text
   */
  @Prop() afCloseButtonText: string;

  /**
   * Sätt attributet 'aria-label' på stäng-knappen
   * @en Set button `aria-label` attribute to the close button
   */
  @Prop() afCloseButtonAriaLabel: string;

  /**
   * Dölj rubrikdelen i sidofältet
   * @en Hide header area in sidebar
   */
  @Prop() afHideHeader: boolean;

  /**
   * Element som ska få fokus när menyn öppnas. Sätts ingen
   * så får rubriken eller stäng-knappen fokus
   * @en Element that is focused when menu is opened,
   * defaults to title or close button
   */
  @Prop() afFocusableElement: string;

  /**
   * Element som ska få fokus när menyn stängs.
   * Sätts ingen så får knappen som öppnade menyn fokus
   * @en Element that is focused when menu is closed,
   * defaults to toggle button that opened the sidebar
   */
  @Prop() afCloseFocusableElement: string;

  /**
   * Ändrar aktiv status
   * @en Toggles sidebar. Pass in true for default active
   */
  @Prop() afActive = false;

  /**
   * Lägger en skugga bakom menyn som täcker sidinnehållet när menyn är aktiv
   * @en Applies a unscrollable backdrop over the page content when the sidebar is active
   */
  @Prop() afBackdrop = true;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-navigation-context-menu');

  /**
   * Stängknappens 'onclick'-event
   * @en Close button's 'onclick' event
   */
  @Event() afOnClose: EventEmitter;

  /**
   * Stängning av sidofält med esc-tangenten
   * @en At close/open of sidebar using esc-key
   */
  @Event() afOnEsc: EventEmitter;

  /**
   * Stängning av sidofält med klick på skuggan bakom menyn
   * @en At close of sidebar when clicking on backdrop
   */
  @Event() afOnBackdropClick: EventEmitter;

  /**
   * Vid öppning/stängning av sidofält
   * @en At close/open of sidebar
   */
  @Event() afOnToggle: EventEmitter;

  @Listen('afOnToggle', { target: 'window' })
  toggleHandler(e) {
    this._closeFocusableElement = e.target.querySelector('button');
  }

  @Listen('afOnChange')
  breakpointHandler(e) {
    if (e.target.matches('digi-util-breakpoint-observer')) {
      this.isMobile =
        e.detail.value === UtilBreakpointObserverBreakpoints.SMALL
          ? true
          : false;
    }
  }

  @Listen('transitionend')
  toggleTransitionEndHandler(e: TransitionEvent): void {
    const element = e.target as Element;
    if (
      element.matches('.digi-navigation-sidebar__wrapper') &&
      e.propertyName === 'visibility'
    ) {
      this.setFocus();
    }
  }

  @Listen('afOnClick')
  clickHandler(e) {
    // If clicking on toggle button, refresh the focusable items list
    // with the new sub-level of items
    if (this.shouldUseFocusTrap) {
      const el = e.detail.target as HTMLElement;
      if (el.tagName.toLowerCase() === 'button') {
        setTimeout(() => {
          this.getFocusableItems();
        }, 100);
      }
    }
  }

  @Watch('isMobile')
  setMobileView(isMobile: boolean) {
    isMobile && this.afActive
      ? (this.disablePageScroll(), this.getFocusableItems())
      : this.enablePageScroll();
  }

  @Watch('afActive')
  activeChange(active: boolean) {
    if (
      this.isMobile ||
      (this.afBackdrop &&
        !this.isMobile &&
        this.afVariation !== NavigationSidebarVariation.STATIC)
    ) {
      this.pageScrollToggler();
    }

    if (this.afVariation === NavigationSidebarVariation.PUSH) {
      this.pushPageContentToggler();
    }

    this.afOnToggle.emit(active);
  }

  @Watch('afHideHeader')
  headerChange(hide: boolean) {
    this.setShowHeader(hide);
  }

  setShowHeader(hide: boolean) {
    this.showHeader = !hide;
  }

  setHasFooter() {
    const footer = !!this.hostElement.querySelector('[slot="footer"]');
    if (footer) {
      this._hasFooter = footer;
    }
  }

  setFocus() {
    if (this.afActive) {
      let el: HTMLElement;
      let firstNavLink: HTMLElement;

      // Set first nav link
      if (!this.afHideHeader) {
        firstNavLink = this.focusableElements[1];
      } else {
        firstNavLink = this.firstFocusableEl;
      }

      if (this.afFocusableElement) {
        el = this.hostElement.querySelector(`${this.afFocusableElement}`);
      } else {
        if (this.afHeading) {
          el = this._heading;
        } else {
          el = firstNavLink;
        }
      }

      if (el) {
        el.focus();
      }
    } else {
      if (this.afCloseFocusableElement) {
        const el: HTMLElement = document.querySelector(
          `${this.afCloseFocusableElement}`
        );
        el.focus();
      } else if (this._closeFocusableElement) {
        const el: HTMLElement = this._closeFocusableElement;
        el.focus();
      }
    }
  }

  pushPageContentToggler() {
    document.body.classList.remove('digi--has-open-sidebar-right-push');
    document.body.classList.remove('digi--has-open-sidebar-left-push');

    if (this.afActive && this.afVariation === NavigationSidebarVariation.PUSH) {
      this.afPosition === NavigationSidebarPosition.END
        ? document.body.classList.add('digi--has-open-sidebar-right-push')
        : document.body.classList.add('digi--has-open-sidebar-left-push');
    }
  }

  pageScrollToggler() {
    this.afActive ? this.disablePageScroll() : this.enablePageScroll();
  }

  enablePageScroll() {
    document.body.style.height = '';
    document.body.style.overflowY = '';
    document.body.style.paddingRight = '';
  }

  disablePageScroll() {
    document.body.style.height = '100vh';
    document.body.style.overflowY = 'hidden';
    // document.body.style.paddingRight = '17px';
  }

  closeHandler(e: Event) {
    this.afActive = false;
    this.afOnClose.emit(e);
  }

  escHandler(e) {
    if (
      this.isMobile ||
      this.afVariation !== NavigationSidebarVariation.STATIC
    ) {
      this.afActive = false;
      this.afOnEsc.emit(e);
    }
  }

  tabHandler(e) {
    if (this.shouldUseFocusTrap) {
      if (document.activeElement === this.lastFocusableEl) {
        e.detail.preventDefault();
        this.firstFocusableEl.focus();
      }
    }
  }

  backdropClickHandler(e) {
    this.afActive = false;
    this.afOnBackdropClick.emit(e);
  }

  shiftHandler(e) {
    if (this.shouldUseFocusTrap) {
      if (document.activeElement === this.firstFocusableEl) {
        e.detail.preventDefault();
        this.lastFocusableEl.focus();
      }
    }
  }

  componentWillLoad() {
    this.setHasFooter();

    if (!this.afMobilePosition) {
      this.afMobilePosition =
        this.afPosition === NavigationSidebarPosition.START
          ? NavigationSidebarMobilePosition.START
          : NavigationSidebarMobilePosition.END;
    }

    if (this.afVariation === NavigationSidebarVariation.PUSH) {
      this.pushPageContentToggler();
      const reducedMotion = this.reducedMotion();

      if (!reducedMotion) {
        document.body.style.transition = 'var(--digi--page--transition)';
      }
    }
  }

  componentDidLoad() {
    setTimeout(() => {
      this.getFocusableItems();
    }, 100);

    this.headerChange(this.afHideHeader);

    if (
      this.afBackdrop &&
      this.afVariation !== NavigationSidebarVariation.STATIC
    ) {
      this.pageScrollToggler();
    }
  }

  componentWillUpdate() {
    this.setHasFooter();
    this.pushPageContentToggler();
  }

  get shouldUseFocusTrap() {
    return (
      (this.isMobile ||
        this.afVariation !== NavigationSidebarVariation.STATIC) &&
      this.focusableElements.length > 0
    );
  }

  getFocusableItems() {
    const allElements = this._wrapper.querySelectorAll(
      this._focusableElementsSelectors
    );

    // Filters out visible items
    this.focusableElements = Array.prototype.slice
      .call(allElements)
      .filter(function (item) {
        return item.offsetParent !== null;
      });

    // Sets first and last focusable element
    if (this.focusableElements.length > 0) {
      this.firstFocusableEl = this.focusableElements[0] as HTMLElement;
      this.lastFocusableEl = this.focusableElements[
        this.focusableElements.length - 1
      ] as HTMLElement;
    }
  }

  reducedMotion() {
    const reduced = window.matchMedia('(prefers-reduced-motion: reduce)');
    return !reduced || reduced.matches ? true : false;
  }

  get cssModifiers() {
    return {
      'digi-navigation-sidebar__wrapper--start':
        this.afPosition === NavigationSidebarPosition.START,
      'digi-navigation-sidebar__wrapper--end':
        this.afPosition === NavigationSidebarPosition.END,
      'digi-navigation-sidebar__wrapper--over':
        this.afVariation === NavigationSidebarVariation.OVER,
      'digi-navigation-sidebar__wrapper--push':
        this.afVariation === NavigationSidebarVariation.PUSH,
      'digi-navigation-sidebar__wrapper--static':
        this.afVariation === NavigationSidebarVariation.STATIC,
      'digi-navigation-sidebar__wrapper--mobile--start':
        this.afMobilePosition === NavigationSidebarMobilePosition.START,
      'digi-navigation-sidebar__wrapper--mobile--end':
        this.afMobilePosition === NavigationSidebarMobilePosition.END,
      'digi-navigation-sidebar__wrapper--mobile--default':
        this.afMobileVariation === NavigationSidebarMobileVariation.DEFAULT,
      'digi-navigation-sidebar__wrapper--mobile--fullwidth':
        this.afMobileVariation === NavigationSidebarMobileVariation.FULLWIDTH,
      'digi-navigation-sidebar__wrapper--mobile--active': this.isMobile,
      'digi-navigation-sidebar__wrapper--active': this.afActive,
      'digi-navigation-sidebar__wrapper--backdrop': this.afBackdrop,
      'digi-navigation-sidebar__wrapper--sticky-header': this.afStickyHeader
    };
  }

  get cssModifiersBackdrop() {
    return {
      'digi-navigation-sidebar__backdrop--active':
        this.afActive && this.afBackdrop,
      'digi-navigation-sidebar__backdrop--active--mobile':
        this.afActive &&
        this.isMobile &&
        this.afMobileVariation !== NavigationSidebarMobileVariation.FULLWIDTH,
      'digi-navigation-sidebar__backdrop--hidden':
        !this.isMobile && this.afVariation === NavigationSidebarVariation.STATIC
    };
  }

  render() {
    return (
      <Host>
        <digi-util-breakpoint-observer>
          <digi-util-keydown-handler
            onAfOnEsc={(e) => this.escHandler(e)}
            onAfOnTab={(e) => this.tabHandler(e)}
            onAfOnShiftTab={(e) => this.shiftHandler(e)}>
            <div
              class="digi-navigation-sidebar"
              aria-hidden={!this.afActive ? 'true' : 'false'}>
              <div
                aria-hidden="true"
                class={{
                  'digi-navigation-sidebar__backdrop': true,
                  ...this.cssModifiersBackdrop
                }}
                onClick={(e) => this.backdropClickHandler(e)}></div>
              <div
                class={{
                  'digi-navigation-sidebar__wrapper': true,
                  ...this.cssModifiers
                }}>
                <div
                  class="digi-navigation-sidebar__inner"
                  ref={(el) => {
                    this._wrapper = el;
                  }}>
                  {this.showHeader ? (
                    <div
                      class={{
                        'digi-navigation-sidebar__header': true,
                        'digi-navigation-sidebar__header--close-button--start':
                          this.afCloseButtonPosition ===
                          NavigationSidebarCloseButtonPosition.START,
                        'digi-navigation-sidebar__header--close-button--end':
                          this.afCloseButtonPosition ===
                          NavigationSidebarCloseButtonPosition.END,
                        'digi-navigation-sidebar__header--reversed':
                          !!this.afHeading
                      }}>
                      {this.afHeading && (
                        <this.afHeadingLevel
                          ref={(el) => (this._heading = el)}
                          class="digi-navigation-sidebar__heading"
                          tabindex="-1">
                          {this.afHeading}
                        </this.afHeadingLevel>
                      )}
                      <digi-button
                        onClick={(e) => this.closeHandler(e)}
                        af-variation="function"
                        af-aria-label={this.afCloseButtonAriaLabel}
                        class="digi-navigation-sidebar__close-button">
                        {this.afCloseButtonText}
                        <digi-icon-x slot="icon-secondary" />
                      </digi-button>
                    </div>
                  ) : (
                    <div></div>
                  )}
                  <div class="digi-navigation-sidebar__content">
                    <div class="digi-navigation-sidebar__nav-wrapper">
                      <slot></slot>
                    </div>
                    {this._hasFooter && (
                      <div class="digi-navigation-sidebar__footer">
                        <slot name="footer"></slot>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </digi-util-keydown-handler>
        </digi-util-breakpoint-observer>
      </Host>
    );
  }
}
