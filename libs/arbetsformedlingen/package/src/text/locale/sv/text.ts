const common = {
  close: 'Stäng',
  loading: 'Laddar',
  show: 'Visa',
  showing: 'Visar',
  show_action: (action: string) => `Visa ${action}`,
  hide: 'Dölj',
  hide_action: (action: string) => `Dölj ${action}`,
  clear: 'Rensa',
  required: 'Obligatoriskt',
  optional: 'Frivilligt',
  of: 'av',
  today: 'Idag',
  cancel: 'Avbryt',
  canceled: 'Avbruten',
  step: 'Steg',
  breadcrumbs: 'Din plats i webbplatsstrukturen',
  back: 'Tillbaka',
  hits: 'Träffar',
  previous: 'Föregående',
  next: 'Nästa',
  menu: 'Meny',
  settings: 'Inställningar',
  removeItem: (item: string) => `Ta bort ${item}`
};

const calendar = {
  week: 'Vecka',
  weekdays: {
    mon: 'Måndag',
    tue: 'Tisdag',
    wed: 'Onsdag',
    thu: 'Torsdag',
    fri: 'Fredag',
    sat: 'Lördag',
    sun: 'Söndag'
  },
  weekdays_short: {
    mon: 'Må',
    tue: 'Ti',
    wed: 'On',
    thu: 'To',
    fri: 'Fr',
    sat: 'Lö',
    sun: 'Sö'
  }
};

const code = {
  copy: 'Kopiera kod',
  show_more: 'Visa mer kod',
  show_less: 'Visa mindre kod',
  language: 'Kodspråk'
};

const form = {
  file_upload: 'Ladda upp fil',
  file_upload_canceled: 'Uppladdning avbruten',
  file_choose: 'Välj fil(er)',
  file_dropzone: 'eller dra och släpp en fil inuti det streckade området',
  file_uploaded_files: 'Uppladdade filer',
  file_cancel_upload: 'Avbryt uppladdning',
  file_error_too_many: 'Max antal filer uppladdade',
  file_error_unknown_error: 'Okänt fel',
  file_upload_progress: 'Laddar upp...',
  file_upload_reset_errors: 'Rensa',
  file_upload_reset_errors_aria_label: 'Rensa felmeddelanden',

  file_cancel_upload_long: (fileName: string) =>
    `Avbryt uppladdning av filen ${fileName}`,
  file_remove: (fileName?: string) => `Ta bort ${fileName || 'fil'}`,
  file_error_too_large: (fileName: string, size: number) =>
    `Filen ${fileName} är för stor. Filen får max vara ${size} MB`,
  file_error_exists: (fileName: string) =>
    `Det finns redan en fil med namnet ${fileName}`,
  file_error_filetype_not_allowed: (fileName: string, allowedTypes: string) =>
    `Filen ${fileName} har fel filformat. Välj en fil i något av de tillåtna formaten ${allowedTypes}`
};

const progress = {
  questions_answered: 'Frågor besvarade',
  questions_next: 'Nästa:'
};

const progressList = {
  progressListItemDone: 'status klar',
  progressListItemCurrent: 'status aktuell',
  progressListItemUpcoming: 'status kommande'
};

const time = {
  days_ago: (days: number | string) => `För ${days} dagar sedan`,
  in_days: (days: number | string) => `Om ${days} dagar`
};

export const _t = {
  ...common,
  calendar,
  code,
  form,
  progress,
  progressList,
  time
};
