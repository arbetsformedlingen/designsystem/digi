import { camelCase, capitalize } from '../helpers';
import type { ScaffoldNode } from '../types';

export function scaffold(name: string, displayName: string): ScaffoldNode[] {
  const kebabName = name;
  const camelName = camelCase(kebabName);
  const pascalName = capitalize(camelName);

  const componentSource = `
import { Component, h, Host, Prop } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-${kebabName}-details',
  styleUrl: 'digi-${kebabName}.scss'
})
export class Digi${pascalName}Details {
  @Prop() component: string;

  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @Prop() exampleToggle = false
  @Prop() exampleVariant = 'primary'

  get exampleCode() {
    return {
      [CodeExampleLanguage.HTML]: \`\
<digi-${kebabName}
  af-variant="\${this.exampleVariant}"
  af-example="\${this.exampleToggle}"
>
</digi-${kebabName}>

<script>
	const element = document.querySelector('digi-${kebabName}')
	element.afExampleList = [1,2,3]
</script>\`,

      [CodeExampleLanguage.ANGULAR]: \`\
<!--component.ts-->
exampleList = [1,2,3]

<!--template.html-->
<digi-${kebabName}
  [attr.af-variant]="'\${this.exampleVariant}'"
  [attr.af-example]="'\${this.exampleToggle}'"
	[afExampleList]='exampleList'
> 
</digi-${kebabName}>\`,

      [CodeExampleLanguage.REACT]: \`\
<Digi${pascalName}
  afVariant="\${this.exampleVariant}"
	afExample={\${this.exampleToggle}}
	afExampleList={[1,2,3]}
/>\`
    };
  }

  render() {
    return (
      <Host>
        <div class="digi-${kebabName}-details">
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              ${displayName} används för att ✏️...
            </digi-typography-preamble>
          )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && <h2>Exempel</h2>}
            <digi-code-example
                af-code={JSON.stringify(this.exampleCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
                <div class="slot__controls" slot="controls">
                  <digi-form-fieldset
                    af-legend="Variant"
                    af-name="variant"
                    onChange={(e) => this.exampleVariant = (e.target as HTMLDigiFormRadiobuttonElement).value}>
                    <digi-form-radiobutton
                      af-name="variant"
                      afLabel="Primär"
                      afValue="primary"
                      afChecked={true}
                    />
                    <digi-form-radiobutton
                      af-name="variant"
                      afLabel="Sekundär"
                      afValue="secondary"
                    />
                  </digi-form-fieldset>

                  <digi-form-fieldset afLegend="Av eller på?">
                    <digi-form-checkbox
                      afChecked={false}
                      afLabel="På"
                      onAfOnChange={() => this.exampleToggle = !this.exampleToggle}
                    ></digi-form-checkbox>
                  </digi-form-fieldset>

                </div>
                <div>
                  <digi-${kebabName}
                    afExample="true"
                    afExampleList={[1,2,3]}
                  >
                  </digi-${kebabName}>
                </div>
              </digi-code-example>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <article>
                <h2>Användningsexempel</h2>
                <h3>Lyssna på Events</h3>
								<digi-code-block
									af-language="html"
									af-variation="dark"
									af-code={\`<script>
  const component = document.querySelector('digi-${kebabName}')
</script>\`}></digi-code-block>
                <h2>Datastruktur</h2>
                <digi-code-block
                  af-language="typescript"
                  af-code={\`interface ExampleType {
	id: string;
	label: string;
}\`}
                  af-variation="dark"></digi-code-block>
              </article>
            </digi-layout-container>
          )}
        </div>
      </Host>
    );
  }
}

  `;
  const componentStyle = `
:host {
  display: block;
}
  `;

  return [
    {
      name: `digi-${kebabName}-details`,
      children: [
        { name: `digi-${kebabName}.tsx`, content: componentSource },
        { name: `digi-${kebabName}.scss`, content: componentStyle }
      ]
    }
  ];
}
