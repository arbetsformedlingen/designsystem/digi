import { camelCase, capitalize } from '../helpers';
import type { ScaffoldNode } from '../types';

export function scaffold(name: string): ScaffoldNode[] {
  const kebabName = name;
  const camelName = camelCase(kebabName);
  const pascalName = capitalize(camelName);

  const componentSource = `
import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-${kebabName}',
  styleUrl: 'icon-${kebabName}.scss',
  scoped: true
})
export class ${pascalName} {
  /**
   * Titel som används av hjälpmedel
   * @en Title used by accessibility tools
   */
  @Prop() afTitle: string;

  /**
   * Beskrivning som används av hjälpmedel
   * @en Description used by accessibility tools
   */
  @Prop() afDesc: string;

  /**
   * Dölj elementet för hjälpmedel Default är satt till true.
   * @en Hide element for accesibility tools. Default is set to true.
   */
  @Prop() afSvgAriaHidden = true;

  /**
   * Referera till ett element via id för att definiera ett tillgängligt namn.
   * @en Refer element via id to define an accessible name.
   */
  @Prop() afSvgAriaLabelledby: string;

  @State() titleId: string = randomIdGenerator('icontitle');

  prefix (id: string) {
    return this.titleId + '-' + id 
  }

  get labelledBy () {
    return this.afSvgAriaLabelledby
      ? this.afSvgAriaLabelledby
      : this.afTitle
        ? this.titleId
        : undefined
  }

  render() {
    return (
      <svg
        class="digi-icon-${kebabName} digi-icon"
        width="64"
        height="64"
        viewBox="0 0 64 64"
        fill="none"
        aria-labelledby={this.labelledBy}
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        xmlns="http://www.w3.org/2000/svg">
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        {/** 
         * * Klistra in markup för svg
         * * Ersätt värden i "stroke" och "fill" med "currentColor"
         * * Se till att alla id använder "prefix"-funktionen, exempel nedan
         *   Rätt:    '<g id={prefix("mask-0")}>'
         *   Fel:     '<g id="mask-0">'
         **/}
      </svg>
    );
  }
}
  `;
  const componentStyle = `
@use '../styles/icon.mixin' as *;

:host {
  @include digi-icon;
}
  `;

  return [
    {
      name: `icon-${kebabName}`,
      children: [
        { name: `icon-${kebabName}.tsx`, content: componentSource },
        { name: `icon-${kebabName}.scss`, content: componentStyle }
      ]
    }
  ];
}
