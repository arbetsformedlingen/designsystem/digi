import { camelCase, capitalize } from '../helpers';
import type { ScaffoldNode } from '../types';

export function scaffold(name: string, pageTitle: string): ScaffoldNode[] {
  const kebabName = name;
  const camelName = camelCase(kebabName);
  const pascalName = capitalize(camelName);

  const componentSource = `
import { Component, h, Prop, Host } from '@stencil/core';

@Component({
  tag: 'digi-docs-${kebabName}',
  styleUrl: 'digi-docs-${kebabName}.scss',
  scoped: true
})
export class ${pascalName} {
  @Prop() pageName = '${pageTitle}';

  render() {
    return (
      <Host>
        <digi-docs-page-layout af-page-heading={this.pageName}>
          <span slot="preamble">
            Lorem ipsum I am a preamble, look at me!
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <p>Has af-margin-top and af-margin-bottom</p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Only one H2 within layout container</h2>
              <p>Any number of P within layout container</p>
              <p>Has af-margin-bottom</p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Only one H2 within layout container</h2>
              <h3>Any number of H3 withing layout container</h3>
              <p>Any number of P within layout container</p>
              <h3>Some heading again</h3>
              <p>Any number of P within layout container</p>
              <p>Any number of P within layout container</p>
              <p>Has af-margin-bottom</p>
            </article>
          </digi-layout-container>
          <digi-layout-block
            af-variation="symbol"
            af-vertical-padding
            af-margin-bottom
          >
            <h2>Some heading</h2>
            <ul>
              <li>has af-vertical-padding</li>
              <li>Has af-margin-bottom</li>
            </ul>
          </digi-layout-block>
          <digi-layout-block
            af-variation="secondary"
            af-vertical-padding
            af-margin-bottom
          >
            <h2>Some heading</h2>
            <ul>
              <li>has af-vertical-padding</li>
              <li>Has af-margin-bottom</li>
            </ul>
          </digi-layout-block>
        </digi-docs-page-layout>
      </Host>
    );
  }
}
  `;
  const componentStyle = `
:host {
  display: block;
}
  `;

  return [
    {
      name: kebabName,
      children: [
        { name: `digi-docs-${kebabName}.tsx`, content: componentSource },
        { name: `digi-docs-${kebabName}.scss`, content: componentStyle }
      ]
    }
  ];
}
