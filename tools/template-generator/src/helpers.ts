export const capitalize = ([firstLetter, ...restOfWord]: string): string => {
  return firstLetter?.toUpperCase?.() + restOfWord?.join?.('');
};

export const camelCase = (string: string): string => {
  return string
    .split(/[\s-_\+]+/g)
    .map((part: string, index: number) =>
      index === 0 ? part : capitalize(part)
    )
    .join('');
};
