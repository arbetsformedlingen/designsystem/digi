import { program } from 'commander';

// style dictionary 
import exportTokens from './commands/exportTokens';

// manage flat tokens (example of flat tokens /themes/arbetsformedlingen/tokens.json)
import nestTokens from './commands/nestTokens';
import transformTokens from './commands/transformTokens';
import filterTokens from './commands/filterTokens';

// figma
import getFigmaVariables from './commands/getFigmaVariables';
import flattenTokens from './commands/flattenTokens';

program
  .name('token-tailor')
  .description('Utility to manage, convert and extract design tokens')
  .version('0.0.3')
  .addCommand(nestTokens.name('flat-to-style-dictionary'))
  .addCommand(exportTokens.name('style-dictionary'))
  .addCommand(flattenTokens.name('figma-to-flat'))

  .addCommand(filterTokens.name('filter-tokens'))
  .addCommand(transformTokens.name('transform-tokens'))
  
  .addCommand(getFigmaVariables.name('get-figma-variables'));
program.parse(process.argv);
