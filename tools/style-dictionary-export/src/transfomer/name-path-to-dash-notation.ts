import StyleDictionary from 'style-dictionary';

/**
 * namePathToDashNotation
 * @description convert the entire `path` to dash notation (mainly for css)
 */
export const namePathToDashNotation = {
  type: `name`,
  transformer: (
    token: StyleDictionary.TransformedToken,
    platform?: StyleDictionary.Platform
  ): string => {
    return [platform?.prefix, ...token.path]
      .filter((part: unknown): part is string => typeof part === 'string')
      .join('--');
  }
};
