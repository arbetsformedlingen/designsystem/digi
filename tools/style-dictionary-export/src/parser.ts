/**
 * @author GPT-o3-mini-high
 * On bugs and errors, send the whole thing to GPT and provide details together with desired solution.
 * Use reasoning model.
 */

type Data = { [key: string]: string };

export interface ParsedItem {
  name: string;
  raw: string;
  value: string;
}

/**
 * Recursively resolve a value that might contain placeholders.
 * @param key The key whose value to resolve.
 * @param data The original data object.
 * @param cache A cache to store resolved values.
 * @param seen A set to track visited keys to detect circular references.
 * @returns The fully resolved value.
 */
function resolveValue(
  key: string,
  data: Data,
  cache: Map<string, string> = new Map(),
  seen: Set<string> = new Set()
): string {
  // If already resolved, return it.
  if (cache.has(key)) {
    return cache.get(key) as string; // already checked if exists
  }

  // Check for circular references.
  if (seen.has(key)) {
    throw new Error(`Circular reference detected at key "${key}"`);
  }
  seen.add(key);

  let rawValue = data[key];
  const regex = /{([^}]+)}/g;
  let match: RegExpExecArray | null;

  // Replace each placeholder with its resolved value.
  while ((match = regex.exec(rawValue)) !== null) {
    const refKey = match[1].trim();
    if (!(refKey in data)) {
      throw new Error(`Reference "${refKey}" not found for key "${key}"`);
    }
    const resolvedRef = resolveValue(refKey, data, cache, seen);
    rawValue = rawValue.replace(match[0], resolvedRef);
  }

  cache.set(key, rawValue);
  seen.delete(key);
  return rawValue;
}

/**
 * Parse the object into a list of items containing the original raw value and the resolved value.
 * @param data The original data object.
 * @returns A list of parsed items.
 */
export function parseData(data: Data): ParsedItem[] {
  const result: ParsedItem[] = [];
  const cache = new Map<string, string>();

  for (const key in data) {
    const raw = data[key];
    const value = resolveValue(key, data, cache);
    result.push({ name: key, raw, value });
  }

  return result;
}

/*
Example usage:
  const data = { a: 'black', b: '{a}', c: 'red', d: '{c}' };
  parseData(data)

Output:
[
  { name: 'a', raw: 'black', value: 'black' },
  { name: 'b', raw: '{a}', value: 'black' },
  { name: 'c', raw: 'red', value: 'red' },
  { name: 'd', raw: '{c}', value: 'red' }
]
*/