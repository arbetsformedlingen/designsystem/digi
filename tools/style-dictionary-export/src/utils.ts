interface NestFlatObjectOpts {
  separator?: string;
}

export function nestFlatObject(obj: any, opts: NestFlatObjectOpts = {}): any {
  const result: any = {};
  for (const key in obj) {
    const parts = key.split(opts.separator || '--');
    let subObject = result;

    for (let i = 0; i < parts.length - 1; i++) {
      const part = parts[i];
      if (!subObject[part]) {
        subObject[part] = {};
      } else {
        subObject[part] = { ...subObject[part] };
      }

      subObject = subObject[part];
    }

    const lastPart = parts[parts.length - 1];
    subObject[lastPart] = obj[key];
  }

  return result;
}
