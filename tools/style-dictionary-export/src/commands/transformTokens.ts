import { Command, Option } from 'commander';
import fs from 'fs-extra';
import { ParsedItem, parseData } from '../parser';

function formatToCss(parsedList: ParsedItem[]) {

  function formatName (name: string) {
    return `--digi--${name}`
  }

  function formatRefValue (value: string) {
    const variableName = value.slice(1, -1) // remove { and }
    return `var(${formatName(variableName)})`;
  }

  function getDefintion (item: ParsedItem) {
    const isReferencingValue = item.raw !== item.value;
    if (isReferencingValue && containsMultipleValues(item.raw)) {
      return `/** ${formatName(item.name)}: ${item.raw} */`;
    } else if (isReferencingValue) {
      return `${formatName(item.name)}: ${formatRefValue(item.raw)}`;
    }
    return `${formatName(item.name)}: ${item.value}`;
  }

  const variables = parsedList.map((item) => getDefintion(item))

  return `
:root {
${variables.join('\n')}
}
  `;
}

function formatToJSONList (parsedList: ParsedItem[]) { 
  return JSON.stringify(parsedList) 
}

function occurences(haystack: string, needle: string): number {
  const prevLength = haystack.length
  const newLength = haystack.replaceAll(needle, '').length
  return prevLength - newLength
}

function containsMultipleValues (value: string) {
  return occurences(value, '{') > 1 || value.includes(' ')
}

export default new Command()
  .description('Transform tokens into different formats')
  .arguments('<tokenFile>')
  .addOption(new Option('--to <format>').choices(['css', 'json-list']))
  .action((input, opts) => {
    const data = fs.readJSONSync(input);
    const parsedList = parseData(data.tokens)
      .sort((a,b) => a.name.localeCompare(b.name))

    let result
    const format = opts.to
    if (format === 'css') {
      result = formatToCss(parsedList)
    } else if (format === 'json-list') {
      result = formatToJSONList(parsedList)
    }

    console.log(result);
  });
