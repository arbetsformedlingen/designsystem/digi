import { Command } from 'commander';
import fs from 'fs-extra';

export default new Command()
  .description('Filter tokens by name of token')
  .arguments('<tokenFile>')
  .option('-o, --out <outFile>')
  .option('-x, --exclude <regex>', 'Exclude tokens by name')
  .option('-i, --include <regex>', 'Include tokens by name')
  .action((input, opts) => {
    const data = fs.readJSONSync(input);

    const filtered = Object.entries(data.tokens)
      .filter(([key]) => {
        if (opts.exclude) {
          const exclude = new RegExp(opts.exclude)
          if (exclude.exec(key)) {
            return false
          } else {
            return true
          }
        }
        return true
      })
      .filter(([key]) => {
        if (opts.include) {
          const include = new RegExp(opts.include);
          if (include.exec(key)) {
            return true;
          } else {
            return false;
          }
        }
        return true
      })

      const result = {
        tokens: Object.fromEntries(filtered)
      }

      if (opts.out != null) {
        fs.writeJSONSync(opts.out, result)
      } else {
        console.log(JSON.stringify(result))
      }
  });
