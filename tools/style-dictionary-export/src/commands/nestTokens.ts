import { Command } from 'commander';
import * as fs from 'fs-extra';
import * as glob from 'glob';

import { nestFlatObject } from '../utils';

function extractVariableName(variable: string): string {
  const match = variable.match(/\{([^)]+)\}/);
  return match ? match[1] : '';
}

function toDictionaryName(variable: string): string {
  const varName = extractVariableName(variable);
  return varName.replace(/--/g, '.');
}

export default new Command()
  .description(
    'Convert token definitions into a style dictionary compatible format'
  )
  .arguments('<tokens...>')
  .action(async (dirs) => {
    for (const dir of dirs) {
      const files = glob.sync(dir);
      for (const file of files) {
        try {
          const content = await fs.readFile(file, 'utf8');
          const flatData = JSON.parse(content);
          const entries = Object.entries(flatData.tokens as Record<string, string>).map(
            ([key, value]) => {
              const dictValue = value.includes('{') // example: {abc--token} true, #303030 false
                ? `{${toDictionaryName(value)}.value}` // example: {abc--token} => {abc.token.value}
                : value;
              return [`${key}--value`, dictValue];
            }
          );
          const unconventionals = entries.filter(([key]) =>
            key.startsWith('--')
          );
          const conventionals = entries.filter(
            ([key]) => !key.startsWith('--')
          );
          const nested = nestFlatObject(Object.fromEntries(conventionals), {
            separator: '--'
          });
          const result = {
            ...nested,
            ...Object.fromEntries(unconventionals)
          };
          console.log(JSON.stringify(result, null, 2));
        } catch (error) {
          console.error(`Error processing ${file}: ${error}`);
          console.error(error);
        }
      }
    }
  });
