import { Command } from 'commander';
import * as fs from 'fs-extra';
import * as glob from 'glob';

function popRightBy(arr: string[], separator: string): string[] {
  return arr.map((str) => {
    return str.split(separator).slice(0, -1).join(separator);
  });
}

interface DictionaryValue {
  type: 'VARIABLE' | 'VALUE';
  value: string;
}

function extractValues(value: string): DictionaryValue[] {
  const parts = value.split(' ').map((val) => val.trim()); // multi value
  const extract = parts.map((value) => {
    let result: DictionaryValue = {
      type: 'VALUE',
      value
    };
    if (value.startsWith('{') && value.endsWith('}')) {
      result.type = 'VARIABLE';
      result.value = value.slice(1, -1);
    }
    return result;
  });
  return extract;
}

export default new Command()
  .description('Validate token definitions')
  .arguments('<dirs...>')
  .option('-a, --action <cssToDigiformat>')
  .option('-f, --fix', 'Try to autofix the validation errors')
  .action(async (dirs, options) => {
    let values = {};
    for (let dir of dirs) {
      const files = glob.sync(dir);
      for (const file of files) {
        const content = await fs.readJSON(file);
        for (let entry of Object.entries(content.tokens)) {
          values[entry[0]] = entry[1];
        }
      }
    }

    const updateTokens = {};

    const tokens = Object.keys(values);
    const first = popRightBy(tokens, '--');

    for (let i = 0; i < tokens.length; i++) {
      const pop1 = first[i];
      for (let j = 0; j < tokens.length; j++) {
        const orig = tokens[j];
        if (orig === pop1) {
          updateTokens[orig] = `${orig}--default`;
          console.warn(`${orig} is overriding ${tokens[i]}`);
        }
      }
    }

    if (options.fix) {
      const fixedEntries = Object.entries(values).map(
        ([key, value]) => {
          const fixedKey = updateTokens[key] ?? key;
          const values = extractValues(value as string).map((value) => ({
            type: value.type,
            value: updateTokens[value.value] ?? value.value
          }));
          const fixedValues = values
            .map(({ value, type }) =>
              type === 'VARIABLE' ? `{${value}}` : value
            )
            .join(' ');
          return [fixedKey, fixedValues];
        }
      );

      const result = {
        tokens: Object.fromEntries(fixedEntries)
      };
      console.log(JSON.stringify(result, null, 2));
    }
  });
