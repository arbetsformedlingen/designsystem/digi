import { Command } from 'commander';

interface GetLocalVariablesResponse {
  status: number;
  error: boolean;
  meta: any;
}

async function getLocalVariables(fileKey: string, accessToken: string) {
  const headers = new Headers();
  headers.set('Accept', '*/*');
  headers.set('X-Figma-Token', accessToken);

  const response = await fetch(
    `https://api.figma.com/v1/files/${fileKey}/variables/local`,
    {
      headers
    }
  );

  const json = (await response.json()) as GetLocalVariablesResponse;
  if (json.error === true) {
    throw new Error('Request failed with status ' + json.status)
  }
  return json.meta;
}

// extract fileKey from figma share url
function getFileKeyFromUrl (figmaUrl: string) {
  try {
    const url = new URL(figmaUrl);
    const path = url.pathname;
    const parts = path.split('/');
    const designPathIdx = parts.indexOf('design');
    if (designPathIdx > -1) {
      const fileKey = parts[designPathIdx + 1]; // fileKey is after the /design/ path
      return fileKey;
    }
    return figmaUrl
  } catch(error) {
    return figmaUrl
  }
}

export default new Command()
  .description('Get variables from a Figma file')
  .arguments('<url|fileKey>')
  .action(async (figmaUrl) => {
    let fileKey = getFileKeyFromUrl(figmaUrl);

    const accessToken = process.env.FIGMA_PERSONAL_TOKEN;
    if (!accessToken)
      throw new Error(
        'Wuampwump: FIGMA_PERSONAL_TOKEN is not defined. Please add to .env'
      );

    const variables = await getLocalVariables(fileKey, accessToken);
    console.log(JSON.stringify(variables));
  });
