import * as fs from 'fs-extra';
import postcss from 'postcss';
import scss from 'postcss-scss';
import { Command } from 'commander';
import * as glob from 'glob';

const validToken = (input: string) => {
  const trimmed = input.replace(/var\(/g, '').replace(/\)/g, '').trim();
  const hasMultipleDefs =
    trimmed.indexOf(' ') > -1 || trimmed.indexOf(',') > -1; // multiple variable definitions, ex: "var(--var-a) var(--var-b)" === true, "var(--var-a)" === false
  const hasSCSS = trimmed.indexOf('#') > -1;
  const hasCalcFunctionality = trimmed.match(/calc\(.*\)/g) != null;
  return !hasMultipleDefs && !hasSCSS && !hasCalcFunctionality;
};

const extractCssVariables = async (
  scssContent: string
): Promise<Record<string, string>> => {
  const result = await postcss().process(scssContent, {
    from: undefined,
    syntax: scss
  });

  const variables: Record<string, string> = {};
  result.root.walkDecls((decl) => {
    if (decl.prop.startsWith('--')) {
      if (!validToken(decl.value)) {
        console.warn(`⚠️  Invalid token: ${decl.prop}`);
        console.warn(`   Value: ${decl.value}`);
        console.warn('');
      }
      variables[decl.prop] = decl.value;
    }
  });

  return variables;
};

export default new Command()
  .description(
    'Extract CSS variables in supplied file paths to token definitions'
  )
  .arguments('<tokens...>')
  .action(async (dirs) => {
    const result = new Map();
    const duplicates = {};
    for (let dir of dirs) {
      const files = glob.sync(dir).filter((file) => {
        return (
          // do not convert copies of the design tokens
          !file.includes('src/design-tokens/') && !file.includes('__core')
        );
      });
      for (const file of files) {
        try {
          const content = await fs.readFile(file, 'utf8');
          const variables = await extractCssVariables(content);
          const entries = Object.entries(variables);
          for (let [name, value] of entries) {
            if (!result.has(name)) {
              result.set(name, { value, file });
            } else {
              if (result.get(name).value !== value) {
                // check if different value
                console.warn(`⚠️  Conflict found for ${name}`);
                console.warn(
                  `   Original: ${result.get(name).file.padEnd(128)}\t${result.get(name).value}`
                );
                console.warn(`   Duplicate: ${file.padEnd(128)}\t${value}`);
                console.warn('');
                duplicates[name] =
                  duplicates[name] == null
                    ? [{ file, value }]
                    : [...duplicates[name], { file, value }];
              }
            }
          }
        } catch (error) {
          console.error(`Error processing ${file}: ${error}`);
          console.log(error);
        }
      }
    }

    let output = new Map();
    for (let [key, { value }] of result) {
      output.set(key, value);
    }
    console.log(JSON.stringify(Object.fromEntries(output.entries()), null, 2));
  });
