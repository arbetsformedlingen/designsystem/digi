# Token Tailor 👗👔

Verktyg för att hantera, analysera och transformera Design Tokens

## Innan du börjar

Bygg projektet med

`npx nx build style-dictionary-export`

## Hjälpkommando

`node dist/tools/style-dictionary-export --help`

## Exempel

### Konvertera JSON-lista med variabler till Style Dictionary-format

```bash
node dist/tools/style-dictionary-export flat-to-style-dictionary themes/arbetsformedlingen/tokens.json > style-dictionary.json
```

### Exportera Style Dictionary till Figma-variabler

```bash
node dist/tools/style-dictionary-export style-dictionary -p figma --config themes/arbetsformedlingen/figma.json sd-tokens.json
```

### Konvertera Style Dictionary-format till JSON-lista
```bash
node dist/tools/style-dictionary-export style-dictionary -o dist/tokens-merged-flat-dash -p json/dash libs/arbetsformedlingen/design-tokens/src/styles/**/*.json
```

### Exportera ut ett urval av flat tokens

Alla kommandon exporterar till fil `filtered-tokens.json`, ändra till valfri sökväg!

Inkludera endast tokens som börjar med `print--`

```bash
node dist/tools/style-dictionary-export filter-tokens themes/arbetsformedlingen/tokens.json --out filtered-tokens.json --include "^print--"
```

Inkludera endast `stratos`, `rose` och `leaf` tokens

```bash
node dist/tools/style-dictionary-export filter-tokens themes/arbetsformedlingen/tokens.json --out filtered-tokens.json --include "(rose|leaf|stratos)"
```

Exkludera tokens som börjar med `global`

```bash
node dist/tools/style-dictionary-export filter-tokens themes/arbetsformedlingen/tokens.json --out filtered-tokens.json --exclude "^global--"
```

Exkludera tokens som börjar med `global`, `animation`, `layout` m.fl.

```bash
node dist/tools/style-dictionary-export filter-tokens themes/arbetsformedlingen/tokens.json --out filtered-tokens.json --exclude "^(global|layout|animation|border|color)"
```


### Konvertera flat tokens till CSS-variabler

```bash
node dist/tools/style-dictionary-export transform-tokens themes/arbetsformedlingen/tokens.json --to css
```

### Hämta variabler från Figma-fil

#### Innan du kör ⚠️
Kopiera `.env.sample` i roten av monorepot till `.env` och uppdatera med din "Figma Personal Access Token"

```bash
# måste använda nx för att komma åt variablerna som är definierade i `.env`
npx nx start style-dictionary-export get-figma-variables "https://www.figma.com/design/SZYTBqqGgG9rJA8Z0lINUN/Af-UI-Kit-(Community)"
```
