import { nestFlatObject } from '../src/utils';

it('should nest flat object', () => {
  expect(
    nestFlatObject(
      { 'step1.step2.step3.value': 'value' },
      { separator: '.' }
    )
  ).toEqual({
    step1: {
      step2: {
        step3: {
          value: 'value'
        }
      }
    }
  });

  expect(
    nestFlatObject(
      { 'digi--element--color--background': '#FFF' },
      { separator: '--' }
    )
  ).toEqual({
    digi: { 
      element: { 
        color: { 
          background: '#FFF' 
        } 
      } 
    }
  });
});
