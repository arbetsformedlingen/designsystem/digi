import { readFileSync } from 'fs';
import typescript from 'typescript';
import * as sass from 'sass';

const indexObject = function (startObject) {
  const result = {};
  for (const key in startObject) {
    indexPath(key, startObject[key], result);
  }
  return result;
};

const isLeaf = function (data) {
  switch (typeof data) {
    case 'bigint':
    case 'boolean':
    case 'number':
    case 'string':
    case 'undefined':
      return true;
  }
  if (data === null) {
    return true;
  }
  return false;
};

const indexPath = function (currentPath, currentObject, result) {
  if (isLeaf(currentObject)) {
    if (currentPath.endsWith('value')) {
      result[currentPath] = currentObject;
    }
  } else {
    for (const key in currentObject) {
      indexPath(`${currentPath}.${key}`, currentObject[key], result);
    }
  }
};

function getStyleUrl(componentStructure) {
  let result = componentStructure
    .filter((p) => p.name?.escapedText === 'styleUrls')
    .flatMap((p) => p.initializer?.elements?.flatMap((e) => e.text));
  if (result.length === 0) {
    result = componentStructure
      .filter((p) => p.name?.escapedText === 'styleUrl')
      .flatMap((p) => p.initializer?.text);
  }
  return result;
}

function extractComponentInformation(node, path, name) {
  const fileName = `${path}/${name}`;
  return node.statements
    .filter((s) => s.modifiers !== undefined)
    .flatMap((s) => s.modifiers)
    .filter((m) => m.expression !== undefined)
    .map((m) => m.expression)
    .filter((e) => e.expression?.escapedText === 'Component')
    .map((e) => ({
      component: e.arguments
        .flatMap((a) => a.properties)
        .filter((p) => p.name?.escapedText === 'tag')
        .map((p) => p.initializer?.text)[0],
      styleFiles: getStyleUrl(e.arguments.flatMap((a) => a.properties)),
      componentFile: fileName,
      path: path
    }))[0];
}

export class Indexer {
  constructor(brandFile, globalFile) {
    this.brandFile = brandFile;
    this.globalFile = globalFile;
  }

  /**
   * @type {Map<String, BrandToken>}
   */
  brandTokens = new Map();

  index(files) {
    const brandFileContent = indexObject(this.brandFile);
    const globalFileContent = indexObject(this.globalFile);

    for (const key in brandFileContent) {
      const globalKey = brandFileContent[key]?.substring(
        1,
        brandFileContent[key].length - 1
      );
      this.brandTokens.set(
        key,
        new BrandToken(key)
          .setGlobalToken(brandFileContent[key])
          .setGlobalTokenValue(globalFileContent[globalKey])
      );
    }

    const components = [];

    for (const file of files) {
      if (file.isFile()) {
        if (file.name.endsWith('tsx')) {
          let fileName = `${file.path}/${file.name}`;
          const data = readFileSync(fileName, 'utf8');
          const node = typescript.createSourceFile(
            'node.tsx',
            data.toString(),
            typescript.ScriptTarget.Latest
          );
          let componentInfo = extractComponentInformation(
            node,
            file.path,
            file.name
          );
          if (componentInfo) {
            components.push(componentInfo);
          }
        }
      }
    }

    for (const component of components) {
      if (component.styleFiles.length > 0) {
        try {
          const css = sass.compile(
            `${component.path}/${component.styleFiles[0]}`
          );
          for (const token of this.brandTokens.keys()) {
            let brandToken = this.brandTokens.get(token);
            if (css.css.includes(brandToken.token)) {
              brandToken.addUsage(component);
            }
          }
        } catch (e) {
          console.error(
            '!!! Error parsing scss for component ',
            component.component,
            e
          );
        }
      }
    }

    const result = {};
    for (const key of this.brandTokens.keys()) {
      result[key] = JSON.parse(JSON.stringify(this.brandTokens.get(key)));
    }
    console.log(JSON.stringify(result, null, ' '));
  }
}

class BrandToken {
  globalToken = '[unset]';
  globalTokenValue = '[unset]';
  key = '[unset]';
  token = '[unset]';

  constructor(key) {
    this.key = key;
    this.token = `--digi--${key.replaceAll('.', '--')}`.replace('--value', '');
  }

  setGlobalToken(token) {
    this.globalToken = token;
    return this;
  }

  setGlobalTokenValue(value) {
    this.globalTokenValue = value;
    return this;
  }

  addUsage(component) {
    this.usage.push(component);
  }

  /**
   * @type {Map<string, UsageTypeLink>}
   */
  usage = [];
}

const UsageType = {
  VARIATION: 'variation',
  VARIABLE: 'variable'
};

class UsageTypeLink {
  /**
   * @type {Component | null}
   */
  component = null;
  /**
   * @type {UsageType.VARIABLE | UsageType.VARIATION}
   */
  type = UsageType.VARIABLE;
  variationName = '';
}

class Component {
  tag = '[component name]';
}
